<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contract extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('contract_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

	// 원청계약목록 ct_status 필드 용도?
	public function contract_list($action = NULL, $id = NULL)
	{
        $action = (!empty($action))?$action:"1";
        $tr_title = $this->input->post('tr_title', true);
        $rco_name = $this->input->post('rco_name', true);
        $sco_name = $this->input->post('sco_name', true);
        $page = $this->input->post('page',true);
        if($page == null){
            $page = 1;
        }

        $data['page'] = 1;
        $data['tr_title'] = $tr_title;

        $data['rco_name'] = $rco_name;
        $data['sco_name'] = $sco_name;
        $data['active'] = $action;
        if($action == "index") { //계약수정
          $data['active'] = "9";
          $data['contract_info'] = $this->db->select('a.*, b.co_name as sco_name, c.co_name as rco_name')->where('a.idx', $id)->get("tbl_contract a left join tbl_members b on (`a`.`s_co_id` = `b`.`dp_id`) left join tbl_members c on (`a`.`r_co_id` = `c`.`dp_id`)")->row();
        }

        $data['title'] = '원청계약관리';

        $this->contract_model->db->select('a.*, b.co_name as sco_name, c.co_name as rco_name');
        $this->contract_model->_table_name = 'tbl_contract a'; //table name
        $this->contract_model->db->join('tbl_members b', 'a.s_co_id = b.dp_id', 'left');
        $this->contract_model->db->join('tbl_members c', 'a.r_co_id = c.dp_id', 'left');
        if($tr_title != null){
            $this->contract_model->db->like('a.tr_title', $tr_title);
        }
        if($rco_name != null){
            $this->contract_model->db->like('c.co_name', $rco_name);
        }
        if($sco_name != null){
            $this->contract_model->db->like('b.co_name', $sco_name);
        }
        $this->contract_model->_order_by = 'a.ct_edate ASC';


        switch($action) {
          case "1" : $this->contract_model->db->where('a.ct_edate >=', date("Y-m-d"))->where('a.ct_sdate <=' , date("Y-m-d")); break; //진행중계약
          case "2" : $this->contract_model->db->where('a.ct_sdate >',date("Y-m-d")); break; // 진행예정계약
          case "3" : $this->contract_model->db->where('date(a.ct_edate)', 'date(subdate(now(), INTERVAL 30 DAY))'); break; //만료대기 (1개월전)
          case "4" : $this->contract_model->db->where('a.ct_edate <', date("Y-m-d")); break; // 만료계약 (종료일 지난계약)

          default  : break;
        }
        $data['all_contract_group'] = $this->contract_model->get();
        $data['action'] = $action;
        $data['subview'] = $this->load->view('admin/contract/contract_list', $data, true);

        $this->load->view('admin/_layout_main', $data);
	}

  // 원청계약정보 저장
  public function save_contract($id = NULL)
  {
    $sql_common = "tr_title='".$this->input->post('tr_title', true)."'";
    $sql_common .= ",ct_title='".$this->input->post('ct_title', true)."'";
    $sql_common .= ",ct_type='".$this->input->post('ct_type', true)."'";
    $sql_common .= ",ct_amount='".$this->input->post('ct_amount', true)."'";
    $sql_common .= ",is_subcontract='".$this->input->post('is_subcontract', true)."'";
    $sql_common .= ",scheduling_yn='".$this->input->post('scheduling_yn', true)."'";
    $sql_common .= ",s_co_id='".$this->input->post('s_co_id', true)."'";
    $sql_common .= ",r_co_id='".$this->input->post('r_co_id', true)."'";
    $sql_common .= ",ct_date='".$this->input->post('ct_date', true)."'";
    $sql_common .= ",ct_sdate='".$this->input->post('ct_sdate', true)."'";
    $sql_common .= ",ct_edate='".$this->input->post('ct_edate', true)."'";
    $sql_common .= ",remark='".$this->input->post('remark', true)."'";

    //1. DB insert or update
    if(!empty($id)) {
      $sql = "UPDATE tbl_contract SET ";
      $sql = $sql . $sql_common;
      $sql .= ",reg_date='".date('Y-m-d')."'" ;
      $sql .= " WHERE idx = '".$id."'";
      $this->db->query($sql);
    } else {
      $sql = "INSERT INTO tbl_contract SET ";
      $sql = $sql . $sql_common;
      $sql .= ",reg_date='".date('Y-m-d')."'" ;
      $this->db->query($sql);
      $id = $this->db->insert_id();
    }

    //2. 진행중 계약 GNB 에 추가 (배차(배차사용여부), 유통물류(하도급여부)), 만료시 GNB에서 삭제
    if(!empty($this->input->post('ct_edate', true)) && $this->input->post('ct_edate', true) >= date("Y-m-d")) { //진행중계약
      //배차사용여부 & 기존 메뉴 존재체크
      if(!empty($this->input->post('scheduling_yn', true)) && $this->input->post('scheduling_yn', true) == "Y") {
        $link = "admin/scheduling/scheduling/".$id;
        $this->contract_model->_table_name = 'tbl_menu'; //table name
        $result = $this->contract_model->get_by(array('link' => $link), true);
          if (empty($result)) {
            $sql = "INSERT INTO tbl_menu SET ";
            $sql .= "label = '".$this->input->post('ct_title', true)."'";
            $sql .= ",link = '".$link."'";
            $sql .= ",icon = 'fa fa-car'";
            $sql .= ",parent = '9'";
            $sql .= ",status = '1'" ;
            $sql .= ",expiration_date = '".$this->input->post('ct_edate', true)." 00:00:00'";
            $this->db->query($sql);
          }
      }

      //하도급여부 & 기존 메뉴 존재체크
      if(!empty($this->input->post('is_subcontract', true)) && $this->input->post('is_subcontract', true) == "Y") {
          $link = "admin/logis/stransaction_write/input/".$id;
          $this->contract_model->_table_name = 'tbl_menu'; //table name
          $result = $this->contract_model->get_by(array('link' => $link), true);
          if (empty($result)) {
            $sql = "INSERT INTO tbl_menu SET ";
            $sql .= "label = '".$this->input->post('ct_title', true)."'";
            $sql .= ",link = '".$link."'";
            $sql .= ",icon = 'fa fa-car'";
            $sql .= ",parent = '30'";
            $sql .= ",status = '1'" ;
            $sql .= ",expiration_date = '".$this->input->post('ct_edate', true)." 00:00:00'";
            $this->db->query($sql);
          }
      }
    } else { //진행만료계약 => 배차,유통물류 메뉴 삭제
      $link = "admin/scheduling/scheduling/".$id;
      $sql = "DELETE FROM tbl_menu WHERE link = '".$link."'";
      $this->db->query($sql);

      $link = "admin/logis/stransaction_write/input/".$id;
      $sql = "DELETE FROM tbl_menu WHERE link = '".$link."'";
      $this->db->query($sql);
    }

    $message = "계약정보가 저장되었습니다.";
    $type = 'success';
    set_message($type, $message);

    redirect('admin/contract/contract_list/1'); //redirect page
  }

  //계약연장
  public function contract_extension($id)
  {
    $contract_info = $this->db->select('*')->where('idx', $id)->get("tbl_contract")->row();
    if (!empty($contract_info)) {
      // 1. 기존데이터 복사(계약일,시작일,만료일 정보 공백,부모아이디 저장)
      // 날짜 1년 후로 계산
      $ct_date = date("Y-m-d",strtotime($contract_info->ct_date.'+12 month'));
      $ct_sdate = date("Y-m-d",strtotime($contract_info->ct_edate.'+1 day'));
      $ct_edate = date("Y-m-d",strtotime($ct_sdate.'+12 month'));

      $sql_common = "tr_title='".$contract_info->tr_title."(연장)'";
      $sql_common .= ",ct_title='".$contract_info->ct_title."'";
      $sql_common .= ",ct_type='".$contract_info->ct_type."'";
      $sql_common .= ",ct_amount='".$contract_info->ct_amount."'";
      $sql_common .= ",is_subcontract='".$contract_info->is_subcontract."'";
      $sql_common .= ",scheduling_yn='".$contract_info->scheduling_yn."'";
      $sql_common .= ",s_co_id='".$contract_info->s_co_id."'";
      $sql_common .= ",r_co_id='".$contract_info->r_co_id."'";
      $sql_common .= ",ct_date='".$ct_date."'";
      $sql_common .= ",ct_sdate='".$ct_sdate."'";
      $sql_common .= ",ct_edate='".$ct_edate."'";
      $sql_common .= ",remark='".$contract_info->remark." 연장됨'";

      $sql = "INSERT INTO tbl_contract SET ";
      $sql = $sql . $sql_common;
      $sql .= ",reg_date='".date('Y-m-d')."'" ;
      $this->db->query($sql);
      $id = $this->db->insert_id();

      // 기존건과 연장계약건과 자동메뉴 추과 관련 작업 필요(통합적으로 관리될 모듈 필요)
    }
    $go_list_number = 1;
    if(strtotime($ct_sdate) > strtotime(date("Y-m-d"))){
        $go_list_number = 2;
    }

    $message = "계약정보가 연장되었습니다.";
    $type = 'success';
    set_message($type, $message);

    redirect('admin/contract/contract_list/'.$go_list_number); //redirect page
  }

    //계약정보삭제
    public function delete_contract($id)
    {
      $contract_info = $this->db->select('*')->where('idx', $id)->get("tbl_contract")->row();
      if (!empty($contract_info)) {
        $sql = "DELETE FROM tbl_contract WHERE idx = '". $id ."'" ;
        $this->db->query($sql);

        $link = "admin/scheduling/scheduling/".$id;
        $sql = "DELETE FROM tbl_menu WHERE link = '".$link."'";
        $this->db->query($sql);

        $link = "admin/logis/stransaction_write/input/".$id;
        $sql = "DELETE FROM tbl_menu WHERE link = '".$link."'";
        $this->db->query($sql);
      }

      $message = "계약정보가 삭제되었습니다.";
      $type = 'success';
//      set_message($type, $message);

//      redirect('admin/contract/contract_list/1'); //redirect page
//      $type = 'success';
      echo json_encode(array("status" => $type, 'message' => $message));
      exit();
    }

  	// 하도급 계약목록
  	public function scontract_list($action = NULL, $id = NULL)
  	{
          $action = (!empty($action))?$action:"1";
          $data['active'] = $action;
          $data['title'] = '하도급계약관리';
          $tr_title = $this->input->post('tr_title', true);
          $rco_name = $this->input->post('rco_name', true);
          $sco_name = $this->input->post('sco_name', true);
          $page = $this->input->post('page',true);
          if($page == null){
              $page = 1;
          }
          $data['page'] = 1;
          $data['tr_title'] = $tr_title;

          $data['rco_name'] = $rco_name;
          $data['sco_name'] = $sco_name;
          $data['active'] = $action;

          if($action == "edit") { //하도급설정
            $data['active'] = "9";
            $data['contract_info'] = $this->db->select('a.*, b.co_name as sco_name, c.co_name as rco_name')->where('a.idx', $id)->get("tbl_contract a left join tbl_members b on (`a`.`s_co_id` = `b`.`dp_id`) left join tbl_members c on (`a`.`r_co_id` = `c`.`dp_id`)")->row();
          } else {
            $this->contract_model->db->select('a.*, b.co_name as sco_name, c.co_name as rco_name');
            $this->contract_model->_table_name = 'tbl_contract a'; //table name
            $this->contract_model->db->join('tbl_members b', 'a.s_co_id = b.dp_id', 'left');
            $this->contract_model->db->join('tbl_members c', 'a.r_co_id = c.dp_id', 'left');
            $this->contract_model->_order_by = 'a.ct_edate ASC';
              if($tr_title != null){
                  $this->contract_model->db->like('a.tr_title', $tr_title);
              }
              if($rco_name != null){
                  $this->contract_model->db->like('c.co_name', $rco_name);
              }
              if($sco_name != null){
                  $this->contract_model->db->like('b.co_name', $sco_name);
              }
            $this->contract_model->db->where('a.is_subcontract', 'Y');
            $this->contract_model->db->where('a.ct_edate >=', date("Y-m-d"))->where('a.ct_sdate <=' , date("Y-m-d")); //진행중계약
            $data['all_contract_group'] = $this->contract_model->get();
          }

          $data['subview'] = $this->load->view('admin/contract/scontract_list', $data, true);
          $this->load->view('admin/_layout_main', $data);
  	}

    //원청 거래항목 설정
    public function pop_contract_items($ct_id, $action = NULL, $item_id = NULL)
    {
      $data['pop_page'] = "contract"; //팝업창 subside 분류용 (원청, 하도급)
		  $data['action'] = (!empty($action))?$action:"";
  		$data['ct_id'] = $ct_id;
      $data['item_id'] = (!empty($item_id))?$item_id:"";
      $data['title'] = '거래항목설정';
      if($action == "insert_item") { //거래항목 신규 등록
        $sql = "INSERT INTO tbl_contract_items SET ";
        $sql .= "ct_id = '".$ct_id."'";
        $sql .= ",title = '".$this->input->post('title', true)."'";
        $sql .= ",remark = '".$this->input->post('remark', true)."'";
        $sql .= ",list_order = '".$this->input->post('list_order', true)."'";
        $this->db->query($sql);
        $id = $this->db->insert_id();
        $message = "추가되었습니다.";
        $type = 'success';
        set_message($type, $message);
        redirect('admin/contract/pop_contract_items/'.$ct_id); //redirect page
      } else if($action == "update_item") { //거래항목 변경
        $sql = "UPDATE tbl_contract_items SET ";
        $sql .= "title = '".$this->input->post('title', true)."'";
        $sql .= ",remark = '".$this->input->post('remark', true)."'";
        $sql .= ",list_order = '".$this->input->post('list_order', true)."'";
        $sql .= "WHERE item_id = '".$item_id."'";
        $this->db->query($sql);
        $message = "변경되었습니다.";
        $type = 'success';
        set_message($type, $message);
        redirect('admin/contract/pop_contract_items/'.$ct_id); //redirect page
      } else if($action == "delete_item") { //거래항목 삭제
        $sql = "DELETE FROM tbl_contract_items WHERE item_id = '".$item_id."'";
        $this->db->query($sql);
        $message = "삭제되었습니다.";
        $type = 'success';
        set_message($type, $message);
        redirect('admin/contract/pop_contract_items/'.$ct_id); //redirect page
      }

      $this->contract_model->_table_name = 'tbl_contract_items'; //table name
      $this->contract_model->_order_by = 'list_order ASC';
      $this->contract_model->db->where('ct_id', $ct_id);
      $data['all_items_group'] = $this->contract_model->get();

  		$data['subview'] = $this->load->view('admin/contract/pop_contract_items', $data, true);
      $this->load->view('admin/_layout_pop_contract', $data);
    }

    //소속 거래처 선택창
    public function select_customer($ct_id, $no=NULL)
    {
      $data['title'] = '거래처 선택';
      $data['ct_id'] = $ct_id;
      $data['no'] = $no;

      $this->contract_model->db->select('a.*, b.co_name, b.ceo, b.bs_number');
      $this->contract_model->_table_name = 'tbl_contract_coops a'; //table name
      $this->contract_model->db->join('tbl_members b', 'a.customer_id = b.dp_id', 'left');
      $this->contract_model->_order_by = 'b.co_name ASC';
      $this->contract_model->db->where('a.ct_id', $ct_id);
      $data['all_coops_group'] = $this->contract_model->get();

      $data['subview'] = $this->load->view('admin/contract/pop_select_customer', $data, true);
      $this->load->view('admin/_layout_pop_common', $data);
    }

    //하도급계약 설정
    public function pop_scontract_config($ct_id, $sub = NULL, $action = NULL, $id = NULL)
    {
      $data['pop_page'] = "scontract"; //팝업창 subside 분류용 (원청, 하도급)
      $data['sub'] = (!empty($sub))?$sub:"cowork";
      $data['ct_id'] = $ct_id;

      /*---------------------------- 거래처 설정 -------------------------*/
      if($data['sub'] == "cowork") {
        $data['coop_id'] = (!empty($id))?$id:"";
        $data['title'] = '거래처설정';
        if($action == "insert_cowork") { //거래처 신규 등록
            $partner_info = $this->db->select('count(*) as count')->where('ct_id', $ct_id)->where('customer_id',$this->input->post('customer_id',true))->get("tbl_contract_coops")->row();

            if($partner_info->count <= 0) {
                $sql = "INSERT INTO tbl_contract_coops SET ";
                $sql .= "ct_id = '" . $ct_id . "'";
                $sql .= ",customer_id = '" . $this->input->post('customer_id', true) . "'";
                $this->db->query($sql);
                $message = "추가되었습니다.";
                $type = 'success';
            }else{
                $message = "이미 등록되어있는 거래처입니다..";
                $type = 'success';
            }
          set_message($type, $message);
          redirect('admin/contract/pop_scontract_config/'.$ct_id.'/'.$sub); //redirect page
        } else if($action == "update_cowork") { //거래처 변경
          $sql = "UPDATE tbl_contract_coops SET ";
          $sql .= "customer_id = '".$this->input->post('customer_id', true)."'";
          $sql .= "WHERE coop_id = '".$coop_id."'";
          $this->db->query($sql);
          $message = "변경되었습니다.";
          $type = 'success';
          set_message($type, $message);
          redirect('admin/contract/pop_scontract_config/'.$ct_id.'/'.$sub); //redirect page
        } else if($action == "delete_cowork") { //거래처 삭제
          $sql = "DELETE FROM tbl_contract_coops WHERE coop_id = '".$coop_id."'";
          $this->db->query($sql);
          $message = "삭제되었습니다.";
          $type = 'success';
          set_message($type, $message);
          redirect('admin/contract/pop_scontract_config/'.$ct_id.'/'.$sub); //redirect page
        }

        $this->contract_model->db->select('a.*, b.co_name, b.ceo');
        $this->contract_model->_table_name = 'tbl_contract_coops a'; //table name
        $this->contract_model->db->join('tbl_members b', 'a.customer_id = b.dp_id', 'left');
        $this->contract_model->_order_by = 'b.co_name ASC';
        $this->contract_model->db->where('a.ct_id', $ct_id);
        $data['all_coops_group'] = $this->contract_model->get();

      /*---------------------------- 파트너 거래 설정 -------------------------*/
      } else if($data['sub'] == "partner") {
        $data['title'] = '하도급파트너설정';
        $data['contract_partner_id'] = (!empty($id))?$id:"";
        if($action == "insert_partner") { //파트너 신규 등록
          //1차적으로 대상자가 있는지 체크한다.
            $partner_info = $this->db->select('count(*) as count')->where('ct_id', $ct_id)->where('partner_id',$this->input->post('partner_id',true))
                ->where('customer_id',$this->input->post('customer_id',true))->get("tbl_contract_partners")->row();

            if($partner_info->count <= 0) {


                $sql = "INSERT INTO tbl_contract_partners SET ";
                $sql .= "ct_id = '" . $ct_id . "'";
                $sql .= ",partner_id = '" . $this->input->post('partner_id', true) . "'";
                $sql .= ",customer_id = '" . $this->input->post('customer_id', true) . "'";
                $this->db->query($sql);

                //거래단가 등록
                $post_item_id = $this->input->post('item_id', true);
                $post_unit_price = $this->input->post('unit_price', true);
                if (!empty($post_item_id)) {
                    for ($i = 0; $i < sizeof($post_item_id); $i++) {
                        $item_id = $post_item_id[$i];
                        $unit_price = $post_unit_price[$i];
                        $sql = "INSERT INTO tbl_contract_partner_unitprice SET ";
                        $sql .= "partner_id = '" . $this->input->post('partner_id', true) . "'";
                        $sql .= ",item_id = '" . $item_id . "'";
                        $sql .= ",unit_price = '" . $unit_price . "'";
                        $this->db->query($sql);
                    }
                }
                $message = "추가되었습니다.";
                $type = 'success';
            }else{
                $message = "이미 등록되어있는 파트너입니다.";
                $type = 'success';
            }

          set_message($type, $message);
          redirect('admin/contract/pop_scontract_config/'.$ct_id.'/'.$sub); //redirect page
        } else if($action == "update_partner") { //파트너 변경
          $sql = "UPDATE tbl_contract_partners SET ";
          $sql .= "partner_id = '".$this->input->post('partner_id', true)."'";
          $sql .= ",customer_id = '".$this->input->post('customer_id', true)."'";
          $sql .= "WHERE contract_partner_id = '".$data['contract_partner_id']."'";
          $this->db->query($sql);

          //거래단가 등록
          $post_unitprice_id = $this->input->post('unitprice_id', true);
          $post_unit_price = $this->input->post('unit_price', true);
          $post_item_id = $this->input->post('item_id', true);
          if(!empty($post_item_id)) {
            for($i=0;$i<sizeof($post_item_id);$i++) {
                $unitprice_id = $post_unitprice_id[$i];
                $unit_price = $post_unit_price[$i];
                if(!empty($unitprice_id)) {
                  $sql = "UPDATE tbl_contract_partner_unitprice SET ";
                  $sql .= ",unit_price = '".$unit_price."'";
                  $sql .= "WHERE unitprice_id = '".$unitprice_id."'";
                } else {
                  $item_id = $post_item_id[$i];
                  $sql = "INSERT INTO tbl_contract_partner_unitprice SET ";
                  $sql .= "partner_id = '".$this->input->post('partner_id', true)."'";
                  $sql .= ",item_id = '".$item_id."'";
                  $sql .= ",unit_price = '".$unit_price."'";
                }
                $this->db->query($sql);
            }
          }

          $message = "변경되었습니다.";
          $type = 'success';
          set_message($type, $message);
          redirect('admin/contract/pop_scontract_config/'.$ct_id.'/'.$sub); //redirect page
        } else if($action == "delete_partner") { //파트너 삭제
          $sql = "DELETE FROM tbl_contract_partners WHERE contract_partner_id = '".$data['contract_partner_id']."'";
          $this->db->query($sql);
          $message = "삭제되었습니다.";
          $type = 'success';
          set_message($type, $message);
          redirect('admin/contract/pop_scontract_config/'.$ct_id.'/'.$sub); //redirect page
        } else if($action == "update_items"){
            //거래단가 일괄변경
            $post_unitprice_id = $this->input->post('unitprice_id', true);
            $post_unit_price = $this->input->post('unit_price', true);
            $post_item_id = $this->input->post('item_id', true);


            if(!empty($post_item_id)) {
                for($i=0;$i<sizeof($post_item_id);$i++) {
                    $unitprice_id = $post_unitprice_id[$i];
                    $unit_price = $post_unit_price[$i];
                    if(!empty($unitprice_id)) {
                        $sql = "UPDATE tbl_contract_partner_unitprice SET ";
                        $sql .= "unit_price = '".$unit_price."'";
                        $sql .= "WHERE unitprice_id = '".$unitprice_id."'";
                    } else {
                        $item_id = $post_item_id[$i];
                        $sql = "UPDATE tbl_contract_partner_unitprice SET ";

                        $sql .= "unit_price = '".$unit_price."'";
                        $sql .= "WHERE item_id = '".$item_id."'";
                    }
                    $this->db->query($sql);
                }
            }
        }

        //해당 계약 거래항목정보
        $this->contract_model->_table_name = 'tbl_contract_items'; //table name
        $this->contract_model->_order_by = 'title ASC';
        $this->contract_model->db->where('ct_id', $ct_id);
        $data['all_items_group'] = $this->contract_model->get();
        $data['item_count'] = count($data['all_items_group']);

        $this->contract_model->db->select('a.*, b.driver as partner, b.tax_yn, c.co_name as customer_name');
        $this->contract_model->_table_name = 'tbl_contract_partners a'; //table name
        $this->contract_model->db->join('tbl_members b', 'a.partner_id = b.dp_id', 'left');
        $this->contract_model->db->join('tbl_members c', 'a.customer_id = c.dp_id', 'left');
        $this->contract_model->_order_by = 'b.co_name ASC';
        $this->contract_model->db->where('a.ct_id', $ct_id);
        $data['all_partner_group'] = $this->contract_model->get();

      }
  		$data['subview'] = $this->load->view('admin/contract/pop_scontract_'.$data['sub'], $data, true);
      $this->load->view('admin/_layout_pop_contract', $data);
    }

    //계약단가 일괄적용
    public function set_unitprice_all($ct_id, $sub = NULL, $action = NULL)
    {
        //해당 계약 거래항목정보
        $this->contract_model->_table_name = 'tbl_contract_items'; //table name
        $this->contract_model->_order_by = 'title ASC';
        $this->contract_model->db->where('ct_id', $ct_id);
        $data['all_items_group'] = $this->contract_model->get();
        $data['item_count'] = count($data['all_items_group']);

      $data['ct_id'] = $ct_id;
      $data['sub'] = $sub;
      $data['title'] = '계약단가 일괄적용';
      $data['modal_subview'] = $this->load->view('admin/contract/_modal_unitprice', $data, FALSE);
      $this->load->view('admin/_layout_modal', $data);
    }

}
