<?php
/**
 * Description of Allocation
 *
 * @author yo
 */
class Allocation extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('allocation_model');
    }

    public function config($id=NULL)
    {
        $data['title'] = "배차설정";
        $data['permission_user'] = $this->allocation_model->all_permission_user('36');
		$data['branch_id'] = $id;
		if(empty($id)) {
			$data['branch_id'] = $this->allocation_model->get_first_branch();
		}

		//지사목록
		$filterBy = null;
        $data['all_branch_list'] = $this->allocation_model->get_branchs($filterBy);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);

        $data['subview'] = $this->load->view('admin/allocation/config', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function scheduling($alloc_date=NULL, $id=NULL)
    {
        $data['title'] = "배차등록";
		$data['alloc_date'] = $alloc_date;
		if(empty($data['alloc_date'])) $data['alloc_date'] = date('Y-m-d');
		$data['branch_id'] = $id;
		if(empty($id)) {
			$data['branch_id'] = $this->allocation_model->get_first_branch();
		}

		//지사목록
        $data['all_branch_group'] = $this->allocation_model->get_branchs();
        $data['all_branch_list'] = $this->allocation_model->get_branchs($data['branch_id']);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);



        $data['subview'] = $this->load->view('admin/allocation/scheduling', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function performance($alloc_date=NULL, $id=NULL)
    {
        $data['title'] = "배차실적";
		$data['alloc_date'] = $alloc_date;
		if(empty($data['alloc_date'])) $data['alloc_date'] = date('Y-m-d');
		$data['branch_id'] = $id;
		if(empty($id)) {
			$data['branch_id'] = $this->allocation_model->get_first_branch();
		}

		//지사목록
        $data['all_branch_group'] = $this->allocation_model->get_branchs();
        $data['all_branch_list'] = $this->allocation_model->get_branchs($data['branch_id']);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);

        $data['subview'] = $this->load->view('admin/allocation/performance', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function statistics($alloc_date=NULL, $id=NULL)
    {
        $data['title'] = "배차통계";
		$data['alloc_date'] = $alloc_date;
		if(empty($data['alloc_date'])) $data['alloc_date'] = date('Y-m-d');
		$data['branch_id'] = $id;
		if(empty($id)) {
			$data['branch_id'] = $this->allocation_model->get_first_branch();
		}

		//지사목록
        $data['all_branch_group'] = $this->allocation_model->get_branchs();
        $data['all_branch_list'] = $this->allocation_model->get_branchs($data['branch_id']);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);

        $data['subview'] = $this->load->view('admin/allocation/statistics', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function set_unit_price()
    {
        $data['title'] = "단가 전체 설정";
		
		$data['modal_subview'] = $this->load->view('admin/allocation/_modal_unit_price', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }




}