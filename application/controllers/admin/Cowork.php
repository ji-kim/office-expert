<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cowork extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('cowork_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

	// 관리비 생성
    public function generate_mfee($df_month=NULL)
    {
        $data['df_month']	= $df_month;
        $data['title']		= "관리비 & 수수료 생성";

		$flag = $this->input->post('flag', TRUE);
        if (!empty($this->input->post('df_month', TRUE))) $data['df_month'] = $this->input->post('df_month', TRUE);

		//if(empty($data['df_month'])) exit;

		if (!empty($flag)) { // check employee id is empty or not
            $data['flag'] = 1;
            $data['calculate_progress'] = $this->input->post('calculate_progress', TRUE);
            $data['ws_co_id'] = $this->input->post('ws_co_id', TRUE);
            $data['co_code'] = $this->input->post('co_code', TRUE);
            $data['co_name'] = $this->input->post('co_name', TRUE);
            $data['ct_id'] = $this->input->post('ct_id', TRUE);
			if ($flag == '1') { // 초기화 & 생성

				//계약단위
				if($data['calculate_progress'] == "through_contracts" && !empty($data['ct_id'])) {
					//기초 입력값 체크
					//if(empty($data['ct_id'])) exit;

					//기존 데이터 삭제
					$this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
					$this->cowork_model->db->where('df_month',$data['df_month']);
					$this->cowork_model->db->where('ct_id',$data['ct_id']);
					$all_mfee_list = $this->cowork_model->get();
					if (!empty($all_mfee_list)) {
						foreach ($all_mfee_list as $mfee_info) {
							//tbl_delivery_fee_add
							$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_fixmfee
							$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_gongje
							$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_insur
							$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_refund_gongje
							$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_levy
							$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);

							$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
						}
					}

					$sql = "DELETE FROM tbl_delivery_fee_sg_items WHERE df_month='".$data['df_month']."' and ws_co_id = '".$data['ws_co_id']."'";
					$this->db->query($sql);

					$sql = "DELETE FROM tbl_delivery_fee WHERE ct_id='".$data['ct_id']."' and df_month='".$data['df_month']."'";
					$this->db->query($sql);

					//해당 파트너 리스팅
					$this->cowork_model->_table_name = 'tbl_scontract_co a, tbl_members dp, tbl_asset_truck tr, tbl_members co, tbl_contract ct'; //table name
					$this->cowork_model->db->select('a.*, dp.*, tr.car_1, tr.ws_co_id as ws_co_id, co.co_name as rco_name, co.dp_id as rco_id');
					$this->cowork_model->db->where("a.dp_id = dp.dp_id and dp.tr_id = tr.idx and ( dp.code = co.code and co.mb_type = 'customer') and ct.idx = '".$data['ct_id']."'")->where('a.ct_id',$data['ct_id']);
					$data['all_dp_list'] = $this->cowork_model->get();
					$data['total_count'] = count($data['all_dp_list']);

				} else if($data['calculate_progress'] == "through_coops" && $data['ws_co_id']) { //거래처단위

					//기초 입력값 체크
					//if(empty($data['ct_id'])) exit;

					$this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
					$this->cowork_model->db->where('df_month',$data['df_month']);
					$this->cowork_model->db->where('gongje_req_co',$data['ws_co_id']);
					$this->cowork_model->_order_by = 'df_id';
					$all_mfee_list = $this->cowork_model->get();
					if (!empty($all_mfee_list)) {
						foreach ($all_mfee_list as $mfee_info) {
							//tbl_delivery_fee_add
							$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_fixmfee
							$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_gongje
							$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_insur
							$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_refund_gongje
							$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_levy
							$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);

							$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
						}
					}
					$sql = "DELETE FROM tbl_delivery_fee WHERE gongje_req_co='".$data['ws_co_id']."' and df_month='".$data['df_month']."'";
					$this->db->query($sql);

					//해당 파트너 리스팅
					$this->cowork_model->_table_name = ' tbl_members dp, tbl_members co'; //table name
					$this->cowork_model->db->select('distinct(dp.dp_id) , dp.*, tr.car_1, tr.ws_co_id as ws_co_id, co.co_name as rco_name, co.dp_id as rco_id');
					$this->cowork_model->db->join('tbl_asset_truck tr', 'tr.idx = dp.tr_id', 'left');
					$this->cowork_model->db->where(" ( dp.code = co.code and co.mb_type = 'customer') and tr.ws_co_id = '".$data['ws_co_id']."'");
					$data['all_dp_list'] = $this->cowork_model->get();
					$data['total_count'] = count($data['all_dp_list']);

					//수당
					$this->cowork_model->_table_name = 'tbl_dp_assign_item'; //table name
					$this->cowork_model->db->where('tbl_dp_assign_item.type', 'sudang');
					$this->cowork_model->db->where('tbl_dp_assign_item.ws_co_id', $data['ws_co_id']);
					$this->cowork_model->_order_by = 'item';
					$data['all_sudang_group'] = $this->cowork_model->get();
					//공제
					$this->cowork_model->_table_name = 'tbl_dp_assign_item'; //table name
					$this->cowork_model->db->where('tbl_dp_assign_item.type', 'gongje');
					$this->cowork_model->db->where('tbl_dp_assign_item.ws_co_id', $data['ws_co_id']);
					$this->cowork_model->_order_by = 'item';
					$data['all_gongje_group'] = $this->cowork_model->get();

				}

			} else if ($flag == '2') { // 초기화
				//계약단위 ? 거래처단위
				if($data['calculate_progress'] == "through_contracts" && !empty($data['ct_id'])) {
					//기존 데이터 삭제
					$this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
					$this->cowork_model->db->where('df_month',$data['df_month']);
					if(!empty($data['ct_id'])) $this->cowork_model->db->where('ct_id',$data['ct_id']);
					$all_mfee_list = $this->cowork_model->get();
					if (!empty($all_mfee_list)) {
						foreach ($all_mfee_list as $mfee_info) {
							//tbl_delivery_fee_add
							$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_fixmfee
							$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_gongje
							$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_insur
							$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_refund_gongje
							$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_levy
							$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);

							$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
						}
					}
					$sql = "DELETE FROM tbl_delivery_fee WHERE ct_id='".$data['ct_id']."' and df_month='".$data['df_month']."'";
					$this->db->query($sql);
				} else if($data['calculate_progress'] == "through_coops") {

				}
			}
		}
			//

/*
			//기존데이터 삭제
			if (!empty($df_month) && !empty($ct_id)) {
	            $cwhere = array('df_month' => $df_month,'ct_id' => $ct_id);
                $this->cowork_model->_table_name = 'tbl_delivery_fee';
                $this->cowork_model->delete_multiple($cwhere);
			}

            // get all designation info by Department id
            $this->cowork_model->_table_name = 'tbl_designations';
            $this->cowork_model->_order_by = 'designations_id';
            $designation_info = $this->cowork_model->get_by(array('departments_id' => $data['departments_id']), FALSE);
            if (!empty($designation_info)) {
                foreach ($designation_info as $v_designatio) {
                    $data['employee_info'][] = $this->cowork_model->get_emp_salary_list('', $v_designatio->designations_id);
                    $employee_info = $this->cowork_model->get_emp_salary_list('', $v_designatio->designations_id);
                    foreach ($employee_info as $value) {
                        // get all allowance info by salary template id
                        if (!empty($value->salary_template_id)) {
                            $data['allowance_info'][$value->user_id] = $this->get_allowance_info_by_id($value->salary_template_id);
                            // get all deduction info by salary template id
                            $data['deduction_info'][$value->user_id] = $this->get_deduction_info_by_id($value->salary_template_id);
                            // get all overtime info by month and employee id
                            $data['overtime_info'][$value->user_id] = $this->get_overtime_info_by_id($value->user_id, $data['payment_month']);
                        }
                        // get all advance salary info by month and employee id
                        $data['advance_salary'][$value->user_id] = $this->get_advance_salary_info_by_id($value->user_id, $data['payment_month']);
                        // get award info by employee id and payment month
                        $data['award_info'][$value->user_id] = $this->get_award_info_by_id($value->user_id, $data['payment_month']);
                        // check hourly payment info
                        // if exist count total hours in a month
                        // get hourly payment info by id
                        if (!empty($value->hourly_rate_id)) {
                            $data['total_hours'][$value->user_id] = $this->get_total_hours_in_month($value->user_id, $data['payment_month']);
                        }
                    }
                }
            }
*/

		// get contract list
        $this->cowork_model->_table_name = 'tbl_contract';
        $this->cowork_model->_order_by = 'idx';
        $this->cowork_model->db->where('tbl_contract.ct_status <>', '계약만료');
        $data['all_contract_info'] = $this->cowork_model->get();

		// get all customer list
        $this->cowork_model->_table_name = 'tbl_members';
        $this->cowork_model->_order_by = 'co_name';
        $this->cowork_model->db->where('tbl_members.mb_type', 'customer');
        $data['all_customer_info'] = $this->cowork_model->get();

        $data['subview'] = $this->load->view('admin/cowork/generate_mfee', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function set_rgongje($id,$dp_id)
    {
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['df_info'] = $this->cowork_model->check_by(array('df_id' => $id), 'tbl_delivery_fee');
        $data['rgongje_info'] = $this->cowork_model->check_by(array('dp_id' => $dp_id,'master_yn' => 'Y'), 'tbl_delivery_fee_refund_gongje_set');

		$this->cowork_model->_table_name = 'tbl_delivery_fee_sg_items'; //table name
		$this->cowork_model->db->where('df_month', $df_month)->where('ws_co_id', $ws_co_id)->where('add_type', 'GR');
		$this->cowork_model->_order_by = 'title';
		$data['all_rgongje_group'] = $this->cowork_model->get();

		$data['modal_subview'] = $this->load->view('admin/cowork/_modal_rgongje', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_rgongje($df_month = null, $gongje_req_co = null)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $df_id = $this->input->post('df_id', true);
            $df_month = $this->input->post('df_month', true);
            $gongje_req_co = $this->input->post('gongje_req_co', true);
            $dp_id = $this->input->post('dp_id', true);
            $ct_id = $this->input->post('ct_id', true);

            $this->cowork_model->_table_name = 'tbl_delivery_fee_refund_gongje'; //table name
            $result = $this->cowork_model->get_by(array('df_id' => $df_id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_delivery_fee_refund_gongje SET ";
				$sql .= "gj_termination_mortgage='".$this->input->post('gj_termination_mortgage', true)."'";
				$sql .= " WHERE df_id = '".$df_id."'";
				$this->db->query($sql);
				$exsql = $sql;
			} else {
				$sql = "INSERT INTO tbl_delivery_fee_refund_gongje SET ";
				$sql .= "dp_id='".$dp_id."'";
				$sql .= ",df_id='".$df_id."'";
				$sql .= ",df_month='".$df_month."'";
				$sql .= ",ws_co_id='".$gongje_req_co."'";
				$sql .= ",ct_id='".$ct_id."'";
				$sql .= ",gj_termination_mortgage='".$this->input->post('gj_termination_mortgage', true)."'";
				$this->db->query($sql);
			}

			$sql = "UPDATE tbl_delivery_fee_refund_gongje_set SET master_yn = 'N' WHERE dp_id = '".$dp_id."'";
			$this->db->query($sql);

			$sql = "INSERT INTO tbl_delivery_fee_refund_gongje_set SET ";
			$sql .= "dp_id='".$dp_id."'";
			$sql .= ",master_yn='Y'";
			$sql .= ",ct_id='".$ct_id."'";
			$sql .= ",gj_termination_mortgage='".$this->input->post('gj_termination_mortgage', true)."'";
			$this->db->query($sql);

			$message = "환급형공제가 저장되었습니다.";
            $type = 'success';

            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co.'/'.$exsql); //redirect page
    }

    public function set_ngongje($id,$dp_id,$df_month,$ws_co_id, $is_editable=NULL)
    {
        $data['df_month'] = $df_month;
        $data['dp_id'] = $dp_id;
        $data['is_editable'] = $is_editable;
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['df_info'] = $this->cowork_model->check_by(array('df_id' => $id), 'tbl_delivery_fee');
        $data['ngongje_info'] = $this->cowork_model->check_by(array('dp_id' => $dp_id,'master_yn' => 'Y'), 'tbl_delivery_fee_gongje_set');

		$this->cowork_model->_table_name = 'tbl_delivery_fee_sg_items'; //table name
		$this->cowork_model->db->where('df_month', $df_month)->where('ws_co_id', $ws_co_id)->where('add_type', 'GN');
		$this->cowork_model->_order_by = 'title';
		$data['all_ngongje_group'] = $this->cowork_model->get();


        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_ngongje', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

	public function set_finefee($action = NULL, $df_id,$dp_id,$df_month,$ws_co_id=NULL, $is_editable=NULL, $id=NULL)
    {
        $data['action'] = $action;
        $data['df_month'] = $df_month;
        $data['dp_id'] = $dp_id;
        $data['df_id'] = $df_id;
        $data['ws_co_id'] = $ws_co_id;
        $data['is_editable'] = $is_editable;
        $data['assign_user'] = $this->cowork_model->allowad_user('57');

		    if($action == "add") {
            $amount = $this->input->post('amount', true);
            $due_date = $this->input->post('due_date', true);
            $attach1 = $this->input->post('attach1', true);
            $memo = $this->input->post('memo', true);

			      //과태료 첨부파일
			      $sql_file = "";
            if (!empty($_FILES['attach1']['name'])) {
                $val = $this->cowork_model->customUploadFile('attach1','car_fines');
               // $val == TRUE || redirect('admin/cowork/jiip_gongje//'.$df_id.'/'.$dp_id.'/'.$df_month.'/'.$ws_co_id);
				        $sql_file .= ", attach1 = '".$val['path']."'";
            }

      			/*
      			$fine_sum = $this->db->select('sum(amount) as sum')->where('dp_id', $dp_id)->where('df_month', $df_month)->get("tbl_delivery_fee_gongje_finefee")->row();
      			if(!empty($fine_sum->sum) && $fine_sum->sum > 0) { // 기존버전과 호환 유지
      				$sql = "UPDATE tbl_delivery_fee_gongje SET ";
      				$sql .= " fine_fee='".$fine_sum->sum."'";
      				$sql .= " WHERE df_id = '".$df_id."'";
      				$this->db->query($sql);
      			}
      			*/
      			$sql = "INSERT INTO tbl_delivery_fee_gongje_finefee SET ";
      			$sql .= " df_id='".$df_id."'";
      			$sql .= ", df_month='".$df_month."'";
      			$sql .= ", dp_id='".$dp_id."'";
      			$sql .= ", amount='".$amount."'";
      			$sql .= ", due_date='".$due_date."'";
      			$sql .= ", memo='".$memo."'";
      			$sql .= $sql_file;
      			$this->db->query($sql);

      			$message = "과태료 정보가 저장되었습니다.";
            $type = 'success';

            set_message($type, $message);
    			redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$ws_co_id.'/'); //redirect page
    		} else if($action == "delete") {
          $sql = "DELETE FROM tbl_delivery_fee_gongje_finefee WHERE idx='".$id."'";
          $this->db->query($sql);

          $message = "과태료 정보가 삭제되었습니다.";
          $type = 'success';

          set_message($type, $message);
          redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$ws_co_id.'/'); //redirect page
        }

        $data['df_info'] = $this->cowork_model->check_by(array('df_id' => $df_id), 'tbl_delivery_fee');
        //$data['ngongje_info'] = $this->cowork_model->check_by(array('dp_id' => $dp_id,'df_month' => $df_month), 'tbl_delivery_fee_gongje_finefee');

    		$this->cowork_model->_table_name = 'tbl_delivery_fee_gongje_finefee'; //table name
    		$this->cowork_model->db->where('dp_id', $dp_id)->where('df_month', $df_month);
    		$this->cowork_model->_order_by = 'due_date DESC';
    		$data['all_finefee_group'] = $this->cowork_model->get();

        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_finefee', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_ngongje($df_month = null, $gongje_req_co = null)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $df_id = $this->input->post('df_id', true);
            $df_month = $this->input->post('df_month', true);
            $gongje_req_co = $this->input->post('gongje_req_co', true);
            $dp_id = $this->input->post('dp_id', true);
            $ct_id = $this->input->post('ct_id', true);

			//과태료 첨부파일
			$sql_file = "";
            if (!empty($_FILES['fine']['name'])) {
                $old_path = $this->input->post('fine_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->cowork_model->uploadAllType('fine','asset/fine/');
                $val == TRUE || redirect('admin/cowork/jiip_gongje/');
                $fine_data['fine_filename'] = $val['fileName'];
                $fine_data['fine'] = $val['path'];
                $fine_data['fine_path'] = $val['fullPath'];
				$sql_file .= ",fine_filename='".$fine_data['fine_filename']."'";
				$sql_file .= ",fine='".$fine_data['fine']."'";
				$sql_file .= ",fine_path='".$fine_data['fine_path']."'";
            }

            $this->cowork_model->_table_name = 'tbl_delivery_fee_gongje'; //table name
            $result = $this->cowork_model->get_by(array('dp_id' => $dp_id, 'df_month' => $df_month), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_delivery_fee_gongje SET ";
				$sql .= "car_tax='".$this->input->post('car_tax', true)."'";
				$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				$sql .= ",fine_fee='".$this->input->post('fine_fee', true)."'";
				$sql .= ",fine_fee_memo='".$this->input->post('fine_fee_memo', true)."'";
				$sql .= $sql_file;
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				$sql .= " WHERE df_id = '".$df_id."'";
				$this->db->query($sql);
				$exsql = $sql;
			} else {
				$sql = "INSERT INTO tbl_delivery_fee_gongje SET ";
				$sql .= "dp_id='".$dp_id."'";
				$sql .= ",df_id='".$df_id."'";
				$sql .= ",df_month='".$df_month."'";
				$sql .= ",ws_co_id='".$gongje_req_co."'";
				$sql .= ",ct_id='".$ct_id."'";
				$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
				$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				$sql .= ",fine_fee='".$this->input->post('fine_fee', true)."'";
				$sql .= ",fine_fee_memo='".$this->input->post('fine_fee_memo', true)."'";
				$sql .= $sql_file;
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				$this->db->query($sql);
			}

			$sql = "UPDATE tbl_delivery_fee_gongje_set SET master_yn = 'N' WHERE dp_id = '".$dp_id."'";
			$this->db->query($sql);

			$sql = "INSERT INTO tbl_delivery_fee_gongje_set SET ";
			$sql .= "dp_id='".$dp_id."'";
			$sql .= ",master_yn='Y'";
			$sql .= ",ct_id='".$ct_id."'";
			$sql .= ",fine_fee='".$this->input->post('fine_fee', true)."'";
			$sql .= ",fine_fee_memo='".$this->input->post('fine_fee_memo', true)."'";
			$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
			$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
			$this->db->query($sql);

			//추가 항목 저장
			for ($i=0; $i<count($this->input->post('chk[]', true)); $i++)
			{
				$k = $this->input->post('chk['.$i.']', true);
				$item_pidx = $this->input->post('item_pidx['.$i.']', true);
				$item_idx = $this->input->post('item_idx['.$i.']', true);
				$item_amount = $this->input->post('item_amount['.$i.']', true);
				$memo = $this->input->post('memo['.$i.']', true);

				if(!empty($item_idx)) $chk = $this->db->where('idx', $item_idx)->get('tbl_delivery_fee_add')->row();
				if(!empty($item_pidx)) $pitem = $this->db->where('idx', $item_pidx)->get('tbl_delivery_fee_sg_items')->row();
				if(!empty($chk->idx)) {
					$sql = "UPDATE tbl_delivery_fee_add SET pid='$item_pidx',df_id='$df_id',add_type='".$pitem->add_type."',df_month='$df_month',dp_id='$dp_id',amount='$item_amount',memo='$memo',ws_co_id='$gongje_req_co',apply_yn='Y' where idx='".$chk->idx."'";
				} else {
					if(!empty($item_pidx)) {
						$sql = "INSERT INTO tbl_delivery_fee_add SET pid='$item_pidx',df_id='$df_id',add_type='".$pitem->add_type."',df_month='$df_month',dp_id='$dp_id',title='".$pitem->title."',amount='$item_amount',memo='$memo',ws_co_id='$gongje_req_co',apply_yn='Y'";
					}
				}
				$this->db->query($sql);
			}


			$message = "공제가 저장되었습니다.";
            $type = 'success';

            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

	public function set_mfee($df_id, $dp_id, $df_month, $ws_co_id=null, $is_editable=NULL)
    {
        $data['is_editable'] = $is_editable;
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        //$data['df_info'] = $this->cowork_model->check_by(array('df_id' => $df_id), 'tbl_delivery_fee');
$data['df_info'] = $this->db->where('df_id', $df_id)->get('tbl_delivery_fee')->row();
        $data['mfee_info'] = $this->cowork_model->check_by(array('dp_id' => $dp_id,'master_yn' => 'Y'), 'tbl_delivery_fee_fixmfee_set');

		$this->cowork_model->_table_name = 'tbl_delivery_fee_sg_items'; //table name
		$this->cowork_model->db->where('df_month', $df_month)->where('ws_co_id', $ws_co_id)->where('add_type', 'GW');
		$this->cowork_model->_order_by = 'title';
		$data['all_wgongje_group'] = $this->cowork_model->get();

		$data['modal_subview'] = $this->load->view('admin/cowork/_modal_mfee', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_mfee($df_month = null, $gongje_req_co = null)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $df_id = $this->input->post('df_id', true);
            $df_month = $this->input->post('df_month', true);
            $gongje_req_co = $this->input->post('gongje_req_co', true);
            $dp_id = $this->input->post('dp_id', true);
            $ct_id = $this->input->post('ct_id', true);

            $this->cowork_model->_table_name = 'tbl_delivery_fee_fixmfee'; //table name
            $result = $this->cowork_model->get_by(array('df_id' => $df_id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_delivery_fee_fixmfee SET ";
				$sql .= "wst_mfee='".$this->input->post('wst_mfee', true)."'";
				$sql .= ",mfee_vat='".$this->input->post('mfee_vat', true)."'";
				$sql .= ",org_fee='".$this->input->post('org_fee', true)."'";
				//$sql .= ",org_fee_vat='".$this->input->post('org_fee_vat', true)."'";
				//$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				//$sql .= ",env_fee_vat='".$this->input->post('env_fee_vat', true)."'";
				//$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
				//$sql .= ",car_tax_vat='".$this->input->post('car_tax_vat', true)."'";
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				//$sql .= ",etc='".$this->input->post('etc', true)."'";
				$sql .= " WHERE df_id = '".$df_id."'";
				$this->db->query($sql);
				$exsql = $sql;

				$sql = "UPDATE tbl_delivery_fee_fixmfee_set SET master_yn = 'N' WHERE dp_id = '".$dp_id."'";
				$this->db->query($sql);

				$sql = "INSERT INTO tbl_delivery_fee_fixmfee_set SET ";
				$sql .= "dp_id='".$dp_id."'";
				$sql .= ",master_yn='Y'";
				$sql .= ",ct_id='".$ct_id."'";
				$sql .= ",wst_mfee='".$this->input->post('wst_mfee', true)."'";
				$sql .= ",mfee_vat='".$this->input->post('mfee_vat', true)."'";
				$sql .= ",org_fee='".$this->input->post('org_fee', true)."'";
				//$sql .= ",org_fee_vat='".$this->input->post('org_fee_vat', true)."'";
				//$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				//$sql .= ",env_fee_vat='".$this->input->post('env_fee_vat', true)."'";
				//$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
				//$sql .= ",car_tax_vat='".$this->input->post('car_tax_vat', true)."'";
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				//$sql .= ",etc='".$this->input->post('etc', true)."'";
				$this->db->query($sql);
			}
			/*
			else {
				$sql = "INSERT INTO tbl_delivery_fee_fixmfee SET ";
				$sql .= "df_id='".$df_id."'";
				$sql .= ",ws_co_id ='".$gongje_req_co."'";
				$sql .= ",df_month ='".$df_month."'";
				$sql .= ",dp_id='".$dp_id."'";
				$sql .= ",ct_id='".$ct_id."'";
				$sql .= ",wst_mfee='".$this->input->post('wst_mfee', true)."'";
				$sql .= ",mfee_vat='".$this->input->post('mfee_vat', true)."'";
				$sql .= ",org_fee='".$this->input->post('org_fee', true)."'";
				$sql .= ",org_fee_vat='".$this->input->post('org_fee_vat', true)."'";
				$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				$sql .= ",env_fee_vat='".$this->input->post('env_fee_vat', true)."'";
				$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
				$sql .= ",car_tax_vat='".$this->input->post('car_tax_vat', true)."'";
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				$sql .= ",etc='".$this->input->post('etc', true)."'";
				$this->db->query($sql);
			}
			*/

			$message = "관리비가 저장되었습니다.";
            $type = 'success';

            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co.'/'.$exsql); //redirect page
    }

    public function set_misu($id, $is_editable=NULL)
    {
        $data['is_editable'] = $is_editable;
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['mfee_info'] = $this->cowork_model->check_by(array('df_id' => $id), 'tbl_delivery_fee');
        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_misu', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_misu($id = null)
    {
        $df_id = $this->input->post('df_id', true);
        $df_month = $this->input->post('df_month', true);
        $gongje_req_co = $this->input->post('gongje_req_co', true);

		$created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $df_data['remark'] = $this->input->post('remark', true);
            $this->cowork_model->_table_name = 'tbl_delivery_fee';
            $this->cowork_model->_primary_key = 'df_id';
            $id = $this->cowork_model->save($df_data, $df_id);

            $this->cowork_model->_table_name = 'tbl_delivery_fee_gongje'; //table name
            $result = $this->cowork_model->get_by(array('df_id' => $df_id), true);
            if (!empty($result)) {
				$sql = "UPDATE tbl_delivery_fee_gongje SET ";
				$sql .= "not_paid='".$this->input->post('not_paid', true)."'";
				$sql .= " WHERE df_id = '".$df_id."'";
				$this->db->query($sql);
				$exsql = $sql;
			} else {
				$sql = "INSERT INTO tbl_delivery_fee_gongje SET ";
				$sql .= "df_id='".$df_id."'";
				$sql .= ",not_paid='".$this->input->post('not_paid', true)."'";
				$this->db->query($sql);
			}

			$message = "초기미수액이 수정되었습니다.";
            $type = 'success';

            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function set_car_insur($id)
    {
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['car_info'] = $this->cowork_model->check_by(array('idx' => $id), 'tbl_asset_truck');
        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_set_car', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function close_mfee_all($df_month = NULL, $gongje_req_co = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			$sql = "UPDATE tbl_delivery_fee SET is_closed='".$is_closed."' WHERE df_month = ?";
			$this->db->query($sql, array($df_month));
			$message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function close_mfee($df_month = NULL, $gongje_req_co = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			      $sql = "UPDATE tbl_delivery_fee SET is_closed='".$is_closed."' WHERE df_month = ? AND gongje_req_co = ?";
			      $this->db->query($sql, array($df_month, $gongje_req_co));
			      $message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function close_mfee_id($df_month = NULL, $gongje_req_co = NULL, $id = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
             $delivery_fee_details = $this->db->where('df_id', $id)->get('tbl_delivery_fee')->row();
					   $sql = "UPDATE tbl_delivery_fee SET is_closed='".$is_closed."' WHERE df_id = ?";
			       $this->db->query($sql, array($id));
             if(!empty($delivery_fee_details->df_id)) {
      											//수납생성
      											$gn_amount = $delivery_fee_details->stot_mfee;
      											$stot_mfee = $delivery_fee_details->stot_mfee;
      											$stot_transaction = 0;
      											$tot_mfee = 0;
      											$tot_transaction = 0;
      											if(!empty($delivery_fee_details->stot_transaction)) $stot_transaction = $delivery_fee_details->stot_transaction;

      											//거래등록시
      											if($stot_transaction > 0) {
      												if($stot_mfee >= $stot_transaction) {
      													$tot_mfee = $stot_mfee - $stot_transaction;
      												} else {
      													$tot_transaction = $stot_transaction - $stot_mfee;
      												}
      											} else {
      												$tot_mfee = $stot_mfee;
      											}
      											$qry_subtotals = ", mf_amount='".$stot_mfee."', tr_amount='".$stot_transaction."'";

      											$chk = $this->cowork_model->db->query("select df_id,gongje_req_co from tbl_delivery_fee_levy where df_id='".$delivery_fee_details->df_id."'")->row();
      											if(empty($chk->df_id)) {
      												$sql = "INSERT INTO tbl_delivery_fee_levy SET df_id='".$delivery_fee_details->df_id."', dp_id='".$delivery_fee_details->dp_id."', df_month='$df_month', gongje_req_co = '".$gongje_req_co."'";
      												$sql .= ", pay_status='N', prev_misu = '$not_paid' ,gn_amount = '".$gn_amount."', paid_amount = '0', balance_amount = '".$gn_amount."'".$qry_subtotals;
      											} else {
      												$sql = "UPDATE tbl_delivery_fee_levy SET prev_misu = '$not_paid' ,gn_amount = '".$gn_amount."', paid_amount = '0', balance_amount = '".$gn_amount."'".$qry_subtotals." WHERE df_id='".$delivery_fee_details->df_id."'";
      											}

      											if($stot_mfee >= $stot_transaction) $gn_amount = $stot_mfee - $stot_transaction;
      											$this->cowork_model->db->query($sql);
            }
			      $message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function close_payment($df_month = NULL, $gongje_req_co = NULL, $id = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			$sql = "UPDATE tbl_delivery_fee SET is_pay_closed='".$is_closed."' WHERE df_id = ?";
			$this->db->query($sql, array($id));
			$message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

	// 위수탁관리비 출력
    public function jiip_gongje($df_month = NULL, $gongje_req_co = NULL, $action = NULL, $df_id = NULL, $dp_id = NULL)
    {
        $data['single_df_id'] = $df_id; //개별 관리비 키
        $data['single_dp_id'] = $dp_id; //개별 파트너 키
        $data['action'] = $action;
        //receive form input by post
        $data['gongje_req_co'] = (!empty($this->input->post('gongje_req_co', true)))?$this->input->post('gongje_req_co', true):"1413"; //공제청구사 미선택시 기본 케이티 지엘에스(로지넥스에서는 필요없음)

/* 문제 없으면 삭제 할것
		    if (!empty($gongje_req_co)) $data['gongje_req_co'] = $gongje_req_co;
        else $data['gongje_req_co'] = $this->input->post('gongje_req_co', true);

        if (empty($data['gongje_req_co'])) $data['gongje_req_co'] = "1413";
        $data['gongje_req_co_name'] = $this->input->post('gongje_req_co_name', true);
        if (empty($data['gongje_req_co_name'])) $data['gongje_req_co_name'] = "케이티지엘에스(주)";
*/

        $data['ws_mode'] = $this->input->post('ws_mode', true); // IC 등록마감처리, CL 마감처리
        $data['close_yn'] = $this->input->post('close_yn', true);
		    $data['search_keyword'] = $this->input->post('search_keyword', true);

        if (!empty($df_month)) $data['df_month'] = $df_month;
        else $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_member', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

	    $data['title'] = '지입공제내역';

		$this->cowork_model->_table_name = ' tbl_delivery_fee df, tbl_members pt, tbl_asset_truck tr'; //table name
		$this->cowork_model->db->select('df.*, pt.co_name, pt.ceo, pt.bs_number, pt.N, pt.O, pt.ws_co_id, pt.driver, tr.car_1, tr.p_tr_id');
		$this->cowork_model->db->where(" ( df.dp_id = pt.dp_id and df.tr_id = tr.idx) ");
        $this->cowork_model->db->where('df.df_month', $data['df_month'])->where("(df.tr_type = 'M' OR df.tr_type = 'MT' )");
		if($data['gongje_req_co'] == "etc") {
			$this->cowork_model->db->where_not_in('df.gongje_req_co', '1413')->where_not_in('df.gongje_req_co', '1414')->where_not_in('df.gongje_req_co', '1415')->where_not_in('df.gongje_req_co', '1903');
		} else if($data['gongje_req_co'] == "all") {
		} else {
			$this->cowork_model->db->where('df.gongje_req_co', $data['gongje_req_co']);
		}
		if(!empty($data['search_keyword'])) {
			$this->cowork_model->db->where("(pt.co_name like '%".$data['search_keyword']."%' OR pt.ceo like '%".$data['search_keyword']."%' OR pt.reg_number like '%".$data['search_keyword']."%' OR pt.bs_number like '%".$data['search_keyword']."%' OR pt.driver like '%".$data['search_keyword']."%' OR df.D like '%".$data['search_keyword']."%' OR tr.car_1 like '%".$data['search_keyword']."%' )");
		}
        $this->cowork_model->_order_by = 'df.C';
        $data['all_delivery_fee_info'] = $this->cowork_model->get();
        $data['total_count'] = count($data['all_delivery_fee_info']);

		if(!empty($data['gongje_req_co'])) {
			//수당
			$this->cowork_model->_table_name = 'tbl_delivery_fee_sg_items'; //table name
			$this->cowork_model->db->where('df_month', $data['df_month'])->where('ws_co_id', $data['gongje_req_co'])->where('add_type', 'S');
			$this->cowork_model->_order_by = 'title';
			$data['all_sudang_group'] = $this->cowork_model->get();

			//공제
			$this->cowork_model->_table_name = 'tbl_delivery_fee_sg_items'; //table name
			$this->cowork_model->db->where('df_month', $data['df_month'])->where('ws_co_id', $data['gongje_req_co'])->where('add_type<>', 'S');
			$this->cowork_model->_order_by = 'title';
			$data['all_gongje_group'] = $this->cowork_model->get();
		}

/*
///////	tmp		//수당
			$this->cowork_model->_table_name = 'tbl_settings_sg'; //table name
			$this->cowork_model->db->where('tbl_settings_sg.add_type', 'S');
			$this->cowork_model->_order_by = 'dp_order asc';
			$data['all_psudang_group'] = $this->cowork_model->get();
			//공제
			$where_au = "(add_type = 'GW' OR add_type = 'GN' OR add_type = 'GR')";
			$this->cowork_model->_table_name = 'tbl_settings_sg'; //table name
			$this->cowork_model->db->where($where_au);
			$this->cowork_model->_order_by = 'dp_order asc';
			$data['all_pgongje_group'] = $this->cowork_model->get();
//////// tmp
*/
        if ($action == 'print') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_print', $data, true);
			$this->load->view('admin/_layout_print', $data);
        } else if ($action == 'excel') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
        } else if ($action == 'excel_list') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_list_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
        } else if ($action == 'generate_all' || $action == 'generate_selected' || $action == 'generate_added') { // 생성
			//해당 소속 파트너
			$this->cowork_model->_table_name = ' tbl_members dp'; //table name
			$this->cowork_model->db->select('dp.*, tr.car_1, tr.ws_co_id as ws_co_id, tr.p_tr_id, tr.idx as ctr_id');
			$this->cowork_model->db->join('tbl_asset_truck tr', 'tr.inv_co_id = dp.dp_id', 'left');
			$this->cowork_model->db->where(" tr.ws_co_id = '".$data['gongje_req_co']."'")->where(" tr.car_status = 'A'");
			$this->cowork_model->_order_by = 'dp.ceo';
			$data['all_dp_list'] = $this->cowork_model->get();
			$data['total_count'] = count($data['all_dp_list']);

			//수당
			$this->cowork_model->_table_name = 'tbl_settings_sg'; //table name
			$this->cowork_model->db->where('tbl_settings_sg.add_type', 'S');
			$this->cowork_model->_order_by = 'dp_order asc';
			$data['all_sudang_group'] = $this->cowork_model->get();
			//공제
			$where_au = "(add_type = 'GW' OR add_type = 'GN' OR add_type = 'GR')";
			$this->cowork_model->_table_name = 'tbl_settings_sg'; //table name
			$this->cowork_model->db->where($where_au);
			$this->cowork_model->_order_by = 'dp_order asc';
			$data['all_gongje_group'] = $this->cowork_model->get();

			//추가항목 레코드 생성 -> 위수탁 관리사 반드시 선택되어있어야 함
			// 1. 수당
			$sql = "";
			if (!empty($data['all_sudang_group'])) {
				foreach ($data['all_sudang_group'] as $sudang_info) {
					$chk = $this->db->where('ws_co_id', $data['gongje_req_co'])->where('df_month', $df_month)->where('add_type', 'S')->get('tbl_delivery_fee_sg_items')->row();
					if (!empty($chk)) {
						$sql = "update tbl_delivery_fee_sg_items set ws_co_id = '".$data['gongje_req_co']."',df_month = '".$df_month."',title = '".$sudang_info->title."',add_type = 'S',dp_order = '".$sudang_info->dp_order."' where idx='".$sudang_info->idx."'";
					} else {
						$sql = "insert into tbl_delivery_fee_sg_items set ws_co_id = '".$data['gongje_req_co']."',df_month = '".$df_month."',title = '".$sudang_info->title."',add_type = 'S',dp_order = '".$sudang_info->dp_order."'";
					}
					$this->cowork_model->db->query($sql);
				}
			}

			// 2. 공제
			if (!empty($data['all_gongje_group'])) {
				foreach ($data['all_gongje_group'] as $gongje_info) {
					$chk = $this->db->where('ws_co_id', $data['gongje_req_co'])->where('df_month', $df_month)->where('add_type', $gongje_info->add_type)->get('tbl_delivery_fee_sg_items')->row();
					if (!empty($chk)) {
						$sql = "update tbl_delivery_fee_sg_items set ws_co_id = '".$data['gongje_req_co']."',df_month = '".$df_month."',title = '".$gongje_info->title."',add_type = '".$gongje_info->add_type."',dp_order = '".$gongje_info->dp_order."' where idx='".$gongje_info->idx."'";
					} else {
						$sql = "insert into tbl_delivery_fee_sg_items set ws_co_id = '".$data['gongje_req_co']."',df_month = '".$df_month."',title = '".$gongje_info->title."',add_type = '".$gongje_info->add_type."',dp_order = '".$gongje_info->dp_order."'";
					}

					$this->cowork_model->db->query($sql);
				}
			}

			if ($action == 'generate_selected') { // 선택 재생성
				//for ($i=0; $i<count($this->input->post('chk[]', true)); $i++)
				if(1)
				{
					//$k = $this->input->post('chk['.$i.']', true);
					$df_id = $data['single_df_id']; //$this->input->post('df_id['.$i.']', true);
					$dp_id = $data['single_dp_id']; //$this->input->post('dp_id['.$i.']', true);
					if(!empty($df_id)) {

						//---기존 데이터 삭제
							//tbl_delivery_fee_add
							$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_fixmfee
							$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_gongje
							$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_insur
							$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_refund_gongje
							$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_levy
							$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$df_id."'";
							$this->db->query($sql);

							$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$df_id."'";
							$this->db->query($sql);

						$sql = "DELETE FROM tbl_delivery_fee WHERE df_id='".$df_id."'";
						$this->db->query($sql);
						//---기존 데이터 삭제


						//생성 시작
						$dp_details = $this->db->select('dp.*, tr.idx as ctr_id, tr.car_1, tr.ws_co_id as ws_co_id, co.co_name as rco_name, co.dp_id as rco_id')->where("( dp.code = co.code and co.mb_type = 'customer') and dp.dp_id = '".$dp_id."' and tr.car_status = 'A'")->get("tbl_members co, tbl_members dp left join tbl_asset_truck tr on (`tr`.`inv_co_id` = `dp`.`dp_id`)")->row();



						if(!empty($dp_details->dp_id)) {
							// 계약기간 확인 N, O
							$inm = "";
							$outm = "";
							if(!empty($dp_details->N)) $inm = substr($dp_details->N,0,7);
							if(!empty($dp_details->O)) $outm = substr($dp_details->O,0,7);


							if((!empty($dp_details->N) && !empty($dp_details->O)) && ($df_month >= $inm && $df_month <= $outm)) {

							//$tr = $this->db->where('idx', $dp_details->tr_id)->get('tbl_asset_truck')->row();
							//$chk = $this->db->where('df_month', $df_month)->where('dp_id', $dp_details->dp_id)->get('tbl_delivery_fee')->row();
							if(1) { //count($chk)==0) { //---------중복처리 나중에 원인 찾을것


								$sn = $total_count--;
								$tot_gongje_req = $tot_gongje = 0;
								$wsm_sum = $insur_sum = $gongje_sum = $rf_gongje_sum = 0;

								$sql_basic = ""; // 기본정보
								$sql_co_info = ""; // 회사들정보
								$sql_unitprice = ""; // 계약단가정보
								$sql_bank = ""; // 은행정보
								$sql_basic_sudang = ""; // 기본수당
								$sql_basic_gongje = ""; // 기본공제
								$sql_insure = ""; // 보험공제
								$sql_ilhal = "";

								$sql_mf = ""; // 관리비정보

								//일할계산처리
								/* 그리고 1일이 되었는 3일이 되었든 아님 10일지났든
								계약당월 이런경우 협회비 차고지비 보험료는 보험만료와 계약만료일 과 비교해서
								생성이 되도록 해야하는것 */
								$is_ilhal = "N";
								if($df_month == $inm || $df_month == $outm) {
									$ilhal_days = 0;
									$s = substr($dp_details->N,8,2);
									$e = substr($dp_details->O,8,2);
									if($df_month == $inm && $df_month == $outm) { // 당월 입.퇴사자
										$ilhal_days = (int)$e - (int)$s + 1;
									} else if($df_month == $inm) { // 당월 입사자
										$ilhal_days = $endDay - (int)$s + 1;
									} else if($df_month == $outm) { // 당월 퇴사자
										$ilhal_days = (int)$e;
									}
									$is_ilhal = "Y";
									$sql_ilhal = ", remark ='일할계산(".$ilhal_days."일)'";
								}

								//--------------------------------- 0. 기본정보
								//$sql_basic .= "ct_id ='".$ct_id."'";
								$sql_basic .= "dp_id ='".$dp_details->dp_id."'";
								$sql_basic .= ",userid ='".$dp_details->userid."'";
								$sql_basic .= ",df_month ='".$df_month."'";
								$sql_basic .= ",co_code ='".$dp_details->code."'";
								$sql_basic .= ",co_name ='".$dp_details->co_name."'";
								$sql_basic .= ",C ='".$dp_details->jiip_co."'";
								$sql_basic .= ",D ='".$dp_details->rco_name."'";
								$sql_basic .= ",E ='".$dp_details->driver."'";
								$sql_basic .= ",status ='대기'";
								if(!empty($dp_details->bs_number)) $sql_basic .= ",bs_number ='".$dp_details->bs_number."'"; // 사업자등록  *---------------------*
								else if(!empty($dp_details->reg_number)) $sql_basic .= ",bs_number ='".$dp_details->reg_number."'"; // 사업자등록없는경우 주민번호로 *---------------------*
								$sql_basic .= ",applytax_yn ='".$dp_details->tax_yn."'";

								$vat_acc_yn = 'N'; //부가세 누적관련
								$acc_vat_dmonth_cnt = 0;
								if($dp_details->acc_vat_yn == "Y") {
									$sql_tax2 = ",vat_acc_yn='Y'";
									$acc_vat_dmonth_cnt = $df_month;
								}
								$sql_basic .= ",vat_acc_yn ='".$vat_acc_yn."'";
								//부가세 누적 = acc_vat_paid_yn 부가세지급여부 acc_vat_month=부가세 누적개월
								$vv = $this->db->select('sum(acc_vat_dmonth) as avd, count(acc_vat_dmonth) as cnt')->where('dp_id', $dp_details->ws_co_id)->get('tbl_delivery_fee')->row();
								if(!empty($vv->avd)) $sql_basic .= ",acc_vat_sum='".$vv->avd."'";
								if(!empty($vv->cnt)) $sql_basic .= ",acc_vat_dmonth_cnt ='".$vv->cnt."'";


								//--------------------------------- 1. 공제청구사(위수탁관리사) 등 회사정보
								$gj_rq_co = $this->db->where('dp_id', $dp_details->ws_co_id)->get('tbl_members')->row();
								$r_co_id = $dp_details->rco_id;//실수요처
								$r_co_name = $dp_details->rco_name;//실수요처
								$s_co_name = $dp_details->driver;//운영공제(송금)사

								//과세여부
								$tax_yn = $dp_details->tax_yn;
								// 회사정보쿼리
								if(!empty($dp_details->ctr_id)) $sql_co_info .= ",tr_id='".$dp_details->ctr_id."'";
								if(!empty($data['gongje_req_co'])) $sql_co_info .= ", gongje_req_co='".$data['gongje_req_co']."'";
								//if(!empty($gj_rq_co->sm_co_id)) $sql_co_info .= ", sendmoney_co='".$gj_rq_co->sm_co_id."'"; //-> 송금사 저장문제 체크

								//--------------------------------- 2. 관리비정보
								//$sql_mf .= "ct_id ='".$ct_id."'";
								$sql_mf .= "dp_id ='".$dp_details->dp_id."'";
								$sql_mf .= ",df_month ='".$df_month."'";
								$sql_mf .= ",co_code ='".$dp_details->code."'";
								if(!empty($dp_details->ws_co_id)) $sql_mf .= ", ws_co_id='".$dp_details->ws_co_id."'";
								//$mf = $this->db->where('dp_id', $dp_details->dp_id)->get('tbl_scontract_mfee')->row(); //기본정보
								//if(!empty($mf->mfee_etc)) $sql_mf .= ",mfee_etc='".$mf->mfee_etc."'";
								$mf = $this->db->where('dp_id', $dp_details->dp_id)->where('master_yn', 'Y')->get('tbl_delivery_fee_fixmfee_set')->row(); //기본정보

								if(!empty($mf->wst_mfee)) $wst_mfee = $mf->wst_mfee; else $wst_mfee = 0;
								if(!empty($mf->mfee_vat)) $mfee_vat = $mf->mfee_vat; else $mfee_vat = 0;
								if(!empty($mf->org_fee)) $org_fee = $mf->org_fee; else $org_fee = 0;
								if(!empty($mf->grg_fee)) $grg_fee = $mf->grg_fee; else $grg_fee = 0;
								if(!empty($mf->grg_fee_vat)) $grg_fee_vat = $mf->grg_fee_vat; else $grg_fee_vat = 0;
								$wsm_sum += ($wst_mfee + $mfee_vat + $org_fee + $grg_fee + $grg_fee_vat);

								$org_fee_vat = 0;
								$env_fee = 0;
								$env_fee_vat = 0;
								$car_tax = 0;
								$car_tax_vat = 0;
								$etc = 0;
								// 일할계산
								if($is_ilhal == "Y") {
									if($df_month == $inm || $df_month != $outm) {
										if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)(($wst_mfee/$endDay) * $ilhal_days);
										if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)(($mfee_vat/$endDay) * $ilhal_days);
									} else if($ilhal_days < 15) {
										if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)($wst_mfee/2);
										if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)($mfee_vat/2);
									}

								}
								$sql_mf .= ",wst_mfee='".$wst_mfee."'";
								$sql_mf .= ",mfee_vat='".$mfee_vat."'";
								$sql_mf .= ",org_fee='".$org_fee."'";
								$sql_mf .= ",grg_fee='".$grg_fee."'";
								$sql_mf .= ",grg_fee_vat='".$grg_fee_vat."'";

								//--------------------------------- 3. 은행정보
								if(!empty($dp_details->bank_code)) $sql_bank .= ",bank_code='".$dp_details->bank_code."'";
								if(!empty($dp_details->bank)) $sql_bank .= ",bank='".$dp_details->bank."'";
								if(!empty($dp_details->account_no)) $sql_bank .= ",account_no='".$dp_details->account_no."'";
								if(!empty($dp_details->account_name)) $sql_bank .= ",account_name='".$dp_details->account_name."'";
								if(!empty($dp_details->account_relation)) $sql_bank .= ",account_relation='".$dp_details->account_relation."'";

								//--------------------------------- 4. 계약단가정보
								if(!empty($ct_id)) {
									$cu = $this->db->where('dp_id', $dp_details->dp_id)->where('ct_id', $ct_id)->get('tbl_scontract_co')->row(); //기본정보
									if(!empty($cu->val1)) $sql_unitprice .= ",unit_price1='".$cu->val1."'";
									if(!empty($cu->val2)) $sql_unitprice .= ",unit_price2='".$cu->val2."'";
									if(!empty($cu->val3)) $sql_unitprice .= ",unit_price3='".$cu->val3."'";
									if(!empty($cu->val4)) $sql_unitprice .= ",unit_price4='".$cu->val4."'";
								}

								//--------------------------------- 5. 수당
								if(!empty($ct_id)) {
									$sd = $this->db->where('dp_id', $dp_details->dp_id)->where('ct_id', $ct_id)->get('tbl_delivery_fee_bsudang')->row(); //기본정보
									if((!empty($sd->sanje_yn) && !empty($sd->sanje_amount)) && $sd->sanje_yn == "Y") $sanje_amount = $sd->sanje_amount; else $sanje_amount = 0;

									if(!empty($sd->manager_pickup)) $sql_basic_sudang .= ",manager_pickup='".$sd->manager_pickup."'";
									if(!empty($sd->team_leader)) $sql_basic_sudang .= ",team_leader='".$sd->team_leader."'";
									if(!empty($sd->diffcult_area)) $sql_basic_sudang .= ",diffcult_area='".$sd->diffcult_area."'";
									if(!empty($sd->subside)) $sql_basic_sudang .= ",subside='".$sd->subside."'";
									if(!empty($sd->hoisik)) $sql_basic_sudang .= ",hoisik='".$sd->hoisik."'";
									if(!empty($sd->sudang_etc)) $sql_basic_sudang .= ",sudang_etc='".$sd->sudang_etc."'";
									if(!empty($sd->sanje_yn)) $sql_basic_sudang .= ",sanje_yn='".$sd->sanje_yn."'";
									$sql_basic_sudang .= ",sanje_amount='".$sanje_amount."'";
								}

								//--------------------------------- 6. 보험 tbl_asset_truck_insur_sub tr_id & pay_month  pay_amt
								if(!empty($dp_details->ctr_id)) {
									$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
									$qry .= " where a.type = '자동차' and b.gj_month = '".$df_month."' and (a.tr_id = '".$dp_details->ctr_id."') and a.co_pay<>'Y' and b.pay_by='01' and a.active_yn='Y'";
									$ins = $this->cowork_model->db->query($qry)->row();
									if(!empty($ins->pay_amt))
										$ins_fee = $ins->pay_amt;
									else
										$ins_fee =0;

									//if($dp_details->dp_id == "2656") echo $qry;
//01 공제 > 미수와 미납에 동시등록 -> 회계등록
//02 회사부담 > 미납에등록 -> 회계등록
//03 기사부담
//04 계약종료

									$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
									$qry .= " where a.type = '적재물' and b.gj_month = '".$df_month."' and a.tr_id = '".$dp_details->p_tr_id."' and a.active_yn='Y' and b.pay_by='01' ";
									$lins = $this->cowork_model->db->query($qry)->row();
									if(!empty($lins->pay_amt))
										$lins_fee = $lins->pay_amt;
									else
										$lins_fee =0;

									// 일할계산
									if($is_ilhal == "Y") {
										//if(!empty($ins_fee) && $ins_fee > 0) $ins_fee = ($ins_fee/$endDay) * $ilhal_days;
										//if(!empty($lins_fee) && $lins_fee > 0) $lins_fee = ($lins_fee/$endDay) * $ilhal_days;
									}
									$sql_insure .= ",ins_car ='".$ins_fee."'";
									$sql_insure .= ",ins_load ='".$lins_fee."'";

								}

								//DB 삽입
								$sql = "INSERT INTO tbl_delivery_fee SET tr_type = 'M', $sql_basic $sql_co_info $sql_unitprice $sql_bank $sql_basic_sudang $sql_basic_gongje $sql_ilhal";
								$this->cowork_model->db->query($sql);
								$df_id = $this->cowork_model->db->insert_id();

								//전월 미납건체크
								$pdate = $df_month . "-01";
								$pm=date("Y-m",strtotime($pdate.'-1month'));
								$not_paid = 0;
								$plv = $this->db->where('df_month', $pm)->where('dp_id', $dp_details->dp_id)->get('tbl_delivery_fee_levy')->row();
								if(!empty($plv->pay_status) && $plv->pay_status != 'B') { //납부전상태가 아니면
									$not_paid = $plv->balance_amount;
									// 기존 dp_id, df_month 가 저정되는데 특별한 이유 없어보여 뺏음
									$sql = "insert into tbl_delivery_fee_gongje set not_paid = '".$not_paid."', df_id='".$df_id."'";
									$this->cowork_model->db->query($sql);
								}

								//관리비 tbl_delivery_fee_fixmfee
								$sql = "INSERT INTO tbl_delivery_fee_fixmfee SET $sql_mf , df_id='$df_id'";
								$this->cowork_model->db->query($sql);

								//보험
								$sql = "INSERT INTO tbl_delivery_fee_insur SET df_id='$df_id', dp_id='".$dp_details->dp_id."', df_month='$df_month', ws_co_id = '".$dp_details->ws_co_id."'".$sql_insure;
								$this->cowork_model->db->query($sql);



								// 일할계산
								if($is_ilhal == "Y") {
									//echo $s_co_name.$sql_mf." <br/>"; //if(!empty($s_co_name)) echo $s_co_name;
								}
							}
							}
						} //-- 생성끝
					}
				}


		        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page

			} else if ($action == 'generate_all' || $action == 'generate_added') {
					//기존 데이터 삭제

					$this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
					$this->cowork_model->db->where('df_month',$data['df_month']);
					$this->cowork_model->db->where('gongje_req_co',$data['gongje_req_co']);
					$this->cowork_model->_order_by = 'df_id';
					$all_mfee_list = $this->cowork_model->get();
					if ($action == 'generate_all') { //전체 생성시에만 기존데이터 삭제
						if (!empty($all_mfee_list)) {
							foreach ($all_mfee_list as $mfee_info) {
								//tbl_delivery_fee_add
								$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
								$this->db->query($sql);
								//tbl_delivery_fee_fixmfee
								$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
								$this->db->query($sql);
								//tbl_delivery_fee_gongje
								$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
								$this->db->query($sql);
								//tbl_delivery_fee_insur
								$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
								$this->db->query($sql);
								//tbl_delivery_fee_refund_gongje
								$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
								$this->db->query($sql);
								//tbl_delivery_fee_levy
								$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
								$this->db->query($sql);

								$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
								$this->db->query($sql);
							}
						}
						$sql = "DELETE FROM tbl_delivery_fee WHERE gongje_req_co='".$data['gongje_req_co']."' and df_month='".$data['df_month']."'";
						$this->db->query($sql);
					}

                    if (!empty($data['all_dp_list'])) {
						foreach ($data['all_dp_list'] as $dp_details) {
							$generate_yn = "Y";
							$df_chk = $this->db->where('dp_id', $dp_details->dp_id)->where('df_month', $data['df_month'])->get('tbl_delivery_fee')->row();
							if(!empty($df_chk->dp_id)) $generate_yn = "N"; //기존 존재건은 패스하고 추가된 기사건만 생성(거래내역등)

							$rco = $this->db->select('co_name as rco_name, dp_id as rco_id')->where("( code = '".$dp_details->code."' and mb_type = 'customer')")->get("tbl_members ")->row();

							if($generate_yn == "Y") {
								// 계약기간 확인 N, O
								$inm = "";
								$outm = "";
								if(!empty($dp_details->N)) $inm = substr($dp_details->N,0,7);
								if(!empty($dp_details->O)) $outm = substr($dp_details->O,0,7);


								if((!empty($dp_details->N) && !empty($dp_details->O)) && ($df_month >= $inm && $df_month <= $outm)) {

								if(1) { //---------기존소스 이해안가는 부분, 나중에 원인 찾을것

									$tot_gongje_req = $tot_gongje = 0;
									$wsm_sum = $insur_sum = $gongje_sum = $rf_gongje_sum = 0;

									$sql_basic = ""; // 기본정보
									$sql_co_info = ""; // 회사들정보
									$sql_unitprice = ""; // 계약단가정보
									$sql_bank = ""; // 은행정보
									$sql_basic_sudang = ""; // 기본수당
									$sql_basic_gongje = ""; // 기본공제
									$sql_insure = ""; // 보험공제
									$sql_ilhal = "";

									$sql_mf = ""; // 관리비정보

									//일할계산처리
									/* 그리고 1일이 되었는 3일이 되었든 아님 10일지났든
									계약당월 이런경우 협회비 차고지비 보험료는 보험만료와 계약만료일 과 비교해서
									생성이 되도록 해야하는것 */
									$is_ilhal = "N";
									if($df_month == $inm || $df_month == $outm) {
										$ilhal_days = 0;
										$s = substr($dp_details->N,8,2);
										$e = substr($dp_details->O,8,2);
										if($df_month == $inm && $df_month == $outm) { // 당월 입.퇴사자
											$ilhal_days = (int)$e - (int)$s + 1;
										} else if($df_month == $inm) { // 당월 입사자
											$ilhal_days = $endDay - (int)$s + 1;
										} else if($df_month == $outm) { // 당월 퇴사자
											$ilhal_days = (int)$e;
										}
										$is_ilhal = "Y";
										$sql_ilhal = ", remark ='일할계산(".$ilhal_days."일)'";
									}

									//--------------------------------- 0. 기본정보
									//$sql_basic .= "ct_id ='".$ct_id."'";
									$sql_basic .= "dp_id ='".$dp_details->dp_id."'";
									$sql_basic .= ",userid ='".$dp_details->userid."'";
									$sql_basic .= ",df_month ='".$df_month."'";
									$sql_basic .= ",co_code ='".$dp_details->code."'";
									$sql_basic .= ",co_name ='".$dp_details->co_name."'";
									$sql_basic .= ",C ='".$dp_details->jiip_co."'";
									$sql_basic .= ",D ='".$rco->rco_name."'";
									$sql_basic .= ",E ='".$dp_details->driver."'";
									$sql_basic .= ",status ='대기'";
									if(!empty($dp_details->bs_number)) $sql_basic .= ",bs_number ='".$dp_details->bs_number."'"; // 사업자등록  *---------------------*
									else if(!empty($dp_details->reg_number)) $sql_basic .= ",bs_number ='".$dp_details->reg_number."'"; // 사업자등록없는경우 주민번호로 *---------------------*
									$sql_basic .= ",applytax_yn ='".$dp_details->tax_yn."'";

									$vat_acc_yn = 'N'; //부가세 누적관련
									$acc_vat_dmonth_cnt = 0;
									if($dp_details->acc_vat_yn == "Y") {
										$sql_tax2 = ",vat_acc_yn='Y'";
										$acc_vat_dmonth_cnt = $df_month;
									}
									$sql_basic .= ",vat_acc_yn ='".$vat_acc_yn."'";
									//부가세 누적 = acc_vat_paid_yn 부가세지급여부 acc_vat_month=부가세 누적개월
									$vv = $this->db->select('sum(acc_vat_dmonth) as avd, count(acc_vat_dmonth) as cnt')->where('dp_id', $dp_details->ws_co_id)->get('tbl_delivery_fee')->row();
									if(!empty($vv->avd)) $sql_basic .= ",acc_vat_sum='".$vv->avd."'";
									if(!empty($vv->cnt)) $sql_basic .= ",acc_vat_dmonth_cnt ='".$vv->cnt."'";


									//--------------------------------- 1. 공제청구사(위수탁관리사) 등 회사정보
									$gj_rq_co = $this->db->where('dp_id', $dp_details->ws_co_id)->get('tbl_members')->row();
									$r_co_id = $rco->rco_id;//실수요처
									$r_co_name = $rco->rco_name;//실수요처
									$s_co_name = $dp_details->driver;//운영공제(송금)사

									//과세여부
									$tax_yn = $dp_details->tax_yn;
									// 회사정보쿼리
									if(!empty($dp_details->ctr_id)) $sql_co_info .= ",tr_id='".$dp_details->ctr_id."'";
									if(!empty($dp_details->ws_co_id)) $sql_co_info .= ", gongje_req_co='".$dp_details->ws_co_id."'";
									//if(!empty($gj_rq_co->sm_co_id)) $sql_co_info .= ", sendmoney_co='".$gj_rq_co->sm_co_id."'"; //-> 송금사 저장문제 체크

									//--------------------------------- 2. 관리비정보
									//$sql_mf .= "ct_id ='".$ct_id."'";
									$sql_mf .= "dp_id ='".$dp_details->dp_id."'";
									$sql_mf .= ",df_month ='".$df_month."'";
									$sql_mf .= ",co_code ='".$dp_details->code."'";
									if(!empty($dp_details->ws_co_id)) $sql_mf .= ", ws_co_id='".$dp_details->ws_co_id."'";
									//$mf = $this->db->where('dp_id', $dp_details->dp_id)->get('tbl_scontract_mfee')->row(); //기본정보
									//if(!empty($mf->mfee_etc)) $sql_mf .= ",mfee_etc='".$mf->mfee_etc."'";
									$mf = $this->db->where('dp_id', $dp_details->dp_id)->where('master_yn', 'Y')->get('tbl_delivery_fee_fixmfee_set')->row(); //기본정보



									if(!empty($mf->wst_mfee)) $wst_mfee = $mf->wst_mfee; else $wst_mfee = 0;
									$wsm_sum += $wst_mfee;

									if(!empty($mf->mfee_vat)) { $mfee_vat = $mf->mfee_vat; $wsm_sum += $mf->mfee_vat; } else $mfee_vat = 0;
									if(!empty($mf->org_fee)) { $org_fee = $mf->org_fee; $wsm_sum += $mf->org_fee; } else $org_fee = 0;
									if(!empty($mf->grg_fee)) { $grg_fee = $mf->grg_fee; $wsm_sum += $mf->grg_fee; } else $grg_fee = 0;
									if(!empty($mf->grg_fee_vat)) { $grg_fee_vat = $mf->grg_fee_vat; $wsm_sum += $mf->grg_fee_vat; } else $grg_fee_vat = 0;
									$org_fee_vat = 0;
									$env_fee = 0;
									$env_fee_vat = 0;
									$car_tax = 0;
									$car_tax_vat = 0;
									$etc = 0;
									// 일할계산
									if($is_ilhal == "Y") {
										if($df_month == $inm || $df_month != $outm) {
											if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)(($wst_mfee/$endDay) * $ilhal_days);
											if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)(($mfee_vat/$endDay) * $ilhal_days);
										} else if($ilhal_days < 15) {
											if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)($wst_mfee/2);
											if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)($mfee_vat/2);
										}
									}
									$sql_mf .= ",wst_mfee='".$wst_mfee."'";
									$sql_mf .= ",mfee_vat='".$mfee_vat."'";
									$sql_mf .= ",org_fee='".$org_fee."'";
									$sql_mf .= ",grg_fee='".$grg_fee."'";
									$sql_mf .= ",grg_fee_vat='".$grg_fee_vat."'";

									//--------------------------------- 3. 은행정보
									if(!empty($dp_details->bank_code)) $sql_bank .= ",bank_code='".$dp_details->bank_code."'";
									if(!empty($dp_details->bank)) $sql_bank .= ",bank='".$dp_details->bank."'";
									if(!empty($dp_details->account_no)) $sql_bank .= ",account_no='".$dp_details->account_no."'";
									if(!empty($dp_details->account_name)) $sql_bank .= ",account_name='".$dp_details->account_name."'";
									if(!empty($dp_details->account_relation)) $sql_bank .= ",account_relation='".$dp_details->account_relation."'";

									//--------------------------------- 4. 계약단가정보
									if(!empty($ct_id)) {
										$cu = $this->db->where('dp_id', $dp_details->dp_id)->where('ct_id', $ct_id)->get('tbl_scontract_co')->row(); //기본정보
										if(!empty($cu->val1)) $sql_unitprice .= ",unit_price1='".$cu->val1."'";
										if(!empty($cu->val2)) $sql_unitprice .= ",unit_price2='".$cu->val2."'";
										if(!empty($cu->val3)) $sql_unitprice .= ",unit_price3='".$cu->val3."'";
										if(!empty($cu->val4)) $sql_unitprice .= ",unit_price4='".$cu->val4."'";
									}

									//--------------------------------- 5. 수당
									if(!empty($ct_id)) {
										$sd = $this->db->where('dp_id', $dp_details->dp_id)->where('ct_id', $ct_id)->get('tbl_delivery_fee_bsudang')->row(); //기본정보
										if((!empty($sd->sanje_yn) && !empty($sd->sanje_amount)) && $sd->sanje_yn == "Y") $sanje_amount = $sd->sanje_amount; else $sanje_amount = 0;

										if(!empty($sd->manager_pickup)) $sql_basic_sudang .= ",manager_pickup='".$sd->manager_pickup."'";
										if(!empty($sd->team_leader)) $sql_basic_sudang .= ",team_leader='".$sd->team_leader."'";
										if(!empty($sd->diffcult_area)) $sql_basic_sudang .= ",diffcult_area='".$sd->diffcult_area."'";
										if(!empty($sd->subside)) $sql_basic_sudang .= ",subside='".$sd->subside."'";
										if(!empty($sd->hoisik)) $sql_basic_sudang .= ",hoisik='".$sd->hoisik."'";
										if(!empty($sd->sudang_etc)) $sql_basic_sudang .= ",sudang_etc='".$sd->sudang_etc."'";
										if(!empty($sd->sanje_yn)) $sql_basic_sudang .= ",sanje_yn='".$sd->sanje_yn."'";
										$sql_basic_sudang .= ",sanje_amount='".$sanje_amount."'";
									}

									//--------------------------------- 6. 보험 tbl_asset_truck_insur_sub tr_id & pay_month  pay_amt
									if(!empty($dp_details->ctr_id)) {
										$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
										$qry .= " where a.type = '자동차' and b.gj_month = '".$df_month."' and (a.tr_id = '".$dp_details->ctr_id."') and a.co_pay<>'Y' and b.pay_by='01' and a.active_yn='Y'";
										$ins = $this->cowork_model->db->query($qry)->row();
										if(!empty($ins->pay_amt))
											$ins_fee = $ins->pay_amt;
										else
											$ins_fee =0;

										//if($dp_details->dp_id == "2656") echo $qry;
	//01 공제 > 미수와 미납에 동시등록 -> 회계등록
	//02 회사부담 > 미납에등록 -> 회계등록

										$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
										$qry .= " where a.type = '적재물' and b.gj_month = '".$df_month."' and a.tr_id = '".$dp_details->p_tr_id."' and a.active_yn='Y' and b.pay_by='01' ";
										$lins = $this->cowork_model->db->query($qry)->row();
										if(!empty($lins->pay_amt))
											$lins_fee = $lins->pay_amt;
										else
											$lins_fee =0;

										// 일할계산
										if($is_ilhal == "Y") {
											//if(!empty($ins_fee) && $ins_fee > 0) $ins_fee = ($ins_fee/$endDay) * $ilhal_days;
											//if(!empty($lins_fee) && $lins_fee > 0) $lins_fee = ($lins_fee/$endDay) * $ilhal_days;
										}
										$sql_insure .= ",ins_car ='".$ins_fee."'";
										$sql_insure .= ",ins_load ='".$lins_fee."'";

									}

									//DB 삽입
									$sql = "INSERT INTO tbl_delivery_fee SET tr_type = 'M', $sql_basic $sql_co_info $sql_unitprice $sql_bank $sql_basic_sudang $sql_basic_gongje $sql_ilhal";
									$this->cowork_model->db->query($sql);
									$df_id = $this->cowork_model->db->insert_id();

									//전월 미납건체크
									$pdate = $df_month . "-01";
									$pm=date("Y-m",strtotime($pdate.'-1month'));
									$not_paid = 0;
									$plv = $this->db->where('df_month', $pm)->where('dp_id', $dp_details->dp_id)->get('tbl_delivery_fee_levy')->row();
									if(!empty($plv->pay_status) && $plv->pay_status != 'B') { //납부전상태가 아니면
										$not_paid = $plv->balance_amount;
										$sql = "insert into tbl_delivery_fee_gongje set not_paid = '".$not_paid."', df_id='".$df_id."'";
										$this->cowork_model->db->query($sql);
									}

									//관리비 tbl_delivery_fee_fixmfee
									$sql = "INSERT INTO tbl_delivery_fee_fixmfee SET $sql_mf , df_id='$df_id'";
									$this->cowork_model->db->query($sql);

									//보험
									$sql = "INSERT INTO tbl_delivery_fee_insur SET df_id='$df_id', dp_id='".$dp_details->dp_id."', df_month='$df_month', ws_co_id = '".$dp_details->ws_co_id."'".$sql_insure;
									$this->cowork_model->db->query($sql);



									// 일할계산
									if($is_ilhal == "Y") {
										//echo $s_co_name.$sql_mf." <br/>"; //if(!empty($s_co_name)) echo $s_co_name;
									}
								}
							}
							}
						}
					}

		        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
			}

		} else {

      //해당 파트너 ---- 삭제할것 임시용
      $this->cowork_model->_order_by = 'dp.co_name ASC';
			$this->cowork_model->_table_name = ' tbl_members dp'; //table name
			$this->cowork_model->db->select('dp.*, tr.car_1, tr.ws_co_id as ws_co_id, tr.p_tr_id, tr.idx as ctr_id');
			$this->cowork_model->db->join('tbl_asset_truck tr', 'tr.inv_co_id = dp.dp_id', 'left');
			$this->cowork_model->db->where(" tr.ws_co_id = '".$data['gongje_req_co']."'")->where(" tr.car_status = 'A'");
			$this->cowork_model->_order_by = 'dp.ceo';
			$data['all_dp_list'] = $this->cowork_model->get();
      $data['total_count'] = count($data['all_dp_list']);



			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje', $data, true);
			$this->load->view('admin/_layout_main', $data);
		}
	}

    public function jiip_gongje_all($df_month = NULL, $gongje_req_co = NULL, $action = NULL, $id = NULL, $page = 1)
    {
		$data['dataTables'] = NULL;
		$data['page'] = $this->input->post('page', true); //$page;

        //receive form input by post
		if (!empty($gongje_req_co)) $data['gongje_req_co'] = $gongje_req_co;
        else $data['gongje_req_co'] = $this->input->post('gongje_req_co', true);
        //if (empty($data['gongje_req_co'])) $data['gongje_req_co'] = "1413";
        $data['gongje_req_co_name'] = $this->input->post('gongje_req_co_name', true);
		$data['search_field'] = $this->input->post('search_field', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
        $data['ws_mode'] = $this->input->post('ws_mode', true);
        $data['close_yn'] = $this->input->post('close_yn', true);

        if (!empty($df_month)) $data['df_month'] = $df_month;
        else $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->cowork_model->can_action('tbl_member', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

		$this->cowork_model->_table_name = ' tbl_delivery_fee df, tbl_members pt, tbl_asset_truck tr'; //table name
		$this->cowork_model->db->select('df.*');
		$this->cowork_model->db->where(" ( df.dp_id = pt.dp_id and df.tr_id = tr.idx) ");
        $this->cowork_model->db->where('df.df_month', $data['df_month'])->where("(df.tr_type = 'M' OR df.tr_type = 'MT' )");
		if(!empty($data['search_keyword'])) {
			$this->cowork_model->db->where("(pt.co_name like '%".$data['search_keyword']."%' OR pt.ceo like '%".$data['search_keyword']."%' OR pt.reg_number like '%".$data['search_keyword']."%' OR pt.bs_number like '%".$data['search_keyword']."%' OR pt.driver like '%".$data['search_keyword']."%' OR df.D like '%".$data['search_keyword']."%' OR tr.car_1 like '%".$data['search_keyword']."%' )");
		}
		$data['total_count'] = count($this->cowork_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

	    $data['title'] = '지입공제내역';

		$this->cowork_model->_table_name = ' tbl_delivery_fee df, tbl_members pt, tbl_asset_truck tr'; //table name
		$this->cowork_model->db->select('df.*, pt.co_name, pt.ceo, pt.bs_number, pt.N, pt.O, pt.ws_co_id, pt.driver, tr.car_1');
		$this->cowork_model->db->where(" ( df.dp_id = pt.dp_id and df.tr_id = tr.idx) ");
        $this->cowork_model->db->where('df.df_month', $data['df_month'])->where("(df.tr_type = 'M' OR df.tr_type = 'MT' )");
		if(!empty($data['search_keyword'])) { //D
			$this->cowork_model->db->where("(pt.co_name like '%".$data['search_keyword']."%' OR pt.ceo like '%".$data['search_keyword']."%' OR pt.reg_number like '%".$data['search_keyword']."%' OR pt.bs_number like '%".$data['search_keyword']."%' OR pt.driver like '%".$data['search_keyword']."%' OR df.D like '%".$data['search_keyword']."%' OR tr.car_1 like '%".$data['search_keyword']."%' )");
		}
// 임시
//$this->cowork_model->db->where("(pt.N like '%2019-02%' OR pt.O like '%2019-02%')");
        $this->cowork_model->_order_by = 'df.C';

		if ($action != 'excel_list') $this->cowork_model->db->limit($limit, $data['from_record']);
		$data['all_delivery_fee_info'] = $this->cowork_model->get();


        if ($action == 'print') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_print', $data, true);
			$this->load->view('admin/_layout_print', $data);
        } else if ($action == 'excel') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
        } else if ($action == 'excel_list') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_list_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
		} else {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_all', $data, true);
			$this->load->view('admin/_layout_main_nd', $data);
		}
	}

	//거래등록현황
    public function jiip_logis($df_month = NULL, $gongje_req_co = NULL, $action = NULL, $id = NULL)
    {
        $member_id = $id;
        //receive form input by post
        $data['gongje_req_co'] = $this->input->post('gongje_req_co', true);
        if (empty($data['gongje_req_co'])) $data['gongje_req_co'] = "1413";

        $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_member', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

	    $data['title'] = '지입공제내역';

        $this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
        $this->cowork_model->_order_by = 'C';
        $this->cowork_model->db->where('tbl_delivery_fee.df_month', '2018-09');
        $this->cowork_model->db->where('tbl_delivery_fee.gongje_req_co', $data['gongje_req_co']);

/*
		$this->cowork_model->db->select('tbl_delivery_fee.*', false);
        $this->cowork_model->db->select('tbl_members.*', false);
        $this->cowork_model->db->from('tbl_user_role');
        $this->cowork_model->db->join('tbl_menu', 'tbl_user_role.menu_id = tbl_menu.menu_id', 'left');
        $this->cowork_model->db->where('tbl_user_role.user_id', $user_id);
*/
        $data['all_delivery_fee_info'] = $this->cowork_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->basic_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->basic_model->get_permission('tbl_member');

        $data['all_designation_info'] = $this->basic_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/cowork/jiip_logis', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function jiip_transfer($action = NULL, $id = NULL)
    {
        $data['title'] = '지입송금완료';
          $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/cowork/jiip_transfer', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function mfee_reg($action = NULL, $id = NULL)
    {
        $data['title'] = '지입송금완료';
          $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/cowork/mfee_reg', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function jiip_gongje_rq($action = NULL, $id = NULL)
    {
        $data['title'] = '지입료공제입금요청';
          $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/cowork/jiip_gongje_rq', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function jiip_pay_finish($action = NULL, $id = NULL)
    {
        $data['title'] = '지입료수납완료';
          $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/cowork/jiip_pay_finish', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}


}
