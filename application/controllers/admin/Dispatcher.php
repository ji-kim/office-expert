<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dispatcher extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('dispatcher_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

    public function dispatcher()
    {
        $data['title'] = "관제"; //Page title

        $data['subview'] = $this->load->view('admin/dispatcher/dispatcher', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
	}

}
