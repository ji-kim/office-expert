<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testbed extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('announcements_model');
    }

    public function index(){
        $userids = '127-42-62053,372-11-00920';
        $userid = explode(",",$userids);

        $this->db->where_in('userid',$userid);
        $all_users = $this->db->get('tbl_members')->result();
        print_r($all_users);
    }

}
