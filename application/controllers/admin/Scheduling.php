<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class Scheduling extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('scheduling_model');
        $this->load->model('utilities_model');
        $this->load->helper('dompdf');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

	// 배차 기본 리스트
    public function scheduling($ct_id, $branch_id = NULL, $action = NULL, $alloc_date = NULL, $id = NULL)
    {
		$data['dataTables'] = "X";
		$data['alloc_date'] = (!empty($alloc_date))?$alloc_date:date("Y-m-d");
		$data['ct_id'] = $ct_id;
		if(!empty($branch_id))
			$data['cur_branch'] = $this->scheduling_model->get_branch_info($branch_id);
		else
			$data['cur_branch'] = $this->scheduling_model->get_first_branch($ct_id);


		$data['action'] = $action;
		$data['work_id'] = $id;
		$data['active'] = 1;
		if(!empty($data['cur_branch']->dp_id)) {
			//현재 선택된 지사 아이디
			$data['branch_id'] = $data['cur_branch']->dp_id;

			//지사목록
	//		$data['all_branch_group'] = $this->scheduling_model->get_branchs();
			$this->scheduling_model->db->select('a.*, b.dp_id, b.co_name, b.ceo');
			$this->scheduling_model->_table_name = 'tbl_contract_coops a'; //table name
			$this->scheduling_model->db->join('tbl_members b', 'a.customer_id = b.dp_id', 'left');
			$this->scheduling_model->_order_by = 'a.list_order ASC';
			$this->scheduling_model->db->where('a.ct_id', $ct_id);
			$data['all_branch_group'] = $this->scheduling_model->get();

			//
	//		$data['all_branch_list'] = $this->scheduling_model->get_branchs($data['branch_id']);
			// 일정목록 해당지사/날짜
			$data['all_work_list'] = $this->scheduling_model->get_work_list($data['ct_id'], $data['branch_id'], $data['alloc_date']);
			$data['total_work_count'] = count($data['all_work_list']);

			// all drivers
			$data['all_driver_list'] = $this->scheduling_model->get_drivers($ct_id, $data['branch_id']);
			$data['total_driver_count'] = count($data['all_driver_list']);
		}

		$data['title'] = '배차관리';

  		$data['subview'] = $this->load->view('admin/scheduling/scheduling', $data, true);
		$this->load->view('admin/_layout_main', $data);
    }

	// 배차 일정 등록
    public function set_schedule($ct_id, $branch_id = NULL, $alloc_date = NULL, $action, $work_id = NULL)
    {
		$data['alloc_date'] = (!empty($alloc_date))?$alloc_date:date("Y-m-d");
		$data['ct_id'] = $ct_id;
		$data['branch_id'] = $branch_id;

		if($action == "ADD") { //일정 추가
			$work = $this->db->select('max(qno) as max')->where('ct_id', $ct_id)->where('branch_id',$branch_id)->where('alloc_date',$alloc_date)->get("tbl_scheduling_works")->row();

			$sql = "INSERT INTO tbl_scheduling_works SET ";
			$sql .= "qno = '" . ($work->max + 1) . "'";
			$sql .= ",ct_id = '" . $ct_id . "'";
			$sql .= ",branch_id = '" . $branch_id . "'";
			$sql .= ",alloc_date = '" . $alloc_date . "'";
			$sql .= ",checker_id = '" . $this->input->post('checker_id',true) . "'";
			$sql .= ",driver_id = '" . $this->input->post('driver_id',true) . "'";
			$sql .= ",work_place = '" . $this->input->post('work_place',true) . "'";
			$sql .= ",address = '" . $this->input->post('address',true) . "'";
			$sql .= ",work_type = '" . $this->input->post('work_type',true) . "'";
			$sql .= ",work_from = '" . $this->input->post('work_from',true) . "'";
			$sql .= ",work_to = '" . $this->input->post('work_to',true) . "'";
			$sql .= ",work_cnt = '" . $this->input->post('work_cnt',true) . "'";
			$sql .= ",real_cnt = '" . $this->input->post('real_cnt',true) . "'";
			$sql .= ",cowork_cnt = '" . $this->input->post('cowork_cnt',true) . "'";
			$sql .= ",work_weight = '" . $this->input->post('work_weight',true) . "'";
			$sql .= ",work_kind = '" . $this->input->post('work_kind',true) . "'";
			$sql .= ",remark = '" . $this->input->post('remark',true) . "'";
			$sql .= ",insert_user = '" . $this->session->userdata('user_id') . "'";
			$this->db->query($sql);
		} else if($action == "UPDATE") { //일정 수정
			$sql = "UPDATE tbl_scheduling_works SET ";
			$sql .= "qno = '" . ($work->max + 1) . "'";
			$sql .= "checker_id = '" . $this->input->post('checker_id',true) . "'";
			$sql .= ",driver_id = '" . $this->input->post('driver_id',true) . "'";
			$sql .= ",work_place = '" . $this->input->post('work_place',true) . "'";
			$sql .= ",address = '" . $this->input->post('address',true) . "'";
			$sql .= ",work_type = '" . $this->input->post('work_type',true) . "'";
			$sql .= ",work_from = '" . $this->input->post('work_from',true) . "'";
			$sql .= ",work_to = '" . $this->input->post('work_to',true) . "'";
			$sql .= ",work_cnt = '" . $this->input->post('work_cnt',true) . "'";
			$sql .= ",real_cnt = '" . $this->input->post('real_cnt',true) . "'";
			$sql .= ",cowork_cnt = '" . $this->input->post('cowork_cnt',true) . "'";
			$sql .= ",work_weight = '" . $this->input->post('work_weight',true) . "'";
			$sql .= ",work_kind = '" . $this->input->post('work_kind',true) . "'";
			$sql .= ",remark = '" . $this->input->post('remark',true) . "'";
			$sql .= ",update_user = '" . $this->session->userdata('user_id') . "'";
			$sql .= " WHERE work_id = '" . $work_id . "'";
			$this->db->query($sql);
		} else if($action == "DELETE") { //일정 삭제
			$sql = "DELETE FROM tbl_scheduling_works WHERE work_id = '" . $work_id . "'";
			$this->db->query($sql);
		}

		$message = "추가되었습니다.";
		$type = 'success';
		set_message($type, $message);
		redirect('admin/scheduling/scheduling/'.$ct_id.'/'.$branch_id.'/list/'.$alloc_date); //redirect page
    }

	//운영설정  계약아이디, 설정구분, 액션, 거래처아이디, 검사원아이디(검사원설정)
    public function config($ct_id, $sub = NULL, $action = NULL, $id = NULL, $id2 = NULL)
    {
  		$dp_id = $id;
		$data['ct_id'] = $ct_id;
		$data['load_config'] = (!empty($sub))?$sub:"cowork";
		$data['action'] = $action;
		if(!empty($id))
			$data['cur_branch'] = $this->scheduling_model->get_branch_info($id);
		else
			$data['cur_branch'] = $this->scheduling_model->get_first_branch($ct_id);

		if(!empty($data['cur_branch']->dp_id))
			$data['branch_id'] = (!empty($id))?$id:$data['cur_branch']->dp_id;

		$data['title'] = '운영설정';
		// 공통
		//거래처목록
		$this->scheduling_model->db->select('a.*, b.dp_id, b.co_name, b.ceo');
		$this->scheduling_model->_table_name = 'tbl_contract_coops a'; //table name
		$this->scheduling_model->db->join('tbl_members b', 'a.customer_id = b.dp_id', 'left');
		$this->scheduling_model->_order_by = 'a.list_order ASC';
		$this->scheduling_model->db->where('a.ct_id', $ct_id);
		$data['all_branch_list'] = $this->scheduling_model->get();

		//거래단가설정
		if($data['load_config'] == "unitprice") { 
			$data['title'] .= " > 거래단가설정";
			
			if(!empty($data['branch_id'])) { //거래처가 등록되어있는 경우만
				// all drivers
				$data['all_driver_list'] = $this->scheduling_model->get_drivers($ct_id, $data['branch_id']);
				$data['total_driver_count'] = count($data['all_driver_list']);

				//거래항목 
				$data['all_item_list'] = $this->scheduling_model->get_items($data['ct_id']);
				$data['item_count'] = count($data['all_item_list']);
			}

		//거래처(지사) 등록관리
		} else if($data['load_config'] == "cowork") { 


			$data['coop_id'] = (!empty($id))?$id:"";
			if($action == "insert_cowork") { //거래처 신규 등록
				$cowork_info = $this->db->select('count(*) as count')->where('ct_id', $ct_id)->where('customer_id',$this->input->post('customer_id',true))->get("tbl_contract_coops")->row();

				if($cowork_info->count <= 0) {
					$sql = "INSERT INTO tbl_contract_coops SET ";
					$sql .= "ct_id = '" . $ct_id . "'";
					$sql .= ",customer_id = '" . $this->input->post('customer_id', true) . "'";
					$sql .= ",list_order = '" . $this->input->post('list_order', true) . "'";
					$this->db->query($sql);
					$message = "추가되었습니다.";
					$type = 'success';
				}else{
					$message = "이미 등록되어있는 거래처입니다..";
					$type = 'success';
				}
			  set_message($type, $message);
			  redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config']); //redirect page
			} else if($action == "update_cowork") { //거래처 변경
			  $sql = "UPDATE tbl_contract_coops SET ";
			  $sql .= "customer_id = '" . $this->input->post('customer_id', true) . "'";
			  $sql .= ",list_order = '" . $this->input->post('list_order', true) . "'";
			  $sql .= " WHERE coop_id = '".$data['coop_id']."'";
			  $this->db->query($sql);
			  $message = "변경되었습니다.";
			  $type = 'success';
			  set_message($type, $message);
			  redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config']); //redirect page
			} else if($action == "delete_cowork") { //거래처 삭제
			  $sql = "DELETE FROM tbl_contract_coops WHERE coop_id = '".$data['coop_id']."'";
			  $this->db->query($sql);
			  $message = "삭제되었습니다.";
			  $type = 'success';
			  set_message($type, $message);
			  redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config']); //redirect page
			}

		//검사원 등록관리
		} else if($data['load_config'] == "checker") { 
			if(!empty($data['branch_id'])) {
				$data['checker_id'] = (!empty($id2))?$id2:"";
				if($action == "insert_checker") { //검사원 신규 등록

					if(empty($data['checker_id'])) {
						$sql = "INSERT INTO tbl_contract_checker SET ";
						$sql .= "ct_id = '" . $ct_id . "'";
						$sql .= ",customer_id = '" . $data['branch_id'] . "'";
						$sql .= ",fullname = '" . $this->input->post('fullname', true) . "'";
						$sql .= ",username = '" . $this->input->post('username', true) . "'";
						$sql .= ",passwd = '" . $this->input->post('passwd', true) . "'";
						$sql .= ",phone = '" . $this->input->post('phone', true) . "'";
						$this->db->query($sql);
						$message = "추가되었습니다.";
						$type = 'success';
					}else{
						$message = "이미 등록되어있는 검사원입니다..";
						$type = 'error';
					}
				  set_message($type, $message);
				  redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config'].'/list/'.$data['branch_id']); //redirect page
				} else if($action == "update_checker") { //검사원 변경
				  $sql = "UPDATE tbl_contract_checker SET ";
				  $sql .= "customer_id = '" . $data['branch_id'] . "'";
				  $sql .= ",fullname = '" . $this->input->post('fullname', true) . "'";
				  $sql .= ",username = '" . $this->input->post('username', true) . "'";
				  $sql .= ",passwd = '" . $this->input->post('passwd', true) . "'";
				  $sql .= ",phone = '" . $this->input->post('phone', true) . "'";
				  $sql .= " WHERE checker_id = '".$data['checker_id']."'";
				  $this->db->query($sql);
				  $message = "변경되었습니다.";
				  $type = 'success';
				  set_message($type, $message);
				  redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config'].'/list/'.$data['branch_id']); //redirect page
				} else if($action == "delete_checker") { //검사원 삭제
				  $sql = "DELETE FROM tbl_contract_checker WHERE checker_id = '".$data['checker_id']."'";
				  $this->db->query($sql);
				  $message = "삭제되었습니다.";
				  $type = 'success';
				  set_message($type, $message);
				  redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config'].'/list/'.$data['branch_id']); //redirect page
				}

				// 검사원목록
				$data['all_checker_list'] = $this->scheduling_model->get_checkers($ct_id, $data['branch_id']);
				$data['total_checker_count'] = count($data['all_checker_list']);
			}

		//기사 등록관리
		} else if($data['load_config'] == "driver") { 
			if(!empty($data['branch_id'])) {
				$data['driver_id'] = (!empty($id2))?$id2:"";
				if($action == "insert_driver") { //기사 신규 등록

					if(empty($data['driver_id'])) {
						$sql = "INSERT INTO tbl_contract_partners SET ";
						$sql .= "ct_id = '" . $ct_id . "'";
						$sql .= ",customer_id = '" . $data['branch_id'] . "'";
						$sql .= ",partner_id = '" . $this->input->post('partner_id', true) . "'";
						$this->db->query($sql);

						//거래단가 레코드 등록
						$this->scheduling_model->_order_by = 'title ASC';
						$this->scheduling_model->_table_name = 'tbl_contract_items'; //table name
						$this->scheduling_model->db->where('ct_id', $ct_id);
						$all_items_group = $this->scheduling_model->get();

						if (!empty($all_items_group)) {
							foreach ($all_items_group as $item_info) {
								$sql = "INSERT INTO tbl_contract_partner_unitprice SET ";
								$sql .= "partner_id = '" . $this->input->post('partner_id', true) . "'";
								$sql .= ",item_id = '" . $item_info->item_id . "'";
								$sql .= ",unit_price = '0'";
								$this->db->query($sql);
							}
						}

						$message = "추가되었습니다.";
						$type = 'success';
					}else{
						$message = "이미 등록되어있는 검사원입니다..";
						$type = 'error';
					}
					set_message($type, $message);
					redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config'].'/list/'.$data['branch_id']); //redirect page
				} else if($action == "update_driver") { //기사 변경
					$sql = "UPDATE tbl_contract_partners SET ";
					$sql .= " partner_id = '" . $this->input->post('partner_id', true) . "'";
					$sql .= " WHERE contract_partner_id = '".$data['driver_id']."'";
					$this->db->query($sql);
					$message = "변경되었습니다.";
					$type = 'success';
					set_message($type, $message);
					redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config'].'/list/'.$data['branch_id']); //redirect page
				} else if($action == "delete_driver") { //기사 삭제
					$sql = "DELETE FROM tbl_contract_partners WHERE partner_id = '".$data['driver_id']."'";
					$this->db->query($sql);

					//단가정보 삭제
					$sql = "DELETE FROM tbl_contract_partner_unitprice WHERE partner_id = '".$data['driver_id']."'";
					$this->db->query($sql);

					$message = "삭제되었습니다.";
					$type = 'success';
					set_message($type, $message);
					redirect('admin/scheduling/config/'.$ct_id.'/'.$data['load_config'].'/list/'.$data['branch_id']); //redirect page
				} else if($action == "set_all_unitprice") { //단가 일괄적용
				}

				// 검사원목록
				$data['all_driver_list'] = $this->scheduling_model->get_drivers($ct_id, $data['branch_id']);
				$data['total_driver_count'] = count($data['all_driver_list']);
			}

		}

  		$data['subview'] = $this->load->view('admin/scheduling/config', $data, true);
		$this->load->view('admin/_layout_main', $data);
    }

	//환경설정 단가 저장(ajax로 변경처리 필요)
	public function update_unitprice($field_name,$field_id,$field_value)
    {
		if(!empty($field_value)) {
			$sql = "UPDATE tbl_contract_partner_unitprice SET ";
			$sql .= $field_name." ='".$field_value."'";
			$sql .= " WHERE 	unitprice_id = '".$field_id."'";
			$this->db->query($sql);
		}

		$message = "저장되었습니다.";
        $type = 'success';
            
        set_message($type, $message);
	}

	//단가 일괄 저장
    public function set_unitprice($ct_id, $branch_id)
    {
        $data['title'] = "단가 일괄 저장";
		$data['ct_id'] = $ct_id;
		$data['branch_id'] = $branch_id;

		//거래항목 
		$data['all_item_list'] = $this->scheduling_model->get_items($data['ct_id']);
		$data['item_count'] = count($data['all_item_list']);
		
		$data['modal_subview'] = $this->load->view('admin/scheduling/_modal_unitprice', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }


    //검사원 선택창
    public function select_checker($ct_id, $branch_id,  $no=NULL)
    {
      $data['title'] = '검사원 선택';
      $data['ct_id'] = $ct_id;
      $data['no'] = $no;

	  // 검사원목록
	  $data['all_checker_list'] = $this->scheduling_model->get_checkers($ct_id, $branch_id);
	  $data['total_checker_count'] = count($data['all_checker_list']);

      $data['subview'] = $this->load->view('admin/scheduling/pop_select_checker', $data, true);
      $this->load->view('admin/_layout_pop_common', $data);
    }

    //기사 선택창
    public function select_driver($ct_id, $branch_id,  $no=NULL)
    {
      $data['title'] = '기사 선택';
      $data['ct_id'] = $ct_id;
      $data['no'] = $no;

	  // 검사원목록
	  $data['all_driver_list'] = $this->scheduling_model->get_drivers($ct_id, $branch_id);
	  $data['total_driver_count'] = count($data['all_driver_list']);

      $data['subview'] = $this->load->view('admin/scheduling/pop_select_driver', $data, true);
      $this->load->view('admin/_layout_pop_common', $data);
    }


    public function work_result($ct_id, $alloc_date=NULL, $id=NULL)
    {
      $data['title'] = "배차실적";
    	$data['alloc_date'] = $alloc_date;
    	if(empty($data['alloc_date'])) $data['alloc_date'] = date('Y-m-d');
    	$data['branch_id'] = $id;
    	if(empty($id)) {
    		$data['branch_id'] = $this->scheduling_model->get_first_branch();
    	}
      $data['active'] = 2;

    	//지사목록
      $data['all_branch_group'] = $this->scheduling_model->get_branchs();
      $data['all_branch_list'] = $this->scheduling_model->get_branchs($data['branch_id']);

    	// all drivers
    	$data['all_driver_list'] = $this->scheduling_model->get_drivers($ct_id, $data['branch_id']);
    	$data['total_driver_count'] = count($data['all_driver_list']);

      $data['subview'] = $this->load->view('admin/scheduling/work_result', $data, TRUE);
      $this->load->view('admin/_layout_main', $data);
    }

    public function work_stats($ct_id, $alloc_date=NULL, $id=NULL)
    {
      $data['title'] = "배차통계";
    	$data['alloc_date'] = $alloc_date;
    	if(empty($data['alloc_date'])) $data['alloc_date'] = date('Y-m-d');
    	$data['branch_id'] = $id;
    	if(empty($id)) {
    		$data['branch_id'] = $this->scheduling_model->get_first_branch();
    	}
      $data['active'] = 3;

    	//지사목록
      $data['all_branch_group'] = $this->scheduling_model->get_branchs();
      $data['all_branch_list'] = $this->scheduling_model->get_branchs($data['branch_id']);

    	// all drivers
    	$data['all_driver_list'] = $this->scheduling_model->get_drivers($ct_id, $data['branch_id']);
    	$data['total_driver_count'] = count($data['all_driver_list']);

      $data['subview'] = $this->load->view('admin/scheduling/work_stats', $data, TRUE);
      $this->load->view('admin/_layout_main', $data);
    }

}
