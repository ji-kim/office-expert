<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: ���� 2:20
 */

class Notice extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
        $this->load->model('expert/Notice_model');
    }

    public function notice_list()
    {
        // �α��� ������ ������ �α��� ��������
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //ȸ������
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //��������
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //����Ź������
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //��������
        }

        $data['list'] = $this->Notice_model->noticeList();
        $data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/notice/list', $data);
        $this->load->view('expert/tail');
    }

    public function view($id)
    {
        // �α��� ������ ������ �α��� ��������
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //ȸ������
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //��������
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //����Ź������
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //��������
        }

        $data['data'] = $this->Notice_model->noticeView($id);
        $data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/notice/view', $data);
        $this->load->view('expert/tail');
    }

}
