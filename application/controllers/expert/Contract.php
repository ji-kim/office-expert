<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Contract extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
        $this->load->model('expert/Contract_model');
    }

    public function index()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //차량정보
		if(!empty($data['truck']['ws_co_id'])) $data['wsco']	= $this->Member_model->member_by_id($data['truck']['ws_co_id']); //위수탁사


        if(@$data['wsco'] == null){

            echo '<script>alert("계약을 진행할 위수탁사가 없습니다.");history.back(-1);</script>';
        }else {
            $mf = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
            if (isset($mf['ct_id'])) {
                $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //계약정보
            }

            $data['econtract'] = $this->Contract_model->getContract($userid);

            $data['member'] = $member;
            $this->load->view('expert/header');
            $this->load->view('expert/menutop', $data);
            $this->load->view('expert/contract', $data);
            $this->load->view('expert/tail');
        }
    }

	public function sign()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //차량정보
        if(!empty($data['truck']['ws_co_id'])) $data['wsco']	= $this->Member_model->member_by_id($data['truck']['ws_co_id']); //위수탁사
		$data['member'] = $member;

        if(@$data['wsco'] == null){

            echo '<script>alert("계약을 진행할 위수탁사가 없습니다.");history.back(-1);</script>';
        }else {

            $this->load->view('expert/header');
            $this->load->view('expert/menutop', $data);
            $this->load->view('expert/contract_sign', $data);
            $this->load->view('expert/tail');
        }
    }

	public function signAction()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
            $dp_id = $this->session->dp_id;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
		$data['member'] = $member;

		$insertData = array();
		$insertData['econtract_legal'] = $data['truck'] != null ? $data['truck']['ws_co_id'] : "위수탁관리" ;
		$insertData['userid'] = $userid;
        $insertData['dp_id'] = $dp_id;
		$insertData['contract_date'] = $this->input->post("contract_date_y")."-".$this->input->post("contract_date_m")."-".$this->input->post("contract_date_d");

		if($member['O'] != ""){
			$insertData['contract_start_date'] = date("Y-m-d",strtotime("+1 day",strtotime($member['O'])));
		}else{
			$insertData['contract_start_date'] = $insertData['contract_date'];
		}
		$insertData['contract_end_date'] = date("Y-m-d",strtotime("+1 year",strtotime($insertData['contract_start_date'])));

		//서명 이미지를 담는다.
		$sign_uri = $this->input->post("contract_sign");
		$encoded_image = explode(",", $sign_uri)[1];
		$decoded_image = base64_decode($encoded_image);
		$filename = "uploads/contract_sign/".$userid."_".$insertData['contract_date']."_".time().".png";

		file_put_contents($filename, $decoded_image);
		$insertData['contract_sign'] = "/".$filename;
		$insertData['contract_state'] = "W";



		$this->Contract_model->signWrite($insertData);

		redirect('expert/contract');
    }

}
