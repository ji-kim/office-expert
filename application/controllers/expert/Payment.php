<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Payment extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
        $this->load->model('expert/Payment_model');
    }

    public function balance()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //차량정보
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //계약정보
		}

		$data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/payment/balance', $data);
        $this->load->view('expert/tail');
    }

    public function history()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //계약정보
		}

		$data['member'] = $member;

		$data['history'] = $this->Payment_model->history();
		/*$data['history'] = array();
		for($i=0; $i<rand(4,9); $i++){
			$historyData = array("date" => "2019.01.".rand(1,31)
								,"amount" => number_format(rand(190000,899200))
								,"paytype" => "전자결제"
								,"etc" => "비고");
			array_push($data['history'],$historyData);
		}*/


        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/payment/history', $data);
        $this->load->view('expert/tail');
    }

	public function taxbill()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //계약정보
		}

		$data['history'] = $this->Payment_model->taxbill();

		$data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/payment/taxbill', $data);
        $this->load->view('expert/tail');
    }


}
