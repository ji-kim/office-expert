<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Design extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();        
    }

    public function login()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/login');
        $this->load->view('expert/design/tail');
    }
   
	public function invoice()
    {        
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/menutop');
        $this->load->view('expert/design/invoice');
        $this->load->view('expert/design/tail');
    }
	
	public function receipt()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/menutop');
        $this->load->view('expert/design/receipt');
        $this->load->view('expert/design/tail');
    }
	
	public function alloc()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/menutop');
        $this->load->view('expert/design/alloc');
        $this->load->view('expert/design/tail');
    }
	
	public function car()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/menutop');
        $this->load->view('expert/design/car');
        $this->load->view('expert/design/tail');
    }
	
	public function contract()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/menutop');
        $this->load->view('expert/design/contract');
        $this->load->view('expert/design/tail');
    }
	
	public function ins_isp()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/menutop');
        $this->load->view('expert/design/ins_isp');
        $this->load->view('expert/design/tail');
    }

	public function pay_balance()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/menutop');
        $this->load->view('expert/design/pay_balance');
        $this->load->view('expert/design/tail');
    }
	
	public function pay_history()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/menutop');
        $this->load->view('expert/design/pay_history');
        $this->load->view('expert/design/tail');
    }

    public function update()
    {
        $this->load->view('expert/design/header');
        $this->load->view('expert/design/update');
        $this->load->view('expert/design/tail');
    }
	
	
}