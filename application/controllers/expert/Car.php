<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: ���� 2:20
 */

class Car extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
    }

    public function car_info()
    {
        // �α��� ������ ������ �α��� ��������
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //ȸ������
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //��������
		if(!empty($data['truck']['ws_co_id'])) $data['wsco']	= $this->Member_model->member_by_id($data['truck']['ws_co_id']); //����Ź��

		$data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/car_info', $data);
        $this->load->view('expert/tail');
    }

}
