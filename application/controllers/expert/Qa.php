<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: ���� 2:20
 */

class Qa extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
		$this->load->model('expert/Qa_model');
    }

    public function qa_list()
    {
        // �α��� ������ ������ �α��� ��������
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //ȸ������
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //��������
		$data['list']	= $this->Qa_model->qnaList();
		$data['member'] = $member;


        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/qa_list', $data);
        $this->load->view('expert/tail');
    }

	public function view($index)
    {
        // �α��� ������ ������ �α��� ��������
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //ȸ������
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //��������
		$data['data']	= $this->Qa_model->qnaView($index);
		$data['member'] = $member;


        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/qa_view', $data);
        $this->load->view('expert/tail');
    }

	public function write()
    {
        // �α��� ������ ������ �α��� ��������
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //ȸ������
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //��������
		$data['member'] = $member;


        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/qa_write', $data);
        $this->load->view('expert/tail');
    }

	public function writeaction()
    {
        // �α��� ������ ������ �α��� ��������
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member = $this->Member_model->member($userid); //ȸ������
		$this->Qa_model->qnaWrite();
        redirect('/expert/qa');
    }

}
