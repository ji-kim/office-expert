<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Invoice extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
    }

    public function invoice($month=NULL)
    {


        $HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
        $MobileArray  = array("iphone","lgtelecom","skt","mobile","samsung","nokia","blackberry","android","android","sony","phone","1.0.2","1.0.3");

        $checkCount = 0;
        for($i=0; $i<sizeof($MobileArray); $i++){
            if(preg_match("/$MobileArray[$i]/", strtolower($HTTP_USER_AGENT))){ $checkCount++; break; }
        }
        $mode = ($checkCount >= 1) ? "Mobile" : "Computer";

        //자동 업데이트 처리 소스
        /*if($mode == "Mobile" &&  !strstr($HTTP_USER_AGENT,"|APP/1.4") && !strstr(strtolower($HTTP_USER_AGENT),"iphone") ){
            redirect('/expert/update/autoupdate');
        }*/
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['total']['price']	= $data['total']['sub'] = $data['total']['vat'] = 0;
        //지급 - 공급가, 부가세 , 총액
        $data['total']['tr_fee_supp'] = $data['total']['tr_fee_vat'] = $data['total']['tr_fee_tot'] = 0;
        //공제 - 공급가, 부가세 , 총액
        $data['total']['mf_fee_supp'] = $data['total']['mf_fee_vat'] = $data['total']['mf_fee_tot'] = 0;
		      $data['total']['total_balance'] = 0; //총 정산액
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //차량정보
        if(!empty($data['truck']['ws_co_id'])) $data['wsco']	= $this->Member_model->member_by_id($data['truck']['ws_co_id']); //위수탁사
        if(!empty($data['truck']['baecha_co_id'])) $data['baecha_co']	= $this->Member_model->member_by_code($data['truck']['baecha_co_id']); //배차

        // 로그인 페이지에서 넘어온 거면 출근처리를 위한 값 셋팅
        if($month == "login") {
            $month = NULL;
            if(isset($data['truck']['car_1'])) {
                $data['applogin'] = TRUE;
            }
        }

        // 월 정보가 없으면 이전 월로
        if(empty($month)) {
            $month = prev_month();
        }

        $data['member'] = $member;
        $data['month']  = $month;
        $data['mf']     = $this->Delivery_model->delivery_fee($member['userid'], $month); //위수탁관리비
        $data['bank']   = $this->Delivery_model->bank_info($member['pay_account_no']); //은행정보
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($data['mf']['ct_id']); //계약정보
		}

        // 계약정보가 없으면 is_contract NULL값 추가
        $data['ct']['is_contract'] = (isset($data['ct']['is_contract']))?$data['ct']['is_contract']:NULL;
        $data['mf']['tr_type'] = (isset($data['mf']['tr_type']))?$data['mf']['tr_type']:NULL;

        //거래정산자 또는 관리비 납부 및 거래정산자일 경우 거래정산내역
	    $data['tr_settings'] = array();
        if ($data['mf']['tr_type']=='T' || $data['mf']['tr_type']=='MT') {
			$data['tr_settings']['fee_datas'] = "";
			$data['tr_settings']['extra_datas'] = "";

			// 1. 거래량정보
			$this->Delivery_model->db->select('title, sum');
			$this->Delivery_model->_table_name = 'tbl_contract_detail';
			$this->Delivery_model->db->where('depth', '1')->where('ct_id', $data['mf']['ct_id']);
			$this->Delivery_model->_order_by = 'ct_id asc';
			$all_stransaction_items = $this->Delivery_model->get();

			$subtotal_amt = $subtotal_fee = $total_fee = 0;
			// 총액 계산을 위해 사전계산 -> DB구조 문제 있음 향후 변경필요
			$scnt = 0;
			if (!empty($all_stransaction_items)) {
				foreach ($all_stransaction_items as $stransaction_item) {
					$scnt++;
					//Table 구조 잘못됨 향후 변경 필요
					$unitprice_info = $this->Delivery_model->unitprice_info($member['dp_id'], $data['mf']['ct_id']); //단가정보

					if($scnt == 1) { $tr_amount = $data['mf']['G']; $unitprice = $unitprice_info['val1']; }
					else if($scnt == 2) { $tr_amount = $data['mf']['H']; $unitprice = $unitprice_info['val2']; }
					else if($scnt == 3) { $tr_amount = $data['mf']['I']; $unitprice = $unitprice_info['val3']; }
					else if($scnt == 4) { $tr_amount = $data['mf']['AD']; $unitprice = $unitprice_info['val4']; }

					$tr_fee = $tr_amount * $unitprice;
					$subtotal_amt += $tr_amount;
					$subtotal_fee += $tr_fee;
					$data['tr_settings']['fee_datas'] .= "<tr>";
					$data['tr_settings']['fee_datas'] .= "	<td>". $stransaction_item->title ."</td>";
					$data['tr_settings']['fee_datas'] .= "	<td>". char_num($unitprice) ."</td>";
					$data['tr_settings']['fee_datas'] .= "	<td>". char_num($tr_amount)  ."</td>";
					$data['tr_settings']['fee_datas'] .= "	<td>". char_num($tr_fee)  ."</td>";
					$data['tr_settings']['fee_datas'] .= "</tr>";
				}

				$total_fee += $subtotal_fee;
				$data['tr_settings']['fee_datas'] .= "<tr>";
				$data['tr_settings']['fee_datas'] .= "	<td class=\"red\">소계</td>";
				$data['tr_settings']['fee_datas'] .= "	<td class=\"subTotalBg\"> </td>";
				$data['tr_settings']['fee_datas'] .= "	<td class=\"subTotalBg\">". char_num($subtotal_amt)  ."</td>";
				$data['tr_settings']['fee_datas'] .= "	<td class=\"subTotalBg\">". char_num($subtotal_fee)  ."</td>";
				$data['tr_settings']['fee_datas'] .= "</tr>";
			}

			//2. 기본수당
      $data['mf']['total_sudang']	= $data['mf']['manager_pickup'] + $data['mf']['team_leader'] + $data['mf']['diffcult_area'] + $data['mf']['subside'] + $data['mf']['hoisik'] + $data['mf']['sudang_etc'];
			$total_fee += $data['mf']['hoisik'];

      //거래액 부가세 분리
      $data['total']['tr_fee_vat'] = round($subtotal_fee / 11,0);
      $data['total']['tr_fee_supp'] = $subtotal_fee - $data['total']['tr_fee_vat'];
      $data['total']['tr_fee_tot'] = $subtotal_fee + $data['mf']['total_sudang'];

			//3. 추가수당
			/*
			$this->Delivery_model->_table_name = 'tbl_settings_sg'; //table name
			$this->Delivery_model->db->where('tbl_settings_sg.add_type', 'S');
			$this->Delivery_model->_order_by = 'dp_order asc';
			$all_sudang_group = $this->Delivery_model->get();
			$extra_amt = $subtotal_extra = 0;
 			if (!empty($all_sudang_group)) {
				foreach ($all_sudang_group as $sudang_item) {
					$scnt++;

					$tr_fee = $tr_amount * $stransaction_item->sum;
					$subtotal_amt += $tr_amount;
					$subtotal_extra += $tr_fee;
					$data['tr_settings']['extra_datas'] .= "<tr>";
					$data['tr_settings']['extra_datas'] .= "	<td>". $sudang_item->title ."</td>";
					$data['tr_settings']['extra_datas'] .= "	<td>". char_num($stransaction_item->sum) ."</td>";
					$data['tr_settings']['extra_datas'] .= "</tr>";
				}

				$total_fee += $subtotal_extra;
				$data['tr_settings']['extra_datas'] .= "<tr>";
				$data['tr_settings']['extra_datas'] .= "	<td class=\"red\">소계</td>";
				$data['tr_settings']['extra_datas'] .= "	<td class=\"subTotalBg\">". char_num($subtotal_extra)  ."</td>";
				$data['tr_settings']['extra_datas'] .= "</tr>";
			}
			*/
       }

       //관리비 납부자 또는 관리비 납부 및 거래정산자일 경우 관리비내역
        if ($data['mf']['tr_type']=='M' || $data['mf']['tr_type']=='MT') {
            if(isset($data['mf']['df_id'])) {
                $df_id = $data['mf']['df_id'];

                $data['wsm_info'] = $this->Delivery_model->delivery_fee_fix($df_id); //기본공제
                if(isset($data['wsm_info'])) {
                    // 기본공제 합계
                    $data['wsm_info']['total_amount'] = (int)$data['wsm_info']['wst_mfee'] + (int)$data['wsm_info']['org_fee'] + (int)$data['wsm_info']['grg_fee'] + (int)$data['wsm_info']['etc'];
                    $data['wsm_info']['total_vat'] = (int)$data['wsm_info']['mfee_vat'] + (int)$data['wsm_info']['grg_fee_vat'];
                    $data['wsm_info']['total_price'] = $data['wsm_info']['total_amount'] + $data['wsm_info']['total_vat'];

                    $data['total']['mf_fee_supp']	+= $data['wsm_info']['total_amount'];
                    $data['total']['mf_fee_vat']	+= $data['wsm_info']['total_vat'];
                }

                $data['gongje_info'] = $this->Delivery_model->delivery_fee_gongje($df_id); //일반공제

                if(isset($data['gongje_info'])) {
                    //과태료 추가합산
                    $fine_sum = $this->db->select('sum(amount) as sum')->where('dp_id', $member['dp_id'])->where('df_month', $data['mf']['df_month'])->get("tbl_delivery_fee_gongje_finefee")->row();
                    if(!empty($fine_sum->sum) && $fine_sum->sum > 0) { // 기존버전과 호환 유지
                        $data['gongje_info']['fine_fee'] += $fine_sum->sum;
                    }

                    // 일반공제 합계
                    $data['gongje_info']['total_amount'] = (int)$data['gongje_info']['fine_fee'] + (int)$data['gongje_info']['not_paid'] + (int)$data['gongje_info']['car_tax'] + (int)$data['gongje_info']['env_fee'] + (int)$data['gongje_info']['grg_fee'];
                    $data['gongje_info']['total_vat'] = (int)$data['gongje_info']['grg_fee_vat'];
                    $data['gongje_info']['total_price'] = $data['gongje_info']['total_amount'] + $data['gongje_info']['total_vat'];

                    $data['total']['mf_fee_supp']	+= $data['gongje_info']['total_amount'];
                    $data['total']['mf_fee_vat']	+= $data['gongje_info']['total_vat'];
                }



                $data['insur_info'] = $this->Delivery_model->delivery_fee_insur($df_id); //각종보험공제
                if(isset($data['insur_info'])) {
                    // 각종보험공제 합계
                    $data['insur_info']['total_amount'] = (int)$data['insur_info']['ins_car'] + (int)$data['insur_info']['ins_load'];
                    $data['insur_info']['total_vat'] = 0;
                    $data['insur_info']['total_price'] = $data['insur_info']['total_amount'] + $data['insur_info']['total_vat'];

                    $data['total']['mf_fee_supp']	+= $data['insur_info']['total_amount'];
                    $data['total']['mf_fee_vat']	+= $data['insur_info']['total_vat'];
                }

                $data['rf_gongje_info'] = $this->Delivery_model->delivery_fee_refund($df_id); //환급형공제
                if(isset($data['rf_gongje_info'])) {
                    // 환급형공제 합계
                    $data['rf_gongje_info']['total_amount'] = (int)$data['rf_gongje_info']['gj_termination_mortgage'];
                    $data['rf_gongje_info']['total_vat'] = 0;
                    $data['rf_gongje_info']['total_price'] = $data['rf_gongje_info']['total_amount'] + $data['rf_gongje_info']['total_vat'];

                    $data['total']['mf_fee_supp']	+= $data['insur_info']['rf_gongje_info'];
                    //$data['total']['vat']	= $data['total']['vat'];
                }
            }
        }

        // 지급총액
        $data['total']['tr_fee_tot'] = $data['total']['tr_fee_vat'] + $data['total']['tr_fee_supp'];
        // 청구총액
        $data['total']['mf_fee_tot']	= $data['total']['mf_fee_supp'] + $data['total']['mf_fee_vat'];
        //최종 부과&지급액
        $data['total']['total_balance']	= $data['total']['tr_fee_tot'] - $data['total']['mf_fee_tot'];

		// 정산액에 따른 금액노출(청구,수수료)
		$data['mf']['total_balance_view'] = $data['title'] = "";
		$data['mf']['balance_type'] = "1"; //1. 청구 , 2.지급

    if ($data['mf']['tr_type']=='M') { //관리비만 부과
			$data['title'] = "관리비 청구서";
			//$data['total']['total_balance'] = $data['total']['price'];
			$data['mf']['total_balance_view'] = "<div class=\"boxText\"><font style=\"font-size:22px;\">청구액 : </font>".char_num($data['total']['total_balance']*-1)."<span>원</span></div>";
		} else if($data['mf']['tr_type']=='MT') { // 관리비 부과 & 거래 등록
			$data['title'] = "수수료 & 관리비 정산서";
			//$data['total']['total_balance'] = $total_fee - $data['total']['price'];
			if($data['total']['total_balance'] > 0) {
				$data['mf']['balance_type'] = "2";
				$data['mf']['total_balance_view'] = "<div class=\"boxText2\"><font style=\"font-size:22px;\">지급액 : </font>".char_num($data['total']['total_balance'])."<span>원</span></div>";
				$data['total']['sub'] = $data['total']['total_balance'] - $data['total']['vat'];
			} else
				$data['mf']['total_balance_view'] = "<div class=\"boxText2\"><font style=\"font-size:22px;\">청구액 : </font>".char_num($data['total']['total_balance']*-1)."<span>원</span></div>";
		} else if($data['mf']['tr_type']=='T') { //거래만 등록
			$data['title'] = "수수료 지급명세서";
			//$data['total']['total_balance'] = $total_fee;
			$data['mf']['balance_type'] = "2";
			$data['mf']['total_balance_view'] = "<div class=\"boxText2\"><font style=\"font-size:22px;\">지급액 : </font>".char_num($data['total']['total_balance'])."<span>원</span></div>";
		}

		// 최근 수납정보
    if(!empty($data['mf']['df_id'])) {
        $data['last_levy']	=  $this->Delivery_model->get_last_levy($data['mf']['df_id']); //;
        $data['last_levy_log']	=  $this->Delivery_model->get_last_levy_log($data['mf']['df_id']); //;
      }

        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/invoice', $data);
        $this->load->view('expert/tail', $data);
    }

    public function receipt($month=NULL)
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        if(empty($month)) {
            $month = prev_month();
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['member'] = $member;
        $data['month']  = $month;
        $data['bank']   = $this->Delivery_model->bank_info($member['pay_account_no']); //은행정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //차량정보

        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/receipt', $data);
        //if($data['ct']['is_contract']=='N') {
        //    $this->load->view('expert/_receipt_02',$data);
        //} else {
            $this->load->view('expert/_receipt_01',$data);
        //}
        $this->load->view('expert/tail');
    }

    public function finefee($month=NULL){
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member = $this->Member_model->member($userid); //회원정보
        $data['member'] = $this->Member_model->member($userid); //회원정보
        $data['mf'] = $this->Delivery_model->delivery_fee($member['userid'], $month); //위수탁관리비
        $data['truck']	= $this->Delivery_model->asset_truck($member['dp_id']); //차량정보
        if(!empty($data['truck']['ws_co_id'])) $data['wsco']	= $this->Member_model->member_by_id($data['truck']['ws_co_id']); //위수탁사
        if(!empty($data['truck']['baecha_co_id'])) $data['baecha_co']	= $this->Member_model->member_by_code($data['truck']['baecha_co_id']); //배차
        $data['fine'] = $this->db->select('*')->where('dp_id', $member['dp_id'])->where('df_month', $data['mf']['df_month'])->get("tbl_delivery_fee_gongje_finefee")->result();

        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/finefee', $data);
        $this->load->view('expert/tail', $data);
    }
}
