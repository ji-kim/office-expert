<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: ���� 2:20
 */

class Update extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
    }

    public function autoupdate()
    {
        $this->load->view('expert/update');
    }

}