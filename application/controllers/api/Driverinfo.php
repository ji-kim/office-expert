<?php
class Driverinfo extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');

    }

    // 차량 데이터 가져오기
    public function driverInfo($userid) {
        $userid = str_replace("-","",$userid);
        $member = $this->Member_model->member($userid); //회원정보

        $truck	= $this->Delivery_model->asset_truck($member['dp_id']); //차량정보


        $data['companycode'] = explode(".",$_SERVER['HTTP_HOST']);
        $data['companycode'] = $data['companycode'][0];
        $data['driver'] = $member['driver'];
        $data['co_name'] = $truck['ws_co_name'];
        $data['type']   = $truck['type'];

        echo json_encode($data);

    }
}