<?php

class Mong extends CI_Controller
{

    private static $DEFAULT_TIME_ZONE = "Asia/Seoul";

    public function __construct() {
        parent::__construct();
        if (!$this->checkAsiaTimezone())
            date_default_timezone_set(self::$DEFAULT_TIME_ZONE);

        header("Access-Control-Allow-Origin: " . $_SERVER["HTTP_REFERER"]);
        header("Access-Control-Allow-Credentials: true");
        $this->load->model('Mongo_model');
    }

    // 차량 운행 경로 데이터
    public function gpsLog() {
        //get 데이터 받기
        $carnum     = $this->input->get("carnum");
        $start_date = $this->input->get('start_date');
        $end_date   = $this->input->get('end_date');
        $inout      = $this->input->get('inout');

        if ($this->isNull($start_date)) $start_date = date('Y-m-d 00:00:00');
        if ($this->isNull($end_date)) $end_date = date('Y-m-d 23:59:59');
        if ($this->isNull($inout)) $inout = "all";

        try {
            $start_date = new DateTime($start_date);
            $end_date = new DateTime($end_date);
        } catch (Exception $e) {
            $res = new stdClass();
            $res->message = "date format is ISO DateTime";
            $res->status = 400;
            http_response_code($res->status);
            echo json_encode($res);
            return;
        }

        if ($this->isNull($carnum)) {
            $model = new stdClass();
            $model->message = 'carnum is required';
            echo json_encode($model);
            exit;
        }

        $start_date = $start_date -> getTimestamp();
        $end_date = $end_date -> getTimestamp();

        $start_date = $start_date * 1000;
        $end_date = $end_date * 1000;

        $data = $this->Mongo_model->gpsLog($carnum, $start_date, $end_date, $inout);        
        echo json_encode($data);
    }

    // 출근 차량 전체 목록
    public function worklist() {
        $data = $this->Mongo_model->getworklist();
        echo json_encode($data);
    }

    /**
     * Utility Methods
     */
    private function checkAsiaTimezone() {
        return date_default_timezone_get() === 'Asia/Seoul';
    }
    
    private function isNull($str) {
        return strlen($str) === 0
            || empty($str)
            || $str === '' 
            || $str === null;
    }
}

?>