<?php
$lang['menu_permission'] = '메뉴 권한';
$lang['module'] = '모듈';
$lang['department_update'] = 'Department Information Successfully Update';
$lang['designation_info_deleted'] = 'Designation Information Successfully Deleted';
$lang['no_designation_create_yet'] = 'No Designation Create yet';
$lang['department_already_used'] = 'Sorry You can\'t delete the departments.This Department Already to used to bellow designations.please change all designation under the departments then delete.';
$lang['undefined_department'] = 'Undefined Department';
$lang['activity_update_a_department'] = 'Update Department';
$lang['activity_delete_a_department'] = 'Deleted Department';
$lang['activity_delete_a_designation'] = 'Designation Deleted';
$lang['department_info_deleted'] = 'Department Information Successfully Deleted';
$lang['designation_already_used'] = 'This Designation Already assigned to users Please change the designation from the users.then you can delete the designation';
$lang['create'] = '생성';
$lang['view_help'] = 'If you select create/edit/delete no need to select the view';
$lang['set_full_permission'] = 'Set Full Permissions for this Designation';


/* End of file departments_lang.php */
/* Location: ./application/language/korean/departments_lang.php */
