<?php
$lang['bugs'] = '버그';
$lang['bugs_email'] = 'Bugs Email';
$lang['bug_assigned'] = 'Bug Assigned';
$lang['bug_comments'] = 'Bug Comments';
$lang['bug_attachment'] = 'Bug Attachment';
$lang['bug_updated'] = 'Bug Updated';
$lang['bug_reported'] = 'Bug Reported';
$lang['update_bug'] = 'Bugs infromation successfully updated';
$lang['save_bug'] = 'Bugs infromation successfully Saved';
$lang['bug_deleted'] = 'Bugs infromation successfully Deleted';
$lang['activity_update_bug'] = 'Update Bugs';
$lang['activity_new_bug_comment'] = 'Bugs New Comments';
$lang['activity_bug_attachfile_deleted'] = 'Bugs Attachment Deleted';
$lang['activity_new_bug_attachment'] = 'Bugs Attach a new files';
$lang['activity_bug_deleted'] = '삭제된 버그';
$lang['activity_new_bug'] = '추가된 신규 버그';
$lang['all_bugs'] = '전체 버그';
$lang['new_bugs'] = '신규 버그';
$lang['bug_status'] = '버그 상태';
$lang['bug_title'] = '버그 제목';
$lang['resolved'] = 'Resolved';
$lang['update_on'] = 'Update On';
$lang['bug_details'] = 'Bugs Details';
$lang['delete_timesheet'] = 'Timer infromation successfully Deleted';
$lang['bug_comment_save'] = 'Bugs Comment successfully saved';
$lang['bug_comment_deleted'] = 'Bugs Comment successfully saved';


/* End of file bugs_lang.php */
/* Location: ./application/language/korean/bugs_lang.php */
