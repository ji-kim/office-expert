<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = "login";
$route['404'] = "login/not_found";
$route['admin/mark_attendance'] = "admin/dashboard/mark_attendance";
$route['knowledgebase'] = "frontend/knowledgebase";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['driver/log'] = 'api/Mong/index';
$route['driver/work'] = 'api/Mong/work';
$route['driver/works'] = 'api/Mong/worklist';
$route['driver/login_dev'] = 'api/V1/login';
$route['driver/memberdetail'] = 'api/V1/user_date';
$route['driver/memberdetail'] = 'api/V1/user_date';
//$route['admin/dispatch'] = 'admin/test';
$route['dispatcher'] = 'admin/Dispatcher';

// 관제 API route
$route['api/gpsLog'] = 'api/Mong/gpsLog';
$route['api/sideMenu'] = 'api/V1/sideMenu';

// expert route
$route['expert'] = 'expert/login/login';
$route['expert/login'] = 'expert/login/login';
$route['expert/logout'] = 'expert/login/logout';
$route['expert/invoice'] = 'expert/invoice/invoice';
$route['expert/invoice/(:any)'] = 'expert/invoice/invoice/$1';
$route['expert/receipt'] = 'expert/invoice/receipt';
$route['expert/receipt/(:any)'] = 'expert/invoice/receipt/$1';
$route['expert/contract'] = 'expert/contract/index';
$route['expert/allocation'] = 'expert/allocation/index';
$route['expert/ins_isp'] = 'expert/ins_isp/index';
$route['expert/car'] = 'expert/car/car_info';
$route['expert/qa'] = 'expert/qa/qa_list';
$route['expert/driverinfo/(:any)'] = 'api/Driverinfo/driverInfo/$1';

//$route['translate_uri_dashes'] = TRUE;


//REST API URI Setting
//$route['api/Example/userlist?start=(:num)&limit=(:num)'] = 'api/example/userlist?start=$1&limit=$2'; // Example 4
//$route['get']['/v/users'] = 'api/Delex/user_list'; // Example 8
//$route['get']['/v/drivers'] = 'api/'; // Example 8
/* End of file routes.php */
/* Location: ./application/config/routes.php */

//$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
//$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
