<?php echo message_box('success') ?>
<!-- =============== VENDOR STYLES ===============-->
<!-- FONT AWESOME-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome/css/font-awesome.min.css">
<!-- SIMPLE LINE ICONS-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/simple-line-icons/css/simple-line-icons.css">
<!-- =============== BOOTSTRAP STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" id="bscss">
<!-- =============== APP STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.css" id="maincss">
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper">
            <div class="<?= ($this->uri->segment(1) == '404' ? 'abs-center ' : ' ') ?>wd-xl" style="margin: auto;">
                <!-- START panel-->
                <div class="text-center mb-xl">
                    <div class="text-lg mb-lg">404</div>
                    <p class="lead m0">페이지를 찾을 수 없습니다.</p>
                    <p>존재하지 않는 페이지 입니다.</p>
                </div>
                <div class="input-group mb-xl">
                    <input type="text" placeholder="Try with a search" class="form-control">
            <span class="input-group-btn">
               <button type="button" class="btn btn-default">
                   <em class="fa fa-search"></em>
               </button>
            </span>
                </div>
                <ul class="list-inline text-center text-sm mb-xl">
                    <li><a href="<?= base_url() ?>admin/dashboard" class="text-muted">홈</a>
                    </li>
                    <li class="text-muted">|</li>
                    <li><a href="<?= base_url() ?>login/logout" class="text-muted">로그인</a>
                    </li>
                </ul>
                <!-- END panel-->
                <div class="p-lg text-center">
                    <span>&copy;</span>
                    <span><a href=""> (주)델타온</a></span>
                    <br/>
                    <span>2016-2017</span>
                    <span>-</span>
                    <span><?= lang('version') . ' ' . config_item('version') ?></span>
                </div>
            </div>
        </div>
    </div>
    <!-- End Form -->
</div>