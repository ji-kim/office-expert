<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">        
        <div class="panel panel-custom">
            <div class="panel-heading"><div class="panel-title">전체활동
                    <a onclick="return confirm('모든 이력을 삭제하시겠습니까?')" href="<?= base_url() ?>admin/settings/clear_activities" class="btn btn-xs btn-primary pull-right">활동삭제</a>
                </div></div>                  
            <div class="row">
                <div class="col-lg-12">                                        
                    <table class="table table-striped" id="Transation_DataTables">
                        <thead>
                            <tr>
                                <th class="col-xs-2">활동일</th>
                                <th class="col-xs-3">사용자</th>
                                <th class="col-xs-1">모듈</th>

                                <th >활동</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($activities_info)) {
                                foreach ($activities_info as $v_activity) {
                                    ?>
                                    <tr>
                                        <td><?= display_datetime($v_activity->activity_date); ?></td>
                                        <td><?= $this->db->where('user_id', $v_activity->user)->get('tbl_account_details')->row()->fullname; ?></td>
                                        <td><?= lang($v_activity->module) ?></td>
                                        <td>
                                            <?= lang($v_activity->activity) ?>
                                            <strong> <?= $v_activity->value1.' '.$v_activity->value2 ?></strong></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#Transation_DataTables').dataTable({
            paging: false,
            "bSort": false
        });
    });
</script>
<!-- end -->