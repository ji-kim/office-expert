<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>
<body>
<div class="book">
	
	<!-- Page 1_팩스전문표지 -->
    <?if($action != null){?>
    <div class="page">
        <div class="subpage">
            <!--인쇄영역-->
            <div id="header">
                <!--span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span-->
                <ul>
                    <!--li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li-->
                    <li class="titCompany"><?=$ws->co_name?></li>
                    <li class="titName">위ㆍ수탁계약해지확인서</li>
                </ul>
            </div>

            <section>
                <div class="confirm">
                    <p><span class="blueD"><?=$ws->co_name?></span> 대표이사 <span class="blueD"><?=$ws->ceo?></span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD"><?=$inv->ceo?></span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD"><?=date("Y")?></span>년 <span class="blueD"><?=date("m")?></span>월 <span class="blueD"><?=date("d")?></span>일 해지하였음을 상호 확인한다.</p>

                    <p class="pdt20"><strong>제1조(계약해지요청자)</strong>
                        “을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>
                    <dl class="pdt20">
                        <dt>제2조(확인서의 효력)</dt>
                        <dd>
                            ① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
                            ② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
                    <p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
                    <dl class="pdt20">
                        <dt>제5조(동시이행)</dt>
                        <dd>
                            ① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
                            ② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제6조(인감증명서 첨부)</strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>

                    <p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
                    <div class="tcenter pdt30">
                        <ul class="date" style="width:32%;">
                            <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
                            <li>년</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
                            <li>월</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
                            <li>일</li>
                        </ul>
                    </div>
                </div>
            </section>

            <section>
                <div class="confirmTable">
                    <table class="table">
                        <colgroup>
                            <col style="width: 5%">
                            <col style="width: 12%">
                            <col style="width: 16%">
                            <col style="width: 16%">
                            <col style="width: 14%">
                            <col style="width: 37%">
                        </colgroup>
                        <tr>
                            <td rowspan="2">갑</td>
                            <td rowspan="2">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
                            <td>사무소</td>
                            <td height="45px"><span class="blue"><?=$ws->co_address?></span></td>
                        </tr>
                        <tr>
                            <td>전화</td>
                            <td height="45px"><span class="blue"><?=$ws->co_tel?></span></td>
                        </tr>
                        <tr>
                            <td rowspan="4">을</td>
                            <td rowspan="4">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
                            <td>차주명</td>
                            <td height="25px"><span class="blue"><?=$inv->ceo?></span></td>
                        </tr>
                        <tr>
                            <td>핸드폰</td>
                            <td height="25px"><span class="blue"></span></td>
                        </tr>
                        <tr>
                            <td rowspan="2">주민등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$car->inv_reg_no?></span></td>
                            <td>차량번호</td>
                            <td height="25px"><span class="blue"><?=$car->car_1?></span></td>
                        </tr>
                        <tr>
                            <td>주소</td>
                            <td height="45px"><span class="blue"><?=$inv->co_address?></span></td>
                        </tr>
                    </table>
                </div>
            </section>
            <!--인쇄영역 끝-->
        </div><!-- subpage 끝 -->
    </div>
	<?}?>
	<!-- Page 2_팩스전문표지 -->
	<div class="page">
		<div class="subpage">
			<section class="daegu" style="width:90%;margin: 0 auto;">
				<div class="title">
					<p class="tcenter"><img src="/assets/img/faxImg.jpg"></p>
					<h2>팩스전문표지</h2>
				</div>
				
				<div class="tright pdt20">
					<ul class="date" style="width:36%;">
						<li>date :</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
						<li>.</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
						<li>.</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
					</ul>
				</div>
								
				<table>
					<tr class="ht30">
						<td width="10%" rowspan="3">받 음</td>
						<td width="20%">팩스번호</td>
						<td width="70%"><?=$ws->fax?></td>
					</tr>
					<tr class="ht30">
						<td>소　　속</td>
						<td><?=$ws->co_name?></td>
					</tr>
					<tr class="ht30">
						<td>성　　명</td>
						<td><?=$ws->ceo?></td>
					</tr>
					<tr class="ht30">
						<td rowspan="4">보 냄</td>
						<td>전화번호</td>
						<td>053) 588-9331</td>
					</tr>
					<tr class="ht30">
						<td>팩스번호</td>
						<td>053) 588-9330</td>
					</tr>
					<tr class="ht30">
						<td>소　　속</td>
						<td>대구광역시화물자동차운송사업협회</td>
					</tr>
					<tr class="ht30">
						<td>담 당 자</td>
						<td>이  경  학</td>
					</tr>
					<tr class="ht30">
						<td>제 목</td>
						<td colspan="2" class="tleft">　　　대ㆍ폐차시 구비서류</td>
					</tr>
					<tr>
						<td>참<br/>고<br/>사<br/>항</td>
						<td colspan="2" class="tleft">
						<br/><br/><br/><br/>
							<h4 class="tcenter">- 대ㆍ폐차기간 15일 적용 -</h4><br/>
							<p style="font-size: 13.5px; line-height: 22px;" >
							<strong>※ 지입계약한 차주로 신청시</strong><br/>
							　① 화물자동차운송사업허가사항변경신고서<br/>
							　② 폐차차량, 대차차량 <strong>자동차등록증 사본 (신차인 경우 제작증)</strong><br/>
							　③ 폐차차량, 대차차량 <strong>자동차등록원부</strong> 발급받은지 <strong>7일이내 사본</strong><br/>
							　④ 지입차주동의서 <strong>(지입차주 인감도장 날인)</strong>와 지입차주 <strong>인감증명서</strong><br/>
							　　(발급받은지 <strong>3달이내)원본</strong><br/>
							　⑤ 지입차주 <strong>인감증명서</strong>(발급받은지 <strong>3달이내)원본</strong><br/><br/>

							<strong>※ 직영(회사차량으로 운전기사고용)으로 신청시</strong><br/>
							　① 화물자동차운송사업허가사항변경신고서<br/>
							　② 폐차차량, 대차차량 <strong>자동차등록증 사본 (신차인 경우 제작증)</strong><br/>
							　③ 폐차차량, 대차차량 <strong>자동차등록원부</strong> 발급받은지 <strong>7일이내 사본</strong><br/>
							　④ 의료보험이나 국민연금 최근 6개월 납입 증명서<br/>
							　⑤ 자동차말소사실증명서<strong>(부활등록일 경우에만 첨부)</strong>
							　</p>
							　<br/><br/><br/><br/>
						</td>
					</tr>
					<tr>
						<td>총매수</td>
						<td colspan="2">매 (표지 포함)</td>
					</tr>
				</table>
				<p class="tcenter pdt10">대구시 달서구 월성동 895-7(대구화물터미널 502호) 전화번호 : 588-9331</p>
				<h2 class="tcenter pdt10">대 구 광 역 시 화 물 자 동 차 운 송 사 업 협 회</h2>
			</section><!-- section/ -->
		</div>
	</div>
	
	
<!-- Page 3/3_화물자동차운송사업허가사항변경신고서 -->
	<div class="page">
		<div class="subpage">
		<!--인쇄영역-->
		<div class="daegu" style="width: 90%; margin: 0 auto;">
			<div class="title">
				<span style="font-size: 12px;">■화물자동차 운수사업법 시행규칙 [별지 제8호서식] <span class="blueD">＜개정 2013.07.11＞</span></span><br/><br/><br/>
				<h2>화물자동차 운송사업 허가사항 변경신고서</h2>
			</div>
		
			<div class="pdt30" >
				<table class="tableLine" style="padding-bottom: 10px;">
					<tr class="gbgray">
						<td>접수번호</td>
						<td>접수일 <?=date("Y.m.d")?></td>
						<td>발급일 <?=date("Y.m.d")?></td>
						<td>처리기간　　즉시</td>
					</tr>
				</table>
			</div>
			<div class="pdt5" >
				<table class="tableLine" >
					<tr class="ht40">
						<td width="14%" rowspan="3">신고</td>
						<td width="43%" class="tstyle" class="ht50">
							<p>성명(법인명 및 대표자 성명)</p>
							<span class="blue tright"> <?=$ws->co_name?> </span>
						</td>	
						<td width="43%" class="tstyle" class="ht50">
							<p>주민등록번호 (법인등록번호)</p>
							<span class="blue tright"><?=$ws->reg_number?> </span>
						</td>			
					</tr>
					<tr class="ht40">
						<td colspan="2">전화번호 <?=$ws->co_tel?></td>
					</tr>
					<tr class="ht40">
						<td colspan="2">주소 <?=$ws->co_address?></td>
					</tr>
				</table>
			</div>
			
			<div class="pdt5" >
				<table class="tableLine" >
					<tr class="ht30">
						<td width="14%" rowspan="4">신고내용</td>
						<td width="86%" colspan="3"class="tstyle" class="ht50">						
							사업의 종류
						</td>		
					</tr>
					<tr class="ht30">
						<td colspan="3">변경 월일 <?=date("Y.m.d")?></td>
					</tr>
					<tr class="ht60">
						<td widht="20">변경사항</td>	
						<td widht="40" class="tstyle">변경전</td>
						<td widht="40" class="tstyle">변경후</td>
					</tr>
					<tr class="ht40">
						<td colspan="3">
							<span class="tstyle">변경 사유</span>
							<p class="ht60"></p>
						</td>					
					</tr>
					<tr>
						<td colspan="4" style="border-bottom: 3px solid #000;">
							<p>　「화물자동차 운수사업법」 제3조제3항 단서 및 같은 법 시행규칙 제10조에 따라 허가사항의 경미한 변경을 신고합니다.</p>
							<div class="tright pdt20">
								<ul class="date" style="width:36%;">
									<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
									<li>년</li>
									<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
									<li>월</li>
									<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
									<li>일</li>
								</ul>
							</div>							
							<div class="tright pdt10">	
								<ul class="stepdate">
									<li style="font-size:14px;">신고인</li>
									<li style="width: 200px;">
										<span class="blue"><?=$ws->ceo?></span>
									</li>
									<li style="font-size:12px;">(서명 또는 인)</li>			
								</ul>					
							</div>
							<h3>　특별시·광역시·특별자치시·도·특별자치도 화물자동차 운송사업협회장 귀하</h3>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="height:120px;"></td>
					</tr>
					<tr>
						<td rowspan="2">첨부서류</td>
						<td colspan="2" rowspan="2" >신·구 허가사항을 대조한 서류 및 도면 각 1부</td>
						<td class="tstyle">수수료</td>
					</tr>
					<tr>
						<td class="tstyle">화물자동차 운송사업 협회장이 정하는 금액</td>
					</tr>
				</table>
				<table class="tableLine" style="width: 100%" >					
					<tr>
						<td class="gbgray " style="border-top: 2px solid #000;text-align:center">처리절차</td>
					</tr>
					<tr>
						<td> 
							<dl class="arrowbox">
								<dt>신고서 작성 </dt>
								<dd><span class="arrow">▶ </span></dd>
								<dt>접수</dt>
								<dd><span class="arrow">▶ </span></dd>
								<dt>검토</dt>
								<dd><span class="arrow">▶ </span></dd>
								<dt>수리</dt>
								<dd><span class="arrow">▶ </span></dd>
								<dt>통보</dt>
							</dl>
							<span class="inlineL" style="margin-bottom: 10px;">　　신고인　　　　특별시ㆍ광역시ㆍ도ㆍ특별자치도 화물자동차 운송사업 협회</span>
						</td>
					</tr>
				</table>
				<p class="tright tstyle">210mm×297mm[일반용지 60g/㎡(재활용품)]</p>
			</div>
		</div>
		<!--인쇄영역 끝-->
		</div><!-- subpage 끝 -->
	</div><!-- page 끝 -->




</div><!-- // book -->
</body>    
</html>