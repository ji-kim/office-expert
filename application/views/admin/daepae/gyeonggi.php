<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>
<body>
<div class="book">
<!-- Page 1/3 위ㆍ수탁계약해지확인서 -->
    <?if($action != null){?>
	<div class="page">
		<div class="subpage">
			<!--인쇄영역-->
			<div id="header">
				<!--span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span-->
				<ul>
					<!--li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li-->
					<li class="titCompany"><?=$ws->co_name?></li>
					<li class="titName">위ㆍ수탁계약해지확인서</li>				
				</ul>						
			</div>

			<section>
				<div class="confirm">
					<p><span class="blueD"><?=$ws->co_name?></span> 대표이사 <span class="blueD"><?=$ws->ceo?></span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD"><?=$inv->ceo?></span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD"><?=date("Y")?></span>년 <span class="blueD"><?=date("m")?></span>월 <span class="blueD"><?=date("d")?></span>일 해지하였음을 상호 확인한다.</p>

					<p class="pdt20"><strong>제1조(계약해지요청자)</strong>
					“을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>	
					<dl class="pdt20">
						<dt>제2조(확인서의 효력)</dt>
						<dd>
						① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
						② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
					</dl>
					<p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
					“갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
					<p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
					<dl class="pdt20">
						<dt>제5조(동시이행)</dt>
						<dd>
						① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
						② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
					</dl>
					<p class="pdt20"><strong>제6조(인감증명서 첨부)</strong> 
					“갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>
					
					<p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
					<div class="tcenter pdt30">
						<ul class="date" style="width:32%;">
							<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
							<li>년</li>
							<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
							<li>월</li>
							<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
							<li>일</li>
						</ul>
					</div>					
				</div>				
			</section>	
			
			<section>
				<div class="confirmTable">
					<table class="table">
					<colgroup>
					<col style="width: 5%">
					<col style="width: 12%">
					<col style="width: 16%">
					<col style="width: 16%">
					<col style="width: 14%">
					<col style="width: 37%">
					</colgroup>
					  <tr>
						<td rowspan="2">갑</td>
						<td rowspan="2">(인)</td>
						<td rowspan="2">사업자등록번호</td>
						<td rowspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
						<td>사무소</td>
						<td height="45px"><span class="blue"><?=$ws->co_address?></span></td>
					  </tr>
					  <tr>
						<td>전화</td>
						<td height="45px"><span class="blue"><?=$ws->co_tel?></span></td>
					  </tr>
					  <tr>
						<td rowspan="4">을</td>
						<td rowspan="4">(인)</td>
						<td rowspan="2">사업자등록번호</td>
						<td rowspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
						<td>차주명</td>
						<td height="25px"><span class="blue"><?=$inv->ceo?></span></td>
					  </tr>
					  <tr>
						<td>핸드폰</td>
						<td height="25px"><span class="blue"></span></td>
					  </tr>
					  <tr>
						<td rowspan="2">주민등록번호</td>
						<td rowspan="2"><span class="blue"><?=$car->inv_reg_no?></span></td>
						<td>차량번호</td>
						<td height="25px"><span class="blue"><?=$car->car_1?></span></td>
					  </tr>
					  <tr>
						<td>주소</td>
						<td height="45px"><span class="blue"><?=$inv->co_address?></span></td>
					  </tr>
					</table>
				</div>
			</section>		
			<!--인쇄영역 끝-->
		</div><!-- subpage 끝 -->
	</div>
    <?}?>
<!-- Page 2/3_대•폐차 동의서 -->
	<div class="page">
		<div class="subpage">
			<section class="gg_agreement">
				<h1 class="gg_agreementTit">대ㆍ폐차 동의서</h1>
				<div class="gg_agreementD">
					<div class="companyN">
						<ul class="ulAlignL">
							<li class="tit">회사명 :</li>
							<li><span class="blue"><?=$inv->co_name?></span></li>
						</ul>
					</div>
					<div class="registrationNum">
						<ul class="ulAlignL">
							<li class="tit">차량번호 :</li>
							<li><span class="blue"><?=$car->car_1?></span></li>
						</ul>
					</div>
					<div class="confirmation">
						<p>상기차량의 차주 <span class="blue inlineSpan alignC" style="width: 128px;"><?=$inv->ceo?></span>(은)는 위․수탁 계약을 해지한 후 운송사업의 허가를 신청하지 아니하고 위․수탁 차량을 자가용전환 또는 다른 운송사업자와의 위․수탁 계약을 체결하고자 하므로 타인의 차량으로 대․폐차 신고를 하는데 동의합니다.</p>
					<div class="tcenter">
						<ul class="date">
							<li class="dateYMDb ml20"><?=date("Y")?></span></li>
							<li>년</li>
							<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
							<li>월</li>
							<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
							<li>일</li>
						</ul>
					</div>					
					</div>
					<div class="sign">
						<ul class="width100p">
							<li class="liW65 tright">위․수탁차주성명:</li>
							<li class="liW27 tright"><span class="blue"><?=$inv->ceo?></span></li>
							<li class="liW8 tright">(인)</li>
						</ul>
						<p class="sign2">
						<ul class="width100p">
							<li class="liW35">&nbsp;</li>
							<li class="liW65 tleft">(인감도장날인)</li>
						</ul>
						</p>
					</div>
					<div class="reference alignJ">
						<p>※첨부서류 : 위․수탁 차주의 인감증명서 1부(3개월이내발급)</p>
						<p class="B">FAX : 031-251-9770, 031-243-5451</p>
					</div>
				</div>
			</section><!-- section/gg_agreement" -->
		</div>
	</div>
<!-- Page 3/3_화물자동차운송사업허가사항변경신고서 -->
	<div class="page">
		<div class="subpage positionR">
			<section class="changeReport">
				<p class="changeReportTit" width="100%">화물자동차운송사업허가사항변경신고서(대․폐차)-[신규, 수정]</p>
				<table class="changeReportDtail1" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td rowspan="3" width="10%">신</br>고</br>인</td>
						<td width="24%">① 성 명</br><span class="font11">(법인명 및 대표자성명)</span></td>
						<td width="22%"><span class="blue"><?=$ws->ceo?></span></td>
						<td width="22%">② 주민등록번호</br>(법인등록번호)</td>
						<td width="22%"><span class="blue"><?=$ws->reg_number?></span></td>
					</tr>
					<tr>
						<td rowspan="2">③ 주 소</td>
						<td rowspan="2" colspan="2"><span class="blue"><?=$ws->co_address?></span></td>
						<td>
							<ul>
								<li>전화 :</li>
								<li class="pdl10"><span class="blue"><?=$ws->co_tel?></span></li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>팩스 :</li>
								<li class="pdl10"><span class="blue"><?=$ws->fax?></span></li>
							</ul>
						</td>
					</tr>
					<tr>
						<td colspan="2">④ 사업의 종류</td>
						<td colspan="3">일반화물자동차운송사업</td>
					</tr>
					<tr>
						<td colspan="2">⑤ 대․폐차 기간</td>
						<td colspan="3">
							<div class="tcenter">
								<ul class="date">
									<li class="dateYMD"><span class="blue"><?=date("Y")?></span></li>
									<li>.</li>
									<li class="dateYMD"><span class="blue"><?=date("m")?></span></li>
									<li>.</li>
									<li class="dateYMD"><span class="blue"><?=date("d")?></span></li>
									<li class="pdLR10">~</li>
									<li class="dateYMD"><span class="blue"><?=date("Y")?></span></li>
									<li>.</li>
									<li class="dateYMD"><span class="blue"><?=date("m")?></span></li>
									<li>.</li>
									<li class="dateYMD"><span class="blue"><?=date("d")?></span></li>
								</ul>
							</div>					
						</td>
					</tr>
					<tr>
						<td colspan="2" class="bdB0">⑥ 차 량 번 호</td>
						<td colspan="3" class="bdB0"><span class="blue"><?=$car->car_1?></span></td>
					</tr>
				</table>
				<table class="changeReportDtail2" cellpadding="0" cellspacing="0" width="100%">
					<tr><td rowspan="7" width="10%">⑦</br>대·폐</br>차량현황</td><td colspan="2">폐 차 차 량</td><td colspan="2">대 차 차 량</td></tr>
                    <tr><td width="15%">차 명</td><td width="30%"><span class="blue"><?=$car->car_4?></span></td><td width="15%">차 명</td><td width="30%"><span class="blue"><?=$car->car_4?></span></td></tr>
                    <tr><td width="15%">차 종</td><td width="30%" ><span class="blue"><?=$car->type?></span></td><td width="15%">차 종</td><td width="30%" ><span class="blue"><?=$car->type?></span></td></tr>
                    <tr><td>유 형</td><td><span class="blue"><?=$car->mode?></span><td>유 형</td><td><span class="blue"><?=$car->mode?></span></tr>
                    <tr><td>세부유형</td><td ><span class="blue"><?=$car->motor_mode?></span></td><td>세부유형</td><td ><span class="blue"><?=$car->motor_mode?></span></td></tr>
                    <tr><td>년 식</td><td><span class="blue"><?=$car->car_5?></span></td><td>년 식</td><td><span class="blue"><?=$car->car_5?></span></td></tr>
                    <tr><td>차대번호</td><td ><span class="blue"><?=$car->car_7?></span></td><td>차대번호</td><td ><span class="blue"><?=$car->car_7?></span></td></tr>

					<tr><!-- (l)최초등록일자/(r)최대 적재량 또는 총중량 -->
						<td>최초등록일자</td>
						<td colspan="2">
					<div class="tcenter" >
						<ul class="date">
							<li class="dateYMDm"><span class="blue"><?=date("Y")?></span></li>
							<li>년</li>
							<li class="dateYMDs"><span class="blue"><?=date("m")?></span></li>
							<li>월</li>
							<li class="dateYMDs"><span class="blue"><?=date("d")?></span></li>
							<li>일</li>
						</ul>
					</div>					
						</td>
						<td>최대 적재량</br>또는 총중량</td>
						<td>
							<ul>
								<li class="inlineL"><span class="blue"><?=$car->max_load?></span></li>
								<li class="inlineL pdL5">kg</li>
							</ul>
						</td>
					</tr>

					<tr><!-- 통보서 수령지 -->
						<td colspan="2">⑧ 통보서 수령지</br>[ ☑ 표시 ]</td>
						<td colspan="3">
							<ul class="localName">
								<li><input type="checkbox" name="" /><span class=""> 금천구</span></li>
								<li><input type="checkbox" name="" /><span class=""> 서초구</span></li>
								<li><input type="checkbox" name="" /><span class=""> 양천구</span></li>
								<li><input type="checkbox" name="" /><span class=""> 안산시</span></li>
								<li><input type="checkbox" name="" /><span class=""> 안양시</span></li>
							</ul>
							<ul class="clear localName">
								<li><input type="checkbox" name="" /><span class=""> 용인시</span></li>
								<li><input type="checkbox" name="" /><span class=""> 평택시</span></li>
								<li><input type="checkbox" name="" /><span class=""> 의정부시</span></li>
								<li><input type="checkbox" name="" /><span class=""> 인천시</span></li>
								<li><input type="checkbox" name="" /><span class=""> 협 회</span></li>
							</ul>
						</td>
					</tr>
					<tr><!-- 「화물자동차 운수사업법」 -->
						<td colspan="5" class="pd12 bdB0">
							<div class="textBlock">
								<p class="tleft pdl10 pdb30">「화물자동차 운수사업법」 제3조제3항 단서 및 같은법 시행규칙 제10조의 규정에 의하여 허가사항의 경미한 변경을 신고합니다.</p>
								<div class="tcenter">
									<ul class="date">
										<li class="dateYMDb"><?=date("Y")?></li>
										<li>년</li>
										<li class="dateYMD"><span class="blue"><?=date("m")?></span></li>
										<li>월</li>
										<li class="dateYMD"><span class="blue"><?=date("d")?></span></li>
										<li>일</li>
									</ul>
								</div>					
								<p class="pdb20">
									<ul class="ulAlignR">
										<li class="li50per">신 고 인</br>< 법인의 경우 법인 인감 날인 ></li>
										<li class="li40per tright"><span class="blue"><?=$ws->ceo?> </span></li>
										<li class="li10per tright">( 인 )</li>

									</ul>
								</p>
								<p class="clear tleft pdT10">경기도화물자동차운송사업협회 이사장  귀하</p>
							</div>
						</td>
					 </tr>
				</table>
				<table class="changeReportDtail3" cellpadding="0" cellspacing="0" width="100%">
					<tr><!-- 구비서류 -->
						<td rowspan="2" width="90%">
							<div class="textBlock font10">
								<p class="tleft">- 구비서류 -</p>
								<p>
									<span class="B">(폐차차량)</span>①비직영-자동차등록원부(갑)-7일이내,자동차등록증,차주동의서,차주인감증명서-<u>3개월이내</u></br>
									<span class="pdL61">②직  영-자동차등록원부(갑)-7일이내, 자동차등록증, 4대보험사업장가입자명부</span>
								</p>
								<p><span class="B">(대차차량)</span>①자동차등록증 또는 자동차제작증(신차에 한함) 1부. ②자동차등록원부(갑)-7일이내</p>
								<p>
									<span class="B">(폐차만신고시추가서류)</span>위수탁해지시-위수탁해지확인서, 법인인감증명서(3개월이내)</br>
									<span class="pdL140">출고지연시- 자동차계약서(대차시 자동차제작증으로만 대차가능)</span></br>
							   </p>
								<p>
									※ 대차 차량 영업용화물자동차가 폐차 차량 최초등록일 이전에 등록 된 자동차는 전 소속</br>
									회사(대차차량) 위․수탁관리계약서 사본 첨부, <u>직영자동차는 대차 불가능</u></br>
									※ 비회원 방문 처리시 추가서류 : 화물자동차사업운송사업허가증 사본, 인감증명서
								</p>
							</div>
						</td>
					<td width="10%">수수료</td>
					</tr>
					<tr><td class="tleft">시·도</br>협회장이</br>정하는</br>금액</td></tr>
				</table>
				<div class="textBox font12">
					<p>※ 유형 [ 화물자동차의 경우 : 일반형, 밴형(탑, 윙바디), 덤프용, 특수용도형(냉장냉동, 탱크로리 자동차</br>
						<span class="pdL210">수송용, 현금및귀금속, 청소용, 노면청소용, 살수용), 피견인형(트레일러)</span></br>
						<span class="pdL68">특수자동차의 경우 : 견인형(트렉타), 구난형(렉카), 특수작업형(사다리차 등)</span>
					</p>
					<p>※ 팩스 신고시 수수료 2,000원 [ <u>회원에 한함</u> ] 입금, 팩스번호 : (031)243-5451, 251-9770</br>
					<span class="pdL28">계좌번호 : 515-01-021182 [ 농협, 경기도화물협회 ],  입금자명 : (<span class="blue alignC inlineSpan" style="width: 170px;"><?=$ws->ceo?></span>)</span></br>
					우편계좌번호 : 302-0318-3705-21[ 농협, 김진성 ] : 2,460원</p>
				</div>
		</section><!-- // changeReport -->
		</div><!-- //  subpage -->
	</div><!-- //  page -->
</div><!-- // book -->
</body>    
</html>