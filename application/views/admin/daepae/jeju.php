<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>

<body>
<div class="book">
<!-- Page 1 위ㆍ수탁계약해지확인서 -->
    <?if($action != null){?>
    <div class="page">
        <div class="subpage">
            <!--인쇄영역-->
            <div id="header">
                <!--span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span-->
                <ul>
                    <!--li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li-->
                    <li class="titCompany"><?=$ws->co_name?></li>
                    <li class="titName">위ㆍ수탁계약해지확인서</li>
                </ul>
            </div>

            <section>
                <div class="confirm">
                    <p><span class="blueD"><?=$ws->co_name?></span> 대표이사 <span class="blueD"><?=$ws->ceo?></span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD"><?=$inv->ceo?></span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD"><?=date("Y")?></span>년 <span class="blueD"><?=date("m")?></span>월 <span class="blueD"><?=date("d")?></span>일 해지하였음을 상호 확인한다.</p>

                    <p class="pdt20"><strong>제1조(계약해지요청자)</strong>
                        “을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>
                    <dl class="pdt20">
                        <dt>제2조(확인서의 효력)</dt>
                        <dd>
                            ① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
                            ② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
                    <p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
                    <dl class="pdt20">
                        <dt>제5조(동시이행)</dt>
                        <dd>
                            ① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
                            ② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제6조(인감증명서 첨부)</strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>

                    <p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
                    <div class="tcenter pdt30">
                        <ul class="date" style="width:32%;">
                            <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
                            <li>년</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
                            <li>월</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
                            <li>일</li>
                        </ul>
                    </div>
                </div>
            </section>

            <section>
                <div class="confirmTable">
                    <table class="table">
                        <colgroup>
                            <col style="width: 5%">
                            <col style="width: 12%">
                            <col style="width: 16%">
                            <col style="width: 16%">
                            <col style="width: 14%">
                            <col style="width: 37%">
                        </colgroup>
                        <tr>
                            <td rowspan="2">갑</td>
                            <td rowspan="2">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
                            <td>사무소</td>
                            <td height="45px"><span class="blue"><?=$ws->co_address?></span></td>
                        </tr>
                        <tr>
                            <td>전화</td>
                            <td height="45px"><span class="blue"><?=$ws->co_tel?></span></td>
                        </tr>
                        <tr>
                            <td rowspan="4">을</td>
                            <td rowspan="4">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
                            <td>차주명</td>
                            <td height="25px"><span class="blue"><?=$inv->ceo?></span></td>
                        </tr>
                        <tr>
                            <td>핸드폰</td>
                            <td height="25px"><span class="blue"></span></td>
                        </tr>
                        <tr>
                            <td rowspan="2">주민등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$car->inv_reg_no?></span></td>
                            <td>차량번호</td>
                            <td height="25px"><span class="blue"><?=$car->car_1?></span></td>
                        </tr>
                        <tr>
                            <td>주소</td>
                            <td height="45px"><span class="blue"><?=$inv->co_address?></span></td>
                        </tr>
                    </table>
                </div>
            </section>
            <!--인쇄영역 끝-->
        </div><!-- subpage 끝 -->
    </div>
    <?}?>
	
<!-- Page 2 대‧폐차 동의서 -->
	<div class="page">
		<div class="subpage">
			<!--인쇄영역-->
			<section>
				<table class="agreementDtail">
				  <tr>
					<td colspan="3" class="agreementTit">
						<h1>대‧폐차 동의서</h1>
						<h4>(유가보조금 지급 대상자 확인)</h4>
					</td>
				  </tr>
				  <tr>
					<td width="35%">소속회사</td>
					<td class="alignL" colspan="2"><span class="blue"><?=$inv->co_name?></span></td>
				  </tr>
				  <tr>
					<td>위‧수탁차주명</td>
				   <td class="alignL" colspan="2"><span class="blue"><?=$inv->ceo?></span></td>
				  </tr>
				  <tr>
					<td>주민번호</td>
					<td class="alignL" colspan="2"><span class="blue"><?=$inv->reg_number?></span></td>
				  </tr>
				  <tr>
					<td>차량번호</td>
					<td colspan="2">
						<ul>
							<li class="inlineL">서울</li>
							<li class="inlineL"><span class="blue"> <?=$car->car_1?></span></li>
						</ul>
					</td>
				  </tr>
				  <tr>
					<td class="textBlock1" colspan="3">
						<p class="alignL">위 본인은 상기 회사와 명의신탁 및 위‧수탁 계약을 체결하고 유가보조금 지급 대상자로서 상기 차량에 대한 대‧폐차 업무처리에 동의합니다.</p>
						<p class="attach">붙임 : 위‧수탁 차주 인감증명서 1부</p>
					</td>
				  </tr>
				  <tr>
					<td class="textBlock2" colspan="3">
						<p class="caution">주<br/>의</p>
						<p class="cautionDetail">인감 증명서 사본 첨부시 최대한 연하게 복사하여 인감 모양이 분명하게 식별되어 진위여부가 파악될 수 있도록 처리 바라며, 식별이 불가능하여 인정되지 않을 경우 대‧폐차 수리가 불가 할 수 있음을 알려드립니다.</p>
					</td>
				  </tr>
				  <tr class="date">
					<td class="deft"></td>
					<td class="denter" width="35%"></td>
					<td class="dight" width="35%">
						<ul>
							<li><span class="blue"><?=date("Y")?></span></li>
							<li> 년</li>
							<li><span class="blue"><?=date("m")?></span></li>
							<li> 월</li>
							<li><span class="blue"><?=date("d")?></span></li>
							<li> 일</li>
						</ul>
					</td>
				  </tr>
				  <tr class="signBlock1">
					<td class="seft"></td>
					<td class="senter">
						<span class="tit18">위 위‧수탁차주명</span>
						<br/>
						<span>(※인감날인)</span>
					</td>
					<td class="sight">
						<ul>
							<li class="name">
								<span class="blue"><?=$inv->ceo?></span>
							</li>
							<li class="sign">(인)</li>
						</ul>
					</td>
				  </tr>
				  <tr>
					<td class="to" colspan="2">서울화물자동차운송사업협회 귀중</td>
					<td class="signBlock2">
						<div>
						<p class="textBox">
							<span class="tit18">원본대조필</span>
							<br/>
							<span>(회사도장날인)</span>
						</p>
						<p class="signBox">인</p>
						</div>
					</td>
				  </tr>
				</table>
			</section>
			<!--인쇄영역 끝-->
		</div><!-- subpage 끝 -->
	</div>
	
<!-- Page 3 신고서 -->
	<div class="page">
		<div class="subpage">
		<p>[별지 제8호서식] ＜개정 2004.4.21＞ </p>		
		
		<table class="table" style="width:100%;">
		<colgroup>
		<col style="width: 43px">
		<col style="width: 121px">
		<col style="width: 82px">
		<col style="width: 60px">
		<col style="width: 61px">
		<col style="width: 61px">
		<col style="width: 61px">
		<col style="width: 86px">
		</colgroup>
		  <tr>
			<td colspan="7" rowspan="2">화물자동차운송사업허가사항변경신고서</td>
			<td>처리기간</td>
		  </tr>
		  <tr>
			<td>즉시</td>
		  </tr>
		  <tr>
			<td rowspan="3">신<br>고<br>인</td>
			<td class="ht100">①성명(법인명 및<br> 대표자 성명)</td>
			<td colspan="2"><span class="blue"><?=$ws->ceo?></span></td>
			<td colspan="2" style="height:50px;">②주민등록번호<br>(법인등록번호)</td>
			<td colspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
		  </tr>
		  <tr>
			<td rowspan="2">③주소</td>
			<td colspan="6" rowspan="2" class="tleft ht60">
				<p><span class="blue"><?=$ws->co_address?></span></p>
				<ul class="stepdate" >					
					<li>(전화 : </li>
					<li class="tcenter" style="width: 100px;"><span class="blue"><?=$ws->co_tel?></span></li>
					<li> )</li>									
				</ul>
			</td>
		  </tr>
		  <tr> </tr>
		  <tr>
			<td colspan="2" class="ht40">④사업의종류</td>
			<td colspan="6"><span class="blue"></span></td>
		  </tr>
		  <tr>
			<td colspan="2" class="ht40">⑤변경연월일</td>
			<td colspan="6"><span class="blue"><?=date("Y-m-d")?></span></td>
		  </tr>
		  <tr>
			<td colspan="2" rowspan="2">⑥변경사항</td>
			<td colspan="3" class="ht40">변경전</td>
			<td colspan="3" >변경후</td>
		  </tr>
		  <tr>
			<td colspan="3" class="ht40"><span class="blue"></span></td>
			<td colspan="3" ><span class="blue"></span></td>
		  </tr>
		  <tr>
			<td colspan="2" class="ht40">⑦변경사유</td>
			<td colspan="6"><span class="blue"></span></td>
		  </tr>
		  <tr>
			<td colspan="8">
				<div>
					<p class="tleft pdt5">　화물자동차운수사업법 제3조제3항 단서 및 동법시행규칙 제10조의 규정에 의하여 허가사항의 경미한 변경을 신고합니다.</p>
				<br/><br/><br/><br/><br/><br/><br/>	
				
				<div class="tcenter">
					<ul class="date" style="width:32%;">
						<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
						<li>년</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
						<li>월</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
						<li>일</li>
					</ul>
				</div>		
						
				<br/><br/><br/><br/>	
				<div class="tright">	
					<ul class="date" style="width:45%;">
						<li>신고인</li>
						<li style="width: 150px;"><span class="blue" ><?=$ws->ceo?></span></li>
						<li> (서명 또는 인)</li>					
					</ul>
				</div>
				<br/><br/><br/><br/><br/><br/>
				
				<div class="line">
					<ul class="tleft pdl10 ">
						<li> ─ 특 별 시 ─ </li>
						<li>　 광 역 시　 　 화물자동차운송사업협회장 귀하</li>
						<li> ─ 　 도 　 ─ </li>
						<span class="lineleft"></span>
						<span class="lineright"></span>
					</ul>
				</div>				
				</div>
			</td>
		  </tr>
		  <tr>
			<td colspan="6" rowspan="2" class="tleft pdl10">구비서류 : 신․구 허가사항을 대조한 서류 및 도면 각1부</td>
			<td colspan="2">수수료</td>
		  </tr>
		  <tr>
			<td colspan="2" class="tleft">시․도협회장이 정하는<br> 금액</td>
		  </tr>
		</table>		
		
		<p class="tright">210mm×297mm(신문용지 54g/㎡(재활용품))</p>
			
		</div>		
	</div>
	
	<!-- Page 4 주소 -->
	<div class="page">
		<div class="subpage">
		제주특별자치도화물자동차운송사업협회<br/><br/>
		대리 고은섭<br/><br/>
		Tel : (064)753-8211<br/><br/>
		H.P : 010-5041-6745<br/><br/>
		E-mail : unkes@naver.com<br/><br/>
		농협 301-0164-3759-81<br/><br/>
		팩스 064-753-8312<br/>
		</div>		
	</div>







</div><!-- book 끝 -->
</body>    
</html>