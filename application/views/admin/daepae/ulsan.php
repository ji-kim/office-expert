<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>

<body>
<div class="book">
<!-- Page 1 위ㆍ수탁계약해지확인서 -->
    <?if($action != null){?>
    <div class="page">
        <div class="subpage">
            <!--인쇄영역-->
            <div id="header">
                <!--span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span-->
                <ul>
                    <!--li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li-->
                    <li class="titCompany"><?=$ws->co_name?></li>
                    <li class="titName">위ㆍ수탁계약해지확인서</li>
                </ul>
            </div>

            <section>
                <div class="confirm">
                    <p><span class="blueD"><?=$ws->co_name?></span> 대표이사 <span class="blueD"><?=$ws->ceo?></span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD"><?=$inv->ceo?></span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD"><?=date("Y")?></span>년 <span class="blueD"><?=date("m")?></span>월 <span class="blueD"><?=date("d")?></span>일 해지하였음을 상호 확인한다.</p>

                    <p class="pdt20"><strong>제1조(계약해지요청자)</strong>
                        “을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>
                    <dl class="pdt20">
                        <dt>제2조(확인서의 효력)</dt>
                        <dd>
                            ① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
                            ② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
                    <p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
                    <dl class="pdt20">
                        <dt>제5조(동시이행)</dt>
                        <dd>
                            ① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
                            ② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제6조(인감증명서 첨부)</strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>

                    <p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
                    <div class="tcenter pdt30">
                        <ul class="date" style="width:32%;">
                            <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
                            <li>년</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
                            <li>월</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
                            <li>일</li>
                        </ul>
                    </div>
                </div>
            </section>

            <section>
                <div class="confirmTable">
                    <table class="table">
                        <colgroup>
                            <col style="width: 5%">
                            <col style="width: 12%">
                            <col style="width: 16%">
                            <col style="width: 16%">
                            <col style="width: 14%">
                            <col style="width: 37%">
                        </colgroup>
                        <tr>
                            <td rowspan="2">갑</td>
                            <td rowspan="2">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
                            <td>사무소</td>
                            <td height="45px"><span class="blue"><?=$ws->co_address?></span></td>
                        </tr>
                        <tr>
                            <td>전화</td>
                            <td height="45px"><span class="blue"><?=$ws->co_tel?></span></td>
                        </tr>
                        <tr>
                            <td rowspan="4">을</td>
                            <td rowspan="4">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
                            <td>차주명</td>
                            <td height="25px"><span class="blue"><?=$inv->ceo?></span></td>
                        </tr>
                        <tr>
                            <td>핸드폰</td>
                            <td height="25px"><span class="blue"></span></td>
                        </tr>
                        <tr>
                            <td rowspan="2">주민등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$car->inv_reg_no?></span></td>
                            <td>차량번호</td>
                            <td height="25px"><span class="blue"><?=$car->car_1?></span></td>
                        </tr>
                        <tr>
                            <td>주소</td>
                            <td height="45px"><span class="blue"><?=$inv->co_address?></span></td>
                        </tr>
                    </table>
                </div>
            </section>
            <!--인쇄영역 끝-->
        </div><!-- subpage 끝 -->
    </div>
    <?}?>
	
<!-- Page 2 (대․폐차)수리동의서 -->
	<div class="page">
		<div class="subpage">		
			<div class="repair">
				<div class="repairTit">
					<h2>화물자동차운송사업허가사항변경(대․폐차)수리동의서</h2>
				</div>
				<br/><br/><br/>
				<dl class="company">
					<dt>회 사 명 : </dt>
					<dd><span class="blue"><?=$inv->co_name?></span></dd>
					<dt>차량번호 :</dt>
					<dd><span class="blue"><?=$car->car_1?></span></dd>
					<dt>차 주 명 :</dt>
					<dd><span class="blue"><?=$inv->ceo?></span></dd>
				</dl>
				<br/><br/><br/>
				
				<p class="lh30">　　　상기 차량을 화물자동차운수사업법 제3조제3항 단서 및 동법시행규칙 제10조의 규정에 의한 허가사항의 경미한 변경신고(대․폐차)에 동의합니다.</p>
				<br/><br/><br/><br/><br/><br/>
				<div class="tcenter pdt30">
					<ul class="date" style="width:36%;">
						<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
						<li>년</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
						<li>월</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
						<li>일</li>
					</ul>
				</div><br/><br/><br/><br/>
				<div class="tright">	
					<ul class="stepdate">
						<li>위․수탁차주:</li>
						<li style="width: 200px;">
							<span class="blue"><?=$inv->ceo?></span>
						</li>
						<li> (인)</li>			
					</ul>					
				</div>				
				<br/><br/><br/><br/>
				<p>첨부서류 : 인감증명 1부.</p>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/>
				<h2 style="text-align:center;">울산광역시화물자동차운송사업협회이사장 귀하</h2>
			</div>			
		</div>		
	</div>
	
<!-- Page 3 신고서 -->
	<div class="page">
		<div class="subpage">
			<div class="ulsanRepair">
				<table class="table" style="width:100%;">
				<colgroup>
				<col style="width: 5%">
				<col style="width: 13%">
				<col style="width: 10%">
				<col style="width: 18%">
				<col style="width: 12%">
				<col style="width: 20%">
				<col style="width: 22%">
				</colgroup>
				  <tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<td colspan="3" >발급번호 : 울산일반-</td>
				  </tr>
				  <tr>
					<td colspan="7" class="ht60"><h2>화물자동차운송사업허가사항변경(대․폐차)수리 신청서</h2></td>
				  </tr>
				  <tr>
					<td rowspan="2" >신<br>고<br>인</td>
					<td colspan="2" class="ht40">①성　　　　명<br>(법인명 및 대표자성명)</td>
					<td colspan="2" ><span class="blue"><?=$ws->ceo?></span></td>
					<td>②주민등록번호<br>(법인등록번호)</td>
					<td><span class="blue"><?=$ws->bs_number?></span></td>
				  </tr>
				  <tr>
					<td colspan="2" class="ht40">주소</td>
					<td colspan="4"><span class="blue"><?=$ws->co_address?></span></td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">④사　업　의　종　류</td>
					<td colspan="4">일반화물운송사업</td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">⑤대　폐　차　기　간</td>
					<td colspan="4">
						<div>
							<ul class="date">
								<li class="dateYMD"><span class="blue">2017</span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue">10</span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue">20</span></li>
								<li>　~　</li>
								<li class="dateYMD"><span class="blue">2017</span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue">10</span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue">20</span></li>
							</ul>
						</div>		
					</td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">⑥차　량　번　호</td>
					<td colspan="4"><span class="blue"><?=$car->car_1?></span></td>
				  </tr>
				  <tr>
					<td colspan="2" class="ht40"></td>
					<td colspan="3">폐 차 차 량</td>
					<td colspan="2">대 차 차 량</td>
				  </tr>
				  <tr>
					<td colspan="2" >⑦대폐차차량현황</td>
					<td colspan="3">
						<dl class="grid30" >
							<dt>·차　　명:</dt>
							<dd><span class="blue"><?=$car->car_4?></span></dd>
							<dt>·차대번호:</dt>
							<dd><span class="blue"><?=$car->car_7?></span></dd>
							<dt>·차　　종 :</dt>
							<dd><span class="blue"><?=$car->type?></span></dd>
							<dt>·유　　형 :</dt>
							<dd><span class="blue"><?=$car->mode?></span></dd>
							<dt>·세부유형 :</dt>
							<dd><span class="blue"><?=$car->motor_mode?></span></dd>
						</dl>
						<div class="tleft">
							<ul class="stepdate">
								<li>연식(최초등록일) :</li>
								<li> <?=$car->car_5?></li>

							</ul>
						</div>
						<div class="tleft">
							<ul class="stepdate">
								<li>최대적재량 또는 총중량: </li>
								<li class="tcenter" style="width:100px;"><span class="blue"><?=$car->max_load?></span></li>
								<li>kg</li>
							</ul>
						</div>
					</td>
					<td colspan="2">
                        <dl class="grid30" >
                            <dt>·차　　명:</dt>
                            <dd><span class="blue"><?=$car->car_4?></span></dd>
                            <dt>·차대번호:</dt>
                            <dd><span class="blue"><?=$car->car_7?></span></dd>
                            <dt>·차　　종 :</dt>
                            <dd><span class="blue"><?=$car->type?></span></dd>
                            <dt>·유　　형 :</dt>
                            <dd><span class="blue"><?=$car->mode?></span></dd>
                            <dt>·세부유형 :</dt>
                            <dd><span class="blue"><?=$car->motor_mode?></span></dd>
                        </dl>
                        <div class="tleft">
                            <ul class="stepdate">
                                <li>연식(최초등록일) :</li>
                                <li> <?=$car->car_5?></li>

                            </ul>
                        </div>
                        <div class="tleft">
                            <ul class="stepdate">
                                <li>최대적재량 또는 총중량: </li>
                                <li class="tcenter" style="width:100px;"><span class="blue"><?=$car->max_load?></span></li>
                                <li>kg</li>
                            </ul>
                        </div>
					</td>
				  </tr>
				  <tr>
					<td colspan="7" class="inner">
						<p class="tleft pdt10">　　◦ 화물자동차운수사업법 제3조제3항 단서 및 동법시행규칙 제10조의 규정에 의한 허가사항의 경미한 변경신고(대․폐차)를 신청합니다. </p>
						<br/><br/>
						<div class="tright">
							
							<ul class="date" style="width:32%;">
                                <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
								<li>년</li>
								<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
								<li>월</li>
								<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
								<li>일</li>
							</ul>
							
							<br/><br/><br/>
							<div class="tright">	
								<ul class="stepdate" >
									<li>신 청 인 :</li>
									<li class="tcenter" style="width: 200px;" ><span class="blue"><?=$ws->ceo?></span></li>
									<li> (인)</li>			
								</ul>
								<br/><br/><br/>
								
								<ul class="stepdate" >
									<li>실무책임자 : </li>
									<li class="tcenter" style="width: 200px;" ><span class="blue"><?=$ws->ceo?></span></li>
									<li>(서명)</li>			
								</ul>								
							</div><br/><br/>								
						</div>
					</td>
				  </tr>
				  <tr>
					<td colspan="7" >
						<div class="month tleft" >
							<strong>※ 사본 또는 수기로 기재된 문서는 무효입니다.</strong><br/>
							<strong>※ 대차차량란이 공란인 문서는 무효입니다.</strong> 
							<dl class="monthInner">
								<dd>유효기간 : </dd>
								<dd style="width:70px;text-align:center;"><span class="blue"></span></dd>
								<dd>개월</dd>
							</dl>				
						</div>		
					</td>					
				  </tr>			  
				</table>
				<dl class="half">
					<dt>※차    종:</dt>
					<dd>자동차등록증(원부)상의 차종(경형화물, 소형화물, 중형화물, 대형화물, 경형특수,             소형특수, 중형특수, 대형특수)</dd>
					<dt>※유    형:</dt>
					<dd>일반형, 밴형, 덤프형, 특수용도형, 견인형, 구난형, 특수작업형</dd>
					<dt>※세부유형:</dt>
					<dd>노면청소용, 청소용, 살수용, 소방용, 탱크로리(석유류), 탱크로리(화학물질), 냉             장냉동용, 자동차수송용, 카고크레인등</dd>
				</dl>
			</div>		
		</div>
	</div>

<!-- Page 4 통보서 -->
	<div class="page">
		<div class="subpage">
			<h6>■ 화물자동차 유가보조금 관리 규정 [별지 제3호서식] <span class="blue"> 〈 개정 2014.  7.  28.〉</span></h6>
			<div class="title pdt20">
				<h2>위·수탁차주 변경 사실 통보서</h2>
			</div>
			
			<ul class="bgtable">
				<li style="width: 30%">
					<dl class="bgtableInner">
						<dt>접수번호</dt>
						<dd></dd>
					</dl>
				</li>
				<li style="width: 50%">
					<dl class="bgtableInner">
						<dt>접수일자</dt>
						<dd><?=date("Y.m.d")?></dd>
					</dl>
				</li>
				<li style="width: 20%">
					<dl class="bgtableInner">
						<dt>처리기간</dt>
						<dd>즉시</dd>
					</dl>
				</li>					
			</ul>			
			<ul class="bgtable" >
				<li style="width:20%;line-height:40px;" class="ht40">
					통지구분
				</li>
				<li style="width:80%;line-height:40px;" class="ht40">
					　　[  ] 신규(위·수탁)　　　　　[  ] 변경　　　　　[  ] 해지(직영 전환)
				</li>
			</ul>
			
			
			<table class="lineTable">
				<colgroup>
				<col style="width: 20%">
				<col style="width: 40%">
				<col style="width: 40%">
				</colgroup>
				<tr>
					<th>통보사항</th>
					<th>현행(신규, 해지)</th>
					<th>변경</th>
				</tr>
				<tr>
					<td>성명(법인명)</td>
					<td><span class="blue"><?=$inv->co_name?></span></td>
					<td><span class="blue"><?=$ws->co_name?></span></td>
				</tr>
				<tr>
					<td>상호(대표자)</td>
					<td><span class="blue"><?=$inv->ceo?></span></td>
					<td><span class="blue"><?=$ws->ceo?></span></td>
				</tr>
				<tr>
					<td>생년월일<br/>(법인번호)</td>
					<td>

					</td>
					<td><span class="blue"></span></td>
				</tr>
				<tr>
					<td>사업자등록번호</td>
					<td><span class="blue"><?=$inv->bs_number?></span></td>
					<td><span class="blue"><?=$ws->bs_number?></span></td>
				</tr>
				<tr>
					<td>주 소</td>
					<td><span class="blue"><?=$inv->co_address?></span></td>
					<td><span class="blue"><?=$ws->co_address?></span></td>
				</tr>
				<tr>
					<td rowspan="2">연락처</td>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:20%;" class ="none" >사무실</li>
							<li style="width:80%;" class ="none" ><span class="blue"><?=$inv->co_tel?></span></li>
						</ul>
					</td>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:20%;" class ="none">사무실</li>
							<li style="width:80%;" class ="none"><span class="blue"><?=$ws->co_tel?></span></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:20%;" class ="none">휴대전화</li>
							<li style="width:80%;" class ="none">
								<span class="blue"></span>
							</li>
						</ul>
					</td>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:20%;" class ="none">휴대전화</li>
							<li style="width:80%;" class ="none">
								<span class="blue"></span>
							</li>
						</ul>
					</td>
				</tr>
			</table>
			
			<table class="lineTable" >
				<colgroup>
				<col style="width: 20%">
				<col style="width: 40%">
				<col style="width: 40%">
				</colgroup>
				<tr>
					<td rowspan="6" style="border-right:1px solid #000;">운송사업자<br/>(소속회사)</td>					
				</tr>				
				<tr>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:30%;" class ="none">성명(법인명) </li>
							<li style="width:70%;" class ="none">
								<span class="blue"><?=$ws->co_name?></span>
							</li>
						</ul>
					</td>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:30%; " class ="none">상호(대표자) </li>
							<li style="width:70%;" class ="none">
								<span class="blue"><?=$ws->ceo?></span>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:50%;" class ="none">생년월일(법인등록번호)</li>
							<li style="width:50%;" class ="none">
								<span class="blue"></span>
							</li>
						</ul>
					</td>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:40%;" class ="none">사업자등록번호</li>
							<li style="width:60%;" class ="none">
								<span class="blue"><?=$ws->bs_number?></span>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:30%;" class ="none">연락처(사무실) </li>
							<li style="width:70%;" class ="none">
								<span class="blue"><?=$ws->co_tel?></span>
							</li>
						</ul>
					</td>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:25%;" class ="none">휴대전화 </li>
							<li style="width:75%;" class ="none">
								<span class="blue"></span>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:35%;" class ="none">자동차등록번호 </li>
							<li style="width:65%;" class ="none">
								<span class="blue"><?=$car->car_1?></span>
							</li>
						</ul>
					</td>
					<td>
						<ul class="stepdate" style="width: 100%;" >
							<li style="width:20%;" class ="none">변경일</li>
							<li style="width:80%;" class ="none">
								<span class="blue"><?=date("Y.m.d")?></span>
							</li>
						</ul>
					</td>
				</tr>				
			</table>
			<p>　위․수탁 화물차주 변경 사실을 위와 같이 통보합니다.</p>
			<div class="tright pdt30">
					<ul class="date" style="width:32%;">
						<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
						<li>년</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
						<li>월</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
						<li>일</li>
					</ul>
				</div>
			<div class="tright pdt20">
				<ul class="stepdate" >					
					<li> 통지인 </li>
					<li style="width: 200px;"><span class="blue"><?=$inv->ceo?></span></li>
					<li>(서명 또는 인) </li>							
				</ul>
			</div>
			<p style="border-bottom:3px solid #000;"><strong>울산광역시 구(군)청장</strong>  귀하</p>
			
			<ul>
				<li class="bgcenter">
					유의사항
				</li>
				<li class="bgleft">
				1. 신규는 신규 위·수탁차주, 변경은 변경된 위·수탁 차주, 해지(직영)는 운송사업자가 직접 사실 통지<br/>
				2. 신규 또는 해지(직영)의 경우 위·수탁차주 변경사항 미기재<br/>
				3. 위·수탁계약을 증빙하는 서류(위·수탁계약서 등) 첨부<br/>
				　　- 변경의 경우 현행 및 변경된 위·수탁차주의 계약서를 각각 첨부'<br/>
				</li>					
			</ul>
		</div>
	</div>
	


</div><!-- book 끝 -->
</body>    
</html>