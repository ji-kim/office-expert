<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>

<body>
<div class="book">
<!-- Page 1 위ㆍ수탁계약해지확인서 -->
    <?if($action != null){?>
    <div class="page">
        <div class="subpage">
            <!--인쇄영역-->
            <div id="header">
                <!--span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span-->
                <ul>
                    <!--li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li-->
                    <li class="titCompany"><?=$ws->co_name?></li>
                    <li class="titName">위ㆍ수탁계약해지확인서</li>
                </ul>
            </div>

            <section>
                <div class="confirm">
                    <p><span class="blueD"><?=$ws->co_name?></span> 대표이사 <span class="blueD"><?=$ws->ceo?></span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD"><?=$inv->ceo?></span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD"><?=date("Y")?></span>년 <span class="blueD"><?=date("m")?></span>월 <span class="blueD"><?=date("d")?></span>일 해지하였음을 상호 확인한다.</p>

                    <p class="pdt20"><strong>제1조(계약해지요청자)</strong>
                        “을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>
                    <dl class="pdt20">
                        <dt>제2조(확인서의 효력)</dt>
                        <dd>
                            ① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
                            ② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
                    <p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
                    <dl class="pdt20">
                        <dt>제5조(동시이행)</dt>
                        <dd>
                            ① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
                            ② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제6조(인감증명서 첨부)</strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>

                    <p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
                    <div class="tcenter pdt30">
                        <ul class="date" style="width:32%;">
                            <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
                            <li>년</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
                            <li>월</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
                            <li>일</li>
                        </ul>
                    </div>
                </div>
            </section>

            <section>
                <div class="confirmTable">
                    <table class="table">
                        <colgroup>
                            <col style="width: 5%">
                            <col style="width: 12%">
                            <col style="width: 16%">
                            <col style="width: 16%">
                            <col style="width: 14%">
                            <col style="width: 37%">
                        </colgroup>
                        <tr>
                            <td rowspan="2">갑</td>
                            <td rowspan="2">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
                            <td>사무소</td>
                            <td height="45px"><span class="blue"><?=$ws->co_address?></span></td>
                        </tr>
                        <tr>
                            <td>전화</td>
                            <td height="45px"><span class="blue"><?=$ws->co_tel?></span></td>
                        </tr>
                        <tr>
                            <td rowspan="4">을</td>
                            <td rowspan="4">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
                            <td>차주명</td>
                            <td height="25px"><span class="blue"><?=$inv->ceo?></span></td>
                        </tr>
                        <tr>
                            <td>핸드폰</td>
                            <td height="25px"><span class="blue"></span></td>
                        </tr>
                        <tr>
                            <td rowspan="2">주민등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$car->inv_reg_no?></span></td>
                            <td>차량번호</td>
                            <td height="25px"><span class="blue"><?=$car->car_1?></span></td>
                        </tr>
                        <tr>
                            <td>주소</td>
                            <td height="45px"><span class="blue"><?=$inv->co_address?></span></td>
                        </tr>
                    </table>
                </div>
            </section>
            <!--인쇄영역 끝-->
        </div><!-- subpage 끝 -->
    </div>
	<?}?>
<!-- Page 2 대‧폐차 동의서 -->
	<div class="page">
		<div class="subpage">		
			<div class="agreement">
				<h2 class="pdt30">대․폐차 동의서</h2>
				<table class="table" style="width:90%;margin:0 auto;">
					<colgroup>
					<col style="width: 25%">
					<col style="width: 15%">
					<col style="width: 30%">
					<col style="width: 30%">
				</colgroup>
					<tr>
						<th class="ht50">- 위 탁 차 주 명:</th>
						<th colspan="2"><?=$inv->ceo?></th>
						<th>(인감날인)</th>
					</tr>
					<tr>
						<th class="ht50">- 주민등록번호 :</th>
						<th colspan="3"><?=$inv->reg_number?></th>
					</tr>
					<tr>
						<th class="ht50">- 차 량 번 호 :</th>
						<th colspan="3"><?=$car->car_1?></th>
					</tr>
					<tr>
						<th class="ht50">- 소 속 회 사 </th>
						<th colspan="3"><?=$inv->co_name?></th>
					</tr>
					<tr>
						<th></th>
						<th class="ht50">업체명 :</th>
						<th><?=$ws->co_name?></th>
						<th></th>
					</tr>
					<tr>
						<th></th>
						<th class="ht50">대표자 :</th>
						<th><?=$ws->ceo?></th>
						<th>(인감날인)</th>
					</tr>
					<tr>
						<th colspan="4"><br/><br/><br/><br/><br/>
							<p style="line-height:40px;">　　　　　　　　　　　상기 위탁차주는 위․수탁화물자동차에 대한 운송사업허가업무처리지침 제10조(화물자동차의 대․폐차)에 의거 1대 개별허가 차량으로의 신청이 아닌 화물자동차의 대․폐차(사업용 이전․자가용이전․수출말소․폐차말소․도난말소 등)를 하기 위하여 이 동의서를 제출하오며, 만일 거짓으로 대․폐차 신청시에는 민․형사상 책임을 질 것입니다.</p><br/><br/><br/><br/><br/>
						</th>
					</tr>				
				</table>
				
				<div class="tcenter pdt30 pdb30">
					
					<ul class="date" style="width:32%;">
						<li class="dateYMD ml20"><span class="blue">2017</span></li>
						<li>년</li>
						<li class="dateYMD ml20"><span class="blue">10</span></li>
						<li>월</li>
						<li class="dateYMD ml20"><span class="blue">20</span></li>
						<li>일</li>
					</ul>				
					
					<h3><br/><br/><br/>충청북도화물자동차운송사업협회 이사장 귀하</h3>
					
					<span class="tleft"><br/><br/><strong>※첨부 : 위탁차주(지입차주) 인감증명 1부 (3개월이내 발급분)</strong></span>
				</div>
			</div>			
		</div>		
	</div>
	
<!-- Page 3 신고서 -->
	<div class="page">
		<div class="subpage">
			<div class="title pdb30"><h2 >허  가  사  항  변  경  신  고  서</h2></div>
			<table class="table fontF" style="width: 100%">
				<colgroup>
				<col style="width: 11%">
				<col style="width: 10%">
				<col style="width: 15%">
				<col style="width: 15%">
				<col style="width: 8%">
				<col style="width: 10%">
				<col style="width: 10%">
				<col style="width: 10%">
				</colgroup>
				<tr class="ht30">
					<td>업체명</td>
					<td colspan="3"><span class="blue"><?=$ws->co_name?></span></td>
					<td colspan="2">법인(주민등록)번호</td>
					<td colspan="2"><span class="blue"> <?=$ws->reg_number?> </span></td>
				</tr>
				<tr class="ht30">
					<td>대표자</td>
					<td colspan="3"><span class="blue"> <?=$ws->ceo?> </span></td>
					<td colspan="2">전화번호</td>
					<td colspan="2"><span class="blue"> <?=$ws->co_tel?> </span></td>
				</tr>
				<tr class="ht30">
					<td>주소</td>
					<td colspan="4"><span class="blue"> <?=$ws->co_address?> </span></td>
					<td>fax</td>
					<td colspan="2"><span class="blue"> <?=$ws->fax?> </span></td>
				</tr>
				<tr class="ht30">
					<td>사업의종류</td>
					<td colspan="7">일반화물자동차운송사업</td>
				</tr>
				<tr class="ht30">
					<td>구분</td>
					<td>허가대수</td>
					<td>차량번호</td>
					<td>차명</td>
					<td>년식</td>
					<td colspan="2">차대번호</td>
					<td>적재적량</td>
				</tr>
				<tr class="ht30">
					<td>변경전</td>
					<td><span class="blue"> 1 </span></td>
					<td><span class="blue"> <?=$car->car_1?> </span></td>
					<td><span class="blue"> <?=$car->car_4?> </span></td>
					<td><span class="blue"> <?=$car->car_5?> </span></td>
					<td colspan="2"> <span class="blue"> <?=$car->car_7?> </span></td>
					<td><span class="blue"> <?=$car->max_load?> </span></td>
				</tr>
				<tr class="ht30">
					<td>병경후</td>
					<td><span class="blue"> 1 </span></td>
                    <td><span class="blue"> <?=$car->car_1?> </span></td>
                    <td><span class="blue"> <?=$car->car_4?> </span></td>
                    <td><span class="blue"> <?=$car->car_5?> </span></td>
                    <td colspan="2"> <span class="blue"> <?=$car->car_7?> </span></td>
                    <td><span class="blue"> <?=$car->max_load?> </span></td>
				</tr>
				<tr class="ht30">
					<td>변경사유</td>
					<td colspan="7">
						<ul class="grid4" style="text-align: left;">
							<li>□ 신규등록</li>
							<li>□ 대체등록</li>
							<li>□ 자가용으로 이전</li>
							<li>□ 사업용으로 이전<li>
							<li>□ 폐차말소</li>
							<li>□ 수출말소</li>
							<li>□ 도난말소</li>
							<li>□ 기 타</li>
						</ul>
					</td>
				</tr>
				<tr class="ht30">
					<td colspan="8">
					<p class="tleft">　　화물자동차운수사업법 제46조 및 동법시행령 제2조, 시행규칙 제10조(허가사항의경미한변경)의 허가사항 변경을 신고합니다.
					<p class="inner">
					 첨   부 : ① 페차차량 자동차등록증  ② 폐차차량 자동차등록원부(갑)<br/>
         			　 　 　③ 대체차량 자동차등록증 또는 자동차제작증 ④ 지입차주 대페차동의서<br/>
         			　  　 　⑤지입차주 인감증명서 ⑥ 직영차량-대표자또는차량운전자 국민연금 또는 건강보험 최근 6개월 납입증명서<br/><br/><br/>
					</p>
					<ul class="grid50">
						<li class="tright"><br/>신고인</li>
						<li>
							<dl class="dateDetail">
								<dt>신 고 일 자 : </dt>
								<dd><span class="blue"> <?=date("Y.m.d")?> </span></dd>
								<dt>업　체　명 :</dt>
								<dd><span class="blue"> <?=$ws->co_name?> </span></dd>
								<dt>성　　　명 :</dt>
								<dd><span class="blue"> <?=$ws->ceo?> </span></dd>
							</dl>
							<span class="stamp"><strong>(인)</strong></span>
						</li>
					</ul>
					<p class="tleft pdl10 pdt30">
					 충청북도화물자동차운송사업협회 이사장 귀하
					</p><br/>				
					</td>
				</tr>				
			</table>
			<p class="pdt10">　　충청북도사무의위탁관리 조례제4조에 의거 충청북도지사의 위임을 받아 화물자동차운수사업법 제46조 및 동법시행령 제2조, 시행규칙 제10조의 허가사항변경신고를 확인필함.</p>
			<br/><br/><br/>
			<ul class="tright">
				<li class="pdt20">
					<div class="tright">

							<?=date("Y년 m월 d일")?>

					</div>	
				</li>
				<li class="pdt10">충청북도화물자동차운송사업협회</li>
				<li class="pdt10">이  사  장　　　　　　　민  경  헌</li>
			</ul>
			<br/><br/><br/><br/>
			<p>
				<strong>수수료 입금계좌 : 신한은행 100-005-144396, 농협 381-01-003895</strong><br/>
                  　　　　　　　　　협회원:3,000원/건, 비협회원:30,000원/건
			</p>
			<p class="tright"> *팩스:043)221-4727</p>
			
						
			
		</div>		
	</div>







</div><!-- book 끝 -->
</body>    
</html>