<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>

<body>
<div class="book">
<!-- Page 1 위ㆍ수탁계약해지확인서 -->
    <?if($action != null){?>
    <div class="page">
        <div class="subpage">
            <!--인쇄영역-->
            <div id="header">
                <!--span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span-->
                <ul>
                    <!--li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li-->
                    <li class="titCompany"><?=$ws->co_name?></li>
                    <li class="titName">위ㆍ수탁계약해지확인서</li>
                </ul>
            </div>

            <section>
                <div class="confirm">
                    <p><span class="blueD"><?=$ws->co_name?></span> 대표이사 <span class="blueD"><?=$ws->ceo?></span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD"><?=$inv->ceo?></span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD"><?=date("Y")?></span>년 <span class="blueD"><?=date("m")?></span>월 <span class="blueD"><?=date("d")?></span>일 해지하였음을 상호 확인한다.</p>

                    <p class="pdt20"><strong>제1조(계약해지요청자)</strong>
                        “을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>
                    <dl class="pdt20">
                        <dt>제2조(확인서의 효력)</dt>
                        <dd>
                            ① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
                            ② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
                    <p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
                    <dl class="pdt20">
                        <dt>제5조(동시이행)</dt>
                        <dd>
                            ① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
                            ② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제6조(인감증명서 첨부)</strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>

                    <p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
                    <div class="tcenter pdt30">
                        <ul class="date" style="width:32%;">
                            <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
                            <li>년</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
                            <li>월</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
                            <li>일</li>
                        </ul>
                    </div>
                </div>
            </section>

            <section>
                <div class="confirmTable">
                    <table class="table">
                        <colgroup>
                            <col style="width: 5%">
                            <col style="width: 12%">
                            <col style="width: 16%">
                            <col style="width: 16%">
                            <col style="width: 14%">
                            <col style="width: 37%">
                        </colgroup>
                        <tr>
                            <td rowspan="2">갑</td>
                            <td rowspan="2">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
                            <td>사무소</td>
                            <td height="45px"><span class="blue"><?=$ws->co_address?></span></td>
                        </tr>
                        <tr>
                            <td>전화</td>
                            <td height="45px"><span class="blue"><?=$ws->co_tel?></span></td>
                        </tr>
                        <tr>
                            <td rowspan="4">을</td>
                            <td rowspan="4">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
                            <td>차주명</td>
                            <td height="25px"><span class="blue"><?=$inv->ceo?></span></td>
                        </tr>
                        <tr>
                            <td>핸드폰</td>
                            <td height="25px"><span class="blue"></span></td>
                        </tr>
                        <tr>
                            <td rowspan="2">주민등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$car->inv_reg_no?></span></td>
                            <td>차량번호</td>
                            <td height="25px"><span class="blue"><?=$car->car_1?></span></td>
                        </tr>
                        <tr>
                            <td>주소</td>
                            <td height="45px"><span class="blue"><?=$inv->co_address?></span></td>
                        </tr>
                    </table>
                </div>
            </section>
            <!--인쇄영역 끝-->
        </div><!-- subpage 끝 -->
    </div>
    <?}?>
	
<!-- Page 2 대‧폐차 동의서 -->
	<div class="page">
		<div class="subpage">		
			<div class="gwangju_agreement">
				<h2 class="pdt30">동　　의　　서</h2>
				<table class="table" style="width:90%;margin:0 auto;text-align: left; font-size: 18px;">
					<colgroup>
					<col style="width: 25%">
					<col style="width: 15%">
					<col style="width: 30%">
					<col style="width: 30%">
				</colgroup>
					<tr>
						<th class="ht50 ">회　　사　 명　:</th>
						<th colspan="3"><span class="blue"><?=$inv->co_name?></span></th>
					</tr>
					<tr>
						<th class="ht50">대　　표　 자　:</th>
						<th colspan="3"><span class="blue"><?=$inv->ceo?></span></th>
					</tr>
					<tr>
						<th class="ht50">차량등록번호　:</th>
						<th colspan="3"><span class="blue"><?=$car->car_1?></span></th>
					</tr>
					<tr>
						<th class="ht50">지입차주성명　:</th>
						<th colspan="3"><span class="blue"><?=$inv->ceo?></span></th>
					</tr>					
					<tr>
						<th colspan="4"><br/><br/><br/><br/><br/>
							<p style="line-height:40px;">　　　　　　　　　　　	상기 본인은 위 차량에 대한 화물운송사업 허가사항변			경 (화물자동차의 	대ㆍ폐차)신고에 동의 합니다.</p><br/><br/><br/><br/><br/>
						</th>
					</tr>				
				</table>			
				
				<div class="tcenter">					
					<ul class="date" style="width:36%;">						
						<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
						<li>년</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
						<li>월</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
						<li>일</li>
					</ul>
					<br/><br/><br/><br/><br/>
					<div class="tright">	
						<ul class="stepdate">
							<li>동의인 : </li>
							<li style="width: 200px;">
								<span class="blue"><?=$inv->ceo?></span>
							</li>
							<li style="width: 100px; text-align:left;"> (인)</li>			
						</ul>					
					</div>
					<br/><br/><br/><br/><br/>
					<h3>광주광역시화물자동차운송사업협회 이사장 귀하</h3>
				</div>
			</div>			
		</div>		
	</div>
	
<!-- Page 3 신고서 -->
	<div class="page">
		<div class="subpage">
			<div class="ulsanRepair">
				<table class="table " style="width:100%;">
				<colgroup>
				<col style="width: 5%">
				<col style="width: 13%">
				<col style="width: 10%">
				<col style="width: 18%">
				<col style="width: 12%">
				<col style="width: 20%">
				<col style="width: 22%">
				</colgroup>
				  <tr style="width:100%;">
					<td colspan="7" class="ht60"><h2>화물자동차운송사업허가사항변경(대․폐차) 신청서</h2></td>
				  </tr>
				  <tr>
					<td rowspan="2" >신<br>청<br>인</td>
					<td colspan="2" class="ht40">성　　　　명<br>(법인명 및 대표자)</td>
					<td colspan="2" ><span class="blue"><?=$ws->ceo?></span></td>
					<td>주민등록번호<br>(법인등록번호)</td>
					<td><span class="blue"><?=$ws->reg_number?></span></td>
				  </tr>
				  <tr>
					<td colspan="2" class="ht40">주소</td>
					<td colspan="4"><span class="blue"><?=$ws->co_address?></span></td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">사　업　의　종　류</td>
					<td colspan="4" >일반화물</td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">대　폐　차　기　간</td>
					<td colspan="4">
						<div>
							<ul class="date">
								<li class="dateYMD"><span class="blue"><?=date("Y")?></span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue"><?=date("m")?></span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue"><?=date("d")?></span></li>
								<li>　~　</li>
								<li class="dateYMD"><span class="blue"><?=date("Y")?></span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue"><?=date("m")?></span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue"><?=date("d")?></span></li>
							</ul>
						</div>			
					</td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">차　량　번　호</td>
					<td colspan="4" class="tleft"><?=$car->car_1?></td>
				  </tr>
				  <tr>
					<td colspan="2" class="ht40" rowspan="2">대폐차차량현황</td>
					<td colspan="3" class="ht40">폐 차 차 량</td>
					<td colspan="2" class="ht40">대 차 차 량</td>
				  </tr>
				  <tr>
					<td colspan="3">
						<dl class="grid30" >
							<dt>차　　명:</dt>
							<dd><span class="blue"><?=$car->car_4?></span></dd>
							<dt>차　　종 :</dt>
							<dd><span class="blue"><?=$car->type?></span></dd>
							<dt>유　　형 :</dt>
							<dd><span class="blue"><?=$car->car_3?></span></dd>
							<dt>차량번호 :</dt>
							<dd><span class="blue"><?=$car->car_1?></span></dd>
							<dt>세부유형 :</dt>
							<dd><span class="blue"></span></dd>
							<dt>연　　식 :</dt>
							<dd>
								<?=$car->car_5?>
							</dd>
							<dt>차대번호 :</dt>
							<dd><span class="blue"><?=$car->car_7?></span></dd>
						</dl>	
						<div class="tleft">
							<ul class="stepdate" style="width:100%;">
								<li>최대적재량 또는 총중량: </li>
								<li style="width: 100px;" class="tcenter">	
									<span class="blue"><?=$car->max_load?></span>
								</li>
								<li>kg</li>
							</ul>
						</div>
					</td>
					<td colspan="2">
                        <dl class="grid30" >
                            <dt>차　　명:</dt>
                            <dd><span class="blue"><?=$car->car_4?></span></dd>
                            <dt>차　　종 :</dt>
                            <dd><span class="blue"><?=$car->type?></span></dd>
                            <dt>유　　형 :</dt>
                            <dd><span class="blue"><?=$car->car_3?></span></dd>
                            <dt>차량번호 :</dt>
                            <dd><span class="blue"><?=$car->car_1?></span></dd>
                            <dt>세부유형 :</dt>
                            <dd><span class="blue"></span></dd>
                            <dt>연　　식 :</dt>
                            <dd>
                                <?=$car->car_5?>
                            </dd>
                            <dt>차대번호 :</dt>
                            <dd><span class="blue"><?=$car->car_7?></span></dd>
                        </dl>
                        <div class="tleft">
                            <ul class="stepdate" style="width:100%;">
                                <li>최대적재량 또는 총중량: </li>
                                <li style="width: 100px;" class="tcenter">
                                    <span class="blue"><?=$car->max_load?></span>
                                </li>
                                <li>kg</li>
                            </ul>
                        </div>
					</td>
				  </tr>
				  <tr>
				  	<td colspan="2">대폐차 사유</td>
				  	<td colspan="5" class="tleft ht40">이전</td>
				  </tr>
				  <tr>
					<td colspan="7" class="inner">
						<p class="tleft pdt10">　　화물자동차운수사업법 제3조 제3항 단서 및 동법 시행규칙 제10조의 규정에 의하여 허가사항의 경미한 변경신고(대·폐차)를 합니다.  </p>
						<br/><br/>						
						<div class="tcenter">
							<ul class="date" style="width:32%;">
								<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
								<li>년</li>
								<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
								<li>월</li>
								<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
								<li>일</li>
							</ul>
						</div>	
						<br/><br/><br/>
						<div class="tright">	
							<ul class="stepdate">
								<li>신고인  : </li>
								<li style="width: 200px;">
									<span class="blue"><?=$ws->ceo?></span>
								</li>
								<li style="width: 100px; text-align:left;"> (서명 또는 인)</li>		
							</ul>
						</div>
						<br/><br/><br/>
						<h3>광주광역시화물자동차운송사업협회 이사장 귀하 </h3>
						<br/>
					</td>
				  </tr>
				  <tr>
					<td colspan="7" class="tleft" >
						첨부서류: 대차·폐차 차량의 자동차등록증 사본, 자동차등록원부(갑) 각 1부.<br/>
						　　　　　1. 위수탁차량 경우(위수탁 차주동의서, 차주 인감증명서 각 1부,<br/>
						　　　　　　 차주 유가보조금 사용 내역서 또는 화물복지카드 앞면 복사본 1부)<br/>
						　　　　　2. 직영차량일 경우(운전자 국민연금 또는 의료보험 납부확인서 1부)
				  	</td>
				  </tr>
				</table>
							
			</div>		
		</div>
	</div>







</div><!-- book 끝 -->
</body>    
</html>