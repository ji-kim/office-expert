<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>

<body>
<div class="book">
<!-- Page 1 위ㆍ수탁계약해지확인서 -->
	<div class="page">
		<div class="subpage">
			<!--인쇄영역-->
			<div id="header">
				<span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<ul>
					<li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li>
					<li class="titCompany">주식회사　델타온</li>
					<li class="titName">위ㆍ수탁계약해지확인서</li>				
				</ul>						
			</div>

			<section>
				<div class="confirm">
					<p><span class="blueD">주식회사 델타온</span> 대표이사 <span class="blueD">이광진</span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD">홍길동</span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD">2018</span>년 <span class="blueD">12</span>월 <span class="blueD">22</span>일 해지하였음을 상호 확인한다.</p>

					<p class="pdt20"><strong>제1조(계약해지요청자)</strong>
					“을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>	
					<dl class="pdt20">
						<dt>제2조(확인서의 효력)</dt>
						<dd>
						① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
						② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
					</dl>
					<p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
					“갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
					<p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
					<dl class="pdt20">
						<dt>제5조(동시이행)</dt>
						<dd>
						① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
						② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
					</dl>
					<p class="pdt20"><strong>제6조(인감증명서 첨부)</strong> 
					“갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>
					
					<p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
					<div class="tcenter pdt30">
						<ul class="date" style="width:32%;">
							<li class="dateYMD ml20"><span class="blue">2017</span></li>
							<li>년</li>
							<li class="dateYMD ml20"><span class="blue">10</span></li>
							<li>월</li>
							<li class="dateYMD ml20"><span class="blue">20</span></li>
							<li>일</li>
						</ul>
					</div>					
				</div>				
			</section>	
			
			<section>
				<div class="confirmTable">
					<table class="table">
					<colgroup>
					<col style="width: 5%">
					<col style="width: 12%">
					<col style="width: 16%">
					<col style="width: 16%">
					<col style="width: 14%">
					<col style="width: 37%">
					</colgroup>
					  <tr>
						<td rowspan="2">갑</td>
						<td rowspan="2">(인)</td>
						<td rowspan="2">사업자등록번호</td>
						<td rowspan="2"><span class="blue">104-81-65037</span></td>
						<td>사무소</td>
						<td height="45px"><span class="blue">서울시 서초구 효령로21길 7(방배동)</span></td>
					  </tr>
					  <tr>
						<td>전화</td>
						<td height="45px"><span class="blue">02-2272-1470</span></td>
					  </tr>
					  <tr>
						<td rowspan="4">을</td>
						<td rowspan="4">(인)</td>
						<td rowspan="2">사업자등록번호</td>
						<td rowspan="2"><span class="blue"></span></td>
						<td>차주명</td>
						<td height="25px"><span class="blue">홍 길 동</span></td>
					  </tr>
					  <tr>
						<td>핸드폰</td>
						<td height="25px"><span class="blue">010-0000-0000</span></td>
					  </tr>
					  <tr>
						<td rowspan="2">주민등록번호</td>
						<td rowspan="2"><span class="blue">520122-2141214</span></td>
						<td>차량번호</td>
						<td height="25px"><span class="blue">서울88바7777</span></td>
					  </tr>
					  <tr>
						<td>주소</td>
						<td height="45px"><span class="blue">서울시 동작구 동작길12로 26 파트너빌라 201호</span></td>
					  </tr>
					</table>
				</div>
			</section>		
			<!--인쇄영역 끝-->
		</div><!-- subpage 끝 -->
	</div>
	
<!-- Page 2 (대․폐차)수리동의서 -->
	<!-- Page 2 대‧폐차 동의서 -->
	<div class="page">
		<div class="subpage">
			<!--인쇄영역-->
			<section>
				<table class="agreementDtail">
				  <tr>
					<td colspan="3" class="agreementTit">
						<h1>대‧폐차 동의서</h1>
						<h4>(유가보조금 지급 대상자 확인)</h4>
					</td>
				  </tr>
				  <tr>
					<td width="35%">소속회사</td>
					<td class="alignL" colspan="2"><span class="blue">케이티지엘에스(주)</span></td>
				  </tr>
				  <tr>
					<td>위‧수탁차주명</td>
				   <td class="alignL" colspan="2"><span class="blue">홍길동</span></td>
				  </tr>
				  <tr>
					<td>주민번호</td>
					<td class="alignL" colspan="2"><span class="blue">900103-2345612</span></td>
				  </tr>
				  <tr>
					<td>차량번호</td>
					<td colspan="2">
						<ul>
							<li class="inlineL">서울</li>
							<li class="inlineL"><span class="blue"> 01바2456</span></li>
						</ul>
					</td>
				  </tr>
				  <tr>
					<td class="textBlock1" colspan="3">
						<p class="alignL">위 본인은 상기 회사와 명의신탁 및 위‧수탁 계약을 체결하고 유가보조금 지급 대상자로서 상기 차량에 대한 대‧폐차 업무처리에 동의합니다.</p>
						<p class="attach">붙임 : 위‧수탁 차주 인감증명서 1부</p>
					</td>
				  </tr>
				  <tr>
					<td class="textBlock2" colspan="3">
						<p class="caution">주<br/>의</p>
						<p class="cautionDetail">인감 증명서 사본 첨부시 최대한 연하게 복사하여 인감 모양이 분명하게 식별되어 진위여부가 파악될 수 있도록 처리 바라며, 식별이 불가능하여 인정되지 않을 경우 대‧폐차 수리가 불가 할 수 있음을 알려드립니다.</p>
					</td>
				  </tr>
				  <tr class="date">
					<td class="deft"></td>
					<td class="denter" width="35%"></td>
					<td class="dight" width="35%">
						<ul>
							<li><span class="blue">2019</span></li>
							<li> 년</li>
							<li><span class="blue">1</span></li>
							<li> 월</li>
							<li><span class="blue">14</span></li>
							<li> 일</li>
						</ul>
					</td>
				  </tr>
				  <tr class="signBlock1">
					<td class="seft"></td>
					<td class="senter">
						<span class="tit18">위 위‧수탁차주명</span>
						<br/>
						<span>(※인감날인)</span>
					</td>
					<td class="sight">
						<ul>
							<li class="name">
								<span class="blue">홍길동</span>
							</li>
							<li class="sign">(인)</li>
						</ul>
					</td>
				  </tr>
				  <tr>
					<td class="to" colspan="2">서울화물자동차운송사업협회 귀중</td>
					<td class="signBlock2">
						<div>
						<p class="textBox">
							<span class="tit18">원본대조필</span>
							<br/>
							<span>(회사도장날인)</span>
						</p>
						<p class="signBox">인</p>
						</div>
					</td>
				  </tr>
				</table>
			</section>
			<!--인쇄영역 끝-->
		</div><!-- subpage 끝 -->
	</div>
<!-- Page 3 신고서 -->
	<div class="page">
		<div class="subpage">
			<div class="ulsanRepair">
				<table class="table">
				<colgroup>
				<col style="width: 5%">
				<col style="width: 13%">
				<col style="width: 10%">
				<col style="width: 18%">
				<col style="width: 12%">
				<col style="width: 20%">
				<col style="width: 22%">
				</colgroup>
				  <tr>
					<td colspan="7" class="ht60"><h2>화물자동차운송사업허가사항변경(대․폐차) 신청서</h2></td>
				  </tr>
				  <tr>
					<td rowspan="2" >신<br>고<br>인</td>
					<td colspan="2" class="ht40">성　　　　명<br>(법인명 및 대표자성명)</td>
					<td colspan="2" ><span class="blue">입력</span></td>
					<td>주민등록번호<br>(법인등록번호)</td>
					<td><span class="blue">851020-1234567</span></td>
				  </tr>
				  <tr>
					<td colspan="2" class="ht40">주소</td>
					<td colspan="2"><span class="blue">입력</span></td>
					<td>
						<span class="block bbsolid" >전화번호</span>
						<span class="block">F　A　X</span>
					</td>
					<td>
						<span class="block bbsolid blue">전화번호</span>
						<span class="block blue">F　A　X</span>
					</td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">사　업　의　종　류</td>
					<td colspan="4" class="tleft">일반운송사업</td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">대　폐　차　기　간</td>
					<td colspan="4">
						<div>
							<ul class="date">
								<li class="dateYMD"><span class="blue">2017</span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue">10</span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue">20</span></li>
								<li>　~　</li>
								<li class="dateYMD"><span class="blue">2017</span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue">10</span></li>
								<li>.</li>
								<li class="dateYMD"><span class="blue">20</span></li>
							</ul>
						</div>			
					</td>
				  </tr>
				  <tr>
					<td colspan="3" class="ht40">차　량　번　호</td>
					<td colspan="4" class="tleft">서울　<span class="blueD">88바7777</span></td>
				  </tr>
				  <tr>
					<td colspan="2" class="ht40"></td>
					<td colspan="3">폐 차 차 량</td>
					<td colspan="2">대 차 차 량</td>
				  </tr>
				  <tr>
					<td colspan="2" >⑦대폐차차량현황</td>
					<td colspan="3">
						<dl class="grid30" >
							<dt>차　　명:</dt>
							<dd><span class="blue">입력</span></dd>
							<dt>유　　형 :</dt>
							<dd><span class="blue">입력</span></dd>
							<dt>세부유형 :</dt>
							<dd><span class="blue">입력</span></dd>
							<dt>연　　식 :</dt>
							<dd>
								<div class="tleft">
									<ul class="date" style="display: inline-block;">
										<li class="dateYMD"><span class="blue">2017</span></li>
										<li>.</li>
										<li class="dateYMD"><span class="blue">10</span></li>
										<li>.</li>
										<li class="dateYMD"><span class="blue">20</span></li>
									</ul>	
									<span style="display:inline-block;left;font-size:12px;">(최초등록일)</span>							
								</div>
							</dd>
							<dt>차대번호 :</dt>
							<dd><span class="blue">입력</span></dd>
						</dl>	
						<div class="tleft">
							<ul class="stepdate" style="width:100%;">
								<li>최대적재량 또는 총중량: </li>
								<li style="width: 100px;" class="tcenter">	
									<span class="blue">2019</span>
								</li>
								<li>kg</li>
							</ul>
						</div>
					</td>
					<td colspan="2">
						<dl class="grid30" >
							<dt>차　　명:</dt>
							<dd><span class="blue"></span></dd>
							<dt>유　　형 :</dt>
							<dd><span class="blue"></span></dd>
							<dt>세부유형 :</dt>
							<dd><span class="blue">입력</span></dd>
							<dt>연　　식 :</dt>
							<dd>
								<div class="tleft">
									<ul class="date" style="display: inline-block;">
										<li class="dateYMD"><span class="blue">2017</span></li>
										<li>.</li>
										<li class="dateYMD"><span class="blue">10</span></li>
										<li>.</li>
										<li class="dateYMD"><span class="blue">20</span></li>
									</ul>	
									<span style="display:inline-block;left;font-size:12px;">(최초등록일)</span>							
								</div>
							</dd>
							<dt>차대번호 :</dt>
							<dd><span class="blue">입력</span></dd>
						</dl>	
						
						<div class="tleft">
							<ul class="stepdate" style="width:100%;">
								<li>최대적재량 또는 총중량: </li>
								<li style="width: 100px;" class="tcenter">	
									<span class="blue">2019</span>
								</li>
								<li>kg</li>
							</ul>
						</div>
					</td>
				  </tr>
				  <tr>
					<td colspan="7" class="inner">
						<p class="tleft pdt10">　　◦ 화물자동차운수사업법 제3조제3항 단서 및 동법시행규칙 제10조의 규정에 의한 허가사항의 경미한 변경신고(대․폐차)를 신청합니다. </p>
						<br/><br/>
						
						<div class="tcenter">
							<ul class="date" style="width:32%;">
								<li class="dateYMD ml20"><span class="blue">2017</span></li>
								<li>년</li>
								<li class="dateYMD ml20"><span class="blue">10</span></li>
								<li>월</li>
								<li class="dateYMD ml20"><span class="blue">20</span></li>
								<li>일</li>
							</ul>
						</div>	
					
						<div class="tcenter">							
							<br/><br/>
							<div class="tright">	
								<ul class="stepdate" >
									<li>신고인 　　　회　사　명 :</li>
									<li class="w100"><span class="blue">델타온</span></li>	
									<li class="w100"></li>		
								</ul>
								<br/><br/><br/>
								<div>	
									<ul class="stepdate" >
										<li >대표자명 : </li>
										<li class="w100"><span class="blue">홍길동</span></li>
										<li class="w100"></li>	
									</ul>					
								</div>											
							</div>
							<br/><br/><br/>
						</div>
						<div class="month tleft" >
							<h2>서울특별시화물자동차운송사업협회 귀하</h2>
							<dl class="monthInner02">
								<dd class="tcenter">대·폐차 처리기한</dd>
								<dd class="tcenter">발급일로부터 15일</dd>
							</dl>	
						</div>
					</td>
				  </tr>
				  <tr>
					<td colspan="7" >
						<div class="month tleft" >
							■ 대‧폐차 수수료 : 6,000원 / 입금계좌 : 신한은행 307-05-014200 (예금주:서울화물협회)<br/>
							■ 대폐차 관련 문의 : TEL 02-415-3011~5, FAX 02-413-3562<br/>
							■ 주 소 : 서울 송파구 올림픽로 319, 5층 (신천동, 교통회관)<br/>	
						</div>		
					</td>									
				  </tr>	
				  		  
				</table>
				<table style="margin-top:5px;">
					<tr>
						<td rowspan="2" width="10%;">결<br/>제</td>
						<td rowspan="2" width="70%;"></td>
						<td width="20%;">발급번호</td>
					</tr>
					<tr>	
						<td class="ht70"></td>
					</tr>
					
				</table>
			</div>		
		</div>
	</div>

</div><!-- book 끝 -->
</body>    
</html>