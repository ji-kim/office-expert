<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>

<body>
<div class="book">
<!-- Page 1 위ㆍ수탁계약해지확인서 -->
    <?if($action != null){?>
    <div class="page">
        <div class="subpage">
            <!--인쇄영역-->
            <div id="header">
                <!--span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span-->
                <ul>
                    <!--li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li-->
                    <li class="titCompany"><?=$ws->co_name?></li>
                    <li class="titName">위ㆍ수탁계약해지확인서</li>
                </ul>
            </div>

            <section>
                <div class="confirm">
                    <p><span class="blueD"><?=$ws->co_name?></span> 대표이사 <span class="blueD"><?=$ws->ceo?></span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD"><?=$inv->ceo?></span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD"><?=date("Y")?></span>년 <span class="blueD"><?=date("m")?></span>월 <span class="blueD"><?=date("d")?></span>일 해지하였음을 상호 확인한다.</p>

                    <p class="pdt20"><strong>제1조(계약해지요청자)</strong>
                        “을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>
                    <dl class="pdt20">
                        <dt>제2조(확인서의 효력)</dt>
                        <dd>
                            ① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
                            ② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
                    <p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
                    <dl class="pdt20">
                        <dt>제5조(동시이행)</dt>
                        <dd>
                            ① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
                            ② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제6조(인감증명서 첨부)</strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>

                    <p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
                    <div class="tcenter pdt30">
                        <ul class="date" style="width:32%;">
                            <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
                            <li>년</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
                            <li>월</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
                            <li>일</li>
                        </ul>
                    </div>
                </div>
            </section>

            <section>
                <div class="confirmTable">
                    <table class="table">
                        <colgroup>
                            <col style="width: 5%">
                            <col style="width: 12%">
                            <col style="width: 16%">
                            <col style="width: 16%">
                            <col style="width: 14%">
                            <col style="width: 37%">
                        </colgroup>
                        <tr>
                            <td rowspan="2">갑</td>
                            <td rowspan="2">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
                            <td>사무소</td>
                            <td height="45px"><span class="blue"><?=$ws->co_address?></span></td>
                        </tr>
                        <tr>
                            <td>전화</td>
                            <td height="45px"><span class="blue"><?=$ws->co_tel?></span></td>
                        </tr>
                        <tr>
                            <td rowspan="4">을</td>
                            <td rowspan="4">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
                            <td>차주명</td>
                            <td height="25px"><span class="blue"><?=$inv->ceo?></span></td>
                        </tr>
                        <tr>
                            <td>핸드폰</td>
                            <td height="25px"><span class="blue"></span></td>
                        </tr>
                        <tr>
                            <td rowspan="2">주민등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$car->inv_reg_no?></span></td>
                            <td>차량번호</td>
                            <td height="25px"><span class="blue"><?=$car->car_1?></span></td>
                        </tr>
                        <tr>
                            <td>주소</td>
                            <td height="45px"><span class="blue"><?=$inv->co_address?></span></td>
                        </tr>
                    </table>
                </div>
            </section>
            <!--인쇄영역 끝-->
        </div><!-- subpage 끝 -->
    </div>
    <?}?>
	
<!-- Page 2 허가사항변경신고 -->
	<div class="page">
		<div class="subpage">		
			<div class="incheon">
				<h2 class="pdt30 pdb30 tcenter">허가사항변경신고(위.수탁차주)접수 동의서</h2>
				<table class="table" style="width:90%;margin:0 auto;text-align: left;">
					<colgroup>
					<col style="width: 25%">
					<col style="width: 15%">
					<col style="width: 30%">
					<col style="width: 30%">
				</colgroup>
					<tr>
						<th class="ht40">1. 소　 　  속 :</th>
						<th colspan="3"><span class="blue"><?=$inv->co_name?></span></th>
					</tr>
					<tr>
						<th class="ht40">2.자동차 번호 : </th>
						<th colspan="3"><span class="blue"><?=$car->car_1?></span></th>
					</tr>
					<tr>
						<th class="ht40">3. 차　  　 명 : </th>
						<th colspan="3"><span class="blue"><?=$car->car_4?></span></th>
					</tr>
					<tr>
						<th class="ht40">4.지입차주성명:  </th>
						<th colspan="3"><span class="blue"><?=$inv->ceo?></span></th>
					</tr>					
					<tr>
						<th colspan="4"><br/><br/><br/><br/><br/>
							<p style="line-height:30px;">　　　　　　　　　　　	위 차량은 본인이 현물출자한 차량이나 본인 사정에 의거 위 차량을 (이전,전출,수출,폐차등)하고져 허가사항변경신고서를 소속회사에서 귀 협회에 서류를 접수함에 동의 하오니 처리 하여 주시기 바랍니다.</p><br/><br/><br/><br/><br/><br/>
						</th>
					</tr>				
				</table>
				
				<div class="tright" style="width:90%;margin:0 auto;">					
					<ul class="date" style="width:38%;">						
						<li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
						<li>년</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
						<li>월</li>
						<li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
						<li>일</li>
					</ul>
					<br/><br/><br/><br/><br/><br/>
					<div class="tright">	
						<ul class="stepdate">
							<li style="font-size:14px;">동의자 지입 차주 성명</li>
							<li style="width: 200px;">
								<span class="blue"><?=$inv->ceo?></span>
							</li>
							<li style="font-size:14px;">(인)인감도장날인</li>			
						</ul>					
					</div>
					<br/><br/><br/><br/><br/><br/><br/>
				</div>
				<h3 style="width:90%;margin:0 auto;">인천시화물자동차운송사업협회 귀중</h3>
			</div>			
		</div>		
	</div>
	
<!-- page 3 변경신고서 -->
	<div class="page">
		<div class="subpage">
			<div>
				<table class="" style="width: 100%">
				<colgroup>
					<col style="width: 15%">
					<col style="width: 6%">
					<col style="width: 15%">
					<col style="width: 6%">
					<col style="width: 15%">
					<col style="width: 6%">
					<col style="width: 15%">
					<col style="width: 6%">
					<col style="width: 15%">
				</colgroup>
				<tr>
					<td class="gbgray" colspan="9">처리절차</td>
				</tr>
				<tr>
					<td class="ht40">신고서 작성</td>
					<td><span class="arrow">▶</span></td>
					<td>접수</td>
					<td><span class="arrow">▶</span></td>
					<td>검토</td>
					<td><span class="arrow">▶</span></td>
					<td>수리</td>
					<td><span class="arrow">▶</span></td>
					<td>통보</td>
				</tr>
				<tr>
					<td>신고인</td>
					<td colspan="8">특별시ㆍ광역시ㆍ도ㆍ특별자치도 화물자동차 운송사업 협회</td>
				</tr>
				<tr>
					<td colspan="9" class="tright">210mm×297mm[일반용지 60g/㎡(재활용품)]</td>
				</tr>
				</table>
			</div>
		</div>
	</div>

<!-- Page 4 대·폐차 구비서류 -->
	<div class="page">
		<div class="subpage">
			<h2>대·폐차 구비서류 (협회 가입 법인)</h2>
			<div class="pdt30" style="line-height: 25px;" >
				<h4>가. 동시 대·폐차 일 경우 (대·폐차 기간 15일)</h4>
				1. 허가사항 변경 신고서 (법인 인감도장 날인)<br/>
				2. 폐차할 차랑 등록증 사본 1부<br/>
				3. 폐차할 차량 등록원부(갑) 1부 (7일 이내 발급본)<br/>
				4. 지입일 경우 -> 지입 차주 인감증명서 1부 (3개월 이내 발급본)<br/>
				　　　　　　　　  지입 차주 동의서 (인감도장 날인)<br/>
				  　 직영일 경우 -> 4대보험 납부확인서<br/>
				　　　　　　　　  (법인명, 납부자명이 들어간 것으로 6개월 내역본)<br/>
				5. 대차 할 차량 등록증 사본 1부 (대차 할 차량이 신조차일 경우 제작증) <br/> 
				6. 대차 할 차량 등록원부 사본 1부 (7일 이내 발급본)<br/>
				7. 대차할 차량이 폐차할 차량보다 연식이 낮을 경우 양쪽 법인에 대한 위수탁 계약서 첨부 
			</div>
			<div class="pdt30" style="line-height: 25px;" >
				<h4>나. 차량이전 (폐차 등) 만 할 경우 (대폐차 기간 3개월) - ※위수탁 해지의 경우만 가능</h4>
				1. 허가사항 변경 신고서 (법인 인감도장 날인)<br/>
				2. 폐차 할 차량 등록증 사본 1부<br/>
				3. 폐차 할 차량 등록원부(갑) 1부 (7일이내 발급본)<br/>
				4. 지입 차주 인감증명서 1부 (3개월 이내 발급본)<br/>
				5. 지입 차주 동의서 (인감도장 날인)<br/>
				6. 위수탁 해지 확인서 사본 1부 (법인인감도장, 차주인감도장 날인)<br/>
				7. 법인 인감증명서 사본 1부
			</div>
			<div class="pdt30" style="line-height: 25px;" >
				<h4>다. 차량이전 (폐차 등) 완료 후 대차만 할 경우 - ※위수탁 해지의 경우만 가능</h4>
				1. 허가사항 변경 신고서 (법인 인감도장 날인)<br/>
				2. 폐차 할 차량 등록증 사본 1부<br/>
				3. 폐차 할 차량 등록원부(갑) 1부 (7일이내 발급본)<br/>
				4. 지입 차주 인감증명서 1부 (3개월 이내 발급본)<br/>
				5. 지입 차주 동의서 (인감도장 날인)<br/>
				6. 위수탁 해지 확인서 사본 1부 (법인인감도장, 차주인감도장 날인)<br/>
				7. 법인 인감증명서 사본 1부
			</div>
			<br/><br/><br/>
			<strong>※ 수수료 : 5,000원</strong>
		</div>
	</div>







</div><!-- book 끝 -->
</body>    
</html>