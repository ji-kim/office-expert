<!DOCTYPE HTML>
<html lang="ko">
<head>
<title>DELEX OFFICE</title>
<link href="<?=base_url()?>/assets/css/daepae/common.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/assets/css/daepae/style.css" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//cdn.jsdelivr.net/font-iropke-batang/1.2/font-iropke-batang.css">-->
</head>
<body>
<div class="book">
<!-- Page 1/4 위ㆍ수탁계약해지확인서 -->
    <?if($action != null){?>
    <div class="page">
        <div class="subpage">
            <!--인쇄영역-->
            <div id="header">
                <!--span class="logoLeft"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span>
				<span class="logoRight"><img src="<?=base_url()?>/assets/img/logo_deltaon.jpg"></span-->
                <ul>
                    <!--li class="titSmall">글로벌 물류 혁신기업 세계를 향해 도전하는 기업</li-->
                    <li class="titCompany"><?=$ws->co_name?></li>
                    <li class="titName">위ㆍ수탁계약해지확인서</li>
                </ul>
            </div>

            <section>
                <div class="confirm">
                    <p><span class="blueD"><?=$ws->co_name?></span> 대표이사 <span class="blueD"><?=$ws->ceo?></span>(이하 “갑”이라 한다)와 위ㆍ수탁차주 <span class="blueD"><?=$inv->ceo?></span>(이하 “을”이라한다)는 아래와 같이 위ㆍ수탁계약을 <span class="blueD"><?=date("Y")?></span>년 <span class="blueD"><?=date("m")?></span>월 <span class="blueD"><?=date("d")?></span>일 해지하였음을 상호 확인한다.</p>

                    <p class="pdt20"><strong>제1조(계약해지요청자)</strong>
                        “을”의 사정에 의해 “을”이 계약을 해지요청하는 경우에 한한다.</p>
                    <dl class="pdt20">
                        <dt>제2조(확인서의 효력)</dt>
                        <dd>
                            ① “을”의 계약해지 요청에 대해 “갑”이 동의한 경우에 한해 효력이 발생한다.<br/>
                            ② “갑”이 “을”의 의사에 반하거나 이를 속이고 받은 인감날인과 인감증명서를 제출하여 위ㆍ수탁계약해지확인서를 작성한 경우 동 확인서는 무효로 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제3조(유가보조금 지급정지신청) </strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 관할관청에 유가보조금 지급정지신청을 하고 이를 “을”에게 고지한다.</p>
                    <p class="pdt20"><strong>제4조(현물출자자 기재말소) </strong> “을”은 위ㆍ수탁계약해지확인서를 작성하고, 폐차말소한 경우 차량번호에 기재된 현물출자 기재를 말소하여야 한다.</p>
                    <dl class="pdt20">
                        <dt>제5조(동시이행)</dt>
                        <dd>
                            ① “갑”과 “을”은  위ㆍ수탁계약해지확인서를 작성한 경우 “갑”은 “을”의 차량을 폐차할 수 있도록 관련서류를 제공하고 “을”은 “갑”에 대한 채무(미납금 등)를 이행하여야 한다.<br/>
                            ② 제1항의 사항이 이행된 경우 “을”은 대폐차를 위한 차주동의서를 “갑”에게 제출하고, “갑”은 관할협회에 대폐차신청을 하여야 한다.</dd>
                    </dl>
                    <p class="pdt20"><strong>제6조(인감증명서 첨부)</strong>
                        “갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</p><br/>

                    <p class="pdt20">　위와 같이 위ㆍ수탁계약해지가 유효하게 성립하였음을 양 당사자가 확인하면서 본 확인서 2통을 작성하여 가각 서명(또는 기명)날인 후 “갑”과 “을”이 각각 1통씩 보관한다.</p>
                    <div class="tcenter pdt30">
                        <ul class="date" style="width:32%;">
                            <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
                            <li>년</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
                            <li>월</li>
                            <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
                            <li>일</li>
                        </ul>
                    </div>
                </div>
            </section>

            <section>
                <div class="confirmTable">
                    <table class="table">
                        <colgroup>
                            <col style="width: 5%">
                            <col style="width: 12%">
                            <col style="width: 16%">
                            <col style="width: 16%">
                            <col style="width: 14%">
                            <col style="width: 37%">
                        </colgroup>
                        <tr>
                            <td rowspan="2">갑</td>
                            <td rowspan="2">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$ws->bs_number?></span></td>
                            <td>사무소</td>
                            <td height="45px"><span class="blue"><?=$ws->co_address?></span></td>
                        </tr>
                        <tr>
                            <td>전화</td>
                            <td height="45px"><span class="blue"><?=$ws->co_tel?></span></td>
                        </tr>
                        <tr>
                            <td rowspan="4">을</td>
                            <td rowspan="4">(인)</td>
                            <td rowspan="2">사업자등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
                            <td>차주명</td>
                            <td height="25px"><span class="blue"><?=$inv->ceo?></span></td>
                        </tr>
                        <tr>
                            <td>핸드폰</td>
                            <td height="25px"><span class="blue"></span></td>
                        </tr>
                        <tr>
                            <td rowspan="2">주민등록번호</td>
                            <td rowspan="2"><span class="blue"><?=$car->inv_reg_no?></span></td>
                            <td>차량번호</td>
                            <td height="25px"><span class="blue"><?=$car->car_1?></span></td>
                        </tr>
                        <tr>
                            <td>주소</td>
                            <td height="45px"><span class="blue"><?=$inv->co_address?></span></td>
                        </tr>
                    </table>
                </div>
            </section>
            <!--인쇄영역 끝-->
        </div><!-- subpage 끝 -->
    </div>
    <?}?>
<!-- Page 2/4_대ㆍ폐차 동의서 -->
	<div class="page">
		<div class="subpage">
			<!--인쇄영역-->
			<section>
				<table class="agreementDtail">
				  <tr>
					<td colspan="3" class="agreementTit">
						<h1>대‧폐차 동의서</h1>
						<h4>(유가보조금 지급 대상자 확인)</h4>
					</td>
				  </tr>
				  <tr>
					<td width="30%">소속회사</td>
					<td class="tleft" colspan="2"><span class="blue"><?=$inv->co_name?></span></td>
				  </tr>
				  <tr>
					<td>위‧수탁차주명</td>
				   <td class="tleft" colspan="2"><span class="blue"><?=$inv->ceo?></span></td>
				  </tr>
				  <tr>
					<td>주민번호</td>
					<td class="tleft" colspan="2"><span class="blue"><?=$inv->bs_number?></span></td>
				  </tr>
				  <tr>
					<td>차량번호</td>
					<td colspan="2">
						<ul class="ulAlignL">
							<li class="inline"><?=$car->car_1?></li>
						</ul>
					</td>
				  </tr>
				  <tr>
					<td class="textBlock1" colspan="3">
						<p class="tleft">위 본인은 상기 회사와 명의신탁 및 위‧수탁 계약을 체결하고 유가보조금 지급 대상자로서 상기 차량에 대한 대‧폐차 업무처리에 동의합니다.</p>
						<p class="attach">붙임 : 위‧수탁 차주 인감증명서 1부</p>
					</td>
				  </tr>
				  <tr>
					<td class="textBlock2" colspan="3">
						<p class="caution">주</br>의</p>
						<p class="cautionDetail">인감 증명서 사본 첨부시 최대한 연하게 복사하여 인감 모양이 분명하게 식별되어 진위여부가 파악될 수 있도록 처리 바라며, 식별이 불가능하여 인정되지 않을 경우 대‧폐차 수리가 불가 할 수 있음을 알려드립니다.</p>
					</td>
				  </tr>
				  <tr>
					<td class="dight bdTB0" colspan="3">
						<div class="tright">
							<ul class="date ulAlignR" style="width:40%;">
							  <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
							  <li>년</li>
							  <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
							  <li>월</li>
							  <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
							  <li>일</li>
							</ul>
						</div>					
					</td>
				  </tr>
				  <tr class="signBlock1">
					<td class="seft"></td>
					<td class="senter bdR0">
						<span class="tit18">위 위‧수탁차주명</span>
						</br>
						<span>(※인감날인)</span>
					</td>
					<td class="sight bdL0 bdTB0">
						<ul>
							<li class="inlineL name">
								<span class="blue"><?=$inv->ceo?></span>
							</li>
							<li class="inlineL sign">(인)</li>
						</ul>
					</td>
				  </tr>
				  <tr>
					<td class="to" colspan="2">서울화물자동차운송사업협회 귀중</td>
					<td class="signBlock2">
						<div>
						<p class="textBox">
							<span class="tit18">원본대조필</span>
							</br>
							<span>(회사도장날인)</span>
						</p>
						<p class="signBox">인</p>
						</div>
					</td>
				  </tr>
				</table>
			</section>
			<!--인쇄영역 끝-->
		</div><!-- subpage 끝 -->
	</div>
<!-- Page 3/4 위임장 -->
	<div class="page">
		<div class="subpage">
			<!--인쇄영역-->
			<p class="hstyle0 lineh110per alignC"></p>
			<div class="powerOfAttorney hstyle0 lineh110per alignC">
				<table border="1" cellspacing="0" cellpadding="0" class="plsAttach">
					<tr>
						<td colspan="5" width="100%" height="71" valign="middle" class="hstyle0alignC">
							<p class="plsAttachTit">&nbsp;위&nbsp; 임&nbsp; 장</p>
						</td>
					</tr>
					<tr>
						<td rowspan="3" width="24%" height="200" valign="middle">
							<p class="hstyle0alignC"><span class="font12">차량소유자</span></p>
							<p class="hstyle0alignC"><span class="font12">(운송사업자)</span></p>
						</td>
						<td width="19%" height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font13">성&nbsp;&nbsp;&nbsp;&nbsp; 명</span></p>
						</td>
						<td width="19%" height="58" valign="middle" class="owner">
							<p class="hstyle0alignC"><span class="blue font13"><?=$inv->ceo?></span></p>
						</td>
						<td width="19%" height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font12">주민등록번호</span></p>
						</td>
						<td width="19%" height="58" valign="middle" class="residentRegistrationNumber">
							<p class="hstyle0alignC"><span class="blue font12"><?=$inv->reg_number?></span></p>
						</td>
					</tr>
					<tr>
						<td height="83" valign="middle">
							<p class="hstyle0alignC"><span class="font13">주&nbsp;&nbsp;&nbsp;&nbsp;소</span></p>
							<p class="hstyle0alignC"><span class="font12">(사업장소재지)</span></p>
						</td>
						<td colspan="3" height="83" valign="middle" class="address">
							<p class="hstyle0alignC"><span class="blue font12"><?=$inv->co_address?></span></p>
						</td>
					</tr>
					<tr>
						<td height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font13">차량번호</span></p>
						</td>
						<td height="58" valign="middle" class="plateNumber">
							<p class="hstyle0alignC"><span class="blue font13"><?=$car->car_1?></span></p>
						</td>
						<td height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font13">연 락 처</span></p>
						</td>
						<td height="58" valign="middle" class="phoneNumberF">
							<p class="hstyle0alignC"><span class="blue font11"><?=$inv->co_tel?></span></p>
						</td>
					</tr>
					<tr>
						<td colspan="5" width="100%" height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font13">상기 본인은 아래&nbsp; 사람을 대리인으로 정하여 다음사항의 권한을 위임합니다.</span></p>
						</td>
					</tr>
					<tr>
						<td rowspan="3" width="24%" height="200" valign="middle">
							<p class="hstyle0alignC"><span class="font13">대 리 인</span></p>
						</td>
						<td width="19%" height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font13">성&nbsp;&nbsp;&nbsp;&nbsp; 명</span></p>
						</td>
						<td width="19%" height="58" valign="middle" class="agentName">
							<p class="hstyle0alignC"><span class="blue font13"><?=$ws->ceo?></span></p>
						</td>
						<td width="19%" height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font12">주민등록번호</span></p>
						</td>
						<td width="19%" height="58" valign="middle" class="agentNum">
							<p class="hstyle0alignC"><span class="blue font12"><?=$ws->bs_number?></span></p>
						</td>
					</tr>
					<tr>
						<td height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font13">주&nbsp;&nbsp;&nbsp;&nbsp; 소</span></p>
						</td>
						<td colspan="3" height="58" valign="middle" class="agentAdd">
							<p class="hstyle0alignC"><span class="blue font12"><?=$ws->co_address?></span></p>
						</td>
					</tr>
					<tr>
						<td height="83" valign="middle">
							<p class="hstyle0alignC"><span class="font13">소유자와</span></p>
							<p class="hstyle0alignC"><span class="font13">관&nbsp;&nbsp;&nbsp; 계</span></p>
						</td>
						<td height="83" valign="middle" class="convention">
							<p class="hstyle0alignC"><span class="blue font13">계약관계</span></p>
						</td>
						<td height="83" valign="middle">
							<p class="hstyle0alignC"><span class="font13">연 락 처</span></p>
						</td>
						<td height="83" valign="middle" class="phoneNumberT">
							<p class="hstyle0alignC"><span class="blue font11"><?=$ws->co_tel?></span></p>
						</td>
					</tr>
					<tr>
						<td colspan="5" width="100%" height="58" valign="middle">
							<p class="hstyle0alignC"><span class="font14">위&nbsp;&nbsp; 임&nbsp;&nbsp; 내&nbsp;&nbsp; 용</span></p>
						</td>
					</tr>
					<tr>
						<td colspan="5" width="100%" height="138" valign="middle">
							<p class="hstyle0"><span class="font13">화물자동차 운송사업 (상호의변경,대표자의 변경,화물취급소의 설치</span></p>
							<p class="hstyle0"><span class="font13">또는 폐지, 대·폐자 ,주사무소 이전, 영업소 이전, 화물취급소 이전,</span></p>
							<p class="hstyle0"><span class="font13">기타———————)변경신고 사항을 위임함.(신청민원에 〇표시)</span></p>
						</td>
					</tr>
					<tr>
						<td colspan="5" width="100%" height="150" valign="middle">
							<div class="tcenter AttyDate">
								<ul class="date" style="width:32%;">
								  <li class="dateYMD ml20"><span class="blue"><?=date("Y")?></span></li>
								  <li>년</li>
								  <li class="dateYMD ml20"><span class="blue"><?=date("m")?></span></li>
								  <li>월</li>
								  <li class="dateYMD ml20"><span class="blue"><?=date("d")?></span></li>
								  <li>일</li>
								</ul>
							</div>					
							<p height="40px">&nbsp;</p>
							<div class="tright">
								<ul class="ulAlignR pdR30">
								  <li>신청인</li>
								  <li class="pdl10 agentSign"><span class="blue"><?=$inv->ceo?></span></li>
								  <li class="pdl10">(인)</li>
								</ul>
							</div>					
						</td>
					</tr>
					<tr>
						<td colspan="5" width="100%" height="56" valign="middle">
							<p class="hstyle0alignC"><strong class="font14">*증빙서류: 대리인의 신분증 사본</strong></p>
						</td>
					</tr>
				</table>
			</div>
			<!--인쇄영역 끝-->
		</div><!-- subpage 끝 -->
	</div>
<!-- Page 4/4 화물자동차 운송사업 허가사항 변경신고서 -->
	<div class="page">
		<div class="subpage positionR">
		<!--인쇄영역-->
		<div class="changeNotice">
			<!-- 별지서식/화물자동차 운송사업 허가사항 변경신고서 -->
			<table border="0" cellspacing="0" cellpadding="0" class="changeNoticeTable">
				<tr>
					<td colspan="16" width="100%" height="42" valign="top" class="topLine">
						<p class="hstyle0"><span class="fontS">■ 화물자동차 운수사업법 시행규칙 [별지 제8호서식] &lt;개정 2010.12.29&gt;</span></p>
					</td>
				</tr>
				<tr>
					<td colspan="16" width="100%" height="36" valign="middle" class="topLine">
						<p class="hstyle0alignC lineh200per changeNoticeTit">화물자동차 운송사업 허가사항 변경신고서</p>
					</td>
				</tr>
				<tr>
					<td colspan="16" width="100%" height="21" valign="middle" class="topMargin"></td>
				</tr>
			</table>
			<!-- 접수번호/접수일/발급일/처리기간 -->
			<table border="0" cellspacing="0" cellpadding="0" class="changeNoticeTable1" width="100%">
				<tr class="topInfo">
					<td width="9%" height="34" valign="top" bgcolor="#bbbbbb" class="receiptTit">
						<p class="hstyle0 line90"><span class="lineh90">접수번호</span></p>
					</td>
					<td width="21%" height="36" valign="middle" bgcolor="#bbbbbb" class="receiptNumber receiptD">
						<p class="hstyle0"><strong class="lineh160"><span class="blue"></span></strong></p>
					</td>
					<td colspan="2" width="9%" height="34" valign="top" bgcolor="#bbbbbb" class="Date">
						<p class="hstyle0 lineh90"><span class="lineh90">접수일</span></p>
					</td>
					<td colspan="2" width="14%" height="34" valign="top" bgcolor="#bbbbbb" class="receiptDate receiptD">
                        <p class="hstyle0"><span class="blue lineh160"><?=date("Y.m.d")?></span></p>
					</td>
					<td colspan="3" width="9%" height="34" valign="top" bgcolor="#bbbbbb" class="date">
						<p class="hstyle0 lineh90"><span class="lineh90">발급일</span></p>
					</td>
					<td colspan="4" width="14%" height="34" valign="middle" bgcolor="#bbbbbb" class="issueDate receiptD">
						<p class="hstyle0"><span class="blue lineh160"><?=date("Y.m.d")?></span></p>
					</td>
					<td colspan="2" width="10%" height="34" valign="top" bgcolor="#bbbbbb" class="Date">
						<p class="hstyle0"><span class="lineh160">처리기간 </span><strong class="lineh160"></strong></p>
					</td>
					<td width="13%" height="34" valign="top" bgcolor="#bbbbbb" class="processingPeriod receiptTit">
						<p class="hstyle0"><span class="lineh160">즉시</span></p>
					</td>
				</tr>
			</table>
			<!-- 신고 -->
			<p class="margin8">　</p>
			<table border="0" cellspacing="0" cellpadding="0" class="changeNoticeTable2" width="100%">
				<tr>
					<td rowspan="3" width="12%" height="" class="bdR">
						<p class="hstyle0alignC line90"><span class="lineh90">신고</span></p>
					</td>
					<td width="26%" height="37" valign="top" class="bdBg">
						<p class="hstyle0 pdL5"><span class="lineh160">성명(법인명 및 대표자 성명)</span></p>
					</td>
					<td width="16%" height="37" valign="middle" class="bdRg bdBg">
						<p class="hstyle0 pdL5 lineh90"><span class="lineh90 blue"><?=$ws->ceo?></span></p>
					</td>
					<td width="26%" height="37" valign="top" class="bdBg">
						<p class="hstyle0 pdL5"><span class="lineh160">주민등록번호(법인등록번호)</span></p>
					</td>
					<td width="22%" height="37" valign="middle" class="bdBg">
						<p class="hstyle0 pdL5 lineh90"><span class="lineh90 blue"><?=$ws->reg_number?></span></p>
					</td>
				</tr>
				<tr>
					<td height="34" valign="top" class="bdBg">
						<p class="hstyle0 pdL5"><span class="lineh160">전화번호;</span></p>
					</td>
					<td height="34" valign="middle" colspan="3" class="bdBg">
						<p class="hstyle0 pdL5 lineh90"><span class="lineh90 blue"><?=$ws->co_tel?></span></p>
					</td>
				</tr>
				<tr>
					<td height="34" valign="top">
						<p class="hstyle0 pdL5"><span class="lineh160">주소;</span></p>
					</td>
					<td height="34" valign="middle" colspan="3">
						<p class="hstyle0 pdL5 lineh90"><span class="lineh90 blue"><?=$ws->co_address?></span></p>
					</td>
				</tr>
			</table>
			<!-- 신고내용/변경신고 -->
			<p class="margin8">&nbsp;</p>
			<table border="0" cellspacing="0" cellpadding="0" class="changeNoticeTable3" width="100%">
				<tr>
					<td rowspan="5" width="12%" height="" class="bdR bdB">
						<p class="hstyle0alignC line90"><span class="lineh90">신고내용</span></p>
					</td>
					<td width="14%" height="27" valign="top" class="bdB">
						<p class="hstyle0 pdL5"><span class="lineh160">사업의 종류</span></p>
					</td>
					<td width="22%" height="27" valign="middle" class="bdB">
						<p class="hstyle0 pdL5 lineh90"><span class="lineh90 blue"></span></p>
					</td>
					<td width="23%" height="27" valign="top" class="bdB">
						<p class="hstyle0 pdL5"><span class="lineh160">차량번호</span></p>
					</td>
					<td width="45%" height="27" valign="middle" class="bdB" colspan="2">
						<p class="hstyle0 pdL5 lineh90"><span class="lineh90 blue"><?=$car->car_1?></span></p>
					</td>
				</tr>

				<tr>
					<td height="27" valign="top" class="bdB">
						<p class="hstyle0 pdL5"><span class="lineh160">대폐차기간</span></p>
					</td>
					<td height="27" valign="middle" class="bdB" colspan="4">
						<p class="hstyle0 pdL5 lineh90"><span class="lineh90 blue"><?=date("Y.m.d"). " ~ " . date("Y.m.d")?></span></p>
					</td>
				</tr>
				<tr>
					<td height="189" valign="middle" class="bdR bdB" rowspan="2">
						<p class="hstyle0alignC">변</br>경</br>사</br>항</p>
					</td>
					<td height="29" valign="top" class="bdB bdRg" colspan="2">
						<p class="hstyle0 pdL5"><span class="lineh160">변경전</span></p>
					</td>
					<td height="29" valign="top" class="bdB" colspan="2">
						<p class="hstyle0 pdL5"><span class="lineh160">변경후</span></p>
					</td>
				</tr>
				<tr>
					<td height="160" valign="top" colspan="2" class="bdRg bdB hstyle0 lineh32">
						<ul class="ulAlignL"><li class="pdL5">차명:</li><li class="pdL5"><span class="blue"> <?=$car->car_4?></span></li></ul>
						<ul class="ulAlignL"><li class="pdL5">차종:</li><li class="pdL5"><span class="blue"> <?=$car->type?></span></li></ul>
						<ul class="ulAlignL"><li class="pdL5">유형:</li><li class="pdL5"><span class="blue"> <?=$car->car_3?></span></li></ul>
						<ul class="ulAlignL"><li class="pdL5">세부유형:</li><li class="pdL5"><span class="blue"> </span></li></ul>
						<ul class="ulAlignL"><li class="pdL5">차대번호:</li><li class="pdL5"><span class="blue"> <?=$car->car_7?></span></li></ul>
						<ul class="ulAlignL date">
							 <li class="pdL5">연식:<?=$car->car_5?>(최초등록일)</li>
						</ul>
						<ul class="ulAlignL"><li class="pdL5">최대적재량 또는총중량:</li><li class="pdL5"><span class="blue"><?=$car->max_load?></span></li><li class="lineh160 pdl10">kg</li></ul>
					</td>
					<td height="160" valign="top" colspan="2" class="bdRg bdB hstyle0 lineh32">
                        <ul class="ulAlignL"><li class="pdL5">차명:</li><li class="pdL5"><span class="blue"> <?=$car->car_4?></span></li></ul>
                        <ul class="ulAlignL"><li class="pdL5">차종:</li><li class="pdL5"><span class="blue"> <?=$car->type?></span></li></ul>
                        <ul class="ulAlignL"><li class="pdL5">유형:</li><li class="pdL5"><span class="blue"> <?=$car->car_3?></span></li></ul>
                        <ul class="ulAlignL"><li class="pdL5">세부유형:</li><li class="pdL5"><span class="blue"> </span></li></ul>
                        <ul class="ulAlignL"><li class="pdL5">차대번호:</li><li class="pdL5"><span class="blue"> <?=$car->car_7?></span></li></ul>
                        <ul class="ulAlignL date">
                            <li class="pdL5">연식:<?=$car->car_5?>(최초등록일)</li>
                        </ul>
                        <ul class="ulAlignL"><li class="pdL5">최대적재량 또는총중량:</li><li class="pdL5"><span class="blue"><?=$car->max_load?></span></li><li class="lineh160 pdl10">kg</li></ul>
					</td>
				</tr>
				<tr>
					<td height="51" valign="top" class="pdL5 bdB"><p class="hstyle0">변경 사유;</p></td>
					<td height="51" valign="top" colspan="4" class="pdL5 tleft bdB"><strong class="font12">화물자동차 대 · 폐차</strong></td>
				</tr>
				<!-- 신고내용End -->
				<tr>
					<td valign="middle" colspan="6" class="textBlock3">
						<p class="tleft pdb60">&nbsp;&nbsp;&nbsp; 「화물자동차 운수사업법」 제3조제3항 단서 및 같은 법 시행규칙 제10조에 따라 허가사항의 경미한 변경을 신고합니다.</p>
						<div class="pdb30">
							<ul class="date_ ulAlignR">
							  <li class="dateYMD"><span class="blue"><?=date("Y")?></span></li>
							  <li>년</li>
							  <li class="dateYMD"><span class="blue"><?=date("m")?></span></li>
							  <li>월</li>
							  <li class="dateYMD"><span class="blue"><?=date("d")?></span></li>
							  <li>일</li>
							</ul>
						</div>					
						<div class="clear 3declarant pdb60">
							<ul class="ulAlignR">
								<li>&nbsp;<!-- 여백 --></li>
								<li>신고인</li>
								<li class="pdl10"><span class="blue"><?=$ws->ceo?></span></li>
							</ul>
						</div>
						<p class="clear tleft">
							<strong class="font13 pdl10">특별시시ㆍ광역시ㆍ도ㆍ특별자치도 화물자동차 운송사업 협회장 </strong>
							<span class="inlineSpan font11"> 귀하</span>
						</p>
					</td>
				</tr>
				<!-- 변경신고End -->
			</table>
			<!-- 첨부서류 -->
			<p class="margin59">&nbsp;</p>
			<table border="0" cellspacing="0" cellpadding="0" class="changeNoticeTable4" width="100%">
				<tr>
					<td width="12%" height="" rowspan="2" class="bdT bdR">
						<p class="alignC">첨부서류</p>
					</td>
					<td width="58%" height="18" valign="middle" rowspan="2" class="bdT bdB bdR">
						<p class="tleft pdL5">신ㆍ구 허가사항을 대조한 서류 및 도면 각 1부</p>
					</td>
					<td width="30%" height="18" valign="middle" class="bdT bdB">
						<p class="alignC">수수료</p>
					</td>
				</tr>
				<tr>
					<td height="32" valign="top" class="bdB">
						<p class="alignC">화물자동차 운송사업 협회장이 정하는 금액</p>
					</td>
				</tr>
				<tr>
					<td height="21" valign="middle" class="bdT2 bdB"colspan="3" bgcolor="#bbbbbb">&nbsp;</td>
				</tr>
			</table>
		</div>
		<!--인쇄영역 끝-->
		</div><!-- subpage 끝 -->
	</div><!-- page 끝 -->
</div><!-- book 끝 -->
</body>    
</html>