<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
$created = can_action('122', 'created');
$edited = can_action('122', 'edited');
$deleted = can_action('122', 'deleted');
?>
<div class="row ">
    <div class="col-md-2">
        <ul class="mt nav nav-pills nav-stacked navbar-custom-nav">
            <?php
			$cnt = 0;
			$cur_branch = "";
            foreach ($all_branch_list as $checker_info):
				if($cnt == 0 && empty($branch_id)) $branch_id = $checker_info->dp_id;
				$cnt++;
                ?>
                <li class="<?php
                if ($branch_id == $checker_info->dp_id) {
					$cur_branch = $checker_info->co_name;
                    echo 'active';
                }
                ?>">
					<a href="<?= base_url() ?>admin/scheduling/config/<?php echo $ct_id ?>/checker/list/<?php echo $checker_info->dp_id ?>">
                        <i class="fa fa-fw fa-building"></i>
						<?php
						if(!empty($checker_info->co_name)) echo $checker_info->co_name;
						?> </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-md-10">
        <div class="tab-content pl0">

                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong><i class="fa fa-building"></i> <?=(!empty($cur_branch))?$cur_branch:""?> ( 총 <?=(!empty($total_checker_count))?number_format($total_checker_count):0?> 명) </strong>
                            </div>

                        </div>

                        <div class="box" style="padding:10px;">
						<table id="FDataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                            <thead>
							<tr align="center" bgcolor="#e0e7ef">
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">검사원명</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">아이디</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비밀번호</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">연락처</td>
							</tr>

                            </thead>
                            <tbody>
            <?php
		$cnt = 0;
		if(!empty($all_checker_list)) {
            foreach ($all_checker_list as $checker_info):
 				$cnt++;
               ?>
                                <tr>
									<td><?php echo $cnt; ?></td>
									<td><?php
										$id = $this->uri->segment(5);
										if (!empty($checker_id) && $checker_id == $checker_info->checker_id) { ?>
										<form method="post"
											  action="<?= base_url() ?>admin/scheduling/config/<?= $ct_id ?>/<?= $load_config ?>/update_checker/<?= $branch_id ?>/<?php
											  if (!empty($checker_info)) {
												  echo $checker_info->checker_id;
											  }
											  ?>" class="form-horizontal">
											<input type="hidden" name="customer_id" id="customer_id<?=$cnt?>" value="<?php
											if (!empty($checker_info)) {
												echo $checker_info->customer_id;
											}
											?>">
											<input type="text" name="fullname" value="<?php
											if (!empty($checker_info)) {
												echo $checker_info->fullname;
											}
											?>" class="form-control" placeholder="검사원명">
										<?php } else {
											echo $checker_info->fullname;
										}
										?></td>
									<td><?php
										$id = $this->uri->segment(5);
										if (!empty($checker_id) && $checker_id == $checker_info->checker_id) { ?>
											<input type="text" name="username" class="form-control" value="<?php
											if (!empty($checker_info->username)) {
												echo $checker_info->username;
											}
											?>"/>
										<?php } else {
											if(!empty($checker_info->username)) echo $checker_info->username;
										}
										?></td>
									<td><?php
										$id = $this->uri->segment(5);
										if (!empty($checker_id) && $checker_id == $checker_info->checker_id) { ?>
											<input type="text" name="passwd" class="form-control" value="<?php
											if (!empty($checker_info->passwd)) {
												echo $checker_info->passwd;
											}
											?>"/>
										<?php } else {
											if(!empty($checker_info->passwd)) echo $checker_info->passwd;
										}
										?></td>
									<td><?php
										$id = $this->uri->segment(5);
										if (!empty($checker_id) && $checker_id == $checker_info->checker_id) { ?>
											<input type="text" name="phone" class="form-control" value="<?php
											if (!empty($checker_info->phone)) {
												echo $checker_info->phone;
											}
											?>"/>
										<?php } else {
											if(!empty($checker_info->phone)) echo $checker_info->phone;
										}
										?></td>
									<?php if (!empty($edited) || !empty($deleted)) { ?>
										<td>
											<?php
											$id = $this->uri->segment(5);
											if (!empty($checker_id) && $checker_id == $checker_info->checker_id) { ?>
												<?= btn_update() ?>
												</form>
												<?= btn_cancel('admin/scheduling/config/' . $ct_id . '/' . $load_config) ?>
											<?php } else { ?>
												<?php if (!empty($edited)) { ?>
													<?= btn_edit('admin/scheduling/config/' . $ct_id . '/' . $load_config . '/edit_checker/' .$branch_id .'/' . $checker_info->checker_id ) ?>
												<?php } ?>
												<?php if (!empty($deleted)) { ?>
												  <?= btn_delete('admin/scheduling/config/' . $ct_id . '/' . $load_config . '/delete_checker/' .$branch_id .'/' . $checker_info->checker_id ) ?>
													<?php //echo ajax_anchor(base_url("admin/contract/delete_checker/" . $checker_info->checker_id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => "삭제", "data-fade-out-on-success" => "#title_" . $checker_info->checker_id)); ?>
												<?php }
											}
											?>
										</td>
                            <?php } ?>


                                </tr>
            <?php endforeach; ?>
         <?php } ?>
                <?php if ((!empty($created) || !empty($edited)) && !empty($branch_id)) { ?>
                    <form method="post" action="<?= base_url() ?>admin/scheduling/config/<?= $ct_id ?>/<?= $load_config ?>/insert_checker/<?=(!empty($branch_id))?$branch_id:"" ?>"
                          class="form-horizontal" data-parsley-validate="" novalidate="">
                        <tr>
                            <td></td>
                            <td>
                              <input type="text" name="fullname" class="form-control" placeholder="검사원명"></td>
                            <td>
                                <input type="text" name="username" placeholder="아이디" class="form-control"/>
                            </td>
                            <td>
                                <input type="text" name="passwd" placeholder="비밀번호" class="form-control"/>
                            </td>
                            <td>
                                <input type="text" name="phone" placeholder="연락처" class="form-control"/>
                            </td>
                            <td><?= btn_add() ?></td>
                        </tr>
                    </form>
                <?php } else { ?>
                        <tr>
                            <td></td>
                            <td colspan="5"> 거래처(지사)를 먼저 등록해 주세요.</td>
                        </tr>
                <?php } ?>
                            </tbody>
                        </table>
					</div>


                </div>
        </div>
    </div>
</div>
