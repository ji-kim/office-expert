<?php include_once 'asset/admin-ajax.php'; ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error');
$created = can_action('71', 'created');
$edited = can_action('71', 'edited');
$deleted = can_action('71', 'deleted');
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<div class="row">
    <div class="col-sm-12">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
              <li class=""><a href="<?php echo base_url(); ?>admin/scheduling/scheduling/<?=$ct_id?>">배차등록</a></li>
              <!--li class=""><a href="<?php echo base_url(); ?>admin/scheduling/work_result/<?=$ct_id?>">배차완료현황</a></li>
              <li class=""><a href="<?php echo base_url(); ?>admin/scheduling/work_stats/<?=$ct_id?>">배차통계</a></li-->
              <li class="active"><a href="<?php echo base_url(); ?>admin/scheduling/config/<?=$ct_id?>">운영설정</a></li>

            </ul>
		</div>
	</div>
</div>

            <div class="col-sm-12 bg-white p0" style="margin-bottom:30px>">
      <!-- 검색 시작 -->

    <form id="contract_form" name="contract_form" method="POST" action="/admin/scheduling/scheduling/<?=$ct_id?>">
				<div class="row ">
					<div class="col-sm-12" style="margin:10px">

						  <div class="form-group" id="border-none">
							<div class="col-sm-12">
								<span class="input-group-btn">
									<a href="<?php echo base_url(); ?>admin/scheduling/config/<?=$ct_id?>/cowork" type="button" class="dt-button buttons-print btn btn-<?=($load_config=="cowork")?"info":"default"?> btn-xs mr">
									   1. 거래처(지사)관리
									</a>
									&nbsp;
									<a href="<?php echo base_url(); ?>admin/scheduling/config/<?=$ct_id?>/driver" type="button" class="dt-button buttons-print btn btn-<?=($load_config=="driver")?"info":"default"?> btn-xs mr">
									   2. 기사관리
									</a>
									&nbsp;
									<a href="<?php echo base_url(); ?>admin/scheduling/config/<?=$ct_id?>/unitprice" type="button" class="dt-button buttons-print btn btn-<?=($load_config=="unitprice")?"info":"default"?> btn-xs mr">
									   3. 거래단가설정
									</a>
									&nbsp;
									<a href="<?php echo base_url(); ?>admin/scheduling/config/<?=$ct_id?>/checker" type="button" class="dt-button buttons-print btn btn-<?=($load_config=="checker")?"info":"default"?> btn-xs mr">
									   4. 검사원관리
									</a>
									&nbsp;
								</span>
							</div>

						</div>
					</div>
				</div>


</form>
      <!-- 검색 끝 -->


	</div>


                <?php $this->load->view('admin/scheduling/_config_' . $load_config) ?>
