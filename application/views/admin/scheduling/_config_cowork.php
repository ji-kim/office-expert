<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
$created = can_action('122', 'created');
$edited = can_action('122', 'edited');
$deleted = can_action('122', 'deleted');
?>
<div class="row">
</div>

<div class="row ">
    <div class="col-md-12">
        <ul class="mt nav nav-pills nav-stacked navbar-custom-nav">

          <div class="panel-heading">
               <div class="panel-title">
               </div>

          </div>

          <div class="box" style="padding:10px;">

            <table class="table table-striped ">
                <thead>
				<tr align="center" bgcolor="#e0e7ef">
				  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
				  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">거래처</td>
				  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대표</td>
				  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">표시순서</td>
				  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">작업</td>
				</tr>
                </thead>
                <tbody>
                <?php
                $cnt = 0;
                if (!empty($all_branch_list)) {
                    foreach ($all_branch_list as $branch_info) {
                      $cnt++;
                        ?>
                        <tr id="cowork_info_<?= $branch_info->coop_id ?>">
                            <td><?php echo $cnt; ?></td>
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($coop_id) && $coop_id == $branch_info->coop_id) { ?>
                                <form method="post"
                                      action="<?= base_url() ?>admin/scheduling/config/<?= $ct_id ?>/<?= $load_config ?>/update_cowork/<?php
                                      if (!empty($branch_info)) {
                                          echo $branch_info->coop_id;
                                      }
                                      ?>" class="form-horizontal">
                                    <input type="hidden" name="customer_id" id="customer_id<?=$cnt?>" value="<?php
                                    if (!empty($branch_info)) {
                                        echo $branch_info->customer_id;
                                    }
                                    ?>">
                                    <input type="text" id="co_name<?=$cnt?>" value="<?php
                                    if (!empty($branch_info)) {
                                        echo $branch_info->co_name;
                                    }
                                    ?>" class="form-control" placeholder="회사명" onClick="selectCompany('customer','customer_id<?=$cnt?>||co_name<?=$cnt?>||__||__||__||__||ceo<?=$cnt?>||__');">
                                <?php } else {
                                    echo $branch_info->co_name;
                                }
                                ?></td>
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($coop_id) && $coop_id == $branch_info->coop_id) { ?>
                                    <input type="text" id="ceo<?=$cnt?>" class="form-control" value="<?php
                                    if (!empty($branch_info->ceo)) {
                                        echo $branch_info->ceo;
                                    }
                                    ?>"/>
                                <?php } else {
                                    if(!empty($branch_info->ceo)) echo $branch_info->ceo;
                                }
                                ?></td>
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($coop_id) && $coop_id == $branch_info->coop_id) { ?>
                                    <input type="text" name="list_order" class="form-control" value="<?php
                                    if (!empty($branch_info->list_order)) {
                                        echo $branch_info->list_order;
                                    }
                                    ?>"/>
                                <?php } else {
                                    if(!empty($branch_info->list_order)) echo $branch_info->list_order;
                                }
                                ?></td>
                            <?php if (!empty($edited) || !empty($deleted)) { ?>
                                <td>
                                    <?php
                                    $id = $this->uri->segment(5);
                                    if (!empty($coop_id) && $coop_id == $branch_info->coop_id) { ?>
                                        <?= btn_update() ?>
                                        </form>
                                        <?= btn_cancel('admin/scheduling/config/' . $ct_id . '/' . $load_config) ?>
                                    <?php } else { ?>
                                        <?php if (!empty($edited)) { ?>
                                            <?= btn_edit('admin/scheduling/config/' . $ct_id . '/' . $load_config . '/edit_cowork/' . $branch_info->coop_id ) ?>
                                        <?php } ?>
                                        <?php if (!empty($deleted)) { ?>
                                          <?= btn_delete('admin/scheduling/config/' . $ct_id . '/' . $load_config . '/delete_cowork/' . $branch_info->coop_id ) ?>
                                            <?php //echo ajax_anchor(base_url("admin/contract/delete_item/" . $branch_info->coop_id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => "삭제", "data-fade-out-on-success" => "#title_" . $branch_info->coop_id)); ?>
                                        <?php }
                                    }
                                    ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php
                    }
                }
                ?>
                <?php if (!empty($created) || !empty($edited)) { ?>
                    <form method="post" action="<?= base_url() ?>admin/scheduling/config/<?= $ct_id ?>/<?= $load_config ?>/insert_cowork"
                          class="form-horizontal" data-parsley-validate="" novalidate="">
                        <tr>
                            <td></td>
                            <td>
                              <input type="hidden" name="customer_id" id="customer_id">
                              <input type="text" id="co_name" class="form-control"
                                       placeholder="거래처" onClick="selectCompany('customer','customer_id||co_name||__||__||__||__||ceo||__');"></td>
                            <td>
                                <input id="ceo" placeholder="대표"
                                       class="form-control"/>
                            </td>
                            <td>
                                <input name="list_order" placeholder="리스트순서"
                                       class="form-control"/>
                            </td>
                            <td><?= btn_add() ?></td>
                        </tr>
                    </form>
                <?php } ?>
                </tbody>
            </table>
			</div>
        </ul>
    </div>
</div>

<script>
	function selectCompany(md,params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSCC', 'left=100, top=100, width=1200, height=700, scrollbars=1');
	}
</script>
