<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
$created = can_action('122', 'created');
$edited = can_action('122', 'edited');
$deleted = can_action('122', 'deleted');
?>
<div class="row ">
    <div class="col-md-2">
        <ul class="mt nav nav-pills nav-stacked navbar-custom-nav">
            <?php
			$cnt = 0;
			$cur_branch = "";
            foreach ($all_branch_list as $branch_info):
				if($cnt == 0 && empty($branch_id)) $branch_id = $branch_info->dp_id;
				$cnt++;
                ?>
                <li class="<?php
                if ($branch_id == $branch_info->dp_id) {
					$cur_branch = $branch_info->co_name;
                    echo 'active';
                }
                ?>">
					<a href="<?= base_url() ?>admin/scheduling/config/<?php echo $ct_id ?>/driver/list/<?php echo $branch_info->dp_id ?>">
                        <i class="fa fa-fw fa-building"></i>
						<?php
						if(!empty($branch_info->co_name)) echo $branch_info->co_name;
						?> </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-md-10">
        <div class="tab-content pl0">

                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong><i class="fa fa-building"></i> <?=(!empty($cur_branch))?$cur_branch:""?> ( 총 <?=(!empty($total_driver_count))?number_format($total_driver_count):0?> 명)</strong>
                            </div>

                        </div>

                        <div class="box" style="padding:10px;">
						<table id="FDataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                            <thead>
							<tr align="center" bgcolor="#e0e7ef">
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기사명</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자명</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자번호(주민번호)</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">연락처</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;"> </td>
							</tr>

                            </thead>
                            <tbody>
            <?php
		$cnt = 0;
		if(!empty($all_driver_list)) {
            foreach ($all_driver_list as $driver_info):
 				$cnt++;
               ?>
                                <tr>
									<td><?php echo $cnt; ?></td>
									<td><?php
										$id = $this->uri->segment(5);
										if (!empty($driver_id) && $driver_id == $driver_info->dp_id) { ?>
										<form method="post"
											  action="<?= base_url() ?>admin/scheduling/config/<?= $ct_id ?>/<?= $load_config ?>/update_driver/<?= $branch_id ?>/<?php
											  if (!empty($driver_info)) {
												  echo $driver_info->contract_partner_id;
											  }
											  ?>" class="form-horizontal">
											<input type="hidden" name="partner_id" id="partner_id<?=$cnt?>" value="<?php
											if (!empty($driver_info)) {
												echo $driver_info->partner_id;
											}
											?>">
											<input type="text" name="ceo" id="driver<?=$cnt?>" value="<?php
											if (!empty($driver_info)) {
												echo $driver_info->ceo;
											}
											?>" class="form-control" placeholder="기사명" onClick="selectCompany('partner','partner_id<?=$cnt?>||__||__||__||__||driver<?=$cnt?>||__||__');">
										<?php } else {
											echo $driver_info->ceo;
										}
										?></td>
									<td><?php
										$id = $this->uri->segment(5);
										if (!empty($driver_id) && $driver_id == $driver_info->dp_id) { ?>
											<input type="text" name="co_name" class="form-control" value="<?php
											if (!empty($driver_info->co_name)) {
												echo $driver_info->co_name;
											}
											?>"/>
										<?php } else {
											if(!empty($driver_info->co_name)) echo $driver_info->co_name;
										}
										?></td>
									<td><?php
										$id = $this->uri->segment(5);
										if(!empty($driver_info->bs_number)) echo $driver_info->bs_number;
										if(!empty($driver_info->reg_number)) echo $driver_info->reg_number;
										?></td>
									<td><?php
										$id = $this->uri->segment(5);
										if(!empty($driver_info->co_tel)) echo $driver_info->co_tel;
										?></td>
									<?php if (!empty($edited) || !empty($deleted)) { ?>
										<td>
											<?php
											$id = $this->uri->segment(5);
											if (!empty($driver_id) && $driver_id == $driver_info->dp_id) { ?>
												<?= btn_update() ?>
												</form>
												<?= btn_cancel('admin/scheduling/config/' . $ct_id . '/' . $load_config) ?>
											<?php } else { ?>
												<?php if (!empty($edited)) { ?>
													<?= btn_edit('admin/scheduling/config/' . $ct_id . '/' . $load_config . '/edit_driver/' .$branch_id .'/' . $driver_info->dp_id ) ?>
												<?php } ?>
												<?php if (!empty($deleted)) { ?>
												  <?= btn_delete('admin/scheduling/config/' . $ct_id . '/' . $load_config . '/delete_driver/' .$branch_id .'/' . $driver_info->dp_id ) ?>
													<?php //echo ajax_anchor(base_url("admin/contract/delete_driver/" . $driver_info->dp_id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => "삭제", "data-fade-out-on-success" => "#title_" . $driver_info->dp_id)); ?>
												<?php }
											}
											?>
										</td>
                            <?php } ?>


                                </tr>
            <?php endforeach; ?>
         <?php } ?>
			
			
                <?php if ((!empty($created) || !empty($edited)) && !empty($branch_id)) { ?>
                    <form method="post" action="<?= base_url() ?>admin/scheduling/config/<?= $ct_id ?>/<?= $load_config ?>/insert_driver/<?=(!empty($branch_id))?$branch_id:"" ?>"
                          class="form-horizontal" data-parsley-validate="" novalidate="">
                        <tr>
                            <td></td>
                            <td colspan="1">
                              <input type="hidden" name="partner_id" id="partner_id">
                              <input type="text" id="driver" class="form-control"
                                       placeholder="기사명" onClick="selectCompany('partner','partner_id||__||__||__||__||driver||__||__');"></td>
                            <td colspan="4"><?= btn_add() ?></td>
                        </tr>
                    </form>
                <?php } else { ?>
                        <tr>
                            <td></td>
                            <td colspan="5"> 거래처(지사)를 먼저 등록해 주세요.</td>
                        </tr>
                <?php } ?>
                            </tbody>
                        </table>
					</div>


                </div>
        </div>
    </div>
</div>


<iframe width=0 height=0 name='hiddenframe' style='display:none;'></iframe>
<script>
// ajax requst 오류로 임시 iframe 사용 향후 ajax 로 변경
function update_driver(field_name, field_id, field_value) {
	document.all.hiddenframe.src = "<?php echo base_url(); ?>admin/scheduling/update_driver/"+field_name+"/"+field_id+"/"+field_value;
}

 // 파트너 선택
 function selectCompany(md,params) {
   window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSCC', 'left=100, top=100, width=1200, height=700, scrollbars=1');
 }

</script>
