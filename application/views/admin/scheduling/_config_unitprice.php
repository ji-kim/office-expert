<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
$created = can_action('122', 'created');
$edited = can_action('122', 'edited');
$deleted = can_action('122', 'deleted');
?>
<div class="row">
    <?php if (!empty($created)) { ?>
        <div class="col-sm-12 mt">&nbsp;&nbsp;&nbsp;
            <a href="<?= base_url() ?>admin/scheduling/set_unitprice/<?php echo $ct_id ?>/<?=(!empty($branch_id))?$branch_id:"" ?>/" class="text-danger btn btn-success btn-xs mr" data-toggle="modal"
               data-placement="top" data-target="#myModal"><span
                    class="fa fa-plus "> 단가 전체적용 </span></a>

					* 단가항목 추가는 <a href="<?= base_url() ?>admin/contract/contract_list">원청계약관리</a>에서, 거래처 및 파트너 추가는 <a href="<?= base_url() ?>admin/contract/scontract_list">하도급계약관리</a>에서 처리해 주세요.
        </div>
    <?php } ?>
</div>
<div class="row ">
    <div class="col-md-2">
        <ul class="mt nav nav-pills nav-stacked navbar-custom-nav">
            <?php
			$cnt = 0;
			$cur_branch = "";
            foreach ($all_branch_list as $branch_info):
				if($cnt == 0 && empty($branch_id)) $branch_id = $branch_info->dp_id;
				$cnt++;
                ?>
                <li class="<?php
                if ($branch_id == $branch_info->dp_id) {
					$cur_branch = $branch_info->co_name;
                    echo 'active';
                }
                ?>">
					<a href="<?= base_url() ?>admin/scheduling/config/<?php echo $ct_id ?>/unitprice/list/<?php echo $branch_info->dp_id ?>">
                        <i class="fa fa-fw fa-building"></i>
						<?php
						if(!empty($branch_info->co_name)) echo $branch_info->co_name;
						?> </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-md-10">
        <div class="tab-content pl0">

                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong><i class="fa fa-building"></i> <?=(!empty($cur_branch))?$cur_branch:""?> ( 총 <?=(!empty($total_driver_count))?number_format($total_driver_count):0?> 명) 작업단가 적용</strong>
								- 값 입력시 바로 자동 저장됩니다.
                            </div>

                        </div>

                        <div class="box" style="padding:10px;">
						<table id="FDataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                            <thead>
							<tr align="center" bgcolor="#e0e7ef">
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기사</td>

            <?php
			//원청계약관리 > 거래항목설정값 가져옴
			if(!empty($all_item_list)) {
            foreach ($all_item_list as $item_info):
                ?>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;"><?php
						if(!empty($item_info->title)) echo $item_info->title;
						?></td>
            <?php endforeach; ?>
            <?php } ?>


							</tr>

                            </thead>
                            <tbody>
            <?php
		$cnt = 0;
		if(!empty($all_driver_list)) {
            foreach ($all_driver_list as $driver_info):
 				$cnt++;
               ?>
                                <tr>
                                    <td><?php echo $cnt; ?></td>
                                    <td><?php echo $driver_info->ceo; ?></td>
<?php 				
					foreach ($all_item_list as $item_info):
						$uprice = $this->db->where('partner_id', $driver_info->dp_id)->where('item_id', $item_info->item_id)->get('tbl_contract_partner_unitprice')->row();
?>

									<td><input type="text" onBlur="update_unitprice('unit_price','<?=(!empty($uprice->unitprice_id))?$uprice->unitprice_id:''?>',this.value);" class="form-control" name="uprice1" id="uprice1" value='<?=(!empty($uprice->unit_price))?$uprice->unit_price:0?>' style="width:100%;text-align:right"/></td>
<?php

					endforeach;
?>


                                </tr>
            <?php endforeach; ?>
         <?php } else { ?>
                                <tr>
									<td colspan="6">
										<strong>등록된 데이터가 없습니다</strong>
									</td>
                                </tr>
         <?php } ?>
                            </tbody>
                        </table>
					</div>


                </div>
        </div>
    </div>
</div>


<iframe width=0 height=0 name='hiddenframe' style='display:none;'></iframe>
<script>
// ajax requst 오류로 임시 iframe 사용 향후 ajax 로 변경
function update_unitprice(field_name, field_id, field_value) {
	document.all.hiddenframe.src = "<?php echo base_url(); ?>admin/scheduling/update_unitprice/"+field_name+"/"+field_id+"/"+field_value;
}
</script>
