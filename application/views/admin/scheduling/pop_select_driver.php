<div>
  <div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">
      <div class="col-sm-12">
                                <label class="col-lg-12 control-label" style="font-size:18px;">기사 선택</label>
      </div>

  </div>
</div>

<div class="row">
<div class="table-responsive" style="padding-left:10px;">

<table class="table table-striped" style="width:98%">
  <thead>
  <tr align="center" bgcolor="#e0e7ef">
    <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이름</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사명</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자번호</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">선택</td>
  </tr>
        </thead>
        <tbody>
        <?php
        if (!empty($all_driver_list)) {
            foreach ($all_driver_list as $driver_details) {
                ?>
                <tr id="coop_details_<?= $driver_details->dp_id ?>">
                    <td align='center'><?=$driver_details->ceo ?></td>
                    <td align='center'><?=$driver_details->co_name ?></td>
                    <td align='center'><?=$driver_details->bs_number ?></td>
                    <td align='center'>
                        <button class="btn btn-primary" type="button" onclick="goSelect('<?=$driver_details->dp_id ?>','<?=$driver_details->ceo ?>');">
							선택
						</button>
                  </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>





</div>
</div>

<script type="text/javascript">
function goSelect(driver_id,fullname) {
  $("#add_driver_id",opener.document).val(driver_id);
  $("#add_driver_name",opener.document).val(fullname);
  window.close();
}
function goSearch() {
$('#page').val('1');
$("#myform").attr("target", "_self");
$("#myform").submit();
}
//-->
</script>
