<?php
?>
<div class="panel panel-custom" style="padding:10px;">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 단가 일괄 적용</h4>
    </div>
    <div class="modal-body wrap-modal wrap">

            <div class="form-group" id="border-none">
			* 기존설정단가도 변경 적용됩니다.<br/>
            </div>

        <form id="form_validation"
              action="<?php echo base_url() ?>admin/scheduling/save_unitprice/" method="post" class="form-horizontal form-groups-bordered">

            <?php
			//원청계약관리 > 거래항목설정값 가져옴
            foreach ($all_item_list as $item_info):
                ?>
				<div class="form-group" id="border-none">
					<label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;"><?php
						if(!empty($item_info->title)) echo $item_info->title;
						?></label>
					<div class="col-sm-8">
						<input type="text" value="0" class="form-control" name="not_paid" style="text-align:right;">
					</div>
				</div>
            <?php endforeach; ?>
			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
