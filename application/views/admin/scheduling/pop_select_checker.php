<div>
  <div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">
      <div class="col-sm-12">
                                <label class="col-lg-12 control-label" style="font-size:18px;">검사원 선택</label>
      </div>

  </div>
</div>

<div class="row">
<div class="table-responsive" style="padding-left:10px;">

<table class="table table-striped" style="width:98%">
  <thead>
  <tr align="center" bgcolor="#e0e7ef">
    <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이름</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">연락처</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">아이디</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">선택</td>
  </tr>
        </thead>
        <tbody>
        <?php
        if (!empty($all_checker_list)) {
            foreach ($all_checker_list as $checker_details) {
                ?>
                <tr id="coop_details_<?= $checker_details->checker_id ?>">
                    <td align='center'><?=$checker_details->fullname ?></td>
                    <td align='center'><?=$checker_details->phone ?></td>
                    <td align='center'><?=$checker_details->username ?></td>
                    <td align='center'>
                        <button class="btn btn-primary" type="button" onclick="goSelect('<?=$checker_details->checker_id ?>','<?=$checker_details->fullname ?>');">
							선택
						</button>
                  </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>





</div>
</div>

<script type="text/javascript">
function goSelect(checker_id,fullname) {
  $("#add_checker_id",opener.document).val(checker_id);
  $("#add_checker_name",opener.document).val(fullname);
  window.close();
}
function goSearch() {
$('#page').val('1');
$("#myform").attr("target", "_self");
$("#myform").submit();
}
//-->
</script>
