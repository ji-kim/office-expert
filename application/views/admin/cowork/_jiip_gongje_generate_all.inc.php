$lastday=array(0,31,28,31,30,31,30,31,31,30,31,30,31);
$tmp = substr($df_month,5,2);
$endDay = $lastday[(int)$tmp];

//추가항목 레코드 생성 -> 위수탁 관리사 반드시 선택되어있어야 함
// 1. 수당
$sql = "";
if (!empty($all_sudang_group)) {
  foreach ($all_sudang_group as $sudang_info) {
    $chk = $this->db->where('ws_co_id', $gongje_req_co)->where('df_month', $df_month)->where('add_type', 'S')->get('tbl_delivery_fee_sg_items')->row();
    if (!empty($chk)) {
      $sql = "update tbl_delivery_fee_sg_items set ws_co_id = '".$gongje_req_co."',df_month = '".$df_month."',title = '".$sudang_info->title."',add_type = 'S',dp_order = '".$sudang_info->dp_order."' where idx='".$sudang_info->idx."'";
    } else {
      $sql = "insert into tbl_delivery_fee_sg_items set ws_co_id = '".$gongje_req_co."',df_month = '".$df_month."',title = '".$sudang_info->title."',add_type = 'S',dp_order = '".$sudang_info->dp_order."'";
    }
    $this->cowork_model->db->query($sql);
  }
}

// 2. 공제
if (!empty($all_gongje_group)) {
  foreach ($all_gongje_group as $gongje_info) {
    $chk = $this->db->where('ws_co_id', $gongje_req_co)->where('df_month', $df_month)->where('add_type', $gongje_info->add_type)->get('tbl_delivery_fee_sg_items')->row();
    if (!empty($chk)) {
      $sql = "update tbl_delivery_fee_sg_items set ws_co_id = '".$gongje_req_co."',df_month = '".$df_month."',title = '".$gongje_info->title."',add_type = '".$gongje_info->add_type."',dp_order = '".$gongje_info->dp_order."' where idx='".$gongje_info->idx."'";
    } else {
      $sql = "insert into tbl_delivery_fee_sg_items set ws_co_id = '".$gongje_req_co."',df_month = '".$df_month."',title = '".$gongje_info->title."',add_type = '".$gongje_info->add_type."',dp_order = '".$gongje_info->dp_order."'";
    }

    $this->cowork_model->db->query($sql);
  }
}

if (!empty($all_dp_list)) {
  foreach ($all_dp_list as $dp_info) {
      // 계약기간 확인 N, O
      $inm = "";
      $outm = "";
      if(!empty($dp_info->N)) $inm = substr($dp_info->N,0,7);
      if(!empty($dp_info->O)) $outm = substr($dp_info->O,0,7);
      $this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
      $this->cowork_model->db->where('df_month',$df_month);
      $this->cowork_model->db->where('gongje_req_co',$gongje_req_co);
      $this->cowork_model->_order_by = 'df_id';
      $all_mfee_list = $this->cowork_model->get();

      if (!empty($all_mfee_list)) {
        foreach ($all_mfee_list as $mfee_info) {
          //tbl_delivery_fee_add
          $sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
          $this->db->query($sql);
          //tbl_delivery_fee_fixmfee
          $sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
          $this->db->query($sql);
          //tbl_delivery_fee_gongje
          $sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
          $this->db->query($sql);
          //tbl_delivery_fee_insur
          $sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
          $this->db->query($sql);
          //tbl_delivery_fee_refund_gongje
          $sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
          $this->db->query($sql);
          //tbl_delivery_fee_levy
          $sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
          $this->db->query($sql);

          $sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
          $this->db->query($sql);
        }
      }
      $sql = "DELETE FROM tbl_delivery_fee WHERE gongje_req_co='".$gongje_req_co."' and df_month='".$df_month."'";
      $this->db->query($sql);

        $generate_yn = "Y";
        $df_chk = $this->db->where('dp_id', $dp_info->dp_id)->where('df_month', $df_month)->get('tbl_delivery_fee')->row();
        if(!empty($df_chk->dp_id)) $generate_yn = "N"; //기존 존재건은 패스하고 추가된 기사건만 생성
        $rco = $this->db->select('co_name as rco_name, dp_id as rco_id')->where("( code = '".$dp_info->code."' and mb_type = 'customer')")->get("tbl_members ")->row();

          echo "xxxxxxxxx".$dp_info->ceo."---"."<br>";

        if($generate_yn == "Y") {
        // 계약기간 확인 N, O
        $inm = "";
        $outm = "";
        if(!empty($dp_info->N)) $inm = substr($dp_info->N,0,7);
        if(!empty($dp_info->O)) $outm = substr($dp_info->O,0,7);


        if((!empty($dp_info->N) && !empty($dp_info->O)) && ($df_month >= $inm && $df_month <= $outm)) {

          $sn = $total_count--;
          $tot_gongje_req = $tot_gongje = 0;
          $wsm_sum = $insur_sum = $gongje_sum = $rf_gongje_sum = 0;

          $sql_basic = ""; // 기본정보
          $sql_co_info = ""; // 회사들정보
          $sql_unitprice = ""; // 계약단가정보
          $sql_bank = ""; // 은행정보
          $sql_basic_sudang = ""; // 기본수당
          $sql_basic_gongje = ""; // 기본공제
          $sql_insure = ""; // 보험공제
          $sql_ilhal = "";

          $sql_mf = ""; // 관리비정보

          //일할계산처리
          /* 그리고 1일이 되었는 3일이 되었든 아님 10일지났든
          계약당월 이런경우 협회비 차고지비 보험료는 보험만료와 계약만료일 과 비교해서
          생성이 되도록 해야하는것 */
          $is_ilhal = "N";
          if($df_month == $inm || $df_month == $outm) {
            $ilhal_days = 0;
            $s = substr($dp_info->N,8,2);
            $e = substr($dp_info->O,8,2);
            if($df_month == $inm && $df_month == $outm) { // 당월 입.퇴사자
              $ilhal_days = (int)$e - (int)$s + 1;
            } else if($df_month == $inm) { // 당월 입사자
              $ilhal_days = $endDay - (int)$s + 1;
            } else if($df_month == $outm) { // 당월 퇴사자
              $ilhal_days = (int)$e;
            }
            $is_ilhal = "Y";
            $sql_ilhal = ", remark ='일할계산(".$ilhal_days."일)'";
          }

          //--------------------------------- 0. 기본정보
          $sql_basic .= "dp_id ='".$dp_info->dp_id."'";
          $sql_basic .= ",userid ='".$dp_info->userid."'";
          $sql_basic .= ",df_month ='".$df_month."'";
          $sql_basic .= ",co_code ='".$dp_info->code."'";
          $sql_basic .= ",co_name ='".$dp_info->co_name."'";
          $sql_basic .= ",C ='".$dp_info->jiip_co."'";
          if(!empty($rco->rco_name)) $sql_basic .= ",D ='".$rco->rco_name."'";
          $sql_basic .= ",E ='".$dp_info->driver."'";
          $sql_basic .= ",status ='대기'";
          if(!empty($dp_info->bs_number)) $sql_basic .= ",bs_number ='".$dp_info->bs_number."'"; // 사업자등록  *---------------------*
          else if(!empty($dp_info->reg_number)) $sql_basic .= ",bs_number ='".$dp_info->reg_number."'"; // 사업자등록없는경우 주민번호로 *---------------------*
          $sql_basic .= ",applytax_yn ='".$dp_info->tax_yn."'";

          $vat_acc_yn = 'N'; //부가세 누적관련
          $acc_vat_dmonth_cnt = 0;
          if($dp_info->acc_vat_yn == "Y") {
            $sql_tax2 = ",vat_acc_yn='Y'";
            $acc_vat_dmonth_cnt = $df_month;
          }
          $sql_basic .= ",vat_acc_yn ='".$vat_acc_yn."'";
          //부가세 누적 = acc_vat_paid_yn 부가세지급여부 acc_vat_month=부가세 누적개월
          $vv = $this->db->select('sum(acc_vat_dmonth) as avd, count(acc_vat_dmonth) as cnt')->where('dp_id', $dp_info->ws_co_id)->get('tbl_delivery_fee')->row();
          if(!empty($vv->avd)) $sql_basic .= ",acc_vat_sum='".$vv->avd."'";
          if(!empty($vv->cnt)) $sql_basic .= ",acc_vat_dmonth_cnt ='".$vv->cnt."'";


          //--------------------------------- 1. 공제청구사(위수탁관리사) 등 회사정보
          $gj_rq_co = $this->db->where('dp_id', $dp_info->ws_co_id)->get('tbl_members')->row();
          if(!empty($rco->rco_name)) $r_co_id = $rco->rco_id;//실수요처
          if(!empty($rco->rco_name)) $r_co_name = $rco->rco_name;//실수요처
          $s_co_name = $dp_info->driver;//운영공제(송금)사

          //과세여부
          $tax_yn = $dp_info->tax_yn;
          // 회사정보쿼리
          if(!empty($dp_info->ctr_id)) $sql_co_info .= ",tr_id='".$dp_info->ctr_id."'";
          if(!empty($dp_info->ws_co_id)) $sql_co_info .= ", gongje_req_co='".$dp_info->ws_co_id."'";

          //--------------------------------- 2. 관리비정보
          $sql_mf .= "dp_id ='".$dp_info->dp_id."'";
          $sql_mf .= ",df_month ='".$df_month."'";
          $sql_mf .= ",co_code ='".$dp_info->code."'";
          if(!empty($dp_info->ws_co_id)) $sql_mf .= ", ws_co_id='".$dp_info->ws_co_id."'";
          $mf = $this->db->where('dp_id', $dp_info->dp_id)->where('master_yn', 'Y')->get('tbl_delivery_fee_fixmfee_set')->row(); //기본정보

          if(!empty($mf->wst_mfee)) $wst_mfee = $mf->wst_mfee; else $wst_mfee = 0;
          $wsm_sum += $wst_mfee;

          if(!empty($mf->mfee_vat)) { $mfee_vat = $mf->mfee_vat; $wsm_sum += $mf->mfee_vat; } else $mfee_vat = 0;
          if(!empty($mf->org_fee)) { $org_fee = $mf->org_fee; $wsm_sum += $mf->org_fee; } else $org_fee = 0;
          if(!empty($mf->grg_fee)) { $grg_fee = $mf->grg_fee; $wsm_sum += $mf->grg_fee; } else $grg_fee = 0;
          if(!empty($mf->grg_fee_vat)) { $grg_fee_vat = $mf->grg_fee_vat; $wsm_sum += $mf->grg_fee_vat; } else $grg_fee_vat = 0;
          $org_fee_vat = 0;
          $env_fee = 0;
          $env_fee_vat = 0;
          $car_tax = 0;
          $car_tax_vat = 0;
          $etc = 0;
          // 일할계산
          if($is_ilhal == "Y") {
            if($df_month == $inm || $df_month != $outm) {
              if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)(($wst_mfee/$endDay) * $ilhal_days);
              if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)(($mfee_vat/$endDay) * $ilhal_days);
            } else if($ilhal_days < 15) {
              if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)($wst_mfee/2);
              if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)($mfee_vat/2);
            }

          }
          $sql_mf .= ",wst_mfee='".$wst_mfee."'";
          $sql_mf .= ",mfee_vat='".$mfee_vat."'";
          $sql_mf .= ",org_fee='".$org_fee."'";
          $sql_mf .= ",grg_fee='".$grg_fee."'";
          $sql_mf .= ",grg_fee_vat='".$grg_fee_vat."'";

          //--------------------------------- 3. 은행정보
          if(!empty($dp_info->bank_code)) $sql_bank .= ",bank_code='".$dp_info->bank_code."'";
          if(!empty($dp_info->bank)) $sql_bank .= ",bank='".$dp_info->bank."'";
          if(!empty($dp_info->account_no)) $sql_bank .= ",account_no='".$dp_info->account_no."'";
          if(!empty($dp_info->account_name)) $sql_bank .= ",account_name='".$dp_info->account_name."'";
          if(!empty($dp_info->account_relation)) $sql_bank .= ",account_relation='".$dp_info->account_relation."'";

          //--------------------------------- 4. 계약단가정보
          if(!empty($ct_id)) {
            $cu = $this->db->where('dp_id', $dp_info->dp_id)->where('ct_id', $ct_id)->get('tbl_scontract_co')->row(); //기본정보
            if(!empty($cu->val1)) $sql_unitprice .= ",unit_price1='".$cu->val1."'";
            if(!empty($cu->val2)) $sql_unitprice .= ",unit_price2='".$cu->val2."'";
            if(!empty($cu->val3)) $sql_unitprice .= ",unit_price3='".$cu->val3."'";
            if(!empty($cu->val4)) $sql_unitprice .= ",unit_price4='".$cu->val4."'";
          }

          //--------------------------------- 5. 수당
          if(!empty($ct_id)) {
            $sd = $this->db->where('dp_id', $dp_info->dp_id)->where('ct_id', $ct_id)->get('tbl_delivery_fee_bsudang')->row(); //기본정보
            if((!empty($sd->sanje_yn) && !empty($sd->sanje_amount)) && $sd->sanje_yn == "Y") $sanje_amount = $sd->sanje_amount; else $sanje_amount = 0;

            if(!empty($sd->manager_pickup)) $sql_basic_sudang .= ",manager_pickup='".$sd->manager_pickup."'";
            if(!empty($sd->team_leader)) $sql_basic_sudang .= ",team_leader='".$sd->team_leader."'";
            if(!empty($sd->diffcult_area)) $sql_basic_sudang .= ",diffcult_area='".$sd->diffcult_area."'";
            if(!empty($sd->subside)) $sql_basic_sudang .= ",subside='".$sd->subside."'";
            if(!empty($sd->hoisik)) $sql_basic_sudang .= ",hoisik='".$sd->hoisik."'";
            if(!empty($sd->sudang_etc)) $sql_basic_sudang .= ",sudang_etc='".$sd->sudang_etc."'";
            if(!empty($sd->sanje_yn)) $sql_basic_sudang .= ",sanje_yn='".$sd->sanje_yn."'";
            $sql_basic_sudang .= ",sanje_amount='".$sanje_amount."'";
          }

          //--------------------------------- 6. 보험 tbl_asset_truck_insur_sub tr_id & pay_month  pay_amt
          if(!empty($dp_info->ctr_id)) {
            $qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
            $qry .= " where a.type = '자동차' and b.gj_month = '".$df_month."' and (a.tr_id = '".$dp_info->ctr_id."') and a.co_pay<>'Y' and b.pay_by='01' and a.active_yn='Y'";
            $ins = $this->cowork_model->db->query($qry)->row();
            if(!empty($ins->pay_amt))
              $ins_fee = $ins->pay_amt;
            else
              $ins_fee =0;

            //if($dp_info->dp_id == "2656") echo $qry;
//01 공제 > 미수와 미납에 동시등록 -> 회계등록
//02 회사부담 > 미납에등록 -> 회계등록

            $qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
            $qry .= " where a.type = '적재물' and b.gj_month = '".$df_month."' and (a.tr_id = '".$dp_info->ctr_id."') and a.co_pay<>'Y' and b.pay_by='01' and a.active_yn='Y'";
            $lins = $this->cowork_model->db->query($qry)->row();
            if(!empty($lins->pay_amt))
              $lins_fee = $lins->pay_amt;
            else
              $lins_fee =0;

            // 일할계산
            if($is_ilhal == "Y") {
              //if(!empty($ins_fee) && $ins_fee > 0) $ins_fee = ($ins_fee/$endDay) * $ilhal_days;
              //if(!empty($lins_fee) && $lins_fee > 0) $lins_fee = ($lins_fee/$endDay) * $ilhal_days;
            }
            $sql_insure .= ",ins_car ='".$ins_fee."'";
            $sql_insure .= ",ins_load ='".$lins_fee."'";

          }
          //DB 삽입
          //echo "--".$dp_info->ceo."---".$sql_insure;
          $sql = "INSERT INTO tbl_delivery_fee SET tr_type = 'M', $sql_basic $sql_co_info $sql_unitprice $sql_bank $sql_basic_sudang $sql_basic_gongje $sql_ilhal";
          //$this->cowork_model->db->query($sql);
          //$df_id = $this->cowork_model->db->insert_id();

          //전월 미납건체크
          $pdate = $df_month . "-01";
          $pm=date("Y-m",strtotime($pdate.'-1month'));
          $not_paid = 0;
          $plv = $this->db->where('df_month', $pm)->where('dp_id', $dp_info->dp_id)->get('tbl_delivery_fee_levy')->row();
          if(!empty($plv->pay_status) && $plv->pay_status != 'B') { //납부전상태가 아니면
            $not_paid = $plv->balance_amount;
            //$sql = "insert into tbl_delivery_fee_gongje set not_paid = '".$not_paid."', df_id='".$df_id."'";
            //$this->cowork_model->db->query($sql);
          }

      }
    }



  }
}
  //    redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
