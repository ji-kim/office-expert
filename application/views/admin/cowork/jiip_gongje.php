<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
// 전체 생성
if($action == "generate_all") {
	$lastday=array(0,31,28,31,30,31,30,31,31,30,31,30,31);
	$tmp = substr($df_month,5,2);
	$endDay = $lastday[(int)$tmp];

	if (!empty($all_dp_list)) {
	  foreach ($all_dp_list as $dp_info) {
				if(1 ) {
					//echo "xxxxxxxxx".$dp_info->ceo."---"."<br>";
	      // 계약기간 확인 N, O
	      $inm = "";
	      $outm = "";
	      if(!empty($dp_info->N)) $inm = substr($dp_info->N,0,7);
	      if(!empty($dp_info->O)) $outm = substr($dp_info->O,0,7);

	        $generate_yn = "Y";
	        $df_chk = $this->db->where('dp_id', $dp_info->dp_id)->where('df_month', $df_month)->get('tbl_delivery_fee')->row();
	        if(!empty($df_chk->dp_id)) {
						$generate_yn = "N"; //기존 존재건은 패스하고 추가된 기사건만 생성
						echo $df_chk->df_id."xxxxxxxxx".$dp_info->ceo."---"."<br>";
					}
	        $rco = $this->db->select('co_name as rco_name, dp_id as rco_id')->where("( code = '".$dp_info->code."' and mb_type = 'customer')")->get("tbl_members ")->row();

	        if($generate_yn == "Y") {
	        // 계약기간 확인 N, O
	        $inm = "";
	        $outm = "";
	        if(!empty($dp_info->N)) $inm = substr($dp_info->N,0,7);
	        if(!empty($dp_info->O)) $outm = substr($dp_info->O,0,7);

	        if((!empty($dp_info->N) && !empty($dp_info->O)) && ($df_month >= $inm && $df_month <= $outm)) {

	          $sn = $total_count--;
	          $tot_gongje_req = $tot_gongje = 0;
	          $wsm_sum = $insur_sum = $gongje_sum = $rf_gongje_sum = 0;

	          $sql_basic = ""; // 기본정보
	          $sql_co_info = ""; // 회사들정보
	          $sql_unitprice = ""; // 계약단가정보
	          $sql_bank = ""; // 은행정보
	          $sql_basic_sudang = ""; // 기본수당
	          $sql_basic_gongje = ""; // 기본공제
	          $sql_insure = ""; // 보험공제
	          $sql_ilhal = "";

	          $sql_mf = ""; // 관리비정보

	          //일할계산처리
	          /* 그리고 1일이 되었는 3일이 되었든 아님 10일지났든
	          계약당월 이런경우 협회비 차고지비 보험료는 보험만료와 계약만료일 과 비교해서
	          생성이 되도록 해야하는것 */
	          $is_ilhal = "N";
	          if($df_month == $inm || $df_month == $outm) {
	            $ilhal_days = 0;
	            $s = substr($dp_info->N,8,2);
	            $e = substr($dp_info->O,8,2);
	            if($df_month == $inm && $df_month == $outm) { // 당월 입.퇴사자
	              $ilhal_days = (int)$e - (int)$s + 1;
	            } else if($df_month == $inm) { // 당월 입사자
	              $ilhal_days = $endDay - (int)$s + 1;
	            } else if($df_month == $outm) { // 당월 퇴사자
	              $ilhal_days = (int)$e;
	            }
	            $is_ilhal = "Y";
	            $sql_ilhal = ", remark ='일할계산(".$ilhal_days."일)'";
	          }

	          //--------------------------------- 0. 기본정보
	          $sql_basic .= "dp_id ='".$dp_info->dp_id."'";
	          $sql_basic .= ",userid ='".$dp_info->userid."'";
	          $sql_basic .= ",df_month ='".$df_month."'";
	          $sql_basic .= ",co_code ='".$dp_info->code."'";
	          $sql_basic .= ",co_name ='".$dp_info->co_name."'";
	          $sql_basic .= ",C ='".$dp_info->jiip_co."'";
	          if(!empty($rco->rco_name)) $sql_basic .= ",D ='".$rco->rco_name."'";
	          $sql_basic .= ",E ='".$dp_info->driver."'";
	          $sql_basic .= ",status ='대기'";
	          if(!empty($dp_info->bs_number)) $sql_basic .= ",bs_number ='".$dp_info->bs_number."'"; // 사업자등록  *---------------------*
	          else if(!empty($dp_info->reg_number)) $sql_basic .= ",bs_number ='".$dp_info->reg_number."'"; // 사업자등록없는경우 주민번호로 *---------------------*
	          $sql_basic .= ",applytax_yn ='".$dp_info->tax_yn."'";

	          $vat_acc_yn = 'N'; //부가세 누적관련
	          $acc_vat_dmonth_cnt = 0;
	          if($dp_info->acc_vat_yn == "Y") {
	            $sql_tax2 = ",vat_acc_yn='Y'";
	            $acc_vat_dmonth_cnt = $df_month;
	          }
	          $sql_basic .= ",vat_acc_yn ='".$vat_acc_yn."'";
	          //부가세 누적 = acc_vat_paid_yn 부가세지급여부 acc_vat_month=부가세 누적개월
	          $vv = $this->db->select('sum(acc_vat_dmonth) as avd, count(acc_vat_dmonth) as cnt')->where('dp_id', $dp_info->ws_co_id)->get('tbl_delivery_fee')->row();
	          if(!empty($vv->avd)) $sql_basic .= ",acc_vat_sum='".$vv->avd."'";
	          if(!empty($vv->cnt)) $sql_basic .= ",acc_vat_dmonth_cnt ='".$vv->cnt."'";


	          //--------------------------------- 1. 공제청구사(위수탁관리사) 등 회사정보
	          $gj_rq_co = $this->db->where('dp_id', $dp_info->ws_co_id)->get('tbl_members')->row();
	          if(!empty($rco->rco_name)) $r_co_id = $rco->rco_id;//실수요처
	          if(!empty($rco->rco_name)) $r_co_name = $rco->rco_name;//실수요처
	          $s_co_name = $dp_info->driver;//운영공제(송금)사

	          //과세여부
	          $tax_yn = $dp_info->tax_yn;
	          // 회사정보쿼리
	          if(!empty($dp_info->ctr_id)) $sql_co_info .= ",tr_id='".$dp_info->ctr_id."'";
	          if(!empty($dp_info->ws_co_id)) $sql_co_info .= ", gongje_req_co='".$dp_info->ws_co_id."'";

	          //--------------------------------- 2. 관리비정보
	          $sql_mf .= "dp_id ='".$dp_info->dp_id."'";
	          $sql_mf .= ",df_month ='".$df_month."'";
	          $sql_mf .= ",co_code ='".$dp_info->code."'";
	          if(!empty($dp_info->ws_co_id)) $sql_mf .= ", ws_co_id='".$dp_info->ws_co_id."'";
	          $mf = $this->db->where('dp_id', $dp_info->dp_id)->where('master_yn', 'Y')->get('tbl_delivery_fee_fixmfee_set')->row(); //기본정보

	          if(!empty($mf->wst_mfee)) $wst_mfee = $mf->wst_mfee; else $wst_mfee = 0;
	          $wsm_sum += $wst_mfee;

	          if(!empty($mf->mfee_vat)) { $mfee_vat = $mf->mfee_vat; $wsm_sum += $mf->mfee_vat; } else $mfee_vat = 0;
	          if(!empty($mf->org_fee)) { $org_fee = $mf->org_fee; $wsm_sum += $mf->org_fee; } else $org_fee = 0;
	          if(!empty($mf->grg_fee)) { $grg_fee = $mf->grg_fee; $wsm_sum += $mf->grg_fee; } else $grg_fee = 0;
	          if(!empty($mf->grg_fee_vat)) { $grg_fee_vat = $mf->grg_fee_vat; $wsm_sum += $mf->grg_fee_vat; } else $grg_fee_vat = 0;
	          $org_fee_vat = 0;
	          $env_fee = 0;
	          $env_fee_vat = 0;
	          $car_tax = 0;
	          $car_tax_vat = 0;
	          $etc = 0;
	          // 일할계산
	          if($is_ilhal == "Y") {
	            if($df_month == $inm || $df_month != $outm) {
	              if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)(($wst_mfee/$endDay) * $ilhal_days);
	              if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)(($mfee_vat/$endDay) * $ilhal_days);
	            } else if($ilhal_days < 15) {
	              if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)($wst_mfee/2);
	              if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)($mfee_vat/2);
	            }

	          }
	          $sql_mf .= ",wst_mfee='".$wst_mfee."'";
	          $sql_mf .= ",mfee_vat='".$mfee_vat."'";
	          $sql_mf .= ",org_fee='".$org_fee."'";
	          $sql_mf .= ",grg_fee='".$grg_fee."'";
	          $sql_mf .= ",grg_fee_vat='".$grg_fee_vat."'";

	          //--------------------------------- 3. 은행정보
	          if(!empty($dp_info->bank_code)) $sql_bank .= ",bank_code='".$dp_info->bank_code."'";
	          if(!empty($dp_info->bank)) $sql_bank .= ",bank='".$dp_info->bank."'";
	          if(!empty($dp_info->account_no)) $sql_bank .= ",account_no='".$dp_info->account_no."'";
	          if(!empty($dp_info->account_name)) $sql_bank .= ",account_name='".$dp_info->account_name."'";
	          if(!empty($dp_info->account_relation)) $sql_bank .= ",account_relation='".$dp_info->account_relation."'";

	          //--------------------------------- 4. 계약단가정보
	          if(!empty($ct_id)) {
	            $cu = $this->db->where('dp_id', $dp_info->dp_id)->where('ct_id', $ct_id)->get('tbl_scontract_co')->row(); //기본정보
	            if(!empty($cu->val1)) $sql_unitprice .= ",unit_price1='".$cu->val1."'";
	            if(!empty($cu->val2)) $sql_unitprice .= ",unit_price2='".$cu->val2."'";
	            if(!empty($cu->val3)) $sql_unitprice .= ",unit_price3='".$cu->val3."'";
	            if(!empty($cu->val4)) $sql_unitprice .= ",unit_price4='".$cu->val4."'";
	          }

	          //--------------------------------- 5. 수당
	          if(!empty($ct_id)) {
	            $sd = $this->db->where('dp_id', $dp_info->dp_id)->where('ct_id', $ct_id)->get('tbl_delivery_fee_bsudang')->row(); //기본정보
	            if((!empty($sd->sanje_yn) && !empty($sd->sanje_amount)) && $sd->sanje_yn == "Y") $sanje_amount = $sd->sanje_amount; else $sanje_amount = 0;

	            if(!empty($sd->manager_pickup)) $sql_basic_sudang .= ",manager_pickup='".$sd->manager_pickup."'";
	            if(!empty($sd->team_leader)) $sql_basic_sudang .= ",team_leader='".$sd->team_leader."'";
	            if(!empty($sd->diffcult_area)) $sql_basic_sudang .= ",diffcult_area='".$sd->diffcult_area."'";
	            if(!empty($sd->subside)) $sql_basic_sudang .= ",subside='".$sd->subside."'";
	            if(!empty($sd->hoisik)) $sql_basic_sudang .= ",hoisik='".$sd->hoisik."'";
	            if(!empty($sd->sudang_etc)) $sql_basic_sudang .= ",sudang_etc='".$sd->sudang_etc."'";
	            if(!empty($sd->sanje_yn)) $sql_basic_sudang .= ",sanje_yn='".$sd->sanje_yn."'";
	            $sql_basic_sudang .= ",sanje_amount='".$sanje_amount."'";
	          }

	          //--------------------------------- 6. 보험 tbl_asset_truck_insur_sub tr_id & pay_month  pay_amt
	          if(!empty($dp_info->ctr_id)) {
	            $qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
	            $qry .= " where a.type = '자동차' and b.gj_month = '".$df_month."' and (a.tr_id = '".$dp_info->ctr_id."') and a.co_pay<>'Y' and b.pay_by='01' and a.active_yn='Y'";
	            $ins = $this->cowork_model->db->query($qry)->row();
	            if(!empty($ins->pay_amt))
	              $ins_fee = $ins->pay_amt;
	            else
	              $ins_fee =0;

	            //if($dp_info->dp_id == "2656") echo $qry;
	//01 공제 > 미수와 미납에 동시등록 -> 회계등록
	//02 회사부담 > 미납에등록 -> 회계등록

	            $qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
	            $qry .= " where a.type = '적재물' and b.gj_month = '".$df_month."' and (a.tr_id = '".$dp_info->ctr_id."') and a.co_pay<>'Y' and b.pay_by='01' and a.active_yn='Y'";
	            $lins = $this->cowork_model->db->query($qry)->row();
	            if(!empty($lins->pay_amt))
	              $lins_fee = $lins->pay_amt;
	            else
	              $lins_fee =0;

	            // 일할계산
	            if($is_ilhal == "Y") {
	              //if(!empty($ins_fee) && $ins_fee > 0) $ins_fee = ($ins_fee/$endDay) * $ilhal_days;
	              //if(!empty($lins_fee) && $lins_fee > 0) $lins_fee = ($lins_fee/$endDay) * $ilhal_days;
	            }
	            $sql_insure .= ",ins_car ='".$ins_fee."'";
	            $sql_insure .= ",ins_load ='".$lins_fee."'";

	          }
	          //DB 삽입
	          //echo "--".$dp_info->ceo."---".$sql_insure;
	          $sql = "INSERT INTO tbl_delivery_fee SET tr_type = 'M', $sql_basic $sql_co_info $sql_unitprice $sql_bank $sql_basic_sudang $sql_basic_gongje $sql_ilhal";
	          $this->cowork_model->db->query($sql);
	          $df_id = $this->cowork_model->db->insert_id();

	          //전월 미납건체크
	          $pdate = $df_month . "-01";
	          $pm=date("Y-m",strtotime($pdate.'-1month'));
	          $not_paid = 0;
	          $plv = $this->db->where('df_month', $pm)->where('dp_id', $dp_info->dp_id)->get('tbl_delivery_fee_levy')->row();
	          if(!empty($plv->pay_status) && $plv->pay_status != 'B') { //납부전상태가 아니면
	            $not_paid = $plv->balance_amount;
	            $sql = "insert into tbl_delivery_fee_gongje set not_paid = '".$not_paid."', df_id='".$df_id."'";
	            $this->cowork_model->db->query($sql);
	          }

	      }
	    }



	  }
	}

} //if(0)
	  //    redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
	//include "./admin/cowork/_jiip_gongje_generate_all.inc.php";
}



//추가항목관련
$add_sudang_cnt = 0;
$add_gongje01_cnt = 0;
$add_gongje02_cnt = 0;
$add_gongje03_cnt = 0;

$add_sudang_cols = "";
$add_gongje01_cols = "";
$add_gongje02_cols = "";
$add_gongje03_cols = "";

if (!empty($all_sudang_group)) {
	foreach ($all_sudang_group as $sudang_info) {
		if($sudang_info->add_type == 'S') { //일반수당
			$add_sudang_cnt++;
			$add_sudang_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$sudang_info->title."</td>";
		}
	}
}

if (!empty($all_gongje_group)) {
	foreach ($all_gongje_group as $gongje_info) {
		if($gongje_info->add_type == 'GW') { //위수탁
			$add_gongje01_cnt++;
			$add_gongje01_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GN') { //일반
			$add_gongje02_cnt++;
			$add_gongje02_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GR') { //환급
			$add_gongje03_cnt++;
			$add_gongje03_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		}
	}
}

//임시 처리
$pdate = $df_month . "-01";
$pm=date("Y-m",strtotime($pdate.'-1month'));


$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));


//추가항목관련
$add_sudang_cnt = 0;
$add_gongje01_cnt = 0;
$add_gongje02_cnt = 0;
$add_gongje03_cnt = 0;

$add_sudang_cols = "";
$add_gongje01_cols = "";
$add_gongje02_cols = "";
$add_gongje03_cols = "";

if (!empty($all_sudang_group)) {
	foreach ($all_sudang_group as $sudang_info) {
		if($sudang_info->add_type == 'S') { //일반수당
			$add_sudang_cnt++;
			$add_sudang_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$sudang_info->title."</td>";
		}
	}
}

if (!empty($all_gongje_group)) {
	foreach ($all_gongje_group as $gongje_info) {
		if($gongje_info->add_type == 'GW') { //위수탁
			$add_gongje01_cnt++;
			$add_gongje01_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GN') { //일반
			$add_gongje02_cnt++;
			$add_gongje02_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GR') { //환급
			$add_gongje03_cnt++;
			$add_gongje03_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		}
	}
}

if (1) { //$this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script>
	function goDeleteMfee() {
		location.href = "<?php echo base_url() ?>admin/cowork/delete_mfee/<?=(!empty($df_month))?$df_month:""?>/<?=(!empty($gongje_req_co))?$gongje_req_co:""?>";
	}

	function popSpecifications() {
	  window.open('http://delex.delta-on.com/prn/work.htm', 'winSF', 'left=50, top=50, width=710, height=650, scrollbars=1');
	}

	function check_all(f)
	{
		var chk = document.getElementsByName("sel_df[]");

		for (i=0; i<chk.length; i++)
			chk[i].checked = f.chkall.checked;
	}

	function popAsWindow(md,tr_id) {
	  window.open('<?php echo base_url(); ?>admin/asset/pop_carinfo/'+md+'/'+tr_id, 'winAS', 'left=50, top=50, width=1480, height=700, scrollbars=1');
	}
	function selectPartner(md,params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
	function goSearch(val) {
		if(val == 'all' || val == '전체') {
			document.myform.action = "<?php echo base_url() ?>admin/cowork/jiip_gongje_all";
			document.myform.ws_co_id.value = '';
		} else {
			document.myform.action = "<?php echo base_url() ?>admin/cowork/jiip_gongje";
		}
		document.myform.submit();
	}

	function goGenerate(gtype) {
		document.myform.action = "<?php echo base_url() ?>admin/cowork/jiip_gongje/<?=$df_month?>/<?=$gongje_req_co?>/generate_"+gtype;
		document.myform.submit();
	  if(confirm("기존데이터는 삭제됩니다. 진행하시겠습니까?")) {
		$("#myform").attr("target", "_self");
		$("#myform").attr("action","<?php echo base_url() ?>admin/cowork/jiip_gongje/<?=$df_month?>/<?=$gongje_req_co?>/generate_"+gtype);
		$("#myform").submit();
	  }
	}

	//총마감 - 기사 보기 가능
	function setClosing(yn) {
		$("#ws_mode").val('CL');
		$("#close_yn").val(yn);
		document.myform.submit();
	}

	//입력마감
	function setInput(yn) {
		$("#ws_mode").val('IC');
		$("#close_yn").val(yn);
		document.myform.submit();
	}
	function goPrintAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje/'+df_month+'/'+gongje_req_co+'/print/';
		document.myform.target = '_blank';
		document.myform.submit();
	}
	function goExcelAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje/'+df_month+'/'+gongje_req_co+'/excel/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	function goExcelListAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje/'+df_month+'/'+gongje_req_co+'/excel_list/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	//임시처리
	function goExcelSingle(df_id) {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje/'+df_month+'/'+gongje_req_co+'/excel/'+df_id+'/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	function goCreateTaxInvoice() {
		//
	}
</script>


            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">

    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/cowork/jiip_gongje"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
				<input type="hidden" name="ws_mode" id="ws_mode" value="">
				<input type="hidden" name="close_yn" id="close_yn" value="">
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff">
              <tr>
                <td width="6%" height="20" align="center" bgcolor="#efefef">년월</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff" >
                            <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($df_month)) {
                                    echo $df_month;
                                }
                                ?>" class="form-control monthyear" name="df_month" id="df_month"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;" onChange="document.myform.submit();">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                </td>
                <td align="center" bgcolor="#efefef">공제청구사</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff;">
						<input type="hidden" name="gongje_req_co" id="gongje_req_co" value="<?php
                                                               if (!empty($gongje_req_co)) {
                                                                   echo $gongje_req_co;
                                                               }
                                                               ?>">
						<input type="text" name="gongje_req_co_name" id="gongje_req_co_name" value="<?=(empty($gongje_req_co_name))?"":$gongje_req_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('group','gongje_req_co||gongje_req_co_name||__||__||__||__||__||__');" onChange="goSearch(this.value)">

                </td>
                <td align="center" bgcolor="#efefef">파트너통합정보</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="text" name="search_keyword" id="search_keyword" value="<?=$search_keyword?>" class="form-control" style="width:100%;">
                </td>
			</tr>
			<tr>
                <td style="padding-left:5px;padding-top:10px;" colspan="10" align="center" bgcolor="#ffffff">

					<button name="sbtn" class="dt-button buttons-print btn btn-success mr btn-xs" id="file-save-button" type="button" onClick="goSearch(document.myform.gongje_req_co.value);" value="1"><i class="fa fa-search"> </i>검색</button>

					<a href="javascript:javascript:goGenerate('all');" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 1. 청구서 <?=($total_count > 0)?"재":""?>생성</span>
					</a>
<?php if($total_count > 0) { ?>
					<a href="javascript:javascript:goGenerate('added');" tabindex="0" class="dt-button buttons-print btn btn-success btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 추가등록분 생성</span>
					</a>
<?php } ?>
					<!--a href="javascript:javascript:goGenerate('selected');" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 선택 청구서 재생성</span>
					</a-->

					<!--a href="/admin/cowork/generate_mfee/<?php echo $df_month;?>" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 1. 청구서생성</span>
					</a-->

					<a href="javascript:setInput('Y');" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 2. 추가입력데이터 적용</span>
					</a>
					<a href="javascript:setClosing('Y');" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 3. 마감</span>
					</a>
<?php if(!empty($gongje_req_co)) { ?>
					<a href="javascript:setClosing('N');" tabindex="0" class="dt-button buttons-print btn btn-success btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 당월분삭제</span>
					</a>
<?php } ?>


					<a href="javascript:setClosing('N');" tabindex="0" class="dt-button buttons-print btn btn-success btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 마감취소</span>
					</a>

<?php if(1) { ?>
					<a href="javascript:goCreateTaxInvoice()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 세금계산서생성</span>
					</a>
<?php } else { ?>
					<a href="javascript:goCloseTaxInvoice()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 세금계산서마감</span>
					</a>
<?php } ?>

					<a href="javascript:goPrintAll()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 청구서 출력</span>
					</a>

					<input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
					<a href="javascript:goExcelAll()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 청구서 엑셀저장</span>
					</a>
					<a href="javascript:goExcelListAll()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 목록 엑셀저장</span>
					</a>
					<!--a href="javascript:goPrintAllTax()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 유통물류에 적용</span>
					</a-->

				</td>

			  </tr>
            </table>
		  </td>
        </tr>
      </table>
      <!-- 검색 끝 -->



            </div>




<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">

        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#client_list" data-toggle="tab"><span style="color:blue;"><?php echo $df_month; ?></span> 위수탁관리비청구현황(총 <?=number_format($total_count)?>건)</a></li>



            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane active" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
							<table id="FDataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>


        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
          <!--td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">등록<br/>마감</td-->
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>마감</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>기사<br/>확인</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>공제청구사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>실수요처</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>운영공제(송금)사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>사업자등록</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2">계약기간</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(6+$add_gongje01_cnt)?>">위.수탁관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">각종보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(7+$add_gongje02_cnt)?>">일반공제</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="<?=(2+$add_gongje03_cnt)?>">환급형공제</td>
          <!--td style="color:#ffffff;background-color: #777777;" colspan="3">누적금환급</td-->

          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;border-right:1px solid #eee;" rowspan="2"><br/>공제총액</td>
          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;border-right:1px solid #eee;" rowspan="2"><br/>하도급지급</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">환급금지급후<br>공제청구총액</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">상세<br>보기</td>
        </tr>


        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종료일</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">협회비</td>
		  <?=$add_gongje01_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자동차</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">적재물</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소  계</td>

		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">미수금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과태료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자동차세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">환경부담금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
		  <?=$add_gongje02_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">해지담보</td>
		  <?=$add_gongje03_cols?>
          <td style="color:#ffffff;background-color: #777777;">소  계</td>
		</tr>


								</thead>
                                <tbody>



                                <?php
								$j = 0;
                                if (!empty($all_delivery_fee_info)) {
                                    foreach ($all_delivery_fee_info as $delivery_fee_details) {
										$sn = $total_count--;
										$add_sudang_vals = "";
										$add_gongje01_vals = "";
										$add_gongje02_vals = "";
										$add_gongje03_vals = "";

										//공제청구사
										$gj_rq_co = "";
										if(!empty($delivery_fee_details->gongje_req_co)) {
											$gj_rq_co = $this->db->where('dp_id', $delivery_fee_details->gongje_req_co)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//차량정보
										$truck = $this->db->where('idx', $delivery_fee_details->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										//위수탁관리비
										$wsm_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get('tbl_delivery_fee_fixmfee')->row();

										if(empty($wsm_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $wsm_info->wst_mfee;
										if(empty($wsm_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $wsm_info->mfee_vat;
										if(empty($wsm_info->org_fee)) $org_fee = 0; else $org_fee = $wsm_info->org_fee;
										if(empty($wsm_info->grg_fee)) $mgrg_fee = 0; else $mgrg_fee = $wsm_info->grg_fee;
										if(empty($wsm_info->grg_fee_vat)) $mgrg_fee_vat = 0; else $mgrg_fee_vat = $wsm_info->grg_fee_vat;
										if(empty($wsm_info->etc)) $etc= 0; else $etc = $wsm_info->etc;
										$org_fee_vat = 0;
										$env_fee = 0;
										$env_fee_vat = 0;
										$car_tax = 0;
										$car_tax_vat = 0;
										$etc = 0;
										$wsm_sum = ($wst_mfee + $mfee_vat + $org_fee + $env_fee + $car_tax + $mgrg_fee + $mgrg_fee_vat + $etc);

										//각종보험
										$insur_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_insur')->row();
										if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
										if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;
										if(empty($insur_info->ins_grnt)) $ins_grnt = 0; else $ins_grnt = $insur_info->ins_grnt;
										$insur_sum = ($ins_car + $ins_load + $ins_grnt);

										//일반공제
										$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										if(empty($gongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $gongje_info->fine_fee;
										if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;
										if(empty($gongje_info->car_tax)) $car_tax = 0; else $car_tax = $gongje_info->car_tax;
										if(empty($gongje_info->env_fee)) $env_fee = 0; else $env_fee = $gongje_info->env_fee;
										if(empty($gongje_info->grg_fee)) $grg_fee = 0; else $grg_fee = $gongje_info->grg_fee;
										if(empty($gongje_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $gongje_info->grg_fee_vat;
										// 벌금
										$fine_sum = $this->db->select('sum(amount) as sum')->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get("tbl_delivery_fee_gongje_finefee")->row();
										if(!empty($fine_sum->sum) && $fine_sum->sum > 0) { // 기존버전과 호환 유지
											$fine_fee += $fine_sum->sum;
										}
										$gongje_sum = ($fine_fee + $not_paid + $car_tax + $env_fee + $grg_fee + $grg_fee_vat);

										//환급형공제
										$rf_gongje_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get('tbl_delivery_fee_refund_gongje')->row();
										if(empty($rf_gongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rf_gongje_info->gj_termination_mortgage;
										$rf_gongje_sum = ($gj_termination_mortgage);

										//---------------------------------------------------------------------------------  추가 항목
										if (!empty($all_sudang_group)) {
											foreach ($all_sudang_group as $sudang_info) {
												if($sudang_info->add_type == 'S') { //일반수당
												}
											}
										}

										if (!empty($all_gongje_group)) {
											foreach ($all_gongje_group as $gongje_info) {
												$add_item = $this->db->where('pid', $gongje_info->idx)->where('df_month', $df_month)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_add')->row();
												if($gongje_info->add_type == 'GW') { //위수탁
													if(!empty($add_item->amount)) {
														$wsm_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje01_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje01_vals .= "<td style='text-align:right;'>*</td>";
												} else if($gongje_info->add_type == 'GN') { //일반
													if(!empty($add_item->amount)) {
														$gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje02_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje02_vals .= "<td style='text-align:right;'>*</td>";

												} else if($gongje_info->add_type == 'GR') { //환급
													if(!empty($add_item->amount)) {
														$rf_gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje03_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje03_vals .= "<td style='text-align:right;'>*</td>";
												}
											}
										}
										//---------------------------------------------------------------------------------  추가 항목



										$tot_gongje = ($wsm_sum + $insur_sum + $gongje_sum + $rf_gongje_sum);
										$tot_gongje_req = ($tot_gongje - $rf_gongje_sum);


										$lv = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get('tbl_delivery_fee_levy')->row();
										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($lv->pay_status)) {
											if($lv->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($lv->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; }
											else if($lv->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; }
										}

										$is_editable = "Y";
										if($delivery_fee_details->is_closed=='Y') $is_editable = "N";
										if($ws_mode == "IC") { // 등록마감처리
											$sql_insure = "";
											$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
											$qry .= " where a.type = '자동차' and b.gj_month = '".$df_month."' and a.tr_id = '".$delivery_fee_details->tr_id."' and a.active_yn='Y' and b.pay_by='01'";
											$ins = $this->cowork_model->db->query($qry)->row();
											if(!empty($ins->pay_amt)) {
												$ins_car = $ins->pay_amt;
											} else {
												$ins_car = 0;
											}
											$sql_insure .= "ins_car ='".$ins_car."'";

											$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
											$qry .= " where a.type = '적재물' and b.gj_month = '".$df_month."' and a.tr_id = '".$delivery_fee_details->tr_id."' and a.active_yn='Y' and b.pay_by='01'";
											$lins = $this->cowork_model->db->query($qry)->row();
											if(!empty($lins->pay_amt)) {
												$ins_load = $lins->pay_amt;
											} else {
												$ins_load = 0;
											}
											$sql_insure .= ",ins_load ='".$ins_load."'";

											$sql = "update tbl_delivery_fee set is_confirm = '".$close_yn."' where df_id='".$delivery_fee_details->df_id."'";
											$this->cowork_model->db->query($sql);
											$sql = "update tbl_delivery_fee_insur set $sql_insure where df_id='".$delivery_fee_details->df_id."'";
											$this->cowork_model->db->query($sql);
											$this->cowork_model->db->query($sql);

											$insur_sum = ($ins_car + $ins_load + $ins_grnt);
											$delivery_fee_details->is_confirm = $close_yn;

										}

										if($ws_mode == "CL") { // 마감처리
										//if(empty($lv->pay_status)) {
										//$close_yn = "Y";
											$sql = "update tbl_delivery_fee set is_closed = '".$close_yn."' where df_id='".$delivery_fee_details->df_id."'";
											$this->cowork_model->db->query($sql);


											//수납생성
											$gn_amount = $tot_gongje_req;
											$stot_mfee = $tot_gongje_req;
											$stot_transaction = 0;
											$tot_mfee = 0;
											$tot_transaction = 0;
											if(!empty($delivery_fee_details->stot_transaction)) $stot_transaction = $delivery_fee_details->stot_transaction;

											//거래등록시
											if($stot_transaction > 0) {
												if($stot_mfee >= $stot_transaction) {
													$tot_mfee = $stot_mfee - $stot_transaction;
												} else {
													$tot_transaction = $stot_transaction - $stot_mfee;
												}
											} else {
												$tot_mfee = $stot_mfee;
											}
											$qry_subtotals = ", mf_amount='".$stot_mfee."', tr_amount='".$stot_transaction."'";

											$chk = $this->cowork_model->db->query("select df_id,gongje_req_co from tbl_delivery_fee_levy where df_id='".$delivery_fee_details->df_id."'")->row();
											if(empty($chk->df_id)) {
												$sql = "INSERT INTO tbl_delivery_fee_levy SET df_id='".$delivery_fee_details->df_id."', dp_id='".$delivery_fee_details->dp_id."', df_month='$df_month', gongje_req_co = '".$delivery_fee_details->ws_co_id."'";
												$sql .= ", pay_status='N', prev_misu = '$not_paid' ,gn_amount = '".$gn_amount."', paid_amount = '0', balance_amount = '".$gn_amount."'".$qry_subtotals;
											} else {
												$sql = "UPDATE tbl_delivery_fee_levy SET prev_misu = '$not_paid' ,gn_amount = '".$gn_amount."', paid_amount = '0', balance_amount = '".$gn_amount."'".$qry_subtotals." WHERE df_id='".$delivery_fee_details->df_id."'";
											}

											if($stot_mfee >= $stot_transaction) $gn_amount = $stot_mfee - $stot_transaction;
											$this->cowork_model->db->query($sql);

											$sql = "UPDATE tbl_delivery_fee SET stot_mfee='".$stot_mfee."', stot_transaction='".$stot_transaction."', tot_mfee='".$tot_mfee."', tot_transaction='".$tot_transaction."' WHERE df_id='".$delivery_fee_details->df_id."'";
											//echo $sql."</br>";
											//$this->cowork_model->db->query($sql);

											$delivery_fee_details->is_closed = $close_yn;
										}


$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
$plog = $this->db->select('sum(pay_amount) as total')->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy_log')->row();

//미납 누락건 발생 원인찾아 수정할것  1. 현월 levy 잔액 == 익월 미수 금액 비교 >>>> 다를경우 처리 >> 익월 levy 값 보정


if(0) { //empty($lv->idx) || $lv->balance_amount != $tot_gongje_req) {
	if(empty($lv->idx)) {
		$sql = "INSERT INTO tbl_delivery_fee_levy SET df_id = '".$delivery_fee_details->df_id."', df_month = '".$df_month."', prev_misu = '".$not_paid."', dp_id = '".$delivery_fee_details->dp_id."', gn_amount = '".$tot_gongje_req."', paid_amount = '0', mf_amount = '".$tot_gongje_req."', balance_amount = '".$tot_gongje_req."'";
	} else {
		$sql = "UPDATE tbl_delivery_fee_levy SET gn_amount = '".$tot_gongje_req."', paid_amount = '0', balance_amount = '".$tot_gongje_req."' WHERE idx='".$lv->idx."'";
	}
	//echo $delivery_fee_details->co_name. "/" .$delivery_fee_details->ceo."---".$sql. ":</br>"; // . $lv->balance_amount ."!=". $tot_gongje_req . " ".$sql. "</br>";
	//$this->cowork_model->db->query($sql);
}



if(0) { //empty($lv->idx) || $lv->balance_amount == 0) {

												$sql = "update tbl_delivery_fee set is_closed = '".$close_yn."' where df_id='".$delivery_fee_details->df_id."'";
												echo $delivery_fee_details->is_closed."-".$sql."</br>";
//												$this->cowork_model->db->query($sql);

												//수납생성
												$gn_amount = $tot_gongje_req;
												$stot_mfee = $tot_gongje_req;
												$stot_transaction = 0;
												$tot_mfee = 0;
												$tot_transaction = 0;
												if(!empty($delivery_fee_details->stot_transaction)) $stot_transaction = $delivery_fee_details->stot_transaction;

												//거래등록시
												if($stot_transaction > 0) {
													if($stot_mfee >= $stot_transaction) {
														$tot_mfee = $stot_mfee - $stot_transaction;
													} else {
														$tot_transaction = $stot_transaction - $stot_mfee;
													}
												} else {
													$tot_mfee = $stot_mfee;
												}
												$qry_subtotals = ", mf_amount='".$stot_mfee."', tr_amount='".$stot_transaction."'";

												$chk = $this->cowork_model->db->query("select df_id,gongje_req_co from tbl_delivery_fee_levy where df_id='".$delivery_fee_details->df_id."'")->row();
												if(empty($chk->df_id)) {
													$sql = "INSERT INTO tbl_delivery_fee_levy SET df_id='".$delivery_fee_details->df_id."', dp_id='".$delivery_fee_details->dp_id."', df_month='$df_month', gongje_req_co = '".$gongje_req_co."'";
													$sql .= ", pay_status='N', prev_misu = '$not_paid' ,gn_amount = '".$gn_amount."', paid_amount = '0', balance_amount = '".$gn_amount."'".$qry_subtotals;
												} else {
													$sql = "UPDATE tbl_delivery_fee_levy SET prev_misu = '$not_paid' ,gn_amount = '".$gn_amount."', paid_amount = '0', balance_amount = '".$gn_amount."'".$qry_subtotals." WHERE df_id='".$delivery_fee_details->df_id."'";
												}

												if($stot_mfee >= $stot_transaction) $gn_amount = $stot_mfee - $stot_transaction;
//												echo "-".$sql."</br>";
												$this->cowork_model->db->query($sql);

	if(empty($lv->idx) ) {
		$sql = $delivery_fee_details->co_name . $sql . "ㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌㅌ";
	} else if( $lv->balance_amount == 0) {
		$sql = $delivery_fee_details->co_name .  $sql . "11111111111111111111111111111111111111111111111111111";
	}
}

	if(0) { //empty($lv->idx)) { //$tot_gongje_req != $lv->gn_amount && $tot_gongje_req > $lv->tr_amount) {
	$close_yn = "Y";

											$sql = "update tbl_delivery_fee set is_closed = '".$close_yn."' where df_id='".$delivery_fee_details->df_id."'";
											echo $delivery_fee_details->co_name."-".$sql."</br>";
											$this->cowork_model->db->query($sql);


											//수납생성
											$gn_amount = $tot_gongje_req;
											$stot_mfee = $tot_gongje_req;
											$stot_transaction = 0;
											$tot_mfee = 0;
											$tot_transaction = 0;
											if(!empty($delivery_fee_details->stot_transaction)) $stot_transaction = $delivery_fee_details->stot_transaction;

											//거래등록시
											if($stot_transaction > 0) {
												if($stot_mfee >= $stot_transaction) {
													$tot_mfee = $stot_mfee - $stot_transaction;
												} else {
													$tot_transaction = $stot_transaction - $stot_mfee;
												}
											} else {
												$tot_mfee = $stot_mfee;
											}
											$qry_subtotals = ", mf_amount='".$stot_mfee."', tr_amount='".$stot_transaction."'";

											$chk = $this->cowork_model->db->query("select df_id,gongje_req_co from tbl_delivery_fee_levy where df_id='".$delivery_fee_details->df_id."'")->row();
											if(empty($chk->df_id)) {
												$sql = "INSERT INTO tbl_delivery_fee_levy SET df_id='".$delivery_fee_details->df_id."', dp_id='".$delivery_fee_details->dp_id."', df_month='$df_month', gongje_req_co = '".$gongje_req_co."'";
												$sql .= ", pay_status='N', prev_misu = '$not_paid' ,gn_amount = '".$gn_amount."', paid_amount = '0', balance_amount = '".$gn_amount."'".$qry_subtotals;
											} else {
												$sql = "UPDATE tbl_delivery_fee_levy SET prev_misu = '$not_paid' ,gn_amount = '".$gn_amount."', paid_amount = '0', balance_amount = '".$gn_amount."'".$qry_subtotals." WHERE df_id='".$delivery_fee_details->df_id."'";
											}

											if($stot_mfee >= $stot_transaction) $gn_amount = $stot_mfee - $stot_transaction;
											//echo "-".$sql."</br>";
											$this->cowork_model->db->query($sql);
}


//----------------------------------------------------------------------------------- 과거 데이터 보정용
$pdate = $df_month . "-01";
$nm=date("Y-m",strtotime($pdate.'+1month'));
$lv = $this->db->where('df_month', $df_month)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
$next_mfee = $this->db->where('df_month', $nm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee')->row();
if(!empty($next_mfee->df_id)) {
	$next_gongje = $this->db->where('df_id', $next_mfee->df_id)->get('tbl_delivery_fee_gongje')->row();
	if ($lv->pay_status == 'F' && $next_gongje->not_paid > 0) { //$lv->balance_amount != $next_gongje->not_paid) {
		$sql = "UPDATE tbl_delivery_fee_gongje SET not_paid = '0' WHERE df_id='".$next_gongje->df_id."'";
		//echo $delivery_fee_details->ceo . "/df_id : ".$delivery_fee_details->df_id . " :: 당월 Levy 잔액 : ".$lv->balance_amount . "<<<>>> Levy 입금액". $lv->paid_amount. "<<<>>> 익월 미수액". $next_gongje->not_paid . "===".$sql."</br>";

	//$this->cowork_model->db->query($sql);

		$next_lv = $this->db->where('df_id', $next_mfee->df_id)->get('tbl_delivery_fee_levy')->row();
		// 익월 총액 != next levy ->balance_amount       mf gn balance
		if($tot_gongje_req != $next_lv->balance_amount) {
			$sql = "UPDATE tbl_delivery_fee_levy SET mf_amount = '$tot_gongje_req', balance_amount = '$tot_gongje_req', gn_amount = '$tot_gongje_req' WHERE df_id='".$next_gongje->df_id."'";
			//echo "xxxxxxxxxxxxx".$delivery_fee_details->ceo . "/df_id : ".$delivery_fee_details->df_id . " :: 당월 Levy 잔액 : ".$lv->balance_amount . "<<<>>> Levy 입금액". $lv->paid_amount. "<<<>>> 익월 미수액". $next_gongje->not_paid . "===".$sql."</br>";
		}
	}
}



?>
                                   <tr>
                                        <td>
											<?php if($delivery_fee_details->is_closed != 'Y') { ?>
												<a href="<?= base_url() ?>admin/cowork/jiip_gongje/<?=$df_month?>/<?=$gongje_req_co?>/generate_selected/<?=$delivery_fee_details->df_id?>/<?=$delivery_fee_details->dp_id?>" class="dt-button buttons-print btn btn-danger btn-xs mr">재생성</a>
											<?php } ?>
                                           <span class='label label-<?=$pay_status_color?>'><?=$pay_status?></span>
                                            <?= $sn ?>
                                        </td>
                                        <td style="text-align:center;">
                                                <div class="btn-group">
                                                    <button class="btn btn-xs btn-<?=($delivery_fee_details->is_closed=='Y')?"success":"danger"?> dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <?=($delivery_fee_details->is_closed=='Y')?"마감":"마감해제"?>
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu animated zoomIn">
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/close_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/Y">마감</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/close_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/N">취소</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                        </td>
                                        <td style="text-align:center;">
										<?=($delivery_fee_details->read_cnt > 0)?"<span class='label label-success'>".$delivery_fee_details->read_cnt."</span>":"<span class='label label-danger'>0</span>"?>

										</td>
                                        <td>
                                            <span class='label label-primary'><?php if(!empty($gj_rq_co)) echo $gj_rq_co; ?></span>
										</td>
                                        <td><?= $delivery_fee_details->D ?></td>
                                        <td><?= $delivery_fee_details->ceo ?>(<?= $delivery_fee_details->driver ?>)</td>
                                        <td><?= $delivery_fee_details->bs_number ?></td>
                                        <td>
										<?php if(!empty($truck->idx)) { ?>
                                              <span data-placement="top" data-toggle="tooltip" title="차량정보 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_info','<?=(!empty($truck->idx))? $truck->idx:"" ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										<?php } ?>

										<span class='label label-primary'><?= $truck_no ?></span>
                                        <td><?php if(!empty($delivery_fee_details->N)) echo $delivery_fee_details->N; ?></td>
                                        <td><?php if(!empty($delivery_fee_details->O)) echo $delivery_fee_details->O; ?></td>
                                        <td style="text-align:right;">
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="관리비 설정">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_mfee/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$is_editable?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										<?= number_format($wst_mfee) ?></td>
                                        <td style="text-align:right;"><? if(!empty($mfee_vat)) echo number_format($mfee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee)) echo number_format($mgrg_fee,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee_vat)) echo number_format($mgrg_fee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($org_fee)) echo number_format($org_fee,0); ?></td>
									  <?=$add_gongje01_vals?>
										<td style="text-align:right;"><? if(!empty($wsm_sum)) echo number_format($wsm_sum,0); ?></td>

										<td style="text-align:right;">
                                              <span data-placement="top" data-toggle="tooltip" title="보험 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_ins','<?= $delivery_fee_details->tr_id ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>

										<?= number_format($ins_car) ?></td>
										<td style="text-align:right;"><?= number_format($ins_load) ?></td>
										<td style="text-align:right;"><?= number_format($insur_sum) ?></td>

										<td style="text-align:right;">
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="초기미수금액 설정">
												<a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/cowork/set_misu/<?= $delivery_fee_details->df_id ?>/<?=$is_editable?>" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										<?= number_format($not_paid) ?></td>
										<td style="text-align:right;">
                                                <span data-placement="top" data-toggle="tooltip" title="일반공제 업데이트">
													<a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/cowork/set_ngongje/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$is_editable?>" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
                                                <span data-placement="top" data-toggle="tooltip" title="과태료 등록">
													<a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/cowork/set_finefee/list/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$is_editable?>" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>

										<?= number_format($fine_fee) ?>
										</td>

										<td style="text-align:right;"><?= number_format($car_tax) ?></td><!--//자동차세 -->
										<td style="text-align:right;"><?= number_format($env_fee) ?></td><!--//환경부담금 -->
										<td style="text-align:right;"><?= number_format($grg_fee) ?></td><!--//차고지비 -->
                                        <td style="text-align:right;"><?= number_format($grg_fee_vat) ?></td><!--//차고지비 부가세 -->

									  <?=$add_gongje02_vals?>

										<td style="text-align:right;"><?= number_format($gongje_sum) ?></td>

                                        <td style="text-align:right;">
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="환급형공제 업데이트">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_rgongje/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $gongje_req_co ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										<?= number_format($gj_termination_mortgage) ?></td>
									  <?=$add_gongje03_vals?>
										<td style="text-align:right;"><?= number_format($rf_gongje_sum) ?></td>

										<td style="text-align:right;"><?= number_format($tot_gongje) ?></td>
										<td style="text-align:right;"><?php if(!empty($lv->tr_amount)) echo number_format($lv->tr_amount); ?></td>
										<td style="text-align:right;"><?= number_format($tot_gongje_req) ?></td>
										<td>
										<a href="javascript:goExcelSingle('<?= $delivery_fee_details->df_id ?>')" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
											<span><i class="fa fa-file-excel-o"> </i> </span>
											</a>
											<a href="javascript:popSpecifications()" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables" title="명세서"> 명세서 </a>

										</td>

                                    </tr>
<?php
									$j++;
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="31">
                                            자료가 없습니다.
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
		</form>






                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
