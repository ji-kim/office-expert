<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
$created = can_action('122', 'created');
$edited = can_action('122', 'edited');
$deleted = can_action('122', 'deleted');
?>
<div class="panel panel-custom">
    <header class="panel-heading ">거래처설정</header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped ">
                <thead>
                <tr>
                    <th>거래처</th>
                    <th>대표</th>
                    <?php if (!empty($edited) || !empty($deleted)) { ?>
                        <th>작업</th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                $cnt = 0;
                if (!empty($all_coops_group)) {
                    foreach ($all_coops_group as $coop_info) {
                      $cnt++;
                        ?>
                        <tr id="cowork_info_<?= $coop_info->coop_id ?>">
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($coop_id) && $coop_id == $coop_info->coop_id) { ?>
                                <form method="post"
                                      action="<?= base_url() ?>admin/contract/pop_scontract_config/<?= $ct_id ?>/<?= $sub ?>/update_cowork/<?php
                                      if (!empty($coop_info)) {
                                          echo $coop_info->coop_id;
                                      }
                                      ?>" class="form-horizontal">
                                    <input type="hidden" name="customer_id" id="customer_id<?=$cnt?>">
                                    <input type="text" id="co_name<?=$cnt?>" value="<?php
                                    if (!empty($coop_info)) {
                                        echo $coop_info->co_name;
                                    }
                                    ?>" class="form-control" placeholder="회사명" onClick="selectCompany('customer','customer_id<?=$cnt?>||co_name<?=$cnt?>||__||__||__||__||ceo<?=$cnt?>||__');">
                                <?php } else {
                                    echo $coop_info->co_name;
                                }
                                ?></td>
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($coop_id) && $coop_id == $coop_info->coop_id) { ?>
                                    <input type="text" id="ceo<?=$cnt?>" class="form-control" value="<?php
                                    if (!empty($coop_info->ceo)) {
                                        echo $coop_info->ceo;
                                    }
                                    ?>"/>
                                <?php } else {
                                    if(!empty($coop_info->ceo)) echo $coop_info->ceo;
                                }
                                ?></td>
                            <?php if (!empty($edited) || !empty($deleted)) { ?>
                                <td>
                                    <?php
                                    $id = $this->uri->segment(5);
                                    if (!empty($coop_id) && $coop_id == $coop_info->coop_id) { ?>
                                        <?= btn_update() ?>
                                        </form>
                                        <?= btn_cancel('admin/contract/pop_scontract_config/' . $ct_id . '/' . $sub) ?>
                                    <?php } else { ?>
                                        <?php if (!empty($edited)) { ?>
                                            <?= btn_edit('admin/contract/pop_scontract_config/' . $ct_id . '/' . $sub . '/edit_cowork/' . $coop_info->coop_id ) ?>
                                        <?php } ?>
                                        <?php if (!empty($deleted)) { ?>
                                          <?= btn_delete('admin/contract/pop_scontract_config/' . $ct_id . '/' . $sub . '/delete_cowork/' . $coop_info->coop_id ) ?>
                                            <?php //echo ajax_anchor(base_url("admin/contract/delete_item/" . $coop_info->coop_id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => "삭제", "data-fade-out-on-success" => "#title_" . $coop_info->coop_id)); ?>
                                        <?php }
                                    }
                                    ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php
                    }
                }
                ?>
                <?php if (!empty($created) || !empty($edited)) { ?>
                    <form method="post" action="<?= base_url() ?>admin/contract/pop_scontract_config/<?= $ct_id ?>/<?= $sub ?>/insert_cowork"
                          class="form-horizontal" data-parsley-validate="" novalidate="">
                        <tr>
                            <td>
                              <input type="hidden" name="customer_id" id="customer_id">
                              <input type="text" id="co_name" class="form-control"
                                       placeholder="거래처" onClick="selectCompany('customer','customer_id||co_name||__||__||__||__||ceo||__');"></td>
                            <td>
                                <input id="ceo" placeholder="대표"
                                       class="form-control"/>
                            </td>
                            <td><?= btn_add() ?></td>
                        </tr>
                    </form>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
	function selectCompany(md,params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSCC', 'left=100, top=100, width=1200, height=700, scrollbars=1');
	}
</script>
