<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>




            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">

      <!-- 검색 시작 -->
                <form id="contract_form" name="contract_form" method="get" action="/admin/contract/scontract_list/">
                    <!-- 검색 시작 -->
                    <table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
                        <tr>
                            <td align="left" valign="top">
                                <table border="0" width="100%" cellpadding="2" cellspacing="5" bgcolor="#ffffff" class="search_table" style="padding-left:5px;font:12px 'Tahoma', sans-serif">
                                    <tr>
                                        <td width="10%" height="20" align="center" bgcolor="#efefef">계약명</td>
                                        <td style="padding-left:3px;" align="left" bgcolor="#ffffff">
                                            <input type="text" name="tr_title" id="tr_title" maxlength="20" value="<?=$tr_title?>" class="form-control" style="width:100%;">
                                        </td>
                                        <td width="10%" height="20" align="center" bgcolor="#efefef">공급사명</td>
                                        <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
                                            <input type="text" name="sco_name" id="sco_name" maxlength="20" value="<?=$sco_name?>" class="form-control" style="width:100%;">

                                        </td>
                                        <td width="10%" height="20" align="center" bgcolor="#efefef">실수요처</td>
                                        <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
                                            <input type="text" name="rco_name" id="rco_name" maxlength="20" value="<?=$rco_name?>" class="form-control" style="width:100%;">
                                        </td>
                                        <td style="padding-left:5px;padding-top:10px;" align="left" bgcolor="#ffffff" COLSPAN="9">
                                            <div align="CENTER">
                                                <a href="javascript:goInit()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
                                                    <span><i class="fa fa-search"> </i> RESET</span>
                                                </a>
                                                <a href="javascript:goSearch()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
                                                    <span><i class="fa fa-print"> </i> 검색</span>
                                                </a>
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
      <!-- 검색 끝 -->

  </div>




<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">

        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
              <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="<?php echo base_url(); ?>admin/contract/scontract_list/1">진행계약현황</a></li>
              <?php if($active == 9) {?><li class="<?= $active == 9 ? 'active' : '' ?>"><a href="#new_contract" data-toggle="tab"><?=$contract_info->tr_title?> 하도급설정</a></li><?php } ?>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active != 9 ? 'active' : '' ?>" id="scontract_list" style="position: relative;">
        <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('scontract_list') ?></strong></div>
                        </header>
        <?php } ?>
                        <div class="box">
							                <table id="DataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>
                                  <tr align="center" bgcolor="#e0e7ef">
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약명</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약형태</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">배차여부</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약(공급)사</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실수요처</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">등록일</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약금액</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약만료일</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">잔여일</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">작업</td>
                                  </tr>
                                </thead>
                                <tbody>

                                <?php
                                if (!empty($all_contract_group)) {
                                    foreach ($all_contract_group as $contract_details) {
                                        ?>
                                        <tr>
                                          <td align="left" style="padding-left:5px;"><?= $contract_details->tr_title  ?></td>
                                          <td height="25" align="center"><?=contract_type($contract_details->ct_type)?></td>
                                          <td align="center" style="padding-left:5px;"><?=$contract_details->scheduling_yn?>
                                          <?php if(!empty($contract_details->scheduling_yn)) { ?>
                                            <span data-placement="top" data-toggle="tooltip" title="배차설정">
                                              <a href="javascript:;" onClick="popAsWindow('car_info','<?=(!empty($truck->idx))? $truck->idx:"" ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                            </span>
                                          <?php } ?>
                                          </td>
                    										  <td align="center" style="padding-left:5px;"><?=$contract_details->sco_name?></td>
                    										  <td align="center" style="padding-left:5px;"><?= $contract_details->rco_name  ?></td>
                                          <td align="center" style="padding-left:5px;"><?= $contract_details->ct_date  ?></td>
                                          <td align="center" style="padding-left:5px;"><?=(!empty($contract_details->ct_amount))?number_format($contract_details->ct_amount,2):"-" ?></td>
                                          <td align="center" style="padding-left:5px;"><?= $contract_details->ct_sdate  ?></td>
                    										  <td align="center" style="padding-left:5px;"><?= $contract_details->ct_edate  ?></td>
                                          <td align="center" style="padding-left:5px;"><?=days_left($contract_details->ct_edate) ?></td>
                                          <td align="center" style="padding-left:5px;">
                                                <a title="하도급설정" href="javascript:popSCWindow('<?= $contract_details->idx ?>');" class="btn btn-xs btn-purple"><i class="fa fa-cog"></i> 하도급 설정</a>
                                          </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <?php if (!empty($created) || !empty($edited)) { ?>
                        <div class="tab-pane <?= $active == 9 ? 'active' : '' ?>" id="new_contract" style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/contract/save_contract/<?php
                              if (!empty($contract_info)) {
                                  echo $contract_info->idx;
                              }
                              ?>" method="post" class="form-horizontal  ">
                            <div class="panel-body">
                                <label class="control-label col-sm-3"></label
                                <div class="col-sm-6">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_contract"
                                                                  data-toggle="tab">계약기초정보</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** 계약기초정보 *************-->
                                            <div class="chart tab-pane active" id="general_contract">
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">계약명
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->tr_title)) {
                                                                   echo $contract_info->tr_title;
                                                               }
                                                               ?>" name="tr_title">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">약칭(5자이내)</label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->ct_title)) {
                                                                   echo $contract_info->ct_title;
                                                               }
                                                               ?>" name="ct_title" maxlength="5">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">계약형태
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                      <div class="input-group">
                                                          <select name="ct_type" class="form-control select_box" style="width: 100%">
                                                            <option value="01" <?php
                                                              if (!empty($contract_info->ct_type) && $contract_info->ct_type == "01") {
                                                                  echo 'selected';
                                                              } ?>
                                                            >총액단가</option>
                                                            <option value="02" <?php
                                                              if (!empty($contract_info->ct_type) && $contract_info->ct_type == "02") {
                                                                  echo 'selected';
                                                              } ?>
                                                            >건당단가</option>
                                                          </select>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">계약금
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->ct_amount)) {
                                                                   echo $contract_info->ct_amount;
                                                               }
                                                               ?>" name="ct_amount">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">하도급여부</label>
                                                    <div class="col-lg-5">
                                                        <input data-id="<?=(!empty($contract_info->idx))?$contract_info->idx:"" ?>" data-toggle="toggle"
                                                               name="is_subcontract" value="Y" <?php
                                                        if (!empty($contract_info->is_subcontract) && $contract_info->is_subcontract == 'Y') {
                                                            echo 'checked';
                                                        }
                                                        ?> data-on="Y" data-off="N"
                                                               data-onstyle="success btn-xs"
                                                               data-offstyle="danger btn-xs" type="checkbox">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">배차여부</label>
                                                    <div class="col-lg-5">
                                                        <input data-id="<?=(!empty($contract_info->idx))?$contract_info->idx:"" ?>" data-toggle="toggle"
                                                               name="scheduling_yn" value="Y" <?php
                                                        if (!empty($contract_info->scheduling_yn) && $contract_info->scheduling_yn == 'Y') {
                                                            echo 'checked';
                                                        }
                                                        ?> data-on="Y" data-off="N"
                                                               data-onstyle="success btn-xs"
                                                               data-offstyle="danger btn-xs" type="checkbox">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">계약(공급)사</label>
                                                    <div class="col-lg-5">
                                                      <div class="input-group">
                                                        <input type="hidden" name="s_co_id" id="s_co_id" value="<?=(!empty($contract_info->s_co_id))?$contract_info->s_co_id:""?>">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->sco_name)) {
                                                                   echo $contract_info->sco_name;
                                                               }
                                                               ?>" name="sco_name" id="sco_name" onClick="selectCompany('group','s_co_id||sco_name||__||__||__||__||__||__');">
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">실수요처</label>
                                                    <div class="col-lg-5">
                                                      <div class="input-group">
                                                        <input type="hidden" name="r_co_id" id="r_co_id" value="<?=(!empty($contract_info->r_co_id))?$contract_info->r_co_id:""?>">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->rco_name)) {
                                                                   echo $contract_info->rco_name;
                                                               }
                                                               ?>" name="rco_name" id="rco_name"id onClick="selectCompany('customer','r_co_id||rco_name||__||__||__||__||__||__');">
                                                      </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">계약일
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                      <div class="input-group">
                                                        <input type="text" class="form-control datepicker" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->ct_date)) {
                                                                   echo $contract_info->ct_date;
                                                               }
                                                               ?>" name="ct_date">
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">계약시작일
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                      <div class="input-group">
                                                        <input type="text" class="form-control datepicker" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->ct_sdate)) {
                                                                   echo $contract_info->ct_sdate;
                                                               }
                                                               ?>" name="ct_sdate">
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">계약만료일
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                      <div class="input-group">
                                                        <input type="text" class="form-control datepicker" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->ct_edate)) {
                                                                   echo $contract_info->ct_edate;
                                                               }
                                                               ?>" name="ct_edate">
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">비고</label>
                                                    <div class="col-lg-5">
                                                        <textarea name="remark" class="form-control" style="height:90px;"><?php
                                                               if (!empty($contract_info->remark)) {
                                                                   echo $contract_info->remark;
                                                               }
                                                               ?></textarea>
                                                    </div>
                                                </div>


                                              </div>



                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">
                                        <label class="col-lg-3"></label>
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary">저장</button>
                                        </div>
                                        <div class="col-lg-3">
                                            <!--button type="submit" name="save_and_set_scontract" value="1"
                                                    class="btn btn-sm btn-primary">저장 및 하도급설정</button-->
                                        </div>

                                    </div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  	function selectCompany(md,params) {
      window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
  	}

    function popSCWindow(ct_id) {
  	  window.open('<?php echo base_url(); ?>admin/contract/pop_scontract_config/'+ct_id, 'winSC', 'left=50, top=50, width=1480, height=700, scrollbars=1');
  	}


    function goInit(){
        $('#sco_name').val('');
        $('#rco_name').val('');
        $('#tr_title').val('');
        $('#page').val('1');
        $('#contract_form').submit();
    }

    function goSearch(){
        $('#contract_form').submit();
    }

</script>
