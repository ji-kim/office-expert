<form method="post" action="<?= base_url() ?>admin/contract/pop_scontract_config/<?= $ct_id ?>/<?= $sub ?>/update_items">
<div class="panel-body">
    <div class="table-responsive">
        <table class="table table-striped ">
            <thead>
            <tr>


                <?php if (!empty($edited) || !empty($deleted)) { ?>
                    <th rowspan="2">작업</th>
                <?php } ?>
            </tr>
            <tr>
                <?php if (!empty($all_items_group)) {
                    foreach ($all_items_group as $item_info) { ?>
                        <th align="center"><?php
                            if (!empty($item_info->title)) {
                                echo $item_info->title;
                            }
                            ?></th>
                    <?php } } ?>
            </tr>
            </thead>
            <tbody>

            <?php
            if (!empty($all_items_group)) {
                foreach ($all_items_group as $item_info) {
                    ?>
                    <td>
                        <input type="hidden" name="item_id[]" value="<?php
                        if (!empty($item_info->item_id)) {
                            echo $item_info->item_id;
                        }
                        ?>">
                        <input type="text" name="unit_price[]" class="form-control" value="" placeholder="<?=$item_info->title?>" />
                    </td>
                <?php } } ?>

            <td><button type="submit" name="add" value="1" class="btn btn-info">일괄적용</button></td>

            </tbody>
        </table>
    </div>
</div>
</form>