<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
$created = can_action('122', 'created');
$edited = can_action('122', 'edited');
$deleted = can_action('122', 'deleted');
?>
<div class="panel panel-custom">
    <header class="panel-heading ">거래항목설정</header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped ">
                <thead>
                <tr>
                    <th>거래항목</th>
                    <th>설명</th>
                    <th>리스트순서</th>
                    <?php if (!empty($edited) || !empty($deleted)) { ?>
                        <th>작업</th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($all_items_group)) {
                    foreach ($all_items_group as $item_info) {
                        ?>
                        <tr id="item_info_<?= $item_info->item_id ?>">
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($item_id) && $item_id == $item_info->item_id) { ?>
                                <form method="post"
                                      action="<?= base_url() ?>admin/contract/pop_contract_items/<?= $ct_id ?>/update_item/<?php
                                      if (!empty($item_info)) {
                                          echo $item_info->item_id;
                                      }
                                      ?>" class="form-horizontal">
                                    <input type="text" name="title" value="<?php
                                    if (!empty($item_info)) {
                                        echo $item_info->title;
                                    }
                                    ?>" class="form-control" placeholder="항목명" required>
                                <?php } else {
                                    echo $item_info->title;
                                }
                                ?></td>
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($item_id) && $item_id == $item_info->item_id) { ?>
                                    <input type="text" name="remark" class="form-control" value="<?php
                                    if (!empty($item_info->remark)) {
                                        echo $item_info->remark;
                                    }
                                    ?>"/>
                                <?php } else {
                                    if(!empty($item_info->remark)) echo $item_info->remark;
                                }
                                ?></td>
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($item_id) && $item_id == $item_info->item_id) { ?>
                                    <input type="text" name="list_order" class="form-control" value="<?php
                                    if (!empty($item_info->list_order)) {
                                        echo $item_info->list_order;
                                    }
                                    ?>"/>
                                <?php } else {
                                    if(!empty($item_info->list_order)) echo $item_info->list_order;
                                }
                                ?></td>
                            <?php if (!empty($edited) || !empty($deleted)) { ?>
                                <td>
                                    <?php
                                    $id = $this->uri->segment(5);
                                    if (!empty($item_id) && $item_id == $item_info->item_id) { ?>
                                        <?= btn_update() ?>
                                        </form>
                                        <?= btn_cancel('admin/contract/pop_contract_items/' . $ct_id) ?>
                                    <?php } else { ?>
                                        <?php if (!empty($edited)) { ?>
                                            <?= btn_edit('admin/contract/pop_contract_items/' . $ct_id . '/edit_item/' . $item_info->item_id ) ?>
                                        <?php } ?>
                                        <?php if (!empty($deleted)) { ?>
                                          <?= btn_delete('admin/contract/pop_contract_items/' . $ct_id . '/delete_item/' . $item_info->item_id ) ?>
                                            <?php //echo ajax_anchor(base_url("admin/contract/delete_item/" . $item_info->item_id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => "삭제", "data-fade-out-on-success" => "#title_" . $item_info->item_id)); ?>
                                        <?php }
                                    }
                                    ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php
                    }
                }
                ?>
                <?php if (!empty($created) || !empty($edited)) { ?>
                    <form method="post" action="<?= base_url() ?>admin/contract/pop_contract_items/<?= $ct_id?>/insert_item"
                          class="form-horizontal" data-parsley-validate="" novalidate="">
                        <tr>
                            <td><input type="text" name="title" class="form-control"
                                       placeholder="항목명" required></td>
                            <td>
                                <input name="remark" placeholder="설명"
                                       class="form-control"/>
                            </td>
                            <td>
                                <input name="list_order" placeholder="표시순서"
                                       class="form-control"/>
                            </td>
                            <td><?= btn_add() ?></td>
                        </tr>
                    </form>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
