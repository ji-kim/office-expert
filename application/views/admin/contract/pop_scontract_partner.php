<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
$created = can_action('122', 'created');
$edited = can_action('122', 'edited');
$deleted = can_action('122', 'deleted');
?>
<div class="panel panel-custom">
    <header class="panel-heading ">파트너등록 & 거래단가 설정 - <font color=red>* 계약단가는 '배차->단가설정'에서 다시 설정 가능합니다.</font></header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped ">
                <thead>
                <tr>
                    <th rowspan="2">파트너</th>
                    <th rowspan="2">소속거래처</th>
                    <th rowspan="2">부가세</th>
                    <th colspan="<?=$item_count?>">하도급계약단가
                      <button href="<?= base_url() ?>admin/contract/set_unitprice_all/<?= $ct_id ?>/<?= $sub ?>" data-toggle="modal" data-placement="top" data-target="#myModal" data-placement="top" type="submit" class="btn btn-xs btn-success" data-original-title="단가일괄적용"><i class="fa fa-check"></i>단가일괄적용</button>
                    </th>
                    <?php if (!empty($edited) || !empty($deleted)) { ?>
                        <th rowspan="2">작업</th>
                    <?php } ?>
                </tr>
                <tr>
                    <?php if (!empty($all_items_group)) {
                        foreach ($all_items_group as $item_info) { ?>
                        <th align="center"><?php
                        if (!empty($item_info->title)) {
                            echo $item_info->title;
                        }
                        ?></th>
                    <?php } } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                $cnt = 0;
                if (!empty($all_partner_group)) {
                    foreach ($all_partner_group as $partner_info) {
                      $cnt++;
                        ?>
                        <tr id="cowork_info_<?= $partner_info->contract_partner_id ?>">
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($contract_partner_id) && $contract_partner_id == $partner_info->contract_partner_id) { ?>
                                <form method="post"
                                      action="<?= base_url() ?>admin/contract/pop_scontract_config/<?= $ct_id ?>/<?= $sub ?>/update_partner/<?php
                                      if (!empty($partner_info)) {
                                          echo $partner_info->contract_partner_id;
                                      }
                                      ?>" class="form-horizontal">
                                    <input type="hidden" name="partner_id" id="partner_id<?=$cnt?>" value="<?php
                                    if (!empty($partner_info->partner_id)) {
                                        echo $partner_info->partner_id;
                                    }
                                    ?>">
                                    <input type="text" id="driver<?=$cnt?>" value="<?php
                                    if (!empty($partner_info)) {
                                        echo $partner_info->partner;
                                    }
                                    ?>" class="form-control" placeholder="파트너" onClick="selectCompany('partner','partner_id<?=$cnt?>||__||__||__||__||driver<?=$cnt?>||__||__');">
                                <?php } else {
                                    echo $partner_info->partner;
                                }
                                ?></td>
                            <td><?php
                                $id = $this->uri->segment(5);
                                if (!empty($contract_partner_id) && $contract_partner_id == $partner_info->contract_partner_id) { ?>
                                  <input type="hidden" name="customer_id" id="customer_id<?=$cnt?>" value="<?php
                                  if (!empty($partner_info->customer_id)) {
                                      echo $partner_info->customer_id;
                                  }
                                  ?>">
                                    <input type="text" id="customer_name<?=$cnt?>" class="form-control" value="<?php
                                    if (!empty($partner_info->customer_name)) {
                                        echo $partner_info->customer_name;
                                    }
                                    ?>" onClick="selectMyCustomer('<?=$cnt?>');"/>
                                <?php } else {
                                    if(!empty($partner_info->customer_name)) echo $partner_info->customer_name;
                                }
                                ?></td>
                                <td><?php
                                    if(!empty($partner_info->customer_name)) echo $partner_info->tax_yn;
                                    ?></td>

                                <?php
                                if (!empty($all_items_group)) {
                                    foreach ($all_items_group as $item_info) {
                                        $item = $this->db->where('item_id', $item_info->item_id)->where('partner_id', $partner_info->partner_id)->get('tbl_contract_partner_unitprice')->row();
                                      ?>
                                      <td><?php
                                          $id = $this->uri->segment(5);
                                          if (!empty($contract_partner_id) && $contract_partner_id == $partner_info->contract_partner_id) { ?>
                                            <input type="hidden" name="unitprice_id[]" value="<?php
                                            if (!empty($item->unitprice_id)) {
                                                echo $item->unitprice_id;
                                            }
                                            ?>">
                                            <input type="hidden" name="item_id[]" value="<?php
                                            if (!empty($item_info->item_id)) {
                                                echo $item_info->item_id;
                                            }
                                            ?>">
                                              <input type="text" name="unit_price[]" class="form-control" value="<?php
                                              if (!empty($item->unit_price)) {
                                                  echo $item->unit_price;
                                              }
                                              ?>" placeholder="<?=$item_info->title?>" />
                                          <?php } else {
                                              if(!empty($item->unit_price)) echo number_format($item->unit_price,0);
                                          }
                                          ?></td>
                                <?php } } ?>



                            <?php if (!empty($edited) || !empty($deleted)) { ?>
                                <td>
                                    <?php
                                    $id = $this->uri->segment(5);
                                    if (!empty($contract_partner_id) && $contract_partner_id == $partner_info->contract_partner_id) { ?>
                                        <?= btn_update() ?>
                                        </form>
                                        <?= btn_cancel('admin/contract/pop_scontract_config/' . $ct_id . '/' . $sub) ?>
                                    <?php } else { ?>
                                        <?php if (!empty($edited)) { ?>
                                            <?= btn_edit('admin/contract/pop_scontract_config/' . $ct_id . '/' . $sub . '/edit_partner/' . $partner_info->contract_partner_id ) ?>
                                        <?php } ?>
                                        <?php if (!empty($deleted)) { ?>
                                          <?= btn_delete('admin/contract/pop_scontract_config/' . $ct_id . '/' . $sub . '/delete_partner/' . $partner_info->contract_partner_id ) ?>
                                            <?php //echo ajax_anchor(base_url("admin/contract/delete_item/" . $partner_info->partner_id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => "삭제", "data-fade-out-on-success" => "#title_" . $partner_info->partner_id)); ?>
                                        <?php }
                                    }
                                    ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php
                    }
                }
                ?>
                <?php if (!empty($created) || !empty($edited)) { ?>
                    <form method="post" action="<?= base_url() ?>admin/contract/pop_scontract_config/<?= $ct_id ?>/<?= $sub ?>/insert_partner"
                          class="form-horizontal" data-parsley-validate="" novalidate="">
                        <tr>
                            <td colspan="1">
                              <input type="hidden" name="partner_id" id="partner_id">
                              <input type="text" id="driver" class="form-control"
                                       placeholder="기사명" onClick="selectCompany('partner','partner_id||__||__||__||__||driver||__||__');"></td>
                            <td colspan="1">
                               <input type="hidden" name="customer_id" id="customer_id">
                               <input type="text" id="customer_name" class="form-control"
                                        placeholder="소속거래처" onClick="selectMyCustomer('');"></td>
                            <td colspan="1"></td>

                            <?php
                            if (!empty($all_items_group)) {
                                foreach ($all_items_group as $item_info) {
                                  ?>
                              <td>
                                                    <input type="hidden" name="item_id[]" value="<?php
                                                    if (!empty($item_info->item_id)) {
                                                        echo $item_info->item_id;
                                                    }
                                                    ?>">
                                                      <input type="text" name="unit_price[]" class="form-control" value="" placeholder="<?=$item_info->title?>" />
                              </td>
                            <?php } } ?>

                            <td><?= btn_add() ?></td>
                        </tr>
                    </form>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
 // 파트너 선택
 function selectCompany(md,params) {
   window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSCC', 'left=100, top=100, width=1200, height=700, scrollbars=1');
 }

 // 소속 거래처 선택
 function selectMyCustomer(no) {
   window.open('<?php echo base_url(); ?>admin/contract/select_customer/<?= $ct_id?>/'+no, 'winSMC', 'left=100, top=100, width=600, height=700, scrollbars=1');
 }
</script>
