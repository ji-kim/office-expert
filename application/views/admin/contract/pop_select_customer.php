<div>
  <div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">
      <div class="col-sm-12">
                                <label class="col-lg-12 control-label" style="font-size:18px;">작업자 선택</label>
      </div>

  </div>
</div>

<div class="row">
<div class="table-responsive" style="padding-left:10px;">

<table class="table table-striped" style="width:98%">
  <thead>
  <tr align="center" bgcolor="#e0e7ef">
    <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">거래처명</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대표</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자번호</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">선택</td>
  </tr>
        </thead>
        <tbody>
        <?php
        if (!empty($all_coops_group)) {
            foreach ($all_coops_group as $coop_details) {
                ?>
                <tr id="coop_details_<?= $coop_details->customer_id ?>">
                    <td align='center'><?=$coop_details->co_name ?></td>
                    <td align='center'><?=$coop_details->ceo ?></td>
                    <td align='center'><?=$coop_details->bs_number ?></td>
                    <td align='center'>

                          <button class="btn btn-primary" type="button" onclick="goSelect('<?=$coop_details->customer_id ?>','<?=$coop_details->co_name ?>','<?= $coop_details->ceo ?>');">
                                                                선택
                                                            </button>
                  </td>


                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>





</div>
</div>

<script type="text/javascript">
function goSelect(customer_id,co_name,ceo) {
  $("#customer_id<?=$no?>",opener.document).val(customer_id);
  $("#customer_name<?=$no?>",opener.document).val(co_name);
  window.close();
}
function goSearch() {
$('#page').val('1');
$("#myform").attr("target", "_self");
$("#myform").submit();
}
//-->
</script>
