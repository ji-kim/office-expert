<?php
$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
      <table width="1000" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef" style="text-align:center;vertical-align:center;">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">NO</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="6">차량정보</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2">계약기간</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">자동차보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">적재물보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">자동차정밀(정기)검사</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="4">최종차주정보</td>
		</tr>
		<tr align="center">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">년식</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">용도</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차종</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">톤수</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">시작</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종료</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험가입일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험만기일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">분납횟수</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험가입일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험만기일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">분납여부</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">최종검사일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">다음검사일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">개시일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">만료일</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">배차지</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">위ㆍ수탁관리회사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이전차주명</td>
          <td style="color:#ffffff;background-color: #777777;">현차주명</td>
		</tr>
                                </thead>

                                <tbody>
                               <?php
                                if (!empty($all_car_info)) {
                                    foreach ($all_car_info as $car_details) {
										// 자동차보험
										$car_ins = $this->db->where('(tr_id = \''.$car_details->idx.'\' or tr_id = \''.$car_details->idx.'\')')->where('active_yn', 'Y')->where('type', '자동차')->get('tbl_asset_truck_insur')->row();
										if(!empty($car_ins->ins_co_id)) {
											$car_ins_co = $this->db->where('idx', $car_ins->ins_co_id)->get('tbl_insur_company')->row();
											if(empty($car_ins_co->deptname)) $ins_co_name = ""; else $ins_co_name = $car_ins_co->deptname;
										}
										// 적재물
										$load_ins = $this->db->where('(tr_id = \''.$car_details->idx.'\' or tr_id = \''.$car_details->idx.'\')')->where('active_yn', 'Y')->where('type', '적재물')->get('tbl_asset_truck_insur')->row();
										if(!empty($load_ins->ins_co_id)) {
											$load_ins_co = $this->db->where('idx', $load_ins->ins_co_id)->get('tbl_insur_company')->row();
											if(empty($load_ins_co->deptname)) $load_ins_co_name = ""; else $load_ins_co_name = $load_ins_co->deptname;
										}


										// 정기검사
										$car_chk = $this->db->where('active_yn','Y')->where('tr_id',$car_details->idx)->get('tbl_asset_truck_check')->row();


// 임시---- 차주 확인
$partner_info = $this->db->where('dp_id', $car_details->inv_co_id)->get('tbl_members')->row();
//if(!empty($partner_info->ceo)) echo $partner_info->ceo."xxx<br/>";

                                        ?>
                                    <tr>
                                        <td>
                                            <?= $car_details->idx ?> 
                                        </td>
                                        <td>
                                            <span class='label label-primary'><?= $car_details->car_1 ?></span>
                                        </td>
                                        <td>
                                            <span class='label label-primary'><?= $car_details->car_7 ?></span>
										</td>
                                        <td>
                                            <?= $car_details->car_5 ?>
                                        </td>
                                        <td>
                                            <?= $car_details->car_3 ?>
                                        </td>
                                        <td>
                                            <?= $car_details->type ?>
                                        </td>
                                        <td>
                                            <?= $car_details->carinfo_11 ?>
                                        </td>

                                        <td> <?= $car_details->N ?></td>
                                        <td> <?= $car_details->O ?></td>

                                        <td>
											<span class='label label-primary'><?php if(!empty($ins_co_name)) echo $ins_co_name; ?></span>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->start_date)) echo $car_ins->start_date; ?>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->end_date)) echo $car_ins->end_date; ?>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->pay_cnt)) echo $car_ins->pay_cnt; ?>
                                        </td>



                                        <td>
											<span class='label label-primary'><?php if(!empty($load_ins_co_name)) echo $load_ins_co_name; ?></span>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->start_date))?"":$load_ins->start_date ?>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->end_date))?"":$load_ins->end_date ?>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->pay_cnt))?"-":$load_ins->pay_cnt ?>
                                        </td>
                                        <td>
                                           <span class='label label-primary'><?php if(!empty($car_chk->last_check))  $car_chk->last_check; ?></span>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_chk->next_assigned)) echo $car_chk->next_assigned; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_chk->next_start)) echo $car_chk->next_start; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_chk->next_expired)) echo  $car_chk->next_expired; ?>
                                        </td>
                                        <td>
                                            <span class='label label-primary'><?php if(!empty($car_details->bc_co_name)) echo $car_details->bc_co_name; ?></span>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->ws_co_name)) echo $car_details->ws_co_name; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->inv_co_name)) echo $car_details->inv_co_name; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->ceo)) echo $car_details->ceo; ?>
                                        </td>
                                    </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php 
								}
                                ?>
                                </tbody>
                            </table>
