                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
									<tr align="center" bgcolor="#e0e7ef">
									  <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>상태</td>
									  <!--td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>년월분</td-->

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2">청구사정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">파트너정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2">계약정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">위수탁관리비 청구정보</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">수납후정보</td>
									</tr>
																						
														

									<tr align="center" bgcolor="#e0e7ef">
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공제청구사</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실수요처</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">파트너</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자번호</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">시작일</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종료일</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">누적미납금</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">당월청구금</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">당월총청구금</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">수납</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">최근수납일</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">수납금액</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">수납후미수잔액</td>
									</tr>
                                </thead>
                                <tbody>
                                <?php
                                //$curency = $this->transactions_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
                                $total_amount = 0;
                                $total_credit = 0;
                                $total_balance = 0;
                                if (!empty($all_levy_info)):
                                    foreach ($all_levy_info as $v_levy_info) :
										$sn = $total_count--;
										$df = $this->db->where('df_id', $v_levy_info->df_id)->get('tbl_delivery_fee')->row();
										//파트너
										$dp = $this->db->where('dp_id', $v_levy_info->dp_id)->get('tbl_members')->row();

										//공제청구사
										if(!empty($df->gongje_req_co)) {
											$gj_rq_co = $this->db->where('dp_id', $df->gongje_req_co)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//실수요처
										$r_co = $this->db->where('code', $df->co_code)->get('tbl_members')->row();
										if(empty($r_co->co_name)) $r_co = ""; else $r_co = $r_co->co_name;

										//차량정보
										$truck = $this->db->where('idx', $df->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($v_levy_info->pay_status)) {
											if($v_levy_info->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($v_levy_info->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; } 
											else if($v_levy_info->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; } 
											else if($v_levy_info->pay_status == "O") { $pay_status = "과납"; $pay_status_color = "primary"; } 
										}

										//최근 수납정보
										$last_levy = $this->db->where('df_id', $v_levy_info->df_id)->order_by('pay_date', 'DESC')->get('tbl_delivery_fee_levy_log')->row();

										//이월 미납액
	
                                        ?>
                                      <tr id="table_deposit_<?= $v_levy_info->df_id ?>">
                                        <td><?=$sn?></td>
                                        <td>
                                            <span class='label label-<?=$pay_status_color?>'><?=$pay_status?></span>
                                        </td>
                                        <!--td>
                                            <?= $df_month ?>
                                        </td-->

                                        <td>
                                            <a href="<?= base_url() ?>admin/basic/v_levy_info/<?= $v_levy_info->df_id ?>"
                                                   class="text-info"><?php if(!empty($gj_rq_co)) echo $gj_rq_co; ?></a>
										</td>
                                        <td><?= $df->D ?></td>

                                        <td> <?= $dp->ceo ?> / <?= $dp->driver ?> </td>
                                        <td><?= $truck_no ?></td>
                                        <td><?= $dp->bs_number ?></td>

                                        <td><?= $dp->N ?></td>
                                        <td><?= $dp->O ?></td>

										<td style='font-weight:bold;color:red;text-align:right;'><?= number_format($v_levy_info->prev_misu,0) ?></td>
										<td style='text-align:right;'><?= number_format($v_levy_info->gn_amount,0) ?></td>
										<td style='text-align:right;'><?= number_format(($v_levy_info->gn_amount+$v_levy_info->prev_misu),0) ?></td>

										<!--td style='text-align:right;'><?= number_format($v_levy_info->paid_amount,0) ?></td>
										<td style='text-align:right;'><?= number_format($v_levy_info->balance_amount,0) ?></td-->
                                                <td>
                                                <div class="btn-group">
<?php if($df->is_pay_closed=='Y') { ?>
													<span class='label label-danger'>수납마감</span>
<?php } else { ?>
<?php } ?>
                                                </div>
                                                </td>
                                        <td><?php if(!empty($last_levy->pay_date)) echo $last_levy->pay_date; ?></td>
										<td style='font-weight:bold;color:blue;text-align:right;'><?= number_format(($v_levy_info->paid_amount),0) ?></td>
										<td style='font-weight:bold;color:red;text-align:right;'><?= number_format($v_levy_info->balance_amount,0) ?></td>
                                      </tr>
                                            <?php
                                            //$total_amount += $v_levy_info->amount;
                                            //$total_credit += $v_levy_info->credit;
                                            //$total_balance = $total_credit;
                                            ?>
                                            <?php
                                        //endif;
                                    endforeach;
                                endif;
                                ?>

                                </tbody>
                            </table>
