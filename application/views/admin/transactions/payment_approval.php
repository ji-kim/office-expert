<?= message_box('success'); ?>
<?= message_box('error'); ?>

<?php
if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

<script>
function goExcel() {
  document.myform.action = '<?php echo base_url() ?>admin/transactions/payment_approval/<?=$active?>/excel/';
  document.myform.target = '_blank';
  document.myform.submit();
}
function goSearch() {

  document.myform.action = '<?php echo base_url() ?>admin/transactions/payment_approval/<?=$active?>/<?=$gongje_req_co?>/';
  document.myform.target = '_self';
  document.myform.submit();
}
function goPlace() {

  document.myform.action = '<?php echo base_url() ?>admin/transactions/payment_approval/<?=$active?>/<?=$gongje_req_co?>/place_all/';
  document.myform.target = '_self';
  document.myform.submit();
}

 </script>
           <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">


                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/transactions/"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
      <!-- 검색 시작 -->
	<table border="0" cellspacing="1" cellpadding="5" width="96%" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
		<tr>
			<td width="15%">
					<select name="gongje_req_co" id="gongje_req_co" style="width:100%;background-color:yellow;" class="form-control input-sm" onChange="document.myform.submit();">
							<option value="" <?=($gongje_req_co=="")?"selected":""?>>전체</option>
							<option value="1413" <?=($gongje_req_co=="1413")?"selected":""?>>케이티지엘에스(주) </option>
							<option value="1414" <?=($gongje_req_co=="1414")?"selected":""?>>(주)아이디일일구닷컴</option>
							<option value="1415" <?=($gongje_req_co=="1415")?"selected":""?>>(주)델타온</option>
							<option value="1903" <?=($gongje_req_co=="1903")?"selected":""?>>(주)휴먼엘지</option>
							<option value="etc" <?=($gongje_req_co=="etc")?"selected":""?>>기타</option>
					</select>
			</td>
			<td width="15%">
			</td>
			<td align="left" valign="top" width="70%">

        <a href="javascript:goExcel()" tabindex="0" class="dt-button buttons-print btn btn-primary btn-xs mr" aria-controls="DataTables">
        <span><i class="fa fa-search"> </i> 검색</span>


                  					<a href="javascript:goExcel()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
                  					<span><i class="fa fa-print"> </i> 엑셀 저장</span>
					</a>

			</td>
        </tr>

            </table>
      <!-- 검색 끝 -->
</form>


            </div>

<?php }
$created = can_action('30', 'created');
$edited = can_action('30', 'edited');
$deleted = can_action('30', 'deleted');
$income_category = $this->db->get('tbl_income_category')->result();
$id = $this->uri->segment(5);
if (!empty($created) || !empty($edited)){
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'category') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/transactions/deposit"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($income_category) > 0) { ?>
                    <?php foreach ($income_category as $v_category) {
                        ?>
                        <li class="<?php if (!empty($id)) {
                           // if ($search_by == 'category') {
                           //     if ($id == $v_category->income_category_id) {
                           //         echo 'active';
                           //     }
                           // }
                        } ?>">
                            <a href="<?= base_url() ?>admin/transactions/deposit/category/<?php echo $v_category->income_category_id; ?>"><?php echo $v_category->income_category; ?></a>
                        </li>
                    <?php }
                    ?>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="<?php echo base_url() ?>admin/transactions/payment_approval/1">전체</a></li>
                <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="<?php echo base_url() ?>admin/transactions/payment_approval/2">일반지출</a></li>
                <li class="<?= $active == 3 ? 'active' : ''; ?>"><a href="<?php echo base_url() ?>admin/transactions/payment_approval/3">SI외주</a></li>
                <li class="<?= $active == 4 ? 'active' : ''; ?>"><a href="<?php echo base_url() ?>admin/transactions/payment_approval/4">SI영업비</a></li>
                <li class="<?= $active == 5 ? 'active' : ''; ?>"><a href="<?php echo base_url() ?>admin/transactions/payment_approval/5">자사수습</a></li>
                <li class="<?= $active == 6 ? 'active' : ''; ?>"><a href="<?php echo base_url() ?>admin/transactions/payment_approval/6">기타</a></li>
                <li class="<?= $active == 2 ? 'active' : ''; ?>"></li>
                <!--li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/transactions/import/Income"><?= lang('import') . ' ' . lang('deposit') ?></a>
                </li-->
            </ul>
            <div class="tab-content no-padding  bg-white">
                <!-- ************** general *************-->
                <div class="tab-pane active" id="manage">
<?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>계산서발행완료</strong></div>
                        </header>
<?php } ?>



                        <div class="table-responsive">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
									<tr align="center" bgcolor="#e0e7ef">
									  <td height="30" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">상태</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">구분</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관련프로젝트</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">회차</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">성명</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">주민(사업자)번호</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">연락처</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기간</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지급액</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실지급액</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">입금구분</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">입금정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비고</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">액션</td>
									</tr>
                                </thead>
                                <tbody>
<?php if($active == 1 || $active == 3) { ?>
                                      <tr>
                                        <td>1</td>
                                        <td><span class='label label-default'>대기</label></td>
                                        <td>SI외주</td>
                                        <td>롯데파견</td>
                                        <td>2</td>
                                        <td>오세영</td>
                                        <td>750420-1402837</td>
                                        <td>010-4409-0964</td>
                                        <td>9/1 ~ 9/30</td>
                                        <td style="text-align:right;">6,300,000 </td><td>6,092,100(3.3%)</td>
                                        <td>개인</td>
                                        <td>국민 578602-01-088526(오세영)</td>
                                        <td>2019/08/28 시작 </td>
                                        <td> </td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td><span class='label label-default'>대기</label></td>
                                        <td>SI외주</td>
                                        <td>롯데파견</td>
                                        <td>3</td>
                                        <td>송해석</td>
                                        <td>780715-1462112</td>
                                        <td>010-7999-3081</td>
                                        <td>9/1 ~ 9/24</td>
                                        <td style="text-align:right;">5,120,000</td><td>4,951,040(3.3%)</td>
                                        <td>개인</td>
                                        <td>국민 037-21-1151-128(송해석)</td>
                                        <td>2019/07/11 ~2019/09/24 (교체) </td>
                                        <td> </td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td><span class='label label-default'>대기</label></td>
                                        <td>SI외주</td>
                                        <td>롯데파견</td>
                                        <td>2</td>
                                        <td>엄교식</br>이케이에스 엔터프라이즈</td>
                                        <td>597-09-00735</td>
                                        <td>010-4409-0964</td>
                                        <td>9/1 ~ 9/30</td>
                                        <td style="text-align:right;">7,300,000 </td><td>8,030,000 (+10%)</td>
                                        <td>사업자</td>
                                        <td>국민 924101-00-015280 엄교식(이케이에스엔터..) </td>
                                        <td>[계산서발행완료]  (PL) 2019/09/01 시작 </td>
                                        <td> </td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                         <td><span class='label label-default'>대기</label></td>
                                       <td>SI외주</td>
                                        <td>롯데파견</td>
                                        <td>2</td>
                                        <td>이우주</br>어플랩&비앤비</td>
                                        <td>479-03-01550</td>
                                        <td> </td>
                                        <td>9/1 ~ 9/30</td>
                                        <td style="text-align:right;">4,650,000</td><td>5,115,000(+10%)</td>
                                        <td>사업자</td>
                                        <td>우리 1002-432-402436 (김재현)</td>
                                        <td>[계산서발행완료] 2019/08/28 시작 </td>
                                        <td> </td>
                                      </tr>
                                      <tr>
                                        <td>5</td>
                                        <td><span class='label label-default'>대기</label></td>
                                        <td>SI외주</td>
                                        <td>롯데파견</td>
                                        <td>2</td>
                                        <td>오병도</br>어플랩&비앤비</td>
                                        <td>479-03-01550</td>
                                        <td> </td>
                                        <td>9/1 ~ 9/30</td>
                                        <td style="text-align:right;">4,650,000</td><td>5,115,000(+10%)</td>
                                        <td>사업자</td>
                                        <td>우리 1002-432-402436 (김재현)</td>
                                        <td>[계산서발행완료] 2019/08/28 시작 </td>
                                        <td> 
                                                    <!--button class="btn btn-xs btn-success"
                                                            onClick="location.href='/admin/transactions/';">
                                                        <i class="fa fa-pencil-square-o"></i>결제
                                                    </button-->
										
										</td>
                                      </tr>
<?php } ?>

<?php if($active == 1 || $active == 5) { ?>
                                      <tr>
                                        <td>6</td>
                                        <td><span class='label label-default'>대기</label></td>
                                        <td>내부시용(수습)</td>
                                        <td>수습직원</td>
                                        <td>2</td>
                                        <td>신은철</td>
                                        <td>850816-1319618</td>
                                        <td>010-7171-9151</td>
                                        <td>9/1 ~ 9/30</td>
                                        <td style="text-align:right;">3,800,000 </td><td>3,674,600(3.3%)</td>
                                        <td>3.3%</td>
                                        <td>우리 1002-756-166875 (신은철)</td>
                                        <td>2019/08/19 입사 3개월 수습 (프로그래머)</td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td>7</td>
                                        <td><span class='label label-default'>대기</label></td>
                                        <td>내부시용(수습)</td>
                                        <td>수습직원</td>
                                        <td>1</td>
                                        <td>권의영</td>
                                        <td> </td>
                                        <td>010-8613-7012</td>
                                        <td>9/1 ~ 9/30</td>
                                        <td style="text-align:right;">1,333,333</td><td>1,289,333(3.3%)</td>
                                        <td>3.3%</td>
                                        <td></td>
                                        <td>9/23 입사 / 김준일팀장님 후임 3개월 수습(월500)</td>
                                        <td></td>
                                      </tr>
<?php } ?>
                                      <!--tr>
                                        <td>7</td>
                                        <td>대기</td>
                                        <td>내부시용(수습)</td>
                                        <td>수습직원</td>
                                        <td>2</td>
                                        <td>신은철</td>
                                        <td>850816-1319618</td>
                                        <td>010-7171-9151</td>
                                        <td>9/1 ~ 9/30</td>
                                        <td style="text-align:right;">3,800,000 </td><td>3,674,600(3.3%)</td>
                                        <td>3.3%</td>
                                        <td>우리 1002-756-166875 (신은철)</td>
                                        <td>2019/08/28 시작 </td>
                                        <td>2019/8/19 입사 (프로그래머)</td>
                                      </tr>
                                      <tr>
                                        <td>6</td>
                                        <td>대기</td>
                                        <td>내부시용(수습)</td>
                                        <td>수습직원</td>
                                        <td>2</td>
                                        <td>성소담</td>
                                        <td>930222-2150113</td>
                                        <td> </td>
                                        <td>9/23 ~ 9/30</td>
                                        <td style="text-align:right;">3,800,000 </td><td>3,674,600(3.3%)</td>
                                        <td>4대보험</td>
                                        <td>우리 1002-756-166875 (신은철)</td>
                                        <td>2019/08/28 시작 </td>
                                        <td>2019/8/19 입사 (디자이너)</td>
                                      </tr-->


                                <?php
                                //$curency = $this->transactions_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
                                $total_amount = 0;
                                $total_credit = 0;
                                $total_balance = 0;
                                if (!empty($all_taxinvoice_list)):
                                    foreach ($all_taxinvoice_list as $taxinvoice_info) :
										                    $sn = $total_count--;
                                        $invoice_status_color = "warning";
                                        $invoice_status = "완료";
                                        ?>
                                      <tr id="table_deposit_<?= $taxinvoice_info->tax_invoice_id ?>">
                                        <td><?=$sn?></td>
                                        <td>
                                            <span class='label label-<?=$invoice_status_color?>'><?=$invoice_status?></span>
                                        </td>
                                        <td>
                                            <?= $taxinvoice_info->df_month ?>
                                        </td>

                                        <td><?= $taxinvoice_info->sco_name ?></td>
                                        <td><?= $taxinvoice_info->sceo ?></td>
                                        <td><?= $taxinvoice_info->sbs_number ?></td>

                                        <td><?= $taxinvoice_info->ico_name ?></td>
                                        <td><?= $taxinvoice_info->iceo ?></td>
                                        <td><?= $taxinvoice_info->ibs_number ?></td>

                                        <td><?= number_format($taxinvoice_info->supp_amt,0) ?></td>
                                        <td><?= number_format($taxinvoice_info->vat_amt,0) ?></td>
                                        <td><?= number_format($taxinvoice_info->tot_amt,0) ?></td>
                                        <td>
                                                <div class="btn-group">
                                                    
                                                </div>
                                                </td>
                                      </tr>
                                            <?php
                                        //endif;
                                    endforeach;
                                endif;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).finish(function () {
        var maxAppend = 0;
        $("#add_more").click(function () {
            if (maxAppend >= 4) {
                alert("Maximum 5 File is allowed");
            } else {
                var add_new = $('<div class="form-group" style="margin-bottom: 0px">\n\
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('attachment') ?></label>\n\
        <div class="col-sm-4">\n\
        <div class="fileinput fileinput-new" data-provides="fileinput">\n\
<span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span><span class="fileinput-exists" >Change</span><input type="file" name="attachement[]" ></span> <span class="fileinput-filename"></span><a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a></div></div>\n\<div class="col-sm-2">\n\<strong>\n\
<a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;Remove</a></strong></div>');
                maxAppend++;
                $("#add_new").append(add_new);
            }
        });

        $("#add_new").on('click', '.remCF', function () {
            $(this).parent().parent().parent().remove();
        });
        $('a.RCF').click(function () {
            $(this).parent().parent().remove();
        });
    });
</script>
