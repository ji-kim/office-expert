<?= message_box('success'); ?>
<?= message_box('error'); ?>

<?php
if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

<script>
function goExcel() {
  document.myform.action = '<?php echo base_url() ?>admin/transactions/taxinvoice_finish/<?=$df_month?>/<?=$gongje_req_co?>/excel/';
  document.myform.target = '_blank';
  document.myform.submit();
}
function goSearch() {
  var df_month = $("#df_month").val();

  document.myform.action = '<?php echo base_url() ?>admin/transactions/taxinvoice_finish/'+df_month+'/<?=$gongje_req_co?>/';
  document.myform.target = '_self';
  document.myform.submit();
}
function goPlace() {
  var df_month = $("#df_month").val();

  document.myform.action = '<?php echo base_url() ?>admin/transactions/taxinvoice_finish/'+df_month+'/<?=$gongje_req_co?>/place_all/';
  document.myform.target = '_self';
  document.myform.submit();
}

 </script>
           <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">


                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/transactions/taxinvoice_finish"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
      <!-- 검색 시작 -->
	<table border="0" cellspacing="1" cellpadding="5" width="96%" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
		<tr>
			<td width="15%">
					<select name="gongje_req_co" id="gongje_req_co" style="width:100%;background-color:yellow;" class="form-control input-sm" onChange="document.myform.submit();">
							<option value="" <?=($gongje_req_co=="")?"selected":""?>>전체</option>
							<option value="1413" <?=($gongje_req_co=="1413")?"selected":""?>>케이티지엘에스(주) </option>
							<option value="1414" <?=($gongje_req_co=="1414")?"selected":""?>>(주)아이디일일구닷컴</option>
							<option value="1415" <?=($gongje_req_co=="1415")?"selected":""?>>(주)델타온</option>
							<option value="1903" <?=($gongje_req_co=="1903")?"selected":""?>>(주)휴먼엘지</option>
							<option value="etc" <?=($gongje_req_co=="etc")?"selected":""?>>기타</option>
					</select>
			</td>
			<td width="15%">
<?php
  if(!empty($last_levy_info->df_month)) echo $last_levy_info->df_month;
?>
        <input type="text" value="<?php
                                if (!empty($df_month)) {
                                    echo $df_month;
                                } else echo date('Y-m');
                                ?>" class="form-control monthyear" name="df_month" id="df_month"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;" onChange="goSearch();">
			</td>
			<td align="left" valign="top" width="70%">

        <a href="javascript:goExcel()" tabindex="0" class="dt-button buttons-print btn btn-primary btn-xs mr" aria-controls="DataTables">
        <span><i class="fa fa-search"> </i> 검색</span>


                  					<a href="javascript:goExcel()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
                  					<span><i class="fa fa-print"> </i> 엑셀 저장</span>
					</a>

			</td>
        </tr>

            </table>
      <!-- 검색 끝 -->
</form>


            </div>

<?php }
$created = can_action('30', 'created');
$edited = can_action('30', 'edited');
$deleted = can_action('30', 'deleted');
$income_category = $this->db->get('tbl_income_category')->result();
$id = $this->uri->segment(5);
if (!empty($created) || !empty($edited)){
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'category') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/transactions/deposit"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($income_category) > 0) { ?>
                    <?php foreach ($income_category as $v_category) {
                        ?>
                        <li class="<?php if (!empty($id)) {
                           // if ($search_by == 'category') {
                           //     if ($id == $v_category->income_category_id) {
                           //         echo 'active';
                           //     }
                           // }
                        } ?>">
                            <a href="<?= base_url() ?>admin/transactions/deposit/category/<?php echo $v_category->income_category_id; ?>"><?php echo $v_category->income_category; ?></a>
                        </li>
                    <?php }
                    ?>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                                    data-toggle="tab">계산서발행완료 (총 <?=number_format($total_count)?>건)</a>
                </li>
                <li class="<?= $active == 2 ? 'active' : ''; ?>">
                </li>
                <!--li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/transactions/import/Income"><?= lang('import') . ' ' . lang('deposit') ?></a>
                </li-->
            </ul>
            <div class="tab-content no-padding  bg-white">
                <!-- ************** general *************-->
                <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
<?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>계산서발행완료</strong></div>
                        </header>
                        <?php } ?>
                        <div class="table-responsive">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
									<tr align="center" bgcolor="#e0e7ef">
									  <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>상태</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>년월분</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">공급자</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">공급받는자</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">발행액</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"></td>
									</tr>



									<tr align="center" bgcolor="#e0e7ef">
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">회사명</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대표</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자번호</td>

                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">회사명</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대표</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자번호</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공급가</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">합계</td>
									</tr>
                                </thead>
                                <tbody>
                                <?php
                                //$curency = $this->transactions_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
                                $total_amount = 0;
                                $total_credit = 0;
                                $total_balance = 0;
                                if (!empty($all_taxinvoice_list)):
                                    foreach ($all_taxinvoice_list as $taxinvoice_info) :
										                    $sn = $total_count--;
                                        $invoice_status_color = "warning";
                                        $invoice_status = "완료";
                                        ?>
                                      <tr id="table_deposit_<?= $taxinvoice_info->tax_invoice_id ?>">
                                        <td><?=$sn?></td>
                                        <td>
                                            <span class='label label-<?=$invoice_status_color?>'><?=$invoice_status?></span>
                                        </td>
                                        <td>
                                            <?= $taxinvoice_info->df_month ?>
                                        </td>

                                        <td><?= $taxinvoice_info->sco_name ?></td>
                                        <td><?= $taxinvoice_info->sceo ?></td>
                                        <td><?= $taxinvoice_info->sbs_number ?></td>

                                        <td><?= $taxinvoice_info->ico_name ?></td>
                                        <td><?= $taxinvoice_info->iceo ?></td>
                                        <td><?= $taxinvoice_info->ibs_number ?></td>

                                        <td><?= number_format($taxinvoice_info->supp_amt,0) ?></td>
                                        <td><?= number_format($taxinvoice_info->vat_amt,0) ?></td>
                                        <td><?= number_format($taxinvoice_info->tot_amt,0) ?></td>
                                        <td>
                                                <div class="btn-group">
                                                    
                                                </div>
                                                </td>
                                      </tr>
                                            <?php
                                        //endif;
                                    endforeach;
                                endif;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).finish(function () {
        var maxAppend = 0;
        $("#add_more").click(function () {
            if (maxAppend >= 4) {
                alert("Maximum 5 File is allowed");
            } else {
                var add_new = $('<div class="form-group" style="margin-bottom: 0px">\n\
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('attachment') ?></label>\n\
        <div class="col-sm-4">\n\
        <div class="fileinput fileinput-new" data-provides="fileinput">\n\
<span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span><span class="fileinput-exists" >Change</span><input type="file" name="attachement[]" ></span> <span class="fileinput-filename"></span><a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a></div></div>\n\<div class="col-sm-2">\n\<strong>\n\
<a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;Remove</a></strong></div>');
                maxAppend++;
                $("#add_new").append(add_new);
            }
        });

        $("#add_new").on('click', '.remCF', function () {
            $(this).parent().parent().parent().remove();
        });
        $('a.RCF').click(function () {
            $(this).parent().parent().remove();
        });
    });
</script>
