<?php
$lColor="364F9E";
$opened = $this->session->userdata('opened');
$this->session->unset_userdata('opened');
$time = date('h:i:s');
$r = display_time($time);
$time1 = explode(' ', $r);
if(empty($title)) $title = "위수탁청구서출력";
?>
<html>
<head>
<title><?= $title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
body,table,tr,td {font-family: "굴림", "Seoul", "arial", "Verdana", "helvetica"; font-size: 9pt; line-height: 13px; color:333333;
	scrollbar-3dlight-color:595959;
	scrollbar-arrow-color:7F7F7F;
	scrollbar-face-color:DFDFDF;
	scrollbar-highlight-color:FFFFF;
	scrollbar-shadow-color:595959
	scrollbar-base-color:CFCFCF;
	scrollbar-darkshadow-color:FFFFFF;
}
.m_a {font-size:8pt;font-family: "돋움";letter-spacing:-1;}
.01 {font-size:9pt; color:#<?=$lColor?>; padding-top:2pt;}
.02 {font-size:9pt; color:#333333; padding-top:2pt;}
.03 {font-size:9pt; color:#FF4633; padding-top:2pt;}
.xx {font-size:9pt; border:solid 1 #A2A2A2; background-color:f0f0f0;}
.yy {font-size:9pt; border:solid 1 #A2A2A2; background-color:;ffffff}

a:link { text-decoration: none; color:585858; }
a:visited { text-decoration: none; color:585858; }
a:active { text-decoration: none; color:585858; }
a:hover { text-decoration:none; color:585858; }
#floater {position:absolute; visibility:visible}
</style>

	<SCRIPT>
//	border: 1px solid #fff;
//	padding-right:5px;
		function Installed()
		{
			try
			{
				return (new ActiveXObject('IEPageSetupX.IEPageSetup'));
			}
			catch (e)
			{
				return false;
			}
		}

		function printMe()
		{
			if (Installed())
			{
				IEPageSetupX.header = "";
				IEPageSetupX.footer = "";
				IEPageSetupX.PrintBackground = true;
				IEPageSetupX.leftMargin   = "10";
				IEPageSetupX.rightMargin   = "10";
				IEPageSetupX.topMargin   = "10";
				IEPageSetupX.bottomMargin   = "10";
				IEPageSetupX.ShrinkToFit = true;
				IEPageSetupX.PaperSize  = "A4";
				IEPageSetupX.Preview();
			}
			else
				alert("컨트롤을 설치하지 않았네요.. 정상적으로 인쇄되지 않을 수 있습니다.");
		}

	</SCRIPT>

	<SCRIPT language="JavaScript" for="IEPageSetupX" event="OnError(ErrCode, ErrMsg)">
		alert('에러 코드: ' + ErrCode + "\n에러 메시지: " + ErrMsg);
	</SCRIPT>
</head>

<body topmargin="0" OnLoad="printMe();" OnUnload="if (Installed()) IEPageSetupX.RollBack();">
<center>

                    <?php echo $subview ?>

</center>

</body>
</html>
