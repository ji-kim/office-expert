<div class="row">
    <div class="col-lg-12">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked navbar-custom-nav">
                <?php
                $can_do = can_do(111);
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($mode == 'sudang') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/basic/sudang_gongje/sudang">
                            <i class="fa fa-fw fa-info-circle"></i>
                            수당항목설정
                        </a>
                    </li>
                <?php }
                $can_do = can_do(112);
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($mode == 'gongje') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/basic/sudang_gongje/gongje">
                            <i class="fa fa-fw fa-pencil-square-o"></i>
                            공제(위.수탁관리비)항목설정
                        </a>
                    </li>
                <?php }
                $can_do = can_do(113);
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($mode == 'ggongje') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/basic/sudang_gongje/ggongje">
                            <i class="fa fa-fw fa-pencil-square-o"></i>
                            공제(일반공제)항목설정
                        </a>
                    </li>
                <?php }
                $can_do = can_do(114);
                if (!empty($can_do)) { ?>
                    <li class="<?php echo ($mode == 'rgongje') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/basic/sudang_gongje/rgongje">
                            <i class="fa fa-fw fa-pencil-square-o"></i>
                            공제(환급형공제)항목설정
                        </a>
                    </li>
                <?php }
                ?>
            </ul>
        </div>

        <section class="col-sm-9">
            <section class="">
                <!-- Load the settings form in views -->
                <?php $this->load->view('admin/basic/' . $load_page) ?>
                <!-- End of settings Form -->
            </section>
        </section>
    </div>
</div>
