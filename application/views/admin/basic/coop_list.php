<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php


if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 40px !important;
        padding-right: 40px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script>
	function popAsWindow(id) {
	  window.open('<?php echo base_url(); ?>admin/basic/set_item/'+id+'/sudang/01', 'winAdd', 'left=50, top=50, width=800, height=700, scrollbars=1');
	}
</script>


            <!--div class="col-sm-12 bg-white p0" style="<?= $margin ?>"-->
                
    <!-- 검색 시작 -->
    <!--
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" bgcolor="#ffffff">
              <tr>
                <td width="20%" height="20" align="center" bgcolor="#efefef">선택조회</td>
                <td width="25%">

					<select name="baecha_co_id" id="baecha_co_id" style="width:100%;" class="form-control input-sm">
							<option value="">선택</option>
							<option value="wmjd" > 협력사명 </option>
							<option value="0003" >대표</option>
							<option value="0001" >사업자번호</option>
							<option value="0001" >휴대폰번호</option>
							<option value="0001" >홈페이지</option>
							<option value="0001" >팩스</option>
					</select>				
				
				</td>
                <td width="25%">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:yellow;" onClick="selectGroup('ws_co_id||ws_co_name||__||__||__||__||__');">

                </td>
                <td width="30%">
                </td>
			</tr>
			<tr>
                <td style="padding-left:5px;padding-top:10px;" colspan="4" align="center" bgcolor="#ffffff">


					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-search"> </i> 검색</span>
					</a>
					<a href="javascript:goPrintAllTax()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 엑셀 저장</span>
					</a>
                    <?if(DATA_IMPORT){?>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀 가져오기</span>
					</a>
                    <?}?>

				</td>
					
			  </tr>
            </table>
		  </td>
        </tr>
      </table>
      -->
      <!-- 검색 끝 -->

				

            <!--/div-->
















    <div class="col-sm-12 bg-white p0" style="<?= $margin ?>;display:none;">
        <div class="col-md-4">
            <div class="row row-table pv-lg">
                <div class="col-xs-6">
                    <p class="m0 lead"><?= ($all_goal) ?></p>
                    <p class="m0">
                        <small><?= lang('without_converted') ?></small>
                    </p>
                </div>
                <div class="col-xs-6">
                    <p class="m0 lead"><?= ($direct_complete_achivement) ?></p>
                    <p class="m0">
                        <small><?= lang('completed') . ' ' . lang('achievements') ?></small>
                    </p>
                </div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="row row-table pv-lg">

                <div class="col-xs-6 ">
                    <p class="m0 lead"><?= ($wthout_all_goal) ?></p>
                    <p class="m0">
                        <small><?= lang('converted_client') ?></small>
                    </p>
                </div>
                <div class="col-xs-6">
                    <p class="m0 lead">

                        <?= $without_complete_achivement ?></p>
                    <p class="m0">
                        <small><?= lang('completed') . ' ' . lang('achievements') ?></small>
                    </p>
                </div>

            </div>

        </div>
        <div class="col-md-4">
            <div class="row row-table ">

                <div class="col-xs-6 pt">
                    <div data-sparkline="" data-bar-color="#23b7e5" data-height="60" data-bar-width="8"
                         data-bar-spacing="6" data-chart-range-min="0" values="<?php
                    if (!empty($invoice_result)) {
                        foreach ($invoice_result as $v_invoice_result) {
                            echo $v_invoice_result . ',';
                        }
                    }
                    ?>">
                    </div>
                    <p class="m0">
                        <small>
                            <?php
                            if (!empty($invoice_result)) {
                                foreach ($invoice_result as $date => $v_invoice_result) {
                                    echo date('d', strtotime($date)) . ' ';
                                }
                            }
                            ?>
                        </small>
                    </p>

                </div>
                <?php
                $total_goal = $all_goal + $wthout_all_goal;
                $complete_achivement = $direct_complete_achivement + $without_complete_achivement;
                if (!empty($tolal_goal)) {
                    if ($tolal_goal <= $complete_achivement) {
                        $total_progress = 100;
                    } else {
                        $progress = ($complete_achivement / $tolal_goal) * 100;
                        $total_progress = round($progress);
                    }
                } else {
                    $total_progress = 0;
                }
                ?>
                <div class="col-xs-6 text-center pt">
                    <div class="inline ">
                        <div class="easypiechart text-success"
                             data-percent="<?= $total_progress ?>"
                             data-line-width="5" data-track-Color="#f0f0f0"
                             data-bar-color="#<?php
                             if ($total_progress == 100) {
                                 echo '8ec165';
                             } elseif ($total_progress >= 40 && $total_progress <= 50) {
                                 echo '5d9cec';
                             } elseif ($total_progress >= 51 && $total_progress <= 99) {
                                 echo '7266ba';
                             } else {
                                 echo 'fb6b5b';
                             }
                             ?>" data-rotate="270" data-scale-Color="false"
                             data-size="50"
                             data-animate="2000">
                                                        <span class="small "><?= $total_progress ?>
                                                            %</span>
                            <span class="easypie-text"><strong><?= lang('done') ?></strong></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <!--button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            -->
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'group') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/client/manage_client"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($all_coop_group) > 0) { ?>
                    <li class="dropdown-submenu pull-left groups <?php if (!empty($id)) {
                        if ($search_by == 'group') {
                            echo 'active';
                        }
                    } ?>">
                        <a href="#" tabindex="-1"><?php echo lang('coop_group'); ?></a>
                        <ul class="dropdown-menu dropdown-menu-left"
                            style="<?php if (!empty($search_by) && $search_by == 'group') {
                                echo 'display:block';
                            } ?>">
                            <?php foreach ($all_coop_group as $group) {
                                ?>
                                <li class="<?php if (!empty($id)) {
                                    if ($search_by == 'group') {
                                        if ($id == $group->coop_group_id) {
                                            echo 'active';
                                        }
                                    }
                                } ?>">
                                    <a href="<?= base_url() ?>admin/client/manage_client/group/<?php echo $group->coop_group_id; ?>"><?php echo $group->coop_group; ?></a>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </li>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#coop_list"
                                                                   data-toggle="tab">협력사목록</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_coop"
                                                                   data-toggle="tab">신규등록</a></li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="coop_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>협력사목록</strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
							<table id="DataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%">
                                <thead>
                                <tr>
           
                                    <th>No</th>
                                    <th class="hidden-sm">협력사명</th>
                                    <!--th>항목설정 </th-->
                                    <th>대표 </th>
                                    <th>사업자등록번호 </th>
                                    <th>업태 </th>
                                    <th>종목 </th>
                                    <th>사업장주소 </th>
                                    <th>홈페이지 </th>
                                    <th>핸드폰번호 </th>
                                    <th>팩스 </th>
                                    <th>이메일 </th>
                                    <?php $show_custom_fields = custom_form_table(12, null);
                                    if (!empty($show_custom_fields)) {
                                        foreach ($show_custom_fields as $c_label => $v_fields) {
                                            if (!empty($c_label)) {
                                                ?>
                                                <th><?= $c_label ?> </th>
                                            <?php }
                                        }
                                    }
                                    ?>
                                    <th class="hidden-print"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>


                                <?php
                                if (!empty($all_coop_info)) {
                                    foreach ($all_coop_info as $coop_details) {
                                       // $client_transactions = $this->db->select_sum('amount')->where(array('paid_by' => $coop_details->client_id))->get('tbl_transactions')->result();
                                       // $coop_group = $this->db->where('coop_group_id', $coop_details->coop_group_id)->get('tbl_coop_group')->row();
                                       // $client_outstanding = $this->invoice_model->client_outstanding($coop_details->client_id);
                                       // $client_currency = $this->invoice_model->client_currency_sambol($coop_details->client_id);
                                       // if (!empty($client_currency)) {
                                       //     $cur = $client_currency->symbol;
                                       // } else {
                                       //     $currency = $this->db->where(array('code' => config_item('default_currency')))->get('tbl_currencies')->row();
                                       //     $cur = $currency->symbol;
                                       // }
                                        ?>
                                    <tr>
                                        <td>
                                            <?= $coop_details->dp_id ?>
                                        </td>
                                        <td>
                                           <?= $coop_details->co_name ?>
										</td>
                                        <!--td>

                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="수당항목 관리">
                                            <a href="javascript:;" onClick="popAsWindow('<?= $coop_details->dp_id ?>');" class="text-default ml"><i class="fa fa-cog"></i>설정</a>
                                                </span>
										
										</td-->
                                        <td>
                                            <?= $coop_details->ceo ?>
                                        </td>
                                        <td>
                                            <?= $coop_details->userid ?>
                                        </td>
                                        <td>
                                            <?= $coop_details->bs_number ?>
                                        </td>
                                        <td>
                                            <?= $coop_details->bs_type1 ?>
                                        </td>
                                        <td>
                                            <?= $coop_details->bs_type2 ?>
                                        </td>
                                        <td>
                                            <?= $coop_details->homepage ?>
                                        </td>
                                        <td>
                                            <?= $coop_details->ceo_hp ?>
                                        </td>
                                        <td>
                                            <?= $coop_details->fax ?>
                                        </td>
                                        <td>
                                            <?= $coop_details->email ?>
                                        </td>
                                        <td>
                                                <?php if (!empty($edited)) { ?>
                                                    <?php echo btn_edit('admin/basic/coop_list/edit_coop/' . $coop_details->dp_id) ?>
                                                <?php }
                                                if (!empty($deleted)) {
                                                    ?>
                                                    <?php echo btn_delete('admin/basic/delete_coop/' . $coop_details->dp_id) ?>
                                                <?php } ?>

										</td>
                                    </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="13">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { 
						include "./application/views/admin/basic/_coop_edit.inc.php";
					?>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>