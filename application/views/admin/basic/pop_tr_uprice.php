<?= message_box('success'); ?>
<?php echo message_box('error');
$created = can_action('123', 'created');
$edited = can_action('123', 'edited');
$deleted = can_action('123', 'deleted');
?>
<script>
function goSSet(field,val,sc_id) { 
	 $("#sc_id").val(sc_id);
	 $("#field").val(field);
	 $("#val").val(val);
	 $("#myform").submit();
}

function goAddPartner() {
	window.open('<?= base_url() ?>admin/basic/tr_add_partner/<?= $ct_id ?>', 'wAddPartner', 'left=50, top=50, width=1000, height=600, scrollbars=1');
}
</script>
                                <form method="post" name="myform" id="myform" target="hiddenframe" 
                                      action="<?= base_url() ?>admin/basic/set_tr_uprice/" class="form-horizontal">
<input type="hidden" name="sc_id" id="sc_id" value="">
<input type="hidden" name="field" id="field" value="">
<input type="hidden" name="val" id="val" value="">

<div class="panel panel-custom">
	<div style="padding-top:10px;height:30px;">
                <div class="col-xs-10">
			<?= $title ?> - <?= $contract_info->ct_title ?> (<?= $contract_info->rr_company ?>)<br/>
                 </div>
               <div class="col-xs-2">
					<a href="javascript:goAddPartner();" tabindex="0" class="dt-button buttons-print btn btn-success btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 거래처(파트너)관리</span>
					</a>
                </div>
    </div>
    <header class="panel-heading ">
	</header>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                <thead>
                <tr>

                    <th width='50'>No</th>
                    <th width='150'>운전자</th>
                    <th width='150'>사업자번호</th>
<?php
                if (!empty($all_item_group)) {
                    foreach ($all_item_group as $item_info) {
						echo "<th>".$item_info->title."</th>";
					}
				}
?>
                    <?php if (!empty($edited) || !empty($deleted)) { ?>
                        <!--th width='50'>삭제</th-->
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php
				$cnt  = 0;
                if (!empty($all_uprice_group)) {
                    foreach ($all_uprice_group as $uprice_info) {
						$cnt++;
                        ?>
                        <tr id="item_info_<?= $uprice_info->idx?>">
							<td><?=$cnt?></td>
							<td><?=$uprice_info->ceo?></td>
							<td><?=$uprice_info->bs_number?></td>
<?php
$x = 1;
$unit_prices = $this->db->where('dp_id', $uprice_info->dp_id)->where('ct_id', $uprice_info->ct_id)->get('tbl_scontract_co')->row();
                if (!empty($all_item_group)) {
                    foreach ($all_item_group as $item_info) {
						$val = 0;
						if($x==1) $val = $unit_prices->val1;
						else if($x==2) $val = $unit_prices->val2;
						else if($x==3) $val = $unit_prices->val3;
						else if($x==4) $val = $unit_prices->val4;
						else if($x==5) $val = $unit_prices->val5;
						else if($x==6) $val = $unit_prices->val6;
?>
			  <td align="center"><input type="text" name="val<?=$x?>[<?=$cnt?>]" id="val<?=$x?>_<?=$cnt?>" value="<?=$val ?>" onBlur="goSSet('val<?=$x?>',this.value,'<?=$unit_prices->idx?>');" class="form-control" style="width:90%;text-align:center; background-color:transparent;"></td>
<?php
$x++;
					}
				}
?>

                            <?php if (!empty($edited) || !empty($deleted)) { ?>
                                <!--td>
                                    <?php
                                    $id = $this->uri->segment(5);
                                    if (!empty($edit_item_info->idx) && $item_info->idx == $edit_item_info->idx) { ?>
                                        <?= btn_cancel('admin/basic/set_item/'.$ct_id.'/'.$type.'/') ?>
                                    <?php } else {
                                        if (!empty($deleted)) { ?>

		  <a href="<?php echo base_url() ?>admin/basic/set_item/<?=$ct_id?>/<?=$type?>//tr_delete/<?=$item_info->idx?>" class="button red" title="삭제"><span class='label label-warning'> 삭제 </span></a>


                                        <?php }
                                    }
                                    ?>
                                </td-->
                            <?php } ?>
                        </tr>
                    <?php }
                }
?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</form>
<!-- 새창 대신 사용하는 iframe -->
<iframe width=0 height=0 name='hiddenframe' style='display:none;'></iframe>