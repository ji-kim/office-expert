<?php
		//if(!empty($dp->tr_id)) $tr = $this->db->where('idx', $dp->tr_id)->get('tbl_asset_truck')->row();
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 파트너 결제계좌 설정 - <?= $dp->ceo ?> / <?=$dp->driver?> / <?php if(!empty($tr->car_1)) echo $tr->car_1; ?> / </h4>
    </div>
    <div class="modal-body wrap-modal wrap">


        <form id="form_validation"
              action="<?php echo base_url() ?>admin/basic/set_partner_payment/save/<?= $dp_id ?>/<?= $page ?>/<?= $list_mode ?>/<?= $ws_co_id ?>"
              method="post" class="form-horizontal form-groups-bordered">

                    <div class="form-group" id="generate_type1">
                      <div class="col-sm-12">
							<label for="field-1" class="col-sm-3 control-label">결제계좌
								<span
									class="required"> *</span></label>

                          <div class="col-sm-9">
							   <div class="input-group">
			<select name="pay_account_no" id="pay_account_no" class="form-control">
				<option value=''>계좌선택</option>
<?
if (!empty($all_bank_info)) {
	foreach ($all_bank_info as $bank_details) {
			echo "<option value='".$bank_details->idx."' ".(($bank_details->idx == $dp->pay_account_no)?"selected":"").">[".$bank_details->account_name."] ".$bank_details->bank_name." ".$bank_details->account_no." </option>";
	}
}
?>
			</select>
								</div>
                            </div>
                        </div>


						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
							<button type="submit" class="btn btn-primary"> 저 장 </button>
						</div>
					</div>
        </form>
    </div>
</div>
