<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));
$all_goal_tracking = $this->basic_model->get_permission('tbl_goal_tracking');



$curency = $this->basic_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');

if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
	<style>
/* Ensure that the demo table scrolls */
	table > thead > tr > th {
		vertical-align:bottom;
		border-bottom:1px solid #eee;
		border-top:0px solid #eee;
		font-weight : none;
	}
    th, td {
        white-space: nowrap;
        padding-left: 10px !important;
        padding-right: 10px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
	</style>
<script>
	function selectTruck(params) {
	  window.open('<?php echo base_url(); ?>admin/asset/select_truck/'+params, 'winTR', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
	function selectPartner(params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/coop/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
	function goSearch(val,page,list_mode) {


		document.myform.page.value = page;
		document.myform.list_mode.value = list_mode;
		if(val == 'all') 
			document.myform.action = "<?php echo base_url() ?>admin/basic/partner_list_all";
		else
			document.myform.action = "<?php echo base_url() ?>admin/basic/partner_list_all";
		document.myform.submit();
	}
	function setPartnerStatus(dp_id,status) {
		document.myform.tdp_id.value = dp_id;
		document.myform.action = "<?php echo base_url() ?>admin/basic/partner_set_status/"+dp_id+"/"+status+"/";
		document.myform.submit();
	}

	function goExcel() {

		document.myform.action = '<?php echo base_url() ?>admin/basic/partner_list_all/excel/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	function editPartner(dp_id) {
		document.myform.action = '<?php echo base_url() ?>admin/basic/partner_list/edit_partner/'+dp_id;
		document.myform.submit();
	}


</script>

            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/basic/partner_list"
                      method="get" enctype="multipart/form-data" class="form-horizontal" name="myform">
					  <input type="hidden" name="list_mode" value="<?= $list_mode ?>">
					  <input type="hidden" name="page" value="<?php if(!empty($page)) echo $page;?>">
    <!-- 검색 시작 -->
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" bgcolor="#ffffff">
              <tr>
                <td width="12%" height="20" align="center" bgcolor="#efefef">위수탁관리사</td>
                <td width="15%"> 
					<select name="ws_co_id" id="ws_co_id" style="width:100%;" class="form-control input-sm" onChange="goSearch(this.value,'<?=1?>','<?=$list_mode?>');">
                 <option value="all" <?=($ws_co_id=="all")?"selected":""?> value="">전체</option>
<?php
if (!empty($all_ws_info)) {
	foreach ($all_ws_info as $ws_details) {
		if(!empty($ws_details->co_name) && $ws_details->co_name != "") {
?>
					<option value="<?=$ws_details->ws_co_id?>" <?=($ws_co_id == $ws_details->ws_co_id) ? "selected" : ""?>><?=$ws_details->co_name?></option>
<?php
		}
	}
}
?>
					</select>				
				</td>
                  <input type="hidden" name="baecha_co_id" value=""/>
                  <!--
                <td width="12%" height="20" align="center" bgcolor="#efefef">배차지</td>

                <td width="15%">
					<select name="baecha_co_id" id="baecha_co_id" style="width:100%;" class="form-control input-sm">
                 <option value="" <?=($baecha_co_id=="")?"selected":""?> value="">선택</option>
<?php
if (!empty($all_bc_info)) {
	foreach ($all_bc_info as $bc_details) {
		if(!empty($bc_details->co_name) && $bc_details->co_name != "") {
?>
					<option value="<?=$bc_details->baecha_co_id?>" <?=($baecha_co_id == $bc_details->baecha_co_id) ? "selected" : ""?>><?=$bc_details->co_name?></option>
<?php
		}
	}
}
?>
					</select>				
				</td>-->

                <!--td style="padding-left:5px;padding-top:10px;" align="center" bgcolor="#ffffff">
			<select name="pay_account_no" id="pay_account_no" class="form-control">
				<option value=''>계좌선택</option>
<?
if (!empty($all_bank_info)) {
	foreach ($all_bank_info as $bank_details) {
			echo "<option value='".$bank_details->idx."' ".(($bank_details->idx == $dp->pay_account_no)?"selected":"").">[".$bank_details->account_name."] ".$bank_details->bank_name." ".$bank_details->account_no." </option>";
	}
}
?>
			</select>

				</td>
                <td width="12%" height="20" align="center">
					<a href="javascript:goSetPayment();" tabindex="0" class="dt-button buttons-print btn btn-success btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-search"> </i> 계좌일괄설정</span>
					</a>
				</td-->
                <td style="padding-left:5px;padding-top:10px;" align="center" bgcolor="#ffffff">
					<a href="javascript:goSearch('<?php if(!empty($ws_co_id)) echo $ws_co_id;?>','<?=1?>','<?=$list_mode?>');" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-search"> </i> 검색</span>
					</a>
					<a href="javascript:goExcel()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 엑셀 저장</span>
					</a>
                    <?if(DATA_IMPORT){?>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀 가져오기</span>
					</a>
                    <?}?>

				</td>
					
			  </tr>
            </table>
		  </td>
        </tr>
      </table>
	  </form>
      <!-- 검색 끝 -->

	
	
	<div class="col-sm-12 bg-white p0">
        <div class="col-md-7">
            <div class="row row-table pv-lg">
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($ws_co_id)) echo $ws_co_id;?>','<?=1?>','ALL');" tabindex="0" class="dt-button buttons-print btn btn-<?=($list_mode=="ALL")?"success":"basic"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 전체파트너현황 </span>
					</a>
                </div>
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($ws_co_id)) echo $ws_co_id;?>','<?=1?>','IN');" tabindex="0" class="dt-button buttons-print btn btn-<?=($list_mode=="IN")?"success":"basic"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약중파트너현황 </span>
					</a>
                </div>
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($ws_co_id)) echo $ws_co_id;?>','<?=1?>','WT');" tabindex="0" class="dt-button buttons-print btn btn-<?=($list_mode=="WT"||$list_mode=="WTU")?"success":"basic"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약종료대기현황 </span>
					</a>
                </div>
<?php if($list_mode=="WT" || $list_mode=="WTU") { ?>
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($ws_co_id)) echo $ws_co_id;?>','<?=1?>','WTU');" tabindex="0" class="dt-button buttons-print btn btn-warning btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 챠량정보갱신 </span>
					</a>
                </div>
<?php } ?>
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($ws_co_id)) echo $ws_co_id;?>','<?=1?>','EP');" tabindex="0" class="dt-button buttons-print btn btn-<?=($list_mode=="EP")?"success":"basic"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약종료현황</span>
					</a>
                </div>

            </div>

        </div>

    </div>
<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'group') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/client/manage_partner"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($all_customer_group) > 0) { ?>
                    <li class="dropdown-submenu pull-left groups <?php if (!empty($id)) {
                        if ($search_by == 'group') {
                            echo 'active';
                        }
                    } ?>">
                        <a href="#" tabindex="-1"><?php echo lang('customer_group'); ?></a>
                        <ul class="dropdown-menu dropdown-menu-left"
                            style="<?php if (!empty($search_by) && $search_by == 'group') {
                                echo 'display:block';
                            } ?>">
                            <?php foreach ($all_customer_group as $group) {
                                ?>
                                <li class="<?php if (!empty($id)) {
                                    if ($search_by == 'group') {
                                        if ($id == $group->customer_group_id) {
                                            echo 'active';
                                        }
                                    }
                                } ?>">
                                    <a href="<?= base_url() ?>admin/client/manage_partner/group/<?php echo $group->customer_group_id; ?>"><?php echo $group->customer_group; ?></a>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </li>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#client_list"
                                                                   data-toggle="tab">파트너관리</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_partner"
                                                                   data-toggle="tab">신규등록</a></li>
                <?if(DATA_IMPORT == true){?>
                <li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/client/import">데이터가져오기</a>
                </li>
                <?}?>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
            <td rowspan="2"><br/>선택</td>
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
<?php if($list_mode=="WT" || $list_mode=="WTU") { ?><td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>계약종료</td><?php } ?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>배차지</td>
          <!--td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>소속지사</td-->
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>납부계좌</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="27">파트너 정보</td>
          <td width="160px" style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;" rowspan="2"></td>
        </tr>
        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">파트너명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대표</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">운전자</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">직위</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">주민등록번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">운전면허번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종사자번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약종료일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과세여부</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세누적</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자등록번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">업태</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종목</td>
          <td width="300px" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업장주소</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">용도</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지입사명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">핸드폰번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">PDA번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">은행</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">예금주</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이메일</td>
          <td width="300px" style="color:#ffffff;background-color: #777777;">주소</td>
        </tr>
                                </thead>

                                <tbody>

                                <?php
                                if (!empty($all_partner_info)) {
                                    foreach ($all_partner_info as $partner_details) {
										//차량정보
										$truck = $this->db->where('idx', $partner_details->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;
										// 배차지
										$rco = $this->db->where('mb_type', 'customer')->where('code', $partner_details->code)->get('tbl_members')->row();


//--------------------------------------------------------------------------------------------
/*
if($list_mode=="WTU") {
					$tr_id = $partner_details->tr_id;
echo $partner_details->ceo;
echo " - trid:".$partner_details->tr_id."<br>";
					//기존 현규씨 데이터 보정(차량정보가 제데로 연결되지 않았음)
					if(!empty($partner_details->U)) {
						$tr = $this->db->where('car_1', TRIM($partner_details->U))->get('tbl_asset_truck')->row();
					echo "U:".$partner_details->U."<br>";
						if(!empty($tr->idx)) {
							$sql = "UPDATE tbl_members SET tr_id='".$tr->idx."' WHERE dp_id = '".$partner_details->dp_id."'";
							$this->db->query($sql);
					echo "sql:".$sql."<br>";
						}
					}
}
*/
//--------------------------------------------------------------------------------------------

                                        ?>
                                        <tr>
                                            <td><input type="checkbox" name="dp_id[]" value="<?=$partner_details->dp_id?>"></td>
				  <td><?= $partner_details->dp_id ?></td>
<?php if($list_mode=="WT" || $list_mode=="WTU") { ?>
				<td>

                    <span data-placement="top" data-toggle="tooltip"  title="계약종료 설정">
                        <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/basic/set_partner_status/CLS/<?= $partner_details->dp_id ?>/<?= $page ?>/<?= $list_mode ?>/<?= $ws_co_id ?>" class="text-default ml"><i class="fa fa-cog"></i>계약종료</a>
                    </span>
				</td>
<?php } ?>
				  <td><?php if(!empty($rco->co_name)) echo $rco->co_name; ?></td>
				  <!--td><?php if(!empty($partner_details->br_name)) echo $partner_details->br_name ?></td-->
				  <td>
					<a data-toggle="modal" data-target="#myModal" title="납부계좌설정" href="<?= base_url() ?>admin/basic/set_partner_payment/set/<?= $partner_details->dp_id ?>/<?= $page ?>/<?= $list_mode ?>/<?= $ws_co_id ?>" class="btn btn-xs btn-purple"><i class="fa fa-copy"></i></a>
					<? if(!empty($partner_details->p_account_name)) echo $partner_details->p_account_name; ?>
					<? if(!empty($partner_details->p_bank_name)) echo $partner_details->p_bank_name; ?>
					<? if(!empty($partner_details->p_account_no)) echo $partner_details->p_account_no; ?>
				  </td>
				  <td><?= $partner_details->co_name ?></td>
				  <td><?= $partner_details->ceo ?></td>
				  <td><?= $partner_details->driver ?></td>
				  <td>
						<select name="position" id="position" class="form-control" style="width:60px;" onChange="goSet('position','<?= $partner_details->dp_id ?>',this.value);">
							<option value="팀원" <?=($partner_details->position == "팀원") ? "selected" : ""?>>팀원</option>
							<option value="조장" <?=($partner_details->position == "조장") ? "selected" : ""?>>조장</option>
							<option value="팀장" <?=($partner_details->position == "팀장") ? "selected" : ""?>>팀장</option>
							<option value="현장대리인" <?=($partner_details->position == "현장대리인") ? "selected" : ""?>>현장대리인</option>
					</select>
								  
				  
				  
				  </td>
				  <!--td><?= $partner_details->H ?></td>
				  <td><?= $partner_details->I ?></td>
				  <td><?= $partner_details->J ?></td-->
				  <td><?= $partner_details->reg_number ?></td>
				  <td><?= $partner_details->L ?></td>
				  <td><?= $partner_details->M ?>1</td>
				  <td><?= $partner_details->N ?>2</td>
				  <td><?= $partner_details->O ?>3</td>
				  <td align="center" style="padding-left:5px;background-color:yellow;">
						<select name="P" id="P" class="form-control" style="width:60px;" onChange="goSet('P','<?= $$partner_details->dp_id ?>',this.value);">
							<option value="" >선택</option>
							<option value="Y" <?=($partner_details->P == "Y") ? "selected" : ""?>>Y</option>
							<option value="N" <?=($partner_details->P == "N") ? "selected" : ""?>>N</option>
							<option value="직원" <?=($partner_details->P == "직원") ? "selected" : ""?>>직원</option>
						</select>
				  </td><!-- 과세여부 -->
				  <td align="center" style="padding-left:5px;background-color:red">
                                <input data-toggle="toggle" name="acc_vat_yn" value="1" <?php
                                if (!empty($partner_details->acc_vat_yn) && $partner_details->acc_vat_yn == "Y") {
                                    echo 'checked';
                                }
                                ?> data-on="Y" data-off="N" data-onstyle="success"
                                       data-offstyle="danger" type="checkbox">

				  </td><!-- 부가세누적여부 -->
						<td><?= $partner_details->bs_number ?></td><!-- 사업자등록번호 -->
						<td><?= $partner_details->bs_type1 ?></td><!-- 업태 -->
						<td><?= $partner_details->bs_type2 ?></td><!-- 종목 -->
						<td align="left" style="padding-left:5px;"><?= $partner_details->T ?></td><!-- 사업장주소 -->

						<td><?= $partner_details->car_1 ?></td><!-- 차량번호 -->
						<td><?= $partner_details->car_3 ?></td><!-- 용도 -->
						<td><?= $partner_details->car_7 ?></td><!-- 차대번호 -->
		  
					  <td><?= $partner_details->ws_co_name ?></td><!-- 지입사명 -->
					  <td><?= $partner_details->co_tel ?></td>
					  <td><?= $partner_details->Z ?></td>
					  <td><?= $partner_details->AA ?></td>
					  <td><?= $partner_details->AB ?></td>
					  <td><?= $partner_details->AC ?></td>
					  <td><?= $partner_details->AD ?></td>
					  <td><?= $partner_details->email ?></td>
					  <td align="left" style="padding-left:5px;"><?= $partner_details->co_address ?></td>
					  <td align="center">
							<a href="javascript:editPartner('<?=$partner_details->dp_id?>');" class="btn btn-primary btn-xs" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i></a>
                                                <?//= btn_edit('admin/basic/partner_list_all/edit_partner/' . $partner_details->dp_id) ?>
                                            <?php if (!empty($can_edit) && !empty($edited)) { ?>

                                            <?php }
                                            ?>
                                               <?php echo btn_delete('admin/basic/delete_partner/' . $partner_details->dp_id) ?>
											   <!--
                                               <?php echo ajax_anchor(base_url("admin/projects/delete_project/" . $partner_details->dp_id), "<i class='btn btn-danger btn-xs fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table-project-" . $partner_details->dp_id)); ?>
											   -->
					  </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { 
						include "./application/views/admin/basic/_partner_edit.inc.php";
					?>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
                <button onClick="selectDelete()">선택삭제</button>
</div>
<script type="text/javascript">
    function selectDelete(){
        var deleteDpids = "";
        $("input[name='dp_id[]']").each(function(k,v){
            if($(this).is(":checked")){
                if(deleteDpids != ""){
                    deleteDpids += "|";
                }
                deleteDpids += $(this).val();
            }


        });
        if(deleteDpids == ""){
            alert("삭제할 대상이 선택되지 않았습니다.");
        }else {
            if(confirm("삭제시 되돌릴 수 없습니다.\n삭제하시겠습니까?")) {
                location.href = "/admin/basic/delete_partners/" + deleteDpids;
            }
        }
    }

    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>