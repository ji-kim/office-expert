<?= message_box('success'); ?>
<?= message_box('error');

$eeror_message = $this->session->userdata('error');
if (!empty($eeror_message)):foreach ($eeror_message as $key => $message):
    ?>
    <div class="alert alert-danger">
        <?php echo $message; ?>
    </div>
    <?php
endforeach;
endif;
$this->session->unset_userdata('error');

?>
<?php
$perc_paid = 0;
?>
<div class="row mt-lg">
    <div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked navbar-custom-nav">
            <li class="<?= empty($url) ? 'active' : '' ?>"><a href="#task_details" data-toggle="tab"
                                                              aria-expanded="true">위수탁관리비</a>
            </li>
            <!--li class=""><a href="#contacts"
                                                                         data-toggle="tab"
                                                                         aria-expanded="false">일반공제
                    <strong
                        class="pull-right"><?= (!empty($customer_contacts) ? count($customer_contacts) : null) ?></strong></a>
            </li-->
            <li class=""><a href="#notes" data-toggle="tab"
                                                                  aria-expanded="false">환급형공제<strong
                        class="pull-right"><?= (!empty($customer_notes) ? count($customer_notes) : null) ?></strong></a>
            </li>
        </ul>
    </div>
    <div class="col-sm-9">
        <div class="tab-content" style="border: 0;padding:0;">
            <!-- Task Details tab Starts -->
            <div class="tab-pane active " id="task_details"
                 style="position: relative;">

			
        <form role="form" id="form" action="<?php echo base_url(); ?>admin/basic/save_settings" method="post"
              class="form-horizontal  ">
                    <section class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong>위수탁관리비 설정</strong>
                            </div>
                        </div>
                        <div class="panel-body">


                        <input type="hidden" name="mode" value="mfee">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">위수탁관리비 <span
                                    class="text-danger">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" name="mfee" class="form-control"
                                       value="<?= $default_settings->mfee ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">위수탁관리비 부가세 <span
                                    class="text-danger">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" name="mfee_vat" class="form-control"
                                       value="<?= $default_settings->mfee_vat ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">차고지비 </label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control"
                                       value="<?= $default_settings->garage_fee ?>"
                                       name="garage_fee">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">차고지비 부가세 </label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control"
                                       value="<?= $default_settings->garage_fee_vat ?>"
                                       name="garage_fee_vat">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">협회비</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control"
                                       value="<?= $default_settings->membership_fee ?>"
                                       name="membership_fee">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3"></label>
                        <div class="col-lg-7">
                            <button type="submit" class="btn btn-sm btn-primary"><?= lang('save_changes') ?></button>
                        </div>
                    </div>
        </form>
                    </section>
			
			
			
			</div>

            <!--            *************** contact tab start ************-->
            <div class="tab-pane " id="contacts"
                 style="position: relative;">
        <form role="form" id="form" action="<?php echo base_url(); ?>admin/basic/save_settings" method="post"
              class="form-horizontal  ">
                    <section class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong>일반공제 설정</strong>
                            </div>
                        </div>
                        <div class="panel-body">


                        <input type="hidden" name="mode" value="mfee">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">미수금 </label>
                            <div class="col-lg-7">
                                자동생성
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">과태료 </label>
                            <div class="col-lg-7">
                                매월입력
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">사고접부비 </label>
                            <div class="col-lg-7">
                                매월입력
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">차고지비 </label>
                            <div class="col-lg-7">
                                개별입력
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">환경부담금 </label>
                            <div class="col-lg-7">
                                개별입력
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3"></label>
                        <div class="col-lg-7">
                            <button type="submit" class="btn btn-sm btn-primary"><?= lang('save_changes') ?></button>
                        </div>
                    </div>
        </form>
                    </section>
			


            </div>
            <div class="tab-pane " id="notes" style="position: relative;">



        <form role="form" id="form" action="<?php echo base_url(); ?>admin/basic/save_settings" method="post"
              class="form-horizontal  ">
                    <section class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong>환급형공제 설정</strong>
                            </div>
                        </div>
                        <div class="panel-body">


                        <input type="hidden" name="mode" value="refund">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">해지담보금 </label>
                            <div class="col-lg-7">
                                <input type="text" name="mortgage" class="form-control"
                                       value="<?= $default_settings->mortgage ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3"></label>
                        <div class="col-lg-7">
                            <button type="submit" class="btn btn-sm btn-primary"><?= lang('save_changes') ?></button>
                        </div>
                    </div>
        </form>
                    </section>
			

            </div>

        </div>
    </div>
</div>