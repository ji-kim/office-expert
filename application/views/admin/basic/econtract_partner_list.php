<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();

if ($this->session->userdata('user_type') == 1) {
$margin = 'margin-bottom:30px';
?>
    <style>
        /* Ensure that the demo table scrolls */
        th, td {
            white-space: nowrap;
            padding-left: 40px !important;
            padding-right: 40px !important;
        }
        div.dataTables_wrapper {
            width: 100%;
            margin: 0 auto;
        }
    </style>
    <script>
        function popCtWindow(dp_id) {
            window.open('<?php echo base_url(); ?>prn/contract.html?dp_id='+dp_id, 'winCT', 'left=50, top=50, width=870, height=800, scrollbars=1');
        }
        function popRCtWindow(dp_id) {
            window.open('<?php echo base_url(); ?>prn/recontract.html?dp_id='+dp_id, 'winCT', 'left=50, top=50, width=870, height=800, scrollbars=1');
        }
        function popCtRWindow(dp_id) {
            window.open('<?php echo base_url(); ?>prn/seoul02.html?dp_id='+dp_id, 'winCTR', 'left=50, top=50, width=870, height=800, scrollbars=1');
        }

        function popECWindow(econtract_id) {
            window.open('<?php echo base_url(); ?>admin/basic/econtract_detail/'+econtract_id, 'winCT', 'left=50, top=50, width=870, height=800, scrollbars=1');
        }
        function selectTruck(params) {
            window.open('<?php echo base_url(); ?>admin/asset/select_truck/'+params, 'winTR', 'left=50, top=50, width=1200, height=700, scrollbars=1');
        }
        function selectPartner(params) {
            window.open('<?php echo base_url(); ?>admin/basic/select_company/coop/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
        }

        function goSearch(val,page,list_mode) {
            document.myform.page.value = page;
            document.myform.list_mode.value = list_mode;
            if(val == 'all' || val == '')
                document.myform.action = "<?php echo base_url() ?>admin/basic/partner_list_all";
            else
                document.myform.action = "<?php echo base_url() ?>admin/basic/partner_list_all";
            document.myform.submit();
        }

        function setPartnerStatus(dp_id,status) {
            document.myform.tdp_id.value = dp_id;
            document.myform.action = "<?php echo base_url() ?>admin/basic/partner_set_status/"+dp_id+"/"+status+"/";
            document.myform.submit();
        }

        function goExcel() {

            document.myform.action = '<?php echo base_url() ?>admin/basic/partner_list_all/excel/';
            document.myform.target = '_blank';
            document.myform.submit();
        }

        function editPartner(dp_id) {
            document.myform.action = '<?php echo base_url() ?>admin/basic/partner_list/edit_partner/'+dp_id;
            document.myform.submit();
        }
    </script>

<div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
    <form data-parsley-validate="" novalidate=""
          action="<?php echo base_url() ?>admin/basic/partner_list_all"
          method="get" enctype="multipart/form-data" class="form-horizontal" name="myform">
        <input type="hidden" name="page" value="<?php if(!empty($page)) echo $page;?>">
        <input type="hidden" name="list_mode" value="<?php if(!empty($page)) echo $page;?>">


    </form>
    <!-- 검색 끝 -->

    <div class="col-sm-12 bg-white p0">
        <div class="col-md-7">
            <div class="row row-table pv-lg">
                <div class="col-xs-4">
                    <a href="javascript:goSearch('all','1','ALL');" tabindex="0" class="dt-button buttons-print btn btn-basic btn-xs mr">
                        <span><i class="fa fa-address-book"> </i> 전체파트너현황 </span>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="javascript:goSearch('all','1','IN');" tabindex="0" class="dt-button buttons-print btn btn-basic btn-xs mr">
                        <span><i class="fa fa-address-book"> </i> 계약중파트너현황 </span>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="javascript:goSearch('all','1','WT');" tabindex="0" class="dt-button buttons-print btn btn-basic btn-xs mr">
                        <span><i class="fa fa-address-book"> </i> 계약종료대기현황 </span>
                    </a>
                </div>

                <div class="col-xs-4">
                    <a href="javascript:goSearch('all','1','EP');" tabindex="0" class="dt-button buttons-print btn btn-basic btn-xs mr">
                        <span><i class="fa fa-address-book"> </i> 계약종료현황</span>
                    </a>
                </div>

                <div class="col-xs-4">
                    <a href="/admin/basic/econtract_partner_list" tabindex="0" class="dt-button buttons-print btn btn-success btn-xs mr">
                        <span><i class="fa fa-address-book"> </i> 전자계약대기현황</span>
                    </a>
                </div>

            </div>

        </div>
    </div>


    <?php }

    $id = $this->uri->segment(5);
    $search_by = $this->uri->segment(4);
    $created = can_action('4', 'created');
    $edited = can_action('4', 'edited');
    $deleted = can_action('4', 'deleted');
    ?>
    <div class="row">
        <div class="col-sm-12">
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#client_list" data-toggle="tab">전자계약대기</a></li>




            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane active" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>
                                <tr align="center" bgcolor="#e0e7ef">
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" >전자계약</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">파트너명</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약내용</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
                                    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약종료일</td>

                                </tr>


                                </thead>

                                <tbody>

                                <?php
                                $cnt = 0;
                                if (!empty($econtract)) {
                                    foreach ($econtract as $econ) {?>


                                            <tr>
                                                <td><?= $econ->econtract_id ?></td>
                                                <td><a href="javascript:;" onClick="popECWindow('<?= $econ->econtract_id ?>');"  class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-cog"></i>전자계약</a></td>
                                                <td><?= $econ->ceo ?></td>
                                                <td><?= $econ->econtract_legal ?></td>
                                                <td><?= $econ->contract_start_date ?></td>
                                                <td><?= $econ->contract_end_date ?></td>

                                            </tr>
                                            <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="6">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) {

                        ?>
                    <?php } else { ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>

