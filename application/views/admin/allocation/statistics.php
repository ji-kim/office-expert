<?php include_once 'asset/admin-ajax.php'; ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error');
$params['alloc_date'] = $alloc_date; //할당일
$params['is_view'] = "T";
$params['page'] = "1";
$params['branch_id'] = $branch_id;

$created = can_action('71', 'created');
$edited = can_action('71', 'edited');
$deleted = can_action('71', 'deleted');
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

<script>
function goSearch() {
	location.href = '<?php echo base_url() ?>admin/allocation/scheduling/';
}
</script>
            <div class="col-sm-12 bg-white p0" >
				<div class="row ">
					<div class="col-sm-12" style="margin:10px">

						  <div class="form-group" id="border-none">
							<div class="col-sm-3">
								<span class="input-group-btn">
									<a href="/pages/batcha/batcha_office.php" type="button" class="dt-button buttons-print btn btn-success btn-xs mr">
									   지사별보기
									</a>
									&nbsp;
									<a href="/pages/batcha/batcha_gisa.php" type="button" class="dt-button buttons-print btn btn-default btn-xs mr">
									   기사별보기
									</a>
									&nbsp;
									<a href="/pages/batcha/batcha_stat.php" type="button" class="dt-button buttons-print btn btn-default btn-xs mr">
									   월별통계
									</a>
								</span>
							</div>
							<div class="col-sm-2">
								<select class="form-control input-sm" onChange="">
									<?php
									$cur_branch = "전체지사 ";
									foreach ($all_branch_group as $branch_info):
										?>
											<option value='<?php echo $branch_info->dp_id ?>' <?=($branch_info->dp_id==$branch_id)?"selected":""?>>
												<?php 
												if(!empty($branch_info->co_name)) echo $branch_info->co_name; 
												if($branch_info->dp_id==$branch_id) $cur_branch = $branch_info->co_name;
												?> </option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-3">
															<div class="input-group input-medium">
																<span class="input-group-btn">
																	<button class="btn blue" type="button" onclick="dateMove(-1);">
																		<i class="fa fa-angle-double-left"></i>
																	</button>
																</span>
																<input type="text" class="form-control datepicker" id="sc_date" data-format="yyyy-mm-dd" name="alloc_date" id="alloc_date" value="<?=$alloc_date;?>">
																<span class="input-group-btn">
																	<button class="btn blue" type="button" onclick="dateMove(1);">
																		<i class="fa fa-angle-double-right"></i>
																	</button>
																</span>
																<span class="input-group-btn">
																	<button class="btn btn-primary" type="button" onclick="goSearch();">
																		<i class="fa fa-search"></i>
																	</button>
																</span>
															</div>
							</div>
							<div class="col-sm-1">
							</div>
							<div class="col-sm-3">
                                                            <div class="input-group">
																<input type="search" value="<?//=$params['sc_key'];?>" name="sc_key" id="search_key" class="form-control" placeholder="Search">
                                                                <span class="input-group-btn">
																	<button class="btn btn-default" type="button" onclick="goSearch();">
                                                                        Search
                                                                        <i class="fa fa-search"></i>
                                                                    </button>
																	<button class="btn btn-default" type="button" onclick="goExcel();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-excel"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>
						</div>
		</div>
    </div>


<div class="row ">
    <div class="col-sm-12" style="margin:10px">
        <div class="panel-title">
             <strong><i class="fa fa-building"></i> <?=$cur_branch?> ( 총 <?=number_format($total_driver_count)?> 명) 배차등록</strong>
        </div>
	</div>
</div>

<div class="row ">
    <div class="col-sm-12">
                        <div class="box" style="padding:10px;">
						<table id="FDataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                            <thead>
							<tr align="center" bgcolor="#e0e7ef">
							  <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소속지사</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">검사원</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기사</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">현장명</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소재지</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">구분</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">시작</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종료</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">수량</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실운반</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공동작업</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">중량</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종류</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비고</td>
							</tr>

                            </thead>
                            <tbody>
            <?php
		$cnt = 1;
		if(!empty($all_branch_list)) {
            foreach ($all_branch_list as $branch_info):

				//일정
				$params['br_id']		= $branch_info->dp_id;
                $status = $this->allocation_model->get_work_status($params['alloc_date'], $params['br_id']); // 지사/날짜별 등록상태값 가져오기
				$work_list = $this->allocation_model->work_list_all($params);
				$row_cnt = count($work_list);
				if($row_cnt==0) $srow_cnt=1; else $srow_cnt=$row_cnt;

				// 지사정보 말풍선
				$br_info = "지사명 : ". $branch_info->co_name;
				$br_info .= " / 아이디 : ". $branch_info->username;
				$br_info .= " / 일정담당자 : ". $branch_info->ceo;
				$br_info .= " / 핸드폰 : ". $branch_info->ceo_hp;
				$br_info .= " / 연락처 : ". $branch_info->co_tel;               ?>
                                <tr>
									<td rowspan="<?=$srow_cnt?>" style="text-align:right;">
										<?=$cnt;?>
									</td>
									<td style='text-align:left;' rowspan="<?=$srow_cnt?>">
										<span data-balloon-length="large" data-balloon="<?=$br_info?>" data-balloon-pos="right"><?=$branch_info->co_name;?></span><br/>
		<?php	if(!empty($status->is_register) && $status->is_register=="1") { //등록완료 ?>
			<?php if(1) { //$session_info[mtype] == "AD" || $session_info[mtype] == "GR" || $session_info[mtype] == "CJ" || $session_info[mtype] == "KS" || $session_info[mtype] == "BR") {  ?>
				<?php	if(!empty($status->is_confirm) && $status->is_confirm=="1") { //확정완료 ?>
                                         <a href="javascript:;" class="btn btn-xs red" onClick="set_schedule('CF_CANCEL','<?=$branch_info->dp_id?>','<?=$branch_info->co_name?>','');"><i class="fa fa-plus"></i> 확정취소 </a>
				<?php	} else {  ?>
                                          <a href="javascript:;" class="btn btn-xs red" onClick="set_schedule('RG_CANCEL','<?=$branch_info->dp_id?>','<?=$branch_info->co_name?>','');"><i class="fa fa-plus"></i> 등록취소 </a>
                                          <a href="javascript:;" class="btn btn-xs blue" onClick="set_schedule('CONFIRM','<?=$branch_info->dp_id?>','','');"><i class="fa fa-link"></i> 확 정 </a>
				<?php	} ?>
			<?php	} ?>
		<?php	} else {  ?>
                <?php if(1) { //$session_info[mtype] != "OB") { //옵저버가 아닐경우만 추가가능 ?>
											<a href="javascript:;" class="dt-button buttons-print btn btn-success btn-xs mr" onClick="set_schedule('INSERT','<?=$branch_info->dp_id?>','<?=$branch_info->co_name?>','');"><i class="fa fa-plus"></i> 일정추가 </a><?php } ?>

				<?php if(1) {//$session_info[mtype] == "AD" || $session_info[mtype] == "GR" || $session_info[mtype] == "CJ" || $session_info[mtype] == "KS" || $session_info[mtype] == "BR") {  ?>
                                            <a href="javascript:;" class="dt-button buttons-print btn btn-success btn-xs mr" onClick="set_schedule('RG_FINISH','<?=$branch_info->dp_id?>','','');"><i class="fa fa-link"></i> 등록완료 </a>
			<?php	} ?>
		<?php	} ?>
									</td>
<?php
		if($row_cnt==0 ) {
			echo "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
			if(1) echo "<td></td>"; 
			echo "</tr>";

		} else {
			$is_moved = $move_to = $is_register = $is_confirm = "";
			if(!empty($status->is_moved)) $is_moved = $status->is_moved;
			if(!empty($status->move_to)) $move_to = $status->move_to;
			if(!empty($status->is_register)) $is_register = $status->is_register;
			if(!empty($status->is_confirm)) $is_confirm = $status->is_confirm;

			if (!empty($work_list)) {
				foreach ($work_list as $work_details) {
			//for($j=0; $j<count($work_list); $j++) {
					//$row_wl = $work_list[$j];
					if($cnt>0) echo "<tr class=\"gradeX\" role=\"row\">";

					if($is_moved==1 && $move_to) { // 작업이동
						include "./application/views/admin/allocation/_scheduling_regist_all_moved.inc.php"; //3
					} else {
						if($is_register=='1' || $is_confirm=='1')  //---------- 등록완료
							include "./application/views/admin/allocation/_scheduling_regist_all.inc.php"; //2
						else   
							include "./application/views/admin/allocation/_scheduling_regist.inc.php"; //1
					}
					echo "</tr>";
				}

			}
		}

			
				$cnt++;
			endforeach; ?>
         <?php } else { ?>
                                <tr>
									<td colspan="16">
										<strong>등록된 데이터가 없습니다</strong>
									</td>
                                </tr>
         <?php } ?>
                            </tbody>
                        </table>
					</div>

    </div>
    </div>
</div>