<aside class="aside">
    <!-- START Sidebar (left)-->
    <?php
    $user_id = $this->session->userdata('user_id');
    $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
    $user_info = $this->db->where('user_id', $user_id)->get('tbl_users')->row();
?>
    <div class="aside-inner">
        <nav data-sidebar-anyclick-close="" class="sidebar">


<ul class='nav'>
<?php if($pop_page == "contract") { ?>
<li class='active' >
  <a  title='거래항목설정' href='<?php echo base_url() ?>admin/contract/pop_contract_items/<?=$ct_id?>'>
 <em class='fa fa-dribbble'></em><span>거래항목설정</span></a>
</li>
<?php } else if($pop_page == "scontract") { ?>
  <li class='<?=($sub=="cowork")?"active":""?>' >
    <a  title='거래처설정' href='<?php echo base_url() ?>admin/contract/pop_scontract_config/<?=$ct_id?>/cowork'>
   <em class='fa fa-dribbble'></em><span>거래처설정</span></a>
  </li>
  <li class='<?=($sub=="partner")?"active":""?>' >
    <a  title='하도급파트너설정' href='<?php echo base_url() ?>admin/contract/pop_scontract_config/<?=$ct_id?>/partner'>
   <em class='fa fa-dribbble'></em><span>하도급파트너설정</span></a>
  </li>
<?php } ?>
</ul>

            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>
