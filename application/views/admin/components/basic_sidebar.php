<aside class="aside">
    <!-- START Sidebar (left)-->
    <?php
    $user_id = $this->session->userdata('user_id');
    $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
    $user_info = $this->db->where('user_id', $user_id)->get('tbl_users')->row();
    ?>
    <div class="aside-inner">
        <nav data-sidebar-anyclick-close="" class="sidebar">


<ul class='nav'>
<li class='<?=($co_type=="all")?"active":""?>' >
  <a  title='전체사목록' href="javascript:selectMember('all','전체','','','','','','all');">
 <em class='fa fa-dribbble' style="color:yellow;font-weight:bold;"></em><span style="color:yellow;font-weight:bold;">전체사선택</span></a>
</li> 
</ul>
<ul class='nav'>
<li class='<?=($co_type=="all")?"active":""?>' >
  <a  title='전체사목록' href='<?php echo base_url() ?>admin/basic/select_company/all/<?=$params?>'>
 <em class='fa fa-dribbble'></em><span>전체사목록</span></a>
</li> 
<li class='<?=($co_type=="customer")?"active":""?>' >
  <a  title='거래처목록' href='<?php echo base_url() ?>admin/basic/select_company/customer/<?=$params?>'>
 <em class='fa fa-dribbble'></em><span>거래처목록</span></a>
</li> 
<li class='<?=($co_type=="partner")?"active":""?>' >
  <a  title='파트너목록' href='<?php echo base_url() ?>admin/basic/select_company/partner/<?=$params?>'>
 <em class='fa fa-dribbble'></em><span>파트너목록</span></a>
</li> 
<li class='<?=($co_type=="coop")?"active":""?>' >
  <a  title='협력사목록' href='<?php echo base_url() ?>admin/basic/select_company/coop/<?=$params?>'>
 <em class='fa fa-dribbble'></em><span>협력사목록</span></a>
</li> 
<li class='<?=($co_type=="group")?"active":""?>' >
  <a  title='그룹사목록' href='<?php echo base_url() ?>admin/basic/select_company/group/<?=$params?>'>
 <em class='fa fa-dribbble'></em><span>그룹사목록</span></a>
</li> 
</ul>

            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>
