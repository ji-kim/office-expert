<aside class="aside">
    <!-- START Sidebar (left)-->
    <?php
    $user_id = $this->session->userdata('user_id');
    $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
    $user_info = $this->db->where('user_id', $user_id)->get('tbl_users')->row();
?>
    <div class="aside-inner">
        <nav data-sidebar-anyclick-close="" class="sidebar">


<ul class='nav'>
<li class='<?=($md=="car_info")?"active":""?>' >
  <a  title='차랑정보변경' href='<?php echo base_url() ?>admin/asset/pop_carinfo/car_info/<?=$tr_id?>'>
 <em class='fa fa-dribbble'></em><span>차랑정보변경</span></a>
</li> 
<li class='<?=($md=="car_ins")?"active":""?>' >
  <a  title='자동차보험' href='<?php echo base_url() ?>admin/asset/pop_carinfo/car_ins/<?=$tr_id?>'>
 <em class='fa fa-dribbble'></em><span>자동차보험</span></a>
</li> 
<li class='<?=($md=="car_lins")?"active":""?>' >
  <a  title='적재물보험' href='<?php echo base_url() ?>admin/asset/pop_carinfo/car_lins/<?=$tr_id?>'>
 <em class='fa fa-dribbble'></em><span>적재물보험</span></a>
</li> 
<li class='<?=($md=="car_chk")?"active":""?>' >
  <a  title='정밀(정기)검사' href='<?php echo base_url() ?>admin/asset/pop_carinfo/car_chk/<?=$tr_id?>'>
 <em class='fa fa-dribbble'></em><span>정밀(정기)검사</span></a>
</li> 
<!--li class='<?=($md=="car_own")?"active":""?>' >
  <a  title='차주정보변경' href='<?php echo base_url() ?>admin/asset/pop_carinfo/car_own/<?=$tr_id?>'>
 <em class='fa fa-dribbble'></em><span>차주정보변경</span></a>
</li--> 
</ul>

            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>
