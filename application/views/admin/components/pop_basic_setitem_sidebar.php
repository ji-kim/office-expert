<aside class="aside">
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
        <nav data-sidebar-anyclick-close="" class="sidebar">


<ul class='nav'>

<li class='sub-menu <?=($type=="tr_item"||$type=="tr_uprice")?"active":""?>' >
  <a data-toggle='collapse' href='#basic0'> <em class='fa fa-usd'></em><span>거래설정</span></a>
	<ul id=basic0 class='nav sidebar-subnav collapse'><li class="sidebar-subnav-header">거래항목설정</li>
		<li class='<?=($type=="tr_item" && $cat=="01")?"active":""?>' >
		  <a  title='거래항목설정' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ct_id?>/tr_item/01'>
		 <em class='fa fa-dribbble'></em><span>거래항목설정</span></a>
		</li> 
		<li class='<?=($type=="tr_uprice" && $cat=="02")?"active":""?>' >
		  <a  title='거래단가설정' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ct_id?>/tr_uprice/02'>
		 <em class='fa fa-dribbble'></em><span>거래단가설정</span></a>
		</li> 
	</ul> 
</li> 


<li class='sub-menu <?=($type=="sudang")?"active":""?>' >
  <a data-toggle='collapse' href='#basic1'> <em class='fa fa-usd'></em><span>수당항목설정</span></a>
	<ul id=basic1 class='nav sidebar-subnav collapse'>
		<li class='<?=($type=="sudang" && $cat=="01")?"active":""?>' >
		  <a  title='각종수당' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ct_id?>/sudang/01'>
		 <em class='fa fa-dribbble'></em><span>각종수당</span></a>
		</li> 
	</ul> 
</li> 



<li class='sub-menu <?=($type=="gongje")?"active":""?>' >
   <a data-toggle='collapse' href='#basic2'> <em class='fa fa-usd'></em><span>공제항목설정</span></a>
	<ul id=basic2 class='nav sidebar-subnav collapse'><li class="sidebar-subnav-header">수당항목설정</li>
		<li class='<?=($type=="gongje" && $cat=="01")?"active":""?>' >
		  <a  title='위.수탁관리비' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ct_id?>/gongje/01'>
		 <em class='fa fa-dribbble'></em><span>위.수탁관리비</span></a>
		</li> 
		<li class='<?=($type=="gongje" && $cat=="02")?"active":""?>' >
		  <a  title='일반공제' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ct_id?>/gongje/02'>
		 <em class='fa fa-dribbble'></em><span>일반공제</span></a>
		</li> 
		<li class='<?=($type=="gongje" && $cat=="03")?"active":""?>' >
		  <a  title='환급형' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ct_id?>/gongje/03'>
		 <em class='fa fa-dribbble'></em><span>환급형공제</span></a>
		</li> 
	</ul> 
</li> 



</ul>

            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>
