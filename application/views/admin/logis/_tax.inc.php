<?
$lColor="364F9E";
?>
<? if($pmode=="A" || $pmode == "C") { ?>
	<!-- 공급자용 -->
	<table width="600" border="0" cellpadding="0" cellspacing="0" style="border:2px #<?=$lColor?> solid">
	  <tr>
		<td><table width="600" border="0" cellspacing="0" cellpadding="0" style="border-left:1px #<?=$lColor?> solid;border-top:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid">
			<tr> 
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td width="50%" align="right" style="font:18pt 굴림;color:#<?=$lColor?>"><strong>세 
					  금 계 산 서</strong></td>
					<td width="50%" class="01" style="padding-left:6px">(공급받는자보관용) 
<? 

if($p1_print>0)  echo "  <font color=red><b>재발행</b></font>"; ?>
					</td>
				  </tr>
				</table></td>
			  <td width="60" style="border-right:3px #<?=$lColor?> solid"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td height="28" align="center" class="01">책<font color="ffffff">_</font>번<font color="ffffff">_</font>호</td>
				  </tr>
				  <tr> 
					<td height="24" align="center" class="01">일련번호</td>
				  </tr>
				</table></td>
			  <td width="150"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td height="28" colspan="3" align="right" style="border-right:1px #<?=$lColor?> solid;padding-right:4px" class="01"><font color="333333"></font> 
					  권</td>
					<td colspan="4" align="right" class="01" style="padding-right:4px"> 
					  <font color="333333"> </font>호</td>
				  </tr>
				  <tr> 
					<td height="1" colspan="7" bgcolor="#<?=$lColor?>"></td>
				  </tr>
				  <tr> 
					<td width="19" height="24" align="center" style="border-right:1px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center" style="border-right:3px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center" style="border-right:3px #<?=$lColor?> solid;" class="02"><font color="#<?=$lColor?>">-</font></td>
					<td width="19" align="center" style="border-right:1px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center" style="border-right:1px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center" style="border-right:1px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center"></td>
				  </tr>
				</table></td>
			</tr>
		  </table>
		  <table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			<!-- 공급자 -->
			  <td width="15" align="center" class="01" style="line-height:36px;border-left:1px #<?=$lColor?> solid;border-top:1px #<?=$lColor?> solid;padding-left:2px">공<br>
				급<br>
				자 </td>
			  <td width="47%" valign="top" background="<?//=$rootpath?>shop/admin/images/tax_ingam.gif"> <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:3px #<?=$lColor?> solid">
				  <tr> 
					<td width="53" height="24" align="center" class="01" style="border-right:1px #<?=$lColor?> solid;">등록번호</td>
					<td colspan="4" style="padding-left:6px" class="02"><?=$sup_business_no?></td>
				  </tr>
				  <tr> 
					<td height="1" colspan="5" bgcolor="#<?=$lColor?>"></td>
				  </tr>

				  <tr> 
					<td height="28" align="center" class="01" style="border-right:1px #<?=$lColor?> solid;">상<font color="ffffff">___</font>호<br>
					  (법인명) </td>
					<td width="100" class="02" style="padding-left:6px;padding-right:6px"><?=$sup_company_name?></td>
					<td width="26" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">성명</td>
					<td align="right" class="02" style="padding-right:10px"><?=$sup_company_ceo?></td>
					<td width="20"><!--<img src="<?=$rootpath?>shop/admin/images/<?=$rootpath?>shop/admin/images/in<?if($i==1) echo "2";?>.gif" width="12" height="14">--></td>
				  </tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td width="54" height="28" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">사 
					  업 장<br>
					  소 재 지 </td>
					<td colspan="3" style="border-right:1px #<?=$lColor?> solid;padding-left:6px;padding-right:6px" class="02"><?=$sup_company_addr?></td>
				  </tr>
				  <tr> 
					<td height="1" colspan="4" bgcolor="#<?=$lColor?>"></td>
				  </tr>
				  <tr> 
					<td height="28" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">업<font color="ffffff">___</font>태</td>
					<td width="90" class="02" style="padding-left:6px;padding-right:6px"><?=$sup_business_type1?></td>
					<td width="10"  class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;padding-left:4px">종<br>
					  목 </td>
					<td style="border-right:1px #<?=$lColor?> solid;padding-left:6px;padding-right:6px" class="02"><?=$sup_business_type2?></td>
				  </tr>
				</table></td>
				<!-- 공급받는자 -->
			  <td width="15" align="center" class="01" style="line-height:22px;border-top:1px #364F9E solid;border-left:1px #<?=$lColor?> solid;padding-left:2px">공<br>
				급<br>
				받<br>
				는<br>
				자 </td>
			  <td width="47%"> <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left:3px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;border-top:3px #<?=$lColor?> solid;border-bottom:3px #<?=$lColor?> solid;">
				  <tr> 
					<td height="24" align="center" class="01" style="border-right:1px #<?=$lColor?> solid;">등록번호</td>
					<td colspan="4" align="left" style="padding-left:6px" class="02">
					<?=$co_business_no?>
					</td>
				  </tr>
				  <tr> 
					<td height="1" colspan="5" bgcolor="#<?=$lColor?>"></td>
				  </tr>
				  <tr> 
					<td width="53" height="28" align="center" class="01" style="border-right:1px #<?=$lColor?> solid;">상<font color="ffffff">___</font>호<br>
					  (법인명) </td>
					<td width="100" class="02" style="padding-left:6px;padding-right:6px"><?=$co_name?>
				    </td>
					<td width="26" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">성명</td>
					<td align="right" class="02" style="padding-right:10px"><?=$co_ceo?></td>
					<td width="20"><img src="<?//=$g4[path]?>/images/in<?//if($i==1) echo "2";?>.gif" width="12" height="14"></td>
				  </tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td width="54" height="28" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">사 
					  업 장<br>
					  소 재 지 </td>
					<td colspan="3" style="border-right:1px #<?=$lColor?> solid;padding-left:6px;padding-right:6px" class="02"><?=$co_address?> <?//=$co[co_address2]?></td>
				  </tr>
				  <tr> 
					<td height="1" colspan="4" bgcolor="#<?=$lColor?>"></td>
				  </tr>
				  <tr> 
					<td height="28" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">업<font color="ffffff">___</font>태</td>
					<td width="90" class="02" style="padding-left:6px;padding-right:6px"><?=$co_business_type1?></td>
					<td width="10"  class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;padding-left:4px">종<br>
					  목 </td>
					<td style="border-right:1px #<?=$lColor?> solid;padding-left:6px;padding-right:6px" class="02"><?=$co_business_type2?></td>
				  </tr>
				</table></td>
			</tr>
		  </table>

		  <!-- 제품list 출력-->
		  <table width="600" border="0" cellpadding="0" cellspacing="1" bgcolor="#<?=$lColor?>">
			<tr bgcolor="ffffff"> 
			  <td colspan="3" align="center" class="01" style="border-top:2px #<?=$lColor?> solid">작<font color="#FFFFFF">___</font>성</td>
			  <td colspan="12" align="center" class="01" style="border-top:2px #<?=$lColor?> solid">공<font color="#FFFFFF">_______</font>급<font color="#FFFFFF">_______</font>가<font color="#FFFFFF">_______</font>액</td>
			  <td width="1" style="border-top:2px #<?=$lColor?> solid"><img src="<?//=$rootpath?>shop/admin/images/<?//=$rootpath?>shop/admin/images/1.gif" width="1" height="1"></td>
			  <td colspan="10" align="center" class="01" style="border-top:2px #<?=$lColor?> solid;border-right:2px #<?=$lColor?> solid">세<font color="#FFFFFF">___________</font>액</td>
			  <td width="64" align="center" class="01">비<font color="#FFFFFF">___</font>고</td>
			</tr>
			<tr bgcolor="ffffff"> 
			  <td width="34" align="center" class="01">년</td>
			  <td width="20" align="center" class="01">월</td>
			  <td width="20" align="center" class="01">일</td>
			  <td width="34" align="center" class="01" style="letter-spacing:-2">공란수</td>
			  <td align="center" class="01">백</td>
			  <td align="center" class="01" style="border-right:2px ">십</td>
			  <td align="center" class="01">억</td>
			  <td align="center" class="01">천</td>
			  <td align="center" class="01" style="border-right:2px ">백</td>
			  <td align="center" class="01">십</td>
			  <td align="center" class="01">만</td>
			  <td align="center" class="01" style="border-right:2px ">천</td>
			  <td align="center" class="01">백</td>
			  <td align="center" class="01">십</td>
			  <td align="center" class="01">일</td>
			  <td><img src="<?//=$rootpath?>shop/admin/images/<?//=$rootpath?>shop/admin/images/1.gif" width="1" height="1"></td>
			  <td align="center" class="01" style="border-right:2px ">십</td>
			  <td align="center" class="01">억</td>
			  <td align="center" class="01">천</td>
			  <td align="center" class="01" style="border-right:2px ">백</td>
			  <td align="center" class="01">십</td>
			  <td align="center" class="01">만</td>
			  <td align="center" class="01" style="border-right:2px ">천</td>
			  <td align="center" class="01">백</td>
			  <td align="center" class="01">십</td>
			  <td align="center" class="01" style="border-right:2px #<?=$lColor?> solid">일</td>
			  <td align="center" class="02">&nbsp;</td>
			</tr>
			<tr bgcolor="ffffff"> 

			<?
			$totamt=round($od_amount,0);
			
			$pricelen=10-strlen($totamt); //-- 전체금액
			$stax=round($totamt/11);
			$mtax=$totamt-$stax; //-- 공급가
			$taxprices="";
			$staxprices="";

			for($ii=0;$ii<strlen($mtax);++$ii)
				$taxprices.='<td align="center" style="border-bottom:2px #'.$lColor.' solid">'.substr($mtax,$ii,1)."</TD>";
			//------------------------
			$xi="";
			for($ii=0;$ii<strlen($stax);++$ii){
				if($ii==strlen($stax)-1) $xi= ';border-right: 2px #'.$lColor.' solid"';
				$staxprices.='<td align="center" style="border-bottom:2px #'.$lColor.' solid'.$xi.';">'.substr($stax,$ii,1)."</TD>";
				$xi="";
			}
			$spricelen=10-strlen($stax);
			?>

			  <td height="34" style="padding-left:6px;border-bottom:2px #<?=$lColor?> solid" class="01"><?=$year?></td>
			  <td align="center" style="border-bottom:2px #<?=$lColor?> solid"><?=$month?></td>
			  <td align="center" style="border-bottom:2px #<?=$lColor?> solid"><?=$day?></td>
			  <td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;<?=$pricelen+1?></td>
			  <td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;</td>
			  <!--td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;</td-->
			  <?for($ixx=0;$ixx<$pricelen;++$ixx){?><td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;</td><?}?>
			  <?=$taxprices?>
			  <td style="border-bottom:2px #<?=$lColor?> solid"><img src="<?//=$rootpath?>shop/admin/images/<?//=$rootpath?>shop/admin/images/1.gif" width="1" height="1"></td>
			  <?for($ioo=0;$ioo<$spricelen;++$ioo){?><td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;</td><?}?><?=$staxprices?>

			  <td align="center" class="02"></td>
			</tr>
		  </table>
		  <table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td width="20" height="24" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid">월</td>
			  <td width="20" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">일</td>
			  <td width="128" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">품<font color="ffffff">________</font>목</td>
			  <td width="52" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">규<font color="ffffff">__</font>격</td>
			  <td width="52" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">수<font color="ffffff">__</font>량</td>
			  <td width="72" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">단<font color="ffffff">__</font>가</td>
			  <td width="120" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">공<font color="ffffff">__</font>급<font color="ffffff">__</font>가<font color="ffffff">__</font>액</td>
			  <td width="80" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">세<font color="ffffff">__</font>액</td>
			  <td align="center" class="01" style="border-right:1px #<?=$lColor?> solid">비<font color="ffffff">__</font>고</td>
			</tr>
		  </table>
		  <table width="600" border="0" cellpadding="0" cellspacing="1" bgcolor="#<?=$lColor?>">
			
			
			<!-- 주문list -->
			<?
				$totprice=0;
				$totpoint=0;
				$taxtotprice=0;
				$kk=1;

				$tot_qty = 0;
				$tot_amount = 0;
				$tot_rtax = 0;
?>

			<tr bgcolor="ffffff"> 
			  <td width="20" height="25" align="center" class="02"><?=$month?></td>
			  <td width="20" align="center" class="02"><?=$day?></td>
			  <td width="116" class="02" style="padding-left:6px;padding-right:6px;word-break:break-all"><?=$product_name?><!--<?//=kSubstr($getGoods['ordgoodinfo'],0,34).".."?>--></td>
			  <td width="52" align="center" class="02">&nbsp;</td>
			  <!--
			  <td width="52" align="center" class="02"><?=$tot_qty?></td>
			  -->
			  <td width="52" align="center" class="02">1</td>
			  <!--
			  <td width="66" align="right" class="02" style="padding-right:6px"><?=number_format($totamt)?>원</td>
			  -->
			  <td width="66" align="right" class="02" style="padding-right:6px"><?=number_format($mtax)?></td>
			  <td width="114" align="right" class="02" style="padding-right:6px"><?=number_format($mtax)?></td>
			  <td width="74" align="right" class="02" style="padding-right:6px"><?=number_format($stax)?></td>
			  <td align="center" class="02">&nbsp;</td>
			</tr>
			<tr bgcolor="ffffff"> 
			  <td width="20" height="25" align="center" class="02"></td>
			  <td width="20" align="center" class="02"></td>
			  <td width="116" class="02" style="padding-left:6px;padding-right:6px;word-break:break-all"></td>
			  <td width="52" align="center" class="02">&nbsp;</td>
			  <td width="52" align="center" class="02"></td>
			  <td width="66" align="right" class="02" style="padding-right:6px"></td>
			  <td width="114" align="right" class="02" style="padding-right:6px"></td>
			  <td width="74" align="right" class="02" style="padding-right:6px"></td>
			  <td align="center" class="02">&nbsp;</td>
			</tr>
			<tr bgcolor="ffffff"> 
			  <td width="20" height="25" align="center" class="02"></td>
			  <td width="20" align="center" class="02"></td>
			  <td width="116" class="02" style="padding-left:6px;padding-right:6px;word-break:break-all"></td>
			  <td width="52" align="center" class="02">&nbsp;</td>
			  <td width="52" align="center" class="02"></td>
			  <td width="66" align="right" class="02" style="padding-right:6px"></td>
			  <td width="114" align="right" class="02" style="padding-right:6px"></td>
			  <td width="74" align="right" class="02" style="padding-right:6px"></td>
			  <td align="center" class="02">&nbsp;</td>
			</tr>
			




			<!-- 주문list끝 -->
			<?while($kk < 4){?>
			<tr bgcolor="ffffff" class="02"> 
			  <td height="25" align="center" class="02">&nbsp;</td>
			  <td align="center" class="02">&nbsp;</td>
			  <td class="02" style="padding-left:6px">&nbsp;</td>
			  <td align="center" class="02">&nbsp;</td>
			  <td align="center" class="02">&nbsp;</td>
			  <td align="right" class="02" style="padding-right:6px">&nbsp;</td>
			  <td align="right" class="02" style="padding-right:6px">&nbsp;</td>
			  <td align="right" class="02" style="padding-right:6px">&nbsp;</td>
			  <td align="center" class="02">&nbsp;</td>
			</tr><?$kk++;?>
			<?}?>
		  </table>
		  <table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td width="100" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid">합 
				계 금 액</td>
			  <td width="88" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">현<font color="ffffff">____</font>금</td>
			  <td width="88" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">수<font color="ffffff">____</font>표</td>
			  <td width="88" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">어<font color="ffffff">____</font>음</td>
			  <td width="88" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">외상미수금</td>
			  <td rowspan="3" align="center" style="border-right:1px #<?=$lColor?> solid"> 
				<table border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td align="right" class="01">위 금액을</td>
					<td width="32" align="center" class="01">영수<br>
				      <strong>청구</strong> </td>
					<td class="01">함.</td>
				  </tr>
				</table></td>
			</tr>
			<tr> 
			  <td height="1" colspan="5" bgcolor="#<?=$lColor?>"></td>
			</tr>
			<tr> 
			  <td height="25" align="center" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid" class="02"><?=number_format($totamt)?></td>
			  <td align="center" style="border-right:1px #<?=$lColor?> solid" class="02">&nbsp;</td>
			  <td align="center" style="border-right:1px #<?=$lColor?> solid" class="02">&nbsp;</td>
			  <td align="center" style="border-right:1px #<?=$lColor?> solid" class="02">&nbsp;</td>
			  <td align="center" style="border-right:1px #<?=$lColor?> solid" class="02">&nbsp;</td>
			</tr>
			<tr> 
			  <td height="1" colspan="6" bgcolor="#<?=$lColor?>"></td>
			</tr>
		  </table></td>
	  </tr>
	</table>
<? } // if($pmode=="A" || $pmode == "S")  ?>


<?
$height = 949;
$stamp_top1 = 77; //$i * $height  + 77;
$stamp_top2 = 550; //$i * $height   + 550;
?>
<? if($pmode=="A" || $pmode == "S") { ?>
	<div id=layer style="position:absolute; left:248px; top:<?=$stamp_top1?>px; width:53px; height:45px; z-index:1"><!--img src="<?=$stamp?>" width="75" height="74" /--></div>
<? } // if($pmode=="A" || $pmode == "S")  ?>
<? if($pmode=="A" || $pmode == "C") { ?>
	<div id=floater style="position:absolute; left:247px; top:<?=$stamp_top2?>px; width:53px; height:45px; z-index:1"><!--img src="<?=$stamp?>" width="75" height="74" /--></div>
<? } // if($pmode=="A" || $pmode == "C")  ?>
	<hr style="border:1px dotted #d9d9d9; width:500;align:left">

	<?
	//	++$i;
//	} //-- endwhile

	//--

?>




















<?
$lColor="FF4633";


?>




<? if($pmode=="A" || $pmode == "S") { ?>
	<!-- 공급자용 -->
	<table width="600" border="0" cellpadding="0" cellspacing="0" style="border:2px #<?=$lColor?> solid">
	  <tr>
		<td><table width="600" border="0" cellspacing="0" cellpadding="0" style="border-left:1px #<?=$lColor?> solid;border-top:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid">
			<tr> 
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td width="50%" align="right" style="font:18pt 굴림;color:#<?=$lColor?>"><strong>세 
					  금 계 산 서</strong></td>
					<td width="50%" class="01" style="padding-left:6px">(공급자보관용) 
<? 

if($p1_print>0)  echo "  <font color=red><b>재발행</b></font>"; ?>
					</td>
				  </tr>
				</table></td>
			  <td width="60" style="border-right:3px #<?=$lColor?> solid"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td height="28" align="center" class="01">책<font color="ffffff">_</font>번<font color="ffffff">_</font>호</td>
				  </tr>
				  <tr> 
					<td height="24" align="center" class="01">일련번호</td>
				  </tr>
				</table></td>
			  <td width="150"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td height="28" colspan="3" align="right" style="border-right:1px #<?=$lColor?> solid;padding-right:4px" class="01"><font color="333333"></font> 
					  권</td>
					<td colspan="4" align="right" class="01" style="padding-right:4px"> 
					  <font color="333333"> </font>호</td>
				  </tr>
				  <tr> 
					<td height="1" colspan="7" bgcolor="#<?=$lColor?>"></td>
				  </tr>
				  <tr> 
					<td width="19" height="24" align="center" style="border-right:1px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center" style="border-right:3px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center" style="border-right:3px #<?=$lColor?> solid;" class="02"><font color="#<?=$lColor?>">-</font></td>
					<td width="19" align="center" style="border-right:1px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center" style="border-right:1px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center" style="border-right:1px #<?=$lColor?> solid" class="02"></td>
					<td width="19" align="center"></td>
				  </tr>
				</table></td>
			</tr>
		  </table>
		  <table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			<!-- 공급자 -->
			  <td width="15" align="center" class="01" style="line-height:36px;border-left:1px #<?=$lColor?> solid;border-top:1px #<?=$lColor?> solid;padding-left:2px">공<br>
				급<br>
				자 </td>
			  <td width="47%" valign="top" background="<?//=$rootpath?>shop/admin/images/tax_ingam.gif"> <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:3px #<?=$lColor?> solid">
				  <tr> 
					<td width="53" height="24" align="center" class="01" style="border-right:1px #<?=$lColor?> solid;">등록번호</td>
					<td colspan="4" style="padding-left:6px" class="02"><?=$sup_business_no?></td>
				  </tr>
				  <tr> 
					<td height="1" colspan="5" bgcolor="#<?=$lColor?>"></td>
				  </tr>

				  <tr> 
					<td height="28" align="center" class="01" style="border-right:1px #<?=$lColor?> solid;">상<font color="ffffff">___</font>호<br>
					  (법인명) </td>
					<td width="100" class="02" style="padding-left:6px;padding-right:6px"><?=$sup_company_name?></td>
					<td width="26" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">성명</td>
					<td align="right" class="02" style="padding-right:10px"><?=$sup_company_ceo?></td>
					<td width="20"><!--<img src="<?=$rootpath?>shop/admin/images/<?=$rootpath?>shop/admin/images/in<?if($i==1) echo "2";?>.gif" width="12" height="14">--></td>
				  </tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td width="54" height="28" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">사 
					  업 장<br>
					  소 재 지 </td>
					<td colspan="3" style="border-right:1px #<?=$lColor?> solid;padding-left:6px;padding-right:6px" class="02"><?=$sup_company_addr?></td>
				  </tr>
				  <tr> 
					<td height="1" colspan="4" bgcolor="#<?=$lColor?>"></td>
				  </tr>
				  <tr> 
					<td height="28" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">업<font color="ffffff">___</font>태</td>
					<td width="90" class="02" style="padding-left:6px;padding-right:6px"><?=$sup_business_type1?></td>
					<td width="10"  class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;padding-left:4px">종<br>
					  목 </td>
					<td style="border-right:1px #<?=$lColor?> solid;padding-left:6px;padding-right:6px" class="02"><?=$sup_business_type2?></td>
				  </tr>
				</table></td>
				<!-- 공급받는자 -->
			  <td width="15" align="center" class="01" style="line-height:22px;border-top:1px #364F9E solid;border-left:1px #<?=$lColor?> solid;padding-left:2px">공<br>
				급<br>
				받<br>
				는<br>
				자 </td>
			  <td width="47%"> <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left:3px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;border-top:3px #<?=$lColor?> solid;border-bottom:3px #<?=$lColor?> solid;">
				  <tr> 
					<td height="24" align="center" class="01" style="border-right:1px #<?=$lColor?> solid;">등록번호</td>
					<td colspan="4" align="left" style="padding-left:6px" class="02">
					<?=$co_business_no?>
					</td>
				  </tr>
				  <tr> 
					<td height="1" colspan="5" bgcolor="#<?=$lColor?>"></td>
				  </tr>
				  <tr> 
					<td width="53" height="28" align="center" class="01" style="border-right:1px #<?=$lColor?> solid;">상<font color="ffffff">___</font>호<br>
					  (법인명) </td>
					<td width="100" class="02" style="padding-left:6px;padding-right:6px"><?=$co_name?>
				    </td>
					<td width="26" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">성명</td>
					<td align="right" class="02" style="padding-right:10px"><?=$co_ceo?></td>
					<td width="20"><img src="<?//=$g4[path]?>/images/in<?//if($i==1) echo "2";?>.gif" width="12" height="14"></td>
				  </tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td width="54" height="28" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">사 
					  업 장<br>
					  소 재 지 </td>
					<td colspan="3" style="border-right:1px #<?=$lColor?> solid;padding-left:6px;padding-right:6px" class="02"><?=$co_address?> <?//=$co[co_address2]?></td>
				  </tr>
				  <tr> 
					<td height="1" colspan="4" bgcolor="#<?=$lColor?>"></td>
				  </tr>
				  <tr> 
					<td height="28" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;">업<font color="ffffff">___</font>태</td>
					<td width="90" class="02" style="padding-left:6px;padding-right:6px"><?=$co_business_type1?></td>
					<td width="10"  class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid;padding-left:4px">종<br>
					  목 </td>
					<td style="border-right:1px #<?=$lColor?> solid;padding-left:6px;padding-right:6px" class="02"><?=$co_business_type2?></td>
				  </tr>
				</table></td>
			</tr>
		  </table>

		  <!-- 제품list 출력-->
		  <table width="600" border="0" cellpadding="0" cellspacing="1" bgcolor="#<?=$lColor?>">
			<tr bgcolor="ffffff"> 
			  <td colspan="3" align="center" class="01" style="border-top:2px #<?=$lColor?> solid">작<font color="#FFFFFF">___</font>성</td>
			  <td colspan="12" align="center" class="01" style="border-top:2px #<?=$lColor?> solid">공<font color="#FFFFFF">_______</font>급<font color="#FFFFFF">_______</font>가<font color="#FFFFFF">_______</font>액</td>
			  <td width="1" style="border-top:2px #<?=$lColor?> solid"><img src="<?//=$rootpath?>shop/admin/images/<?//=$rootpath?>shop/admin/images/1.gif" width="1" height="1"></td>
			  <td colspan="10" align="center" class="01" style="border-top:2px #<?=$lColor?> solid;border-right:2px #<?=$lColor?> solid">세<font color="#FFFFFF">___________</font>액</td>
			  <td width="64" align="center" class="01">비<font color="#FFFFFF">___</font>고</td>
			</tr>
			<tr bgcolor="ffffff"> 
			  <td width="34" align="center" class="01">년</td>
			  <td width="20" align="center" class="01">월</td>
			  <td width="20" align="center" class="01">일</td>
			  <td width="34" align="center" class="01" style="letter-spacing:-2">공란수</td>
			  <td align="center" class="01">백</td>
			  <td align="center" class="01" style="border-right:2px ">십</td>
			  <td align="center" class="01">억</td>
			  <td align="center" class="01">천</td>
			  <td align="center" class="01" style="border-right:2px ">백</td>
			  <td align="center" class="01">십</td>
			  <td align="center" class="01">만</td>
			  <td align="center" class="01" style="border-right:2px ">천</td>
			  <td align="center" class="01">백</td>
			  <td align="center" class="01">십</td>
			  <td align="center" class="01">일</td>
			  <td><img src="<?//=$rootpath?>shop/admin/images/<?//=$rootpath?>shop/admin/images/1.gif" width="1" height="1"></td>
			  <td align="center" class="01" style="border-right:2px ">십</td>
			  <td align="center" class="01">억</td>
			  <td align="center" class="01">천</td>
			  <td align="center" class="01" style="border-right:2px ">백</td>
			  <td align="center" class="01">십</td>
			  <td align="center" class="01">만</td>
			  <td align="center" class="01" style="border-right:2px ">천</td>
			  <td align="center" class="01">백</td>
			  <td align="center" class="01">십</td>
			  <td align="center" class="01" style="border-right:2px #<?=$lColor?> solid">일</td>
			  <td align="center" class="02">&nbsp;</td>
			</tr>
			<tr bgcolor="ffffff"> 

			<?
			//$totamt=$getOrdno[0];
			$totamt=round($od_amount,0);
			
			$pricelen=10-strlen($totamt); //-- 전체금액
			$stax=round($totamt/11);
			$mtax=$totamt-$stax; //-- 공급가
			$taxprices="";
			$staxprices="";

			for($ii=0;$ii<strlen($mtax);++$ii)
				$taxprices.='<td align="center" style="border-bottom:2px #'.$lColor.' solid">'.substr($mtax,$ii,1)."</TD>";
			//------------------------
			$xi="";
			for($ii=0;$ii<strlen($stax);++$ii){
				if($ii==strlen($stax)-1) $xi= ';border-right: 2px #'.$lColor.' solid"';
				$staxprices.='<td align="center" style="border-bottom:2px #'.$lColor.' solid'.$xi.';">'.substr($stax,$ii,1)."</TD>";
				$xi="";
			}
			$spricelen=10-strlen($stax);
			?>

			  <td height="34" style="padding-left:6px;border-bottom:2px #<?=$lColor?> solid" class="01"><?=$year?></td>
			  <td align="center" style="border-bottom:2px #<?=$lColor?> solid"><?=$month?></td>
			  <td align="center" style="border-bottom:2px #<?=$lColor?> solid"><?=$day?></td>
			  <td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;<?=$pricelen+1?></td>
			  <td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;</td>
			  <!--td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;</td-->
			  <?for($ixx=0;$ixx<$pricelen;++$ixx){?><td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;</td><?}?>
			  <?=$taxprices?>
			  <td style="border-bottom:2px #<?=$lColor?> solid"><img src="<?//=$rootpath?>shop/admin/images/<?//=$rootpath?>shop/admin/images/1.gif" width="1" height="1"></td>
			  <?for($ioo=0;$ioo<$spricelen;++$ioo){?><td align="center" style="border-bottom:2px #<?=$lColor?> solid">&nbsp;</td><?}?><?=$staxprices?>

			  <td align="center" class="02"></td>
			</tr>
		  </table>
		  <table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td width="20" height="24" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid">월</td>
			  <td width="20" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">일</td>
			  <td width="128" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">품<font color="ffffff">________</font>목</td>
			  <td width="52" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">규<font color="ffffff">__</font>격</td>
			  <td width="52" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">수<font color="ffffff">__</font>량</td>
			  <td width="72" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">단<font color="ffffff">__</font>가</td>
			  <td width="120" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">공<font color="ffffff">__</font>급<font color="ffffff">__</font>가<font color="ffffff">__</font>액</td>
			  <td width="80" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">세<font color="ffffff">__</font>액</td>
			  <td align="center" class="01" style="border-right:1px #<?=$lColor?> solid">비<font color="ffffff">__</font>고</td>
			</tr>
		  </table>
		  <table width="600" border="0" cellpadding="0" cellspacing="1" bgcolor="#<?=$lColor?>">
			
			
			<!-- 주문list -->
			<?
				$totprice=0;
				$totpoint=0;
				$taxtotprice=0;
				$kk=1;

				$tot_qty = 0;
				$tot_amount = 0;
				$tot_rtax = 0;
				//$year=substr($getPdccorpq['od_time'],5,2);
				//$mon=substr($getPdccorpq['od_time'],8,2);
/*			while($getGoods=moveArray($getOrderinfq)){	
				
				$totprice += ((double)$getGoods[goodprice] * (int)$getGoods[orderamt]);
				$itemprice = ((double)$getGoods[goodprice] * (int)$getGoods[orderamt]);

				taxCal($itemprice,$rdPrice,$rtax);
				
				if($kk > 3){
					$Etc_goodprice = $Etc_goodprice + $getGoods['goodprice'];
					$Etc_rdPrice = $Etc_rdPrice + $rdPrice;
					$Etc_rtax = $Etc_rtax + $rtax;
					
					if($taxGoodsnum == $kk){
			?>
			<!--tr bgcolor="ffffff"> 
			  <td width="20" height="25" align="center" class="02"><?=$year?></td>
			  <td width="20" align="center" class="02"><?=$mon?></td>
			  <td width="116" class="02" style="padding-left:6px;padding-right:6px;word-break:break-all">컴퓨터및주변기기--><!--<?=kSubstr($getGoods['ordgoodinfo'],0,34).".."?> <?if($taxGoodsnum>4){?>외 <?=number_format($taxGoodsnum -4)?><?}?>--><!--/td>
			  <td width="52" align="center" class="02">&nbsp;</td>
			  <td width="52" align="center" class="02"><?=$getGoods['orderamt']?></td>
			  <td width="66" align="right" class="02" style="padding-right:6px"><?=number_format($Etc_goodprice)?>원</td>
			  <td width="114" align="right" class="02" style="padding-right:6px"><?=number_format($Etc_rdPrice)?>원</td>
			  <td width="74" align="right" class="02" style="padding-right:6px"><?=number_format($Etc_rtax)?>원</td>
			  <td align="center" class="02">&nbsp;</td>
			</tr-->
			<?/*		
						$tot_rtax += $Etc_rtax;
						$tot_qty += $getGoods['orderamt'];
						$tot_amount += $Etc_rdPrice;
					}
				}else{
			*/?>
			<!--tr bgcolor="ffffff"> 
			  <td width="20" height="25" align="center" class="02"><?=$year?></td>
			  <td width="20" align="center" class="02"><?=$mon?></td>
			  <td width="116" class="02" style="padding-left:6px;padding-right:6px;word-break:break-all">컴퓨터및주변기기--><!--<?//=kSubstr($getGoods['ordgoodinfo'],0,34).".."?>--><!--/td>
			  <td width="52" align="center" class="02">&nbsp;</td>
			  <td width="52" align="center" class="02"><?=$getGoods['orderamt']?></td>
			  <td width="66" align="right" class="02" style="padding-right:6px"><?=number_format($getGoods['goodprice'])?>원</td>
			  <td width="114" align="right" class="02" style="padding-right:6px"><?=number_format($rdPrice)?>원</td>
			  <td width="74" align="right" class="02" style="padding-right:6px"><?=number_format($rtax)?>원</td>
			  <td align="center" class="02">&nbsp;</td>
			</tr-->
			<?
			/*		$tot_rtax += $rtax;
					$tot_qty += $getGoods['orderamt'];
					$tot_amount += $rdPrice;
				}

				$kk++;
			}
			
			*/?>

			<tr bgcolor="ffffff"> 
			  <td width="20" height="25" align="center" class="02"><?=$month?></td>
			  <td width="20" align="center" class="02"><?=$day?></td>
			  <td width="116" class="02" style="padding-left:6px;padding-right:6px;word-break:break-all"><?=$product_name?><!--<?//=kSubstr($getGoods['ordgoodinfo'],0,34).".."?>--></td>
			  <td width="52" align="center" class="02">&nbsp;</td>
			  <!--
			  <td width="52" align="center" class="02"><?=$tot_qty?></td>
			  -->
			  <td width="52" align="center" class="02">1</td>
			  <!--
			  <td width="66" align="right" class="02" style="padding-right:6px"><?=number_format($totamt)?>원</td>
			  -->
			  <td width="66" align="right" class="02" style="padding-right:6px"><?=number_format($mtax)?></td>
			  <td width="114" align="right" class="02" style="padding-right:6px"><?=number_format($mtax)?></td>
			  <td width="74" align="right" class="02" style="padding-right:6px"><?=number_format($stax)?></td>
			  <td align="center" class="02">&nbsp;</td>
			</tr>
			<tr bgcolor="ffffff"> 
			  <td width="20" height="25" align="center" class="02"></td>
			  <td width="20" align="center" class="02"></td>
			  <td width="116" class="02" style="padding-left:6px;padding-right:6px;word-break:break-all"></td>
			  <td width="52" align="center" class="02">&nbsp;</td>
			  <td width="52" align="center" class="02"></td>
			  <td width="66" align="right" class="02" style="padding-right:6px"></td>
			  <td width="114" align="right" class="02" style="padding-right:6px"></td>
			  <td width="74" align="right" class="02" style="padding-right:6px"></td>
			  <td align="center" class="02">&nbsp;</td>
			</tr>
			<tr bgcolor="ffffff"> 
			  <td width="20" height="25" align="center" class="02"></td>
			  <td width="20" align="center" class="02"></td>
			  <td width="116" class="02" style="padding-left:6px;padding-right:6px;word-break:break-all"></td>
			  <td width="52" align="center" class="02">&nbsp;</td>
			  <td width="52" align="center" class="02"></td>
			  <td width="66" align="right" class="02" style="padding-right:6px"></td>
			  <td width="114" align="right" class="02" style="padding-right:6px"></td>
			  <td width="74" align="right" class="02" style="padding-right:6px"></td>
			  <td align="center" class="02">&nbsp;</td>
			</tr>
			




			<!-- 주문list끝 -->
			<?while($kk < 4){?>
			<tr bgcolor="ffffff" class="02"> 
			  <td height="25" align="center" class="02">&nbsp;</td>
			  <td align="center" class="02">&nbsp;</td>
			  <td class="02" style="padding-left:6px">&nbsp;</td>
			  <td align="center" class="02">&nbsp;</td>
			  <td align="center" class="02">&nbsp;</td>
			  <td align="right" class="02" style="padding-right:6px">&nbsp;</td>
			  <td align="right" class="02" style="padding-right:6px">&nbsp;</td>
			  <td align="right" class="02" style="padding-right:6px">&nbsp;</td>
			  <td align="center" class="02">&nbsp;</td>
			</tr><?$kk++;?>
			<?}?>
		  </table>
		  <table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td width="100" align="center" class="01" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid">합 
				계 금 액</td>
			  <td width="88" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">현<font color="ffffff">____</font>금</td>
			  <td width="88" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">수<font color="ffffff">____</font>표</td>
			  <td width="88" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">어<font color="ffffff">____</font>음</td>
			  <td width="88" align="center" class="01" style="border-right:1px #<?=$lColor?> solid">외상미수금</td>
			  <td rowspan="3" align="center" style="border-right:1px #<?=$lColor?> solid"> 
				<table border="0" cellspacing="0" cellpadding="0">
				  <tr> 
					<td align="right" class="01">위 금액을</td>
					<td width="32" align="center" class="01">영수<br>
				      <strong>청구</strong> </td>
					<td class="01">함.</td>
				  </tr>
				</table></td>
			</tr>
			<tr> 
			  <td height="1" colspan="5" bgcolor="#<?=$lColor?>"></td>
			</tr>
			<tr> 
			  <td height="25" align="center" style="border-left:1px #<?=$lColor?> solid;border-right:1px #<?=$lColor?> solid" class="02"><?=number_format($totamt)?>원</td>
			  <td align="center" style="border-right:1px #<?=$lColor?> solid" class="02">&nbsp;</td>
			  <td align="center" style="border-right:1px #<?=$lColor?> solid" class="02">&nbsp;</td>
			  <td align="center" style="border-right:1px #<?=$lColor?> solid" class="02">&nbsp;</td>
			  <td align="center" style="border-right:1px #<?=$lColor?> solid" class="02">&nbsp;</td>
			</tr>
			<tr> 
			  <td height="1" colspan="6" bgcolor="#<?=$lColor?>"></td>
			</tr>
		  </table></td>
	  </tr>
	</table>
<? } // if($pmode=="A" || $pmode == "C")  ?>
	
	