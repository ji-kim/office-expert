<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= $title ?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">

        <form id="form_validation"
              action="<?php echo base_url() ?>admin/logis/update_taxinvoice/<?= $ct_id ?>/<?= $df_month ?>/<?= $tax_invoice_id ?>"
              method="post" class="form-horizontal form-groups-bordered">

              <div class="form-group" id="border-none">
                  <label for="field-1" class="col-sm-4 control-label">공급받는자 </label>
                  <div class="col-sm-5" style="text-align:center;">
                    <select name="p_co_id" id="p_co_id" class="form-control input-sm" style="width:100%;">
          					<?php
          					// 무슨소리인이 이해불가 일단 주석처리
          					if (!empty($all_group_list)) {
          						foreach ($all_group_list as $group_info) {
          							?>
          							<option value="<?=(!empty($group_info->dp_id))?$group_info->dp_id:""?>" <?= ($group_info->dp_id==$tax_invoice_info->supp_co_id) ? " selected" : "" ?>><?=(!empty($group_info->co_name))?$group_info->co_name:""?></option>
          							<?php
          						}
          					}
                    ?>
          					</select>
                  </div>
              </div>
              <div class="form-group" id="border-none">
                  <label for="field-1" class="col-sm-4 control-label">발행일 </label>
                  <div class="col-sm-5" style="text-align:center;">
                    <div class="input-group">
                        <input type="text" value="<?php
                        if (!empty($tax_invoice_info->tax_date)) {
                            echo $tax_invoice_info->tax_date;
                        }
                        ?>" class="form-control datepicker" name="tax_date" id="tax_date"
                               data-format="yyyy/mm/dd" style="width:100%;">

                        <div class="input-group-addon">
                            <a href="#"><i class="fa fa-calendar"></i></a>
                        </div>
                    </div>
                  </div>
              </div>




            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"> 저 장 </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
            </div>
        </form>
    </div>
</div>
