<?php
$lastday = array(0,31,28,31,30,31,30,31,31,30,31,30,31);

// 계약정보
$ct = $this->db->where('idx', $ct_id)->get('tbl_contract')->row();
$cnt = 0;
if (!empty($all_stransaction_ready)) {
	foreach ($all_stransaction_ready as $str_details) {

										$sn = $total_count--;
										$add_sudang_vals = "";
										$add_gongje01_vals = "";
										$add_gongje02_vals = "";
										$add_gongje03_vals = "";

										//우체국
										$dp = $this->db->where('dp_id', $str_details->dp_id)->get('tbl_members')->row();
										$cs = $this->db->where('code', $str_details->co_code)->where('mb_type', 'customer')->get('tbl_members')->row();


										//공제청구사
										$gj_rq_co = "";
										if(!empty($str_details->gongje_req_co)) {
											$gj_rq_co = $this->db->where('dp_id', $str_details->gongje_req_co)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//차량정보
										$truck = $this->db->where('idx', $str_details->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										//위수탁관리비
										$wsm_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_fixmfee')->row();

										if(empty($wsm_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $wsm_info->wst_mfee;
										if(empty($wsm_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $wsm_info->mfee_vat;
										if(empty($wsm_info->org_fee)) $org_fee = 0; else $org_fee = $wsm_info->org_fee;
										if(empty($wsm_info->grg_fee)) $mgrg_fee = 0; else $mgrg_fee = $wsm_info->grg_fee;
										if(empty($wsm_info->grg_fee_vat)) $mgrg_fee_vat = 0; else $mgrg_fee_vat = $wsm_info->grg_fee_vat;
										if(empty($wsm_info->etc)) $etc= 0; else $etc = $wsm_info->etc;
										$org_fee_vat = 0;
										$env_fee = 0;
										$env_fee_vat = 0;
										$car_tax = 0;
										$car_tax_vat = 0;
										$etc = 0;
										$wsm_sum = ($wst_mfee + $mfee_vat + $org_fee + $env_fee + $car_tax + $mgrg_fee + $mgrg_fee_vat + $etc);

										//각종보험
										$insur_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_insur')->row();
										if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
										if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;
										if(empty($insur_info->ins_grnt)) $ins_grnt = 0; else $ins_grnt = $insur_info->ins_grnt;
										$insur_sum = ($ins_car + $ins_load + $ins_grnt);

										//일반공제
										$gongje_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										if(empty($gongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $gongje_info->fine_fee;
										if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;
										if(empty($gongje_info->car_tax)) $car_tax = 0; else $car_tax = $gongje_info->car_tax;
										if(empty($gongje_info->env_fee)) $env_fee = 0; else $env_fee = $gongje_info->env_fee;
										if(empty($gongje_info->grg_fee)) $grg_fee = 0; else $grg_fee = $gongje_info->grg_fee;
										if(empty($gongje_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $gongje_info->grg_fee_vat;
										$gongje_sum = ($fine_fee + $not_paid + $car_tax + $env_fee + $grg_fee + $grg_fee_vat);

										//환급형공제
										$rf_gongje_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_refund_gongje')->row();
										if(empty($rf_gongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rf_gongje_info->gj_termination_mortgage;
										$rf_gongje_sum = ($gj_termination_mortgage);

										//---------------------------------------------------------------------------------  추가 항목
										$sudang_added = "N";
										$gongje_added = "N";
										if (!empty($all_sudang_group)) {
											foreach ($all_sudang_group as $sudang_info) {
												if($sudang_info->add_type == 'S') { //일반수당
													$sudang_added = "Y";
												} else if($sudang_info->add_type == 'GN') { //일반공제
													$gongje_added = "Y";
												}
											}
										}

										if (!empty($all_gongje_group)) {
											foreach ($all_gongje_group as $gongje_info) {
												$add_item = $this->db->where('df_month', $df_month)->where('dp_id', $str_details->dp_id)->get('tbl_delivery_fee_add')->row();
												if($gongje_info->add_type == 'GW') { //위수탁
													if(!empty($add_item->amount)) {
														$wsm_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje01_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje01_vals .= "<td style='text-align:right;'>*</td>";
												} else if($gongje_info->add_type == 'GN') { //일반
													if(!empty($add_item->amount)) {
														$gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje02_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje02_vals .= "<td style='text-align:right;'>*</td>";

												} else if($gongje_info->add_type == 'GR') { //환급
													if(!empty($add_item->amount)) {
														$rf_gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje03_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje03_vals .= "<td style='text-align:right;'>*</td>";
												}
											}
										}
										//---------------------------------------------------------------------------------  추가 항목

										$tot_gongje = ($wsm_sum + $insur_sum + $gongje_sum + $rf_gongje_sum);

										$lv = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_levy')->row();
										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($lv->pay_status)) {
											if($lv->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($lv->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; } 
											else if($lv->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; } 
										}

										if(empty($str_details->G)) $str_details->G = 0;
										if(empty($str_details->H)) $str_details->H = 0;
										if(empty($str_details->I)) $str_details->I = 0;
										if(empty($str_details->AD)) $str_details->AD = 0;
										// 단가
										$unit_price1 = 0;
										$unit_price2 = 0;
										$unit_price3 = 0;
										$unit_price4 = 0;
										$unit_prices = $this->db->where('dp_id', $str_details->dp_id)->where('ct_id', $str_details->ct_id)->get('tbl_scontract_co')->row();
										if(!empty($unit_prices->val1)) $unit_price1 = $unit_prices->val1;
										if(!empty($unit_prices->val2)) $unit_price2 = $unit_prices->val2;
										if(!empty($unit_prices->val3)) $unit_price3 = $unit_prices->val3;
										if(!empty($unit_prices->val4)) $unit_price4 = $unit_prices->val4;
										//수수료
										$tr_tot1 = $str_details->G * $unit_price1;
										$tr_tot2 = $str_details->H * $unit_price2;
										$tr_tot3 = $str_details->I * $unit_price3;
										$tr_tot4 = $str_details->AD * $unit_price4;
										$tr_sub_total = $tr_tot1 + $tr_tot2 + $tr_tot3 + $tr_tot4;

										$tr_vat = $tr_sub_total / 11;
										$tr_supp = $tr_sub_total - $tr_vat;
										$total_amount = $tr_sub_total - $tot_gongje;


		//기본정보
		$arr_date = explode("-",$tax_date);
		$year = $arr_date[0];
		$month = $arr_date[1];
		if ($year%4 == 0) $lastday[2] = 29;
		$month_df = (int)($month);
		$day = $lastday[$month_df];

		//공급받는자
		if(!empty($str_details->gongje_req_co)) {
			//$psupp = $this->db->where('dp_id', $str_details->gongje_req_co)->get('tbl_members')->row();
			$psupp = $this->db->where('dp_id', '1413')->get('tbl_members')->row();

			$co_business_no = $psupp->bs_number; //사업자등록번호
			$co_name = $psupp->co_name; //상호
			$co_ceo = $psupp->ceo; //상호
			$co_address = $psupp->co_address; //소재지
			$co_business_type1 = $psupp->bs_type1; //업종
			$co_business_type2 = $psupp->bs_type2; //업태
		}
		
		//공급자 $dp
		$sup_business_no = $dp->bs_number; //사업자등록번호
		$sup_company_name = $dp->co_name; //상호
		$sup_company_ceo = $dp->ceo; //대표
		$sup_company_addr = $dp->co_address; //소재지
		$sup_business_type1 = $dp->bs_type1; //업종
		$sup_business_type2 = $dp->bs_type2; //업태

		//상품정보
		$product_name = "위탁배달수수료";
		$od_amount = $tr_sub_total; //총액

		//$pmode = "A";
		$is_pbreak = "Y";
		$p1_print = 0;

		if($pmode != "A") {
			//echo $cnt."---".($cnt%2)."xxx";
			if($cnt > 0 && $cnt%2==0) $is_pbreak = "Y"; else $is_pbreak = "N";
		}
		
		if(!empty($dp->bs_number) && $dp->bs_number != "") {
			if($is_pbreak == "Y") echo "<p style=\"page-break-after: always;\"></p>";
			echo "<div style='position: relative;'>";
			include "./application/views/admin/logis/_tax.inc.php";
			echo "</div>";
			$cnt++;
		}
	
	}

}
?>