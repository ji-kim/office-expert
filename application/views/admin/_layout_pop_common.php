<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php echo $title; ?></title>
    <?php if (config_item('favicon') != '') : ?>
        <link rel="icon" href="<?php echo base_url() . config_item('favicon'); ?>" type="image/png">
    <?php else: ?>
        <link rel="icon" href="<?php echo base_url('assets/img/favicon.ico'); ?>" type="image/png">
    <?php endif; ?>
    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome/css/font-awesome.min.css">
    <!-- Toastr-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/toastr.min.css">
    <!-- =============== BOOTSTRAP STYLES ===============-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" id="bscss">
    <!-- =============== APP STYLES ===============-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.min.css" id="maincss">
    <!-- Datepicker-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timepicker.min.css">

    <!-- JQUERY-->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/dist/jquery.min.js"></script>

    <?php if (config_item('recaptcha_secret_key') != '' && config_item('recaptcha_site_key') != '') { ?>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php } ?>
    <?php

    $this->load->helper('file');
    $lbg = config_item('login_background');
    if (!empty($lbg)) {
        $login_background = _mime_content_type($lbg);
        $login_background = explode('/', $login_background);
    }
    ?>
    <?php if (!empty($login_background[0]) && $login_background[0] == 'video') { ?>
        <style type="text/css">
            body {
                padding: 0;
                margin: 0;
                background-color: #ffffff;
            }

            .vid-container {
                position: relative;
                height: 100vh;
                overflow: hidden;
            }

            .bgvid.back {
                position: fixed;
                right: 0;
                bottom: 0;
                min-width: 100%;
                min-height: 100%;
                width: auto;
                height: auto;
                z-index: -100;
            }

            .inner {
                position: absolute;
            }

            .inner-container {
                width: 400px;
                height: 400px;
                position: absolute;
                top: calc(50vh - 200px);
                left: calc(50vw - 200px);
                overflow: hidden;
            }

            .bgvid.inner {
                width: 100%;
                /*border: 10px solid red;*/
                min-height: 100%;
                height: auto;
            }

            .box {
                position: absolute;
                height: 100%;
                width: 100%;
                font-family: Helvetica;
                color: #fff;
                background: rgba(0, 0, 0, 0.13);
                padding: 30px 0px;
            }

            .box h1 {
                text-align: center;
                margin: 30px 0;
                font-size: 30px;
            }

            .panel {
                background: transparent;

            }

            input {
                background: rgba(0, 0, 0, 0.2);
                color: #fff;
                border: 0;
            }

            input:focus, input:active, button:focus, button:active {
                outline: none;
            }

            .box button:active {
                background: #27ae60;
            }

            .box p {
                font-size: 14px;
                text-align: center;
            }

            .box p span {
                cursor: pointer;
                color: #666;
            }
        </style>
    <?php } ?>
</head>
<?php
$login_position = config_item('login_position');
if (!empty($login_background[0]) && $login_background[0] == 'image') {
    $login_background = config_item('login_background');
    if (!empty($login_background)) {
        $back_img = base_url() . '/' . config_item('login_background');
    }
} ?>
<style>
    body {
        background-color: #ffffff;
    }

    .left-login {
        height: auto;
        min-height: 100%;
        background: #fff;
        -webkit-box-shadow: 2px 0px 7px 1px rgba(0, 0, 0, 0.08);
        -moz-box-shadow: 2px 0px 7px 1px rgba(0, 0, 0, 0.08);
        box-shadow: 2px 0px 7px 1px rgba(0, 0, 0, 0.08);
    }

    .left-login-panel {
        -webkit-box-shadow: 0px 0px 28px -9px rgba(0, 0, 0, 0.74);
        -moz-box-shadow: 0px 0px 28px -9px rgba(0, 0, 0, 0.74);
        box-shadow: 0px 0px 28px -9px rgba(0, 0, 0, 0.74);
    }

    .apply_jobs {
        position: absolute;
        z-index: 1;
        right: 0;
        top: 0
    }

    .login-center {
        background: #fff;
        width: 400px;
        margin: 0 auto;
    }

    @media only screen and (max-width: 380px) {
        .login-center {
            width: 320px;
            padding: 10px;
        }

        .wd-xl {
            width: 260px;
        }
    }
</style>
<?php

if (!empty($login_position) && $login_position == 'center') {
    if (!empty($back_img)) {
        $body_style = 'style="background: url(' . $back_img . ') no-repeat center center fixed;
 -webkit-background-size: cover;
 -moz-background-size: cover;
 -o-background-size: cover;
 background-size: cover;min-height: 100%;width:100%"';
    } else {
        $body_style = '';
    }
} else {
    $body_style = '';
}
$type = $this->session->userdata('c_message');
?>
<body <?= $body_style ?>>
<?php if (!empty($login_position) && $login_position == 'left') {
    $lcol = 'col-lg-4 col-sm-6 left-login';
} else if (!empty($login_position) && $login_position == 'right') {
    $lcol = 'col-lg-4 col-sm-6 left-login pull-right';
} else {
    $lcol = 'login-center';
}
    $lcol = 'col-sm-12';
?>
<div class="<?= $lcol ?>">
    <div class="wrapper" style="margin: 0% 0 0 auto">

                    <?php echo $subview ?>

	</div>
</div>


<!-- =============== VENDOR SCRIPTS ===============-->

<!-- MODERNIZR-->
<script src="<?= base_url() ?>assets/plugins/modernizr/modernizr.custom.js"></script>
<!-- BOOTSTRAP-->
<script src="<?= base_url() ?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- STORAGE API-->
<script src="<?= base_url() ?>assets/plugins/jQuery-Storage-API/jquery.storageapi.min.js"></script>
<!-- ANIMO-->
<script src="<?= base_url() ?>assets/plugins/animo.js/animo.min.js"></script>
<?php if (empty($select_2)) { ?>
    <!-- SELECT2-->
    <script src="<?= base_url() ?>assets/plugins/select2/dist/js/select2.min.js"></script>
<?php } ?>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js'); ?>"></script>
<!-- Data Table -->
<?php if (empty($dataTables)) { ?>
    <?php include_once 'assets/plugins/dataTables/js/jquery.dataTables.min.php'; ?>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/dataTables.select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/dataTables/js/dataTables.bootstrapPagination.js"></script>
<?php } ?>
<!-- summernote Editor -->
<script src="<?php echo base_url() ?>assets/plugins/summernote/summernote.min.js"></script>
<?php if (empty($datepicker)) { ?>
    <!-- =============== Date and time picker ===============-->
    <?php include_once 'assets/js/bootstrap-datepicker.php'; ?>
<?php } ?>
</body>

</html>
