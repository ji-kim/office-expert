<div>
  <div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">
      <div class="col-sm-12">
                                <label class="col-lg-12 control-label" style="font-size:18px;">영업비용 지급이력</label>
      </div>

  </div>
</div>

<div class="row">
<div class="table-responsive" style="padding-left:10px;">

<table class="table table-striped" style="width:98%">
  <thead>
  <tr align="center" bgcolor="#e0e7ef">
    <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차수</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대상</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지급예정일</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지급예정액</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실지급일</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지급액</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌정보</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비고</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">작업</td>
  </tr>
        </thead>
        <tbody>
        <?php
        if (!empty($all_payback_info)) {
            foreach ($all_payback_info as $payback_details) {
                ?>
                <tr id="work_details_<?= $payback_details->prj_payback_id ?>">
                    <td align='center'><?php
                        //$id = $this->uri->segment(5);
                        if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                        <form method="post"
                              action="<?= base_url() ?>admin/projects/save_project_payback/<?=$project_id?>/<?php
                              if (!empty($payback_details)) {
                                  echo $payback_details->prj_payback_id;
                              }
                              ?>" class="form-horizontal">
                              <input type="text" name="payback_cnt" class="form-control" value="<?php
                              if (!empty($payback_details->payment_cnt)) {
                                  echo $payback_details->payment_cnt;
                              }
                              ?>"/>
                        <?php } else {
                                      echo $payback_details->payment_cnt;
                        }
                        ?></td>
                    <td align='center'><?php
                        //$id = $this->uri->segment(5);
                        if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                            <input type="text" name="fullname" class="form-control" value="<?php
                            if (!empty($payback_details)) {
                                echo $payback_details->fullname;
                            }
                            ?>"/>
                        <?php } else {
                            echo $payback_details->fullname;
                        }
                        ?></td>
                        <td align='center'><?php
                            //$id = $this->uri->segment(5);
                            if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                                <input type="text" name="plan_date" class="form-control" value="<?php
                                if (!empty($payback_details)) {
                                    echo $payback_details->plan_date;
                                }
                                ?>"/>
                            <?php } else {
                                echo $payback_details->plan_date;
                            }
                            ?></td>
                    <td align='center'><?php
                        //$id = $this->uri->segment(5);
                        if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                            <input type="text" name="plan_amount" class="form-control" value="<?php
                            if (!empty($payback_details)) {
                                echo $payback_details->plan_amount;
                            }
                            ?>"/>
                        <?php } else {
                            echo $payback_details->plan_amount;
                        }
                        ?></td>
                    <td align='center'><?php
                        //$id = $this->uri->segment(5);
                        if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                            <input type="text" name="pay_date" class="form-control" value="<?php
                            if (!empty($payback_details)) {
                                echo $payback_details->pay_date;
                            }
                            ?>"/>
                        <?php } else {
                            echo $payback_details->pay_date;
                        }
                        ?></td>
                        <td align='center'><?php
                            //$id = $this->uri->segment(5);
                            if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                                <input type="text" name="pay_amount" class="form-control" value="<?php
                                if (!empty($payback_details)) {
                                    echo $payback_details->pay_amount;
                                }
                                ?>"/>
                            <?php } else {
                                echo $payback_details->pay_amount;
                            }
                            ?></td>
                        <td align='center'><?php
                            //$id = $this->uri->segment(5);
                            if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                                <input type="text" name="account_info" class="form-control" value="<?php
                                if (!empty($payback_details)) {
                                    echo $payback_details->account_info;
                                }
                                ?>"/>
                            <?php } else {
                                echo $payback_details->account_info;
                            }
                            ?></td>
                            <td align='center'><?php
                                //$id = $this->uri->segment(5);
                                if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                                    <input type="text" name="remark" class="form-control" value="<?php
                                    if (!empty($payback_details)) {
                                        echo $payback_details->remark;
                                    }
                                    ?>"/>
                                <?php } else {
                                    echo $payback_details->remark;
                                }
                                ?></td>
                        <td>
                            <?php
                            //$id = $this->uri->segment(5);
                            if (!empty($id) && $id == $payback_details->prj_payback_id) { ?>
                                <?= btn_update() ?>
                                </form>
                                <?= btn_cancel('admin/projects/payback_history/') ?>
                            <?php } else { ?>
                                <?php //if (!empty($edited)) { ?>
                                    <?= btn_edit('admin/projects/payback_history/'.$project_id.'/edit/' . $payback_details->prj_payback_id) ?>
                                <?php //} ?>
                                <?php //if (!empty($deleted)) { ?>
                                    <?= btn_delete('admin/projects/delete_payback_history/' . $project_id . '/' . $payback_details->prj_payback_id) ?>
                                <?php //}
                            }
                            ?>
                        </td>


                </tr>
                <?php
            }
        }
        ?>
        <?php if (1) { //!empty($created) || !empty($edited)) { ?>
            <form method="post" action="<?= base_url() ?>admin/projects/save_project_payback/<?=$project_id?>/<?php
                if (!empty($wo_info->prj_payback_id)) {
                  echo $wo_info->prj_payback_id;
                }
                ?>"
                  class="form-horizontal" data-parsley-validate="" novalidate="">
                <tr>
                    <td>
            <div class="input-group">
              <input type="text" name="payment_cnt" id="payment_cnt" class="form-control datepicker" placeholder="차수"
                   value="">
            </div>
        </td>
        <td><input type="text" name="fullname" class="form-control"
                   placeholder="대상" ></td>
                    <td><input type="text" name="plan_date" class="form-control"
                               placeholder="결제예정일" ></td>
                    <td><input type="text" name="plan_amount" class="form-control"
                               placeholder="결제예정액" ></td>
                    <td><input type="text" name="pay_date" class="form-control"
                               placeholder="실결제일" ></td>
                    <td><input type="text" name="pay_amount" class="form-control"
                               placeholder="실결제액" ></td>
                   <td><input type="text" name="account_info" class="form-control"
                                          placeholder="지급계좌" ></td>
                                          <td><input type="text" name="remark" class="form-control"
                                                                 placeholder="비고" ></td>
                    <td><?= btn_add() ?></td>
                </tr>
            </form>
        <?php } ?>
        </tbody>
    </table>





</div>
</div>
