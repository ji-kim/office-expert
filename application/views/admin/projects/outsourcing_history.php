<div>
  <div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">
      <div class="col-sm-12">
                                <label class="col-lg-12 control-label" style="font-size:18px;">외주비용 지급이력</label>
      </div>

  </div>
</div>

<div class="row">
<div class="table-responsive" style="padding-left:10px;">

<table class="table table-striped" style="width:98%">
  <thead>
  <tr align="center" bgcolor="#e0e7ef">
    <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차수</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대상</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지급예정일</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지급예정액</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실지급일</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지급액</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌정보</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비고</td>
    <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">작업</td>
  </tr>
        </thead>
        <tbody>
        <?php
        if (!empty($all_outsourcing_info)) {
            foreach ($all_outsourcing_info as $outsourcing_details) {
                ?>
                <tr id="work_details_<?= $outsourcing_details->prj_outsourcing_id ?>">
                    <td align='center'><?php
                        //$id = $this->uri->segment(5);
                        if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                        <form method="post"
                              action="<?= base_url() ?>admin/projects/save_project_outsourcing/<?=$project_id?>/<?php
                              if (!empty($outsourcing_details)) {
                                  echo $outsourcing_details->prj_outsourcing_id;
                              }
                              ?>" class="form-horizontal">
                              <input type="text" name="payment_cnt" class="form-control" value="<?php
                              if (!empty($outsourcing_details->payment_cnt)) {
                                  echo $outsourcing_details->payment_cnt;
                              }
                              ?>"/>
                        <?php } else {
                                      echo $outsourcing_details->payment_cnt;
                        }
                        ?></td>
                    <td align='center'><?php
                        //$id = $this->uri->segment(5);
                        if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                            <input type="text" name="fullname" class="form-control" value="<?php
                            if (!empty($outsourcing_details)) {
                                echo $outsourcing_details->fullname;
                            }
                            ?>"/>
                        <?php } else {
                            echo $outsourcing_details->fullname;
                        }
                        ?></td>
                        <td align='center'><?php
                            //$id = $this->uri->segment(5);
                            if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                                <input type="text" name="plan_date" class="form-control" value="<?php
                                if (!empty($outsourcing_details)) {
                                    echo $outsourcing_details->plan_date;
                                }
                                ?>"/>
                            <?php } else {
                                echo $outsourcing_details->plan_date;
                            }
                            ?></td>
                    <td align='center'><?php
                        //$id = $this->uri->segment(5);
                        if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                            <input type="text" name="plan_amount" class="form-control" value="<?php
                            if (!empty($outsourcing_details)) {
                                echo $outsourcing_details->plan_amount;
                            }
                            ?>"/>
                        <?php } else {
                            echo $outsourcing_details->plan_amount;
                        }
                        ?></td>
                    <td align='center'><?php
                        //$id = $this->uri->segment(5);
                        if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                            <input type="text" name="pay_date" class="form-control" value="<?php
                            if (!empty($outsourcing_details)) {
                                echo $outsourcing_details->pay_date;
                            }
                            ?>"/>
                        <?php } else {
                            echo $outsourcing_details->pay_date;
                        }
                        ?></td>
                        <td align='center'><?php
                            //$id = $this->uri->segment(5);
                            if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                                <input type="text" name="pay_amount" class="form-control" value="<?php
                                if (!empty($outsourcing_details)) {
                                    echo $outsourcing_details->pay_amount;
                                }
                                ?>"/>
                            <?php } else {
                                echo $outsourcing_details->pay_amount;
                            }
                            ?></td>
                        <td align='center'><?php
                            //$id = $this->uri->segment(5);
                            if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                                <input type="text" name="account_info" class="form-control" value="<?php
                                if (!empty($outsourcing_details)) {
                                    echo $outsourcing_details->account_info;
                                }
                                ?>"/>
                            <?php } else {
                                echo $outsourcing_details->account_info;
                            }
                            ?></td>
                            <td align='center'><?php
                                //$id = $this->uri->segment(5);
                                if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                                    <input type="text" name="remark" class="form-control" value="<?php
                                    if (!empty($outsourcing_details)) {
                                        echo $outsourcing_details->remark;
                                    }
                                    ?>"/>
                                <?php } else {
                                    echo $outsourcing_details->remark;
                                }
                                ?></td>
                        <td>
                            <?php
                            //$id = $this->uri->segment(5);
                            if (!empty($id) && $id == $outsourcing_details->prj_outsourcing_id) { ?>
                                <?= btn_update() ?>
                                </form>
                                <?= btn_cancel('admin/projects/outsourcing_history/') ?>
                            <?php } else { ?>
                                <?php //if (!empty($edited)) { ?>
                                    <?= btn_edit('admin/projects/outsourcing_history/'.$project_id.'/edit/' . $outsourcing_details->prj_outsourcing_id) ?>
                                <?php //} ?>
                                <?php //if (!empty($deleted)) { ?>
                                    <?= btn_delete('admin/projects/delete_outsourcing_history/' . $project_id . '/' . $outsourcing_details->prj_outsourcing_id) ?>
                                <?php //}
                            }
                            ?>
                        </td>


                </tr>
                <?php
            }
        }
        ?>
        <?php if (1) { //!empty($created) || !empty($edited)) { ?>
            <form method="post" action="<?= base_url() ?>admin/projects/save_project_outsourcing/<?=$project_id?>/<?php
                if (!empty($wo_info->prj_outsourcing_id)) {
                  echo $wo_info->prj_outsourcing_id;
                }
                ?>"
                  class="form-horizontal" data-parsley-validate="" novalidate="">
                <tr>
                    <td>
            <div class="input-group">
              <input type="text" name="payment_cnt" id="payment_cnt" class="form-control datepicker" placeholder="차수"
                   value="">
            </div>
        </td>
        <td><input type="text" name="fullname" class="form-control"
                   placeholder="대상" ></td>
                    <td><input type="text" name="plan_date" class="form-control"
                               placeholder="결제예정일" ></td>
                    <td><input type="text" name="plan_amount" class="form-control"
                               placeholder="결제예정액" ></td>
                    <td><input type="text" name="pay_date" class="form-control"
                               placeholder="실결제일" ></td>
                    <td><input type="text" name="pay_amount" class="form-control"
                               placeholder="실결제액" ></td>
                   <td><input type="text" name="account_info" class="form-control"
                                          placeholder="지급계좌" ></td>
                                          <td><input type="text" name="remark" class="form-control"
                                                                 placeholder="비고" ></td>
                    <td><?= btn_add() ?></td>
                </tr>
            </form>
        <?php } ?>
        </tbody>
    </table>





</div>
</div>
