<section class="invoiceDetail">
    <!-- [table] 기본공제S -->
    <div class="basicDeduction">
        <p class="Tit"><span>공제</span> 기본공제</p>
        <table class="basicDeductionTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <td class="leftTit">항목</td>
                <td>공급가</td>
                <td>부가세</td>
                <td width="23%">합계</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftTit">위·탁관리비</td>
                <td><?=char_num($mf['AL'])?></td>
                <td><?=char_num($mf['AL_vat'])?></td>
                <td class="B"><?=char_num($mf['AL']+$mf['AL_vat'])?></td>
            </tr>
            <tr>
                <td class="leftTit">협회비</td>
                <td><?=char_num($mf['AM'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['AM'])?></td>
            </tr>
            <tr>
                <td class="leftTit">환경부담금</td>
                <td><?=char_num($mf['AN'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['AN'])?></td>
            </tr>
            <tr>
                <td class="leftTit">자동차세</td>
                <td><?=char_num($mf['car_tax'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['car_tax'])?></td>
            </tr>
            <tr>
                <td class="leftTit">차고지비</td>
                <td><?=char_num($mf['AP'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['AP'])?></td>
            </tr>
            <tr>
                <td class="leftTit">차고지비</td>
                <td><?=char_num($mf['non_paid'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['non_paid'])?></td>
            </tr>
            <tr>
                <td class="leftTit">기 타</td>
                <td><?=char_num($mf['wst_etc'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['wst_etc'])?></td>
            </tr>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="subtotaldetail"><?=char_num($wsm_info['total_amount'])?></td>
                <td class="subtotaldetail"><?=char_num($wsm_info['total_vat'])?></td>
                <td class="B subtotaldetail"><?=char_num($wsm_info['total_price'])?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 기본공제E -->

    <!-- [table] 일반공제S -->
    <div class="generalDeduction">
        <p class="Tit"><span>공제</span> 일반공제</p>
        <table class="generalDeductionTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr><td class="leftTit">항목</td><td>공급가</td><td>부가세</td><td>합계</td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftTit">과오납환수</td>
                <td><?=char_num($mf['back_overs'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['back_overs'])?></td>
            </tr>
            <tr>
                <td class="leftTit">PDA</td>
                <td><?=char_num($mf['BI'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['BI'])?></td>
            </tr>
            <tr>
                <td class="leftTit">민원처리</td>
                <td><?=char_num($mf['BH'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['BH'])?></td>
            </tr>
            <tr>
                <td class="leftTit">과태료</td>
                <td><?=char_num($mf['fine_fee'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['fine_fee'])?></td>
            </tr>
            <tr>
                <td class="leftTit">사고접수비</td>
                <td><?=char_num($mf['BJ'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['BJ'])?></td>
            </tr>
            <tr>
                <td class="leftTit">비사업자원천징수</td>
                <td><?=char_num($mf['nbs_fee'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['nbs_fee'])?></td>
            </tr>
            <tr>
                <td class="leftTit">비사업자부가세환수</td>
                <td><?=char_num($mf['nbs_back_vat'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['nbs_back_vat'])?></td>
            </tr>
            <tr>
                <td class="leftTit">기타</td>
                <td><?=char_num($mf['gongje_etc'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($mf['gongje_etc'])?></td>
            </tr>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="subtotaldetail"><?=char_num($gongje_info['total_amount'])?></td>
                <td class="subtotaldetail"><?=char_num($gongje_info['total_vat'])?></td>
                <td class="B subtotaldetail"><?=char_num($gongje_info['total_price'])?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 일반공제E -->

    <!-- [table] 각종보험공제S -->
    <div class="insuranceExpenseDeduction">
        <p class="Tit"><span>공제</span> 각종보험공제</p>
        <table class="insuranceExpenseDeductionTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr><td width="48%" class="leftTit">항목</td><td>금액</td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftTit">자동차보험</td>
                <td class="B"><?=char_num($mf['ASS'])?></td>
            </tr>
            <tr>
                <td class="leftTit">적재물보험</td>
                <td class="B"><?=char_num($mf['AT'])?></td>
            </tr>
            <tr>
                <td class="leftTit">위수탁보즘</td>
                <td class="B"><?=char_num($mf['AU'])?></td>
            </tr>
            <tr>
                <td class="leftTit">산재보험료</td>
                <td class="B"><?=char_num($mf['BE'])?></td>
            </tr>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="B subtotaldetail"><?=char_num($insur_info['total_price'])?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 각종보험공제E -->

    <!-- [table] 청구총액S -->
    <div class="claimAmount">
        <p class="Tit">청구총액</p>
        <table class="claimAmountTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <td class="leftTit">항목</td>
                <td>공급가</td>
                <td>부가세</td>
                <td>합계</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="subtotaldetail"><?=char_num($total['sub'])?></td>
                <td class="subtotaldetail"><?=char_num($total['vat'])?></td>
                <td class="B subtotaldetail"><?=char_num($total['price'])?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 청구총액E -->

    <!-- 비고S -->
    <div class="note">
        <p class="Tit">비 고</p>
        <div class="noteD"><br /><br /><br /></div>
    </div>
    <!-- 비고E -->
    <!-- 입금계좌S -->
    <div class="accountInfo">
        <p class="Tit">입금계좌</p>
        <div class="Info">
            <ul>
                <li><span class="_Info"><?=$bank['bank_name']?></span><?=$bank['account_no']?></li>
                <li><span class="_Info">예금주</span><?=$bank['account_name']?></li>
            </ul>
        </div>
    </div>
    <!-- 입금계좌E -->
</section>