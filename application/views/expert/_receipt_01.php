<section class="invoiceDetail">
    <!-- [table] 거래단가S -->
    <div class="basicDeduction">
        <p class="Tit"><span>공제</span> 거래단가</p>
        <table class="basicDeductionTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <td class="leftTit">구분</td>
                <td>공급가</td>
                <td>부가세</td>
                <td width="23%">합계</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftTit">10Kg이하</td>
                <td></td>
                <td></td>
                <td class="B"></td>
            </tr>
            <tr>
                <td class="leftTit">10Kg ~ 20Kg</td>
                <td></td>
                <td></td>
                <td class="B"></td>
            </tr>
            <tr>
                <td class="leftTit">20Kg초과</td>
                <td></td>
                <td></td>
                <td class="B"></td>
            </tr>
            <tr>
                <td class="leftTit">픽업</td>
                <td></td>
                <td></td>
                <td class="B"></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 거래단가E -->

    <!-- 입금계좌S -->
    <div class="accountInfo">
        <p class="Tit">입금계좌</p>
        <div class="Info">
            <ul>
                <li><span class="_Info"><?=$bank['bank_name']?></span><?=$bank['account_no']?></li>
                <li><span class="_Info">예금주</span><?=$bank['account_name']?></li>
            </ul>
        </div>
    </div>
    <!-- 입금계좌E -->
</section>