<div class="payHistoryWrap">

    <!--상단 및 타이틀 box-->
    <h2 class="subTitle">과태료 내역</h2>

    <!--수납정보-->
    <div class="basicDeduction">
        <h2 class="mgt20">과태료 정보</h2>
        <table class="basicTable">
            <tr>
                <th width="27%">부과일시</th>
                <th>금액</th>
                <th>첨부</th>
                <th>내용</th>
            </tr>

            <?
            if(count($fine) > 0){
                foreach($fine as $fin){?>
                    <tr>
                        <td class="center"><?=$fin->due_date?></td>
                        <td><?=number_format($fin->amount)?>원</td>
                        <td><a href="/<?=$fin->attach1?>" target="_blank">[첨부]</a></td>
                        <td><?=$fin->memo?></td>
                    </tr>
                <?}?>
            <?}else{?>
                <tr>
                    <td class="center" colspan="4">과태료가 존재하지 않습니다.</td>

                </tr>
            <?}?>
            <!--tr>
                <td class="center">2019.02.12</td>
                <td>775,000</td>
                <td>전자결제</td>
                <td></td>
            </tr>
            <tr>
                <td class="center">2019.03.18</td>
                <td>882,000</td>
                <td>무통장</td>
                <td></td>
            </tr-->
        </table>
    </div>
    <!--//수납정보-->

</div><!--//contractWrap-->