<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:26
 */

//var_dump($result);
//$userid = (empty($userid))?"114-07-51923":$userid;
$userid = (empty($userid))?"":$userid;
$msg = (empty($msg))?"사업자(주민)등록 번호와 비밀번호를 입력하세요.":$msg;
echo validation_errors();
?>
<main>
    <script>
       
        if(window.Android) {
            window.Android.logout();
        }
    </script>
<div class="loginWrap">
	<div class="loginBg">
		<img src="<?=img_url()?>/logo_wht.png" class="logoImg" />
		<div class="loginBox">        
			<?php echo form_open('expert/login/login'); ?>
				<h2 class="logintit">로그인</h2>
				<input type="hidden" name="mode" value="logincheck" />
				<input type="text" class="num" name="userid" value="<?=$userid?>" placeholder="사업자(주민)등록번호" /><br/>
				<input type="password" class="userpwd" name="userpwd" placeholder="비밀번호" /><br/>
				<input type="submit" class="loginBtn" value="로그인"/><br/>
				<p class="msg">사업자(주민)등록번호와 비밀번호를 입력하세요.</p>		
			</form>
			<p class="error-msg"><?=$msg?></p>
		</div>
    </div>
</div>




<!--main>
<div id="loginWrap">
    <div class="login">
        <h2>LOGIN</h2>
        <img src="<?=img_url()?>/expert_logo.jpg" class="login-logo" />
        <?php echo form_open('expert/login/login') ?>
            <input type="text" class="userid" name="userid" value="<?=$userid?>" placeholder="사업자(주민)등록번호" />
            <input type="password" class="userpwd" name="userpwd" placeholder="비밀번호" />
            <input type="button" class="login-btn" value="로그인" onclick="login();" />
        </form>
        <p class="error-msg"><?=$msg?></p>
    </div>
</div-->