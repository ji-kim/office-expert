<meta http-equiv=”Content-Type” content=”text/html;charset=euc-kr”/>
<?
//수수료
$vat = 3.3;


//price_vat는 수정하지 마세요
$price_vat = (100 + $vat) / 100;
$username = $member['driver'];
?>

	<!--입금계좌-->
	<div class="account">
		<div class="box">


			<div class="accountNum">
				<h3>입금계좌</h3>
				<p class="accountTit"><span class="info"><?=$bank['bank_name']?></span><?=$bank['account_no']?></p>
				<p class="accountTit"><span class="info">예금주</span><?=$bank['account_name']?></p>


                <?if($last_levy['balance_amount'] > 0){?>
                    <br>
                    <h3>입금금액</h3>
                    <p class="accountTit"><span class="info">납부금액 <?=number_format($last_levy['balance_amount'])?>원</p>

                    <br>
                    <h3>바로납부시 입금금액</h3>
                    <p class="accountTit"><span class="info">총납부액 <?=number_format((int)($last_levy['balance_amount'] * $price_vat))?>원 (수수료 <?=number_format((int)(($last_levy['balance_amount'] * $price_vat) - $last_levy['balance_amount']))?>원 포함)</p>


                <?}?>
			</div>

			<?$mid = "deltapay01";
			$orderid = "DE".date("YmdHis").rand(1,999);
			$timestamp = time();
			$merchantkey = "Z3V6VmN3cHlpcVc0LzF0OHdMdjRXQT09";

			?>

			<form id="autoPay" action="https://inilite.inicis.com/inibill/inibill_card.jsp">
			<input type="hidden" name="mid" value="<?=$mid?>">
            <input type="hidden" name="levy_idx" value="<?=$last_levy['idx']?>">
			<input type="hidden" name="userid" value="<?=$_SESSION['userid']?>">
			<input type="hidden" name="carnum" value="<?=$_SESSION['carnum']?>">
			<input type="hidden" name="billkey" value="">
			<input type="hidden" name="goodname" value="납부">
            <input type="hidden" name="autopay" value="0">

			<input type="hidden" name="price" value="<?=(int)($last_levy['balance_amount'] * $price_vat)?>">
            <input type="hidden" name="real_price" value="<?=$last_levy['balance_amount']?>">
			<input type="hidden" name="currency" value="WON">
			<input type="hidden" name="cardquota" value="00">
			<input type="hidden" name="authentification" value="01">
			<input type="hidden" name="quotainterest" value="0">
			<input type="hidden" name="buyertel" value="01000000000">
			<input type="hidden" name="buyeremail" value="test@test123111.co.kr">

			<input type="hidden" name="buyername" value="<?=$username?>">
			<input type="hidden" name="orderid" value="<?=$orderid?>">

			<input type="hidden" name="oid" value="<?=$orderid?>">
			<input type="hidden" name="returnurl" value="http://<?=$_SERVER['HTTP_HOST']?>/paymodule/inicis/getBillKey.php">
			<input type="hidden" name="timestamp" value="<?=$timestamp?>">
			<input type="hidden" name="period" value="M2">
			<input type="hidden" name="hashdata" value="<?=hash("sha256",$mid.$orderid.$timestamp.$merchantkey)?>">
			</form>

            <form id="instantPay" action="https://mobile.inicis.com/smart/wcard">
                <input type="hidden" name="P_MID" value="<?=$mid?>">
                <input type="hidden" name="P_OID" value="<?=$orderid?>">
                <input type="hidden" name="P_AMT" value="<?=(int)($last_levy['balance_amount'] * $price_vat)?>">
                <input type="hidden" name="P_UNAME" value="<?=$username?>">
                <input type="hidden" name="P_NOTI" value="<?=$last_levy['idx']?>|<?=$_SESSION['userid']?>|<?=$_SESSION['carnum']?>|<?=(int)($last_levy['balance_amount'])?>">
                <input type="hidden" name="P_GOODS" value="공제 납부">
                <input type="hidden" name="P_NEXT_URL" value="http://<?=$_SERVER['HTTP_HOST']?>/paymodule/inicis/mobileReq.php">
                <input type="hidden" name="P_RESERVED" value="twotrs_isp=Y&block_isp=Y&twotrs_isp_noti=N"/>

                <input type="hidden" name="hashdata" value="<?=hash("sha256",$mid.$orderid.$timestamp.$merchantkey)?>">
            </form>
			<div class="accountBtn">
				<!--p class="redBg"><a href="javascript:autoPay()">월자동납부</a></p-->
                <?if($last_levy['balance_amount'] > 0){?>
				<p class="navyBg"><a href="javascript:instantPay()">바로납부</a></p>
                <?}else{?>
                    <p class="navyBg"><a href="javascript:alert('납부할 내역이 없습니다.')">납부완료</a></p>
                <?}?>
			</div>
	</div>
	<!--//입금계좌-->
	<script>
        var auto = false;
	function instantPay(){
		var myForm = document.getElementById('instantPay');

		var w = window.open('about:blank','Popup_Window','toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=400,height=300,left = 312,top = 234');
		myForm.target = 'Popup_Window';
		myForm.submit();
	}

    function autoPay(){
	    alert("월 자동납부는 매월수납 금액을 자동으로 입금할 수 있는 시스템이며\n관리자를 통해 이를 취소할 수 있습니다.");
        auto = true;

        var myForm = document.getElementById('autoPay');
        myForm.period.value = 'M2';
        var w = window.open('about:blank','Popup_Window','toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=400,height=300,left = 312,top = 234');
        myForm.target = 'Popup_Window';
        myForm.submit();
    }

	function payment(resultcode , resultmsg , billkey){
        if(resultcode != '00'){
            alert(resultmsg);
        }else {
            var myForm = document.getElementById('autoPay');
            myForm.billkey.value = billkey;
            if (auto) {
                myForm.autopay.value = 1;
            }
            myForm.method = 'POST';
            myForm.action = '/paymodule/inicis/INIreqrealbill.php';
            myForm.target = '';
            myForm.submit();
        }
	}
	</script>
