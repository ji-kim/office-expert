<div class="payHistoryWrap">

    <!--상단 및 타이틀 box-->
    <h2 class="subTitle">알림</h2>

    <!--수납정보-->
    <div class="basicDeduction">
        <h2 class="mgt20">알림정보</h2>
        <table class="basicTable">
            <tr>
                <th width="27%">알림일자</th>
                <th width="45%">제목</th>
                <th>읽음여부</th>


            </tr>


            <?
            if (count($list) > 0) {
                foreach ($list as $data) {
                    ?>
                    <tr>
                        <td class="center"><?= $data->created_date ?></td>
                        <td class="left"><a href="view/<?= $data->announcements_id ?>"><?= $data->title ?></a></td>
                        <td class="center"><?= $data->read == 0 ? "읽지않음" : "읽음" ?></td>
                    </tr>
                <? } ?>
            <? } else {
                ?>
                <tr>
                    <td class="center" colspan="3">알림이 존재하지 않습니다.</td>

                </tr>
            <? } ?>
            <!--tr>
                <td class="center">2019.02.12</td>
                <td>775,000</td>
                <td>전자결제</td>
                <td></td>
            </tr>
            <tr>
                <td class="center">2019.03.18</td>
                <td>882,000</td>
                <td>무통장</td>
                <td></td>
            </tr-->
        </table>
    </div>
    <!--//수납정보-->

</div><!--//contractWrap-->