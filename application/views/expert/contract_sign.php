
<!DOCTYPE HTML>
<html lang="ko">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DELEX OFFICE</title>
<link href="/assets/expert/css/common.css" rel="stylesheet" type="text/css" />
<link href="/assets/expert/css/jquery.signaturepad.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/expert/js/jquery.signaturepad.js"></script>
<script src="/assets/expert/js/json2.min.js"></script>
</head>

<body>
	

    <div class="mobileContract">
    <!-- Page 1/4 위ㆍ수탁계약해지확인서 -->
        <!--인쇄영역-->
        <div id="header">
            <span class="logoLeft"><img src="/uploads/partner_file/<?=$wsco['logo_img']?>"></span>
            <span class="logoRight"><img src="/uploads/partner_file/<?=$wsco['logo_img']?>"></span>
            <ul class="title">
                <li class="titSmall"></li>
                <li class="titCompany"><?=$wsco['co_name']?></li>
                <li class="titName">위ㆍ수탁 전자 계약서</li>				
            </ul>

        </div>

        <section class="contractText">
            <div class="confirm ">
                <p><span class="blueD">화물자동차운수사업법 제40조 및 관련 규정에 따라 운송사업자 <?=$wsco['co_name']?></span> 대표이사 <span class="blueD"><?=$wsco['ceo']?></span>(이하 “위탁자”라 한다)와 화물자동차의 현물출자자인 위ㆍ수탁차주 <span class="blueD"><?=$member['ceo']?></span>(이하 “을”이라한다)은 화물자동차 운송사업의 경영의 일부를 위탁함에 있어 다음과 같이 위‧수탁계약을 체결한다.</p>
                
                <table class="basic" border="0">
                  <col width="5%"><col width="95%">
                   <tr>
                        <th colspan="2">제1조(기본원칙)</th>
                    </tr>
                    <tr>
                        <td>①</td>
                        <td>“위탁자”와 “수탁자”는 상호 대등한 지위에서 위‧수탁계약을 체결하여 상호 이익 존중 및 신의성실의 원칙에 따라 이행하여야 한다. </td>
                    </tr>
                    <tr>
                        <td>②</td>
                        <td>위탁자”와 “수탁자”는 이 계약의 이행에 있어서 화물자동차운수사업법 등 관련 법령의 규정을 준수하여야 한다. </td>
                    </tr>
                    <tr>
                        <td>③</td>
                        <td>이 계약은 “위탁자”와 “수탁자”간의 화물자동차 운송사업 경영의 위‧수탁계약에 관한 기본사항을 정한 계약으로서 이 계약의 내용과 배치되는 타 계약에 대해서는 이 계약에 의한 내용을 우선하여 적용하며, 이 계약에서 정하지 아니한 사항에 대하여 “위탁자”와 “수탁자”는 대등한 지위에서 합의하여 별도의 특약을 정할 수 있다.</td>
                    </tr>                    
                </table>
                
                <table class="basic" border="0">
                  <col width="5%"><col width="95%">
                   <tr>
                        <th colspan="2">제2조(차량소유자 및 위‧수탁 대상차량)</th>
                    </tr>
                    <tr>
                        <td>①</td>
                        <td>“위탁자”는 “수탁자”가 현물출자한 아래의 차량에 대한 운송사업의 경영을 “수탁자”에게 위탁한다.</td>
                    </tr>
                    <tr>
                        <td>②</td>
                        <td>“위탁자”는 위‧수탁계약으로 차량을 현물출자 받은 경우에는 “수탁자”를 「자동차관리법」에 따른 자동차등록원부에 현물출자자로 기재하여야 한다.</td>
                    </tr>
                </table>
                
                <table class="basic" border="0">
                  <col width="5%"><col width="95%">
                   <tr>
                        <th colspan="2">제3조(계약기간 및 갱신)</th>
                    </tr>
                    <tr>
                        <td>①</td>
                        <td>이 계약의 유효기간은 체결일로부터 __년(2년 이상)으로 하며, 기간 만료시 “위탁자” 또는 “수탁자”가 계약해지의 의사를 표시하지 아니하는 한 동일한 내용으로 계약이 자동 연장된 것으로 본다.</td>
                    </tr>
                    <tr>
                        <td>②</td>
                        <td>그 밖에 계약의 갱신에 관한 사항은 화물자동차운수사업법 제40조의2의 규정에 따른다.</td>
                    </tr>            
                </table>
                
                
                <table class="basic" border="0">
                   <tr>
                        <th>제4조(계약의 변경)</th>
                    </tr>
                    <tr>
                        <td>합리적이고 객관적인 사유가 발생하여 부득이하게 계약변경이 필요한 경우에는 “위탁자”와 “수탁자”는 상호 합의하여 기명․날인한 서면에 의해 계약을 변경할 수 있다.</td>
                    </tr>          
                </table>
                
                <table class="basic" border="0">
                   <tr>
                        <th>제5조(차량의 대폐차)</th>
                    </tr>
                    <tr>
                        <td>“위탁자”는 “수탁자”가 차량의 노후, 사고 등 사유로 현물출자차량의 대폐차를 요구하는 경우 지체 없이 응해야 하며, 법적 소요비용은 “수탁자”가 부담한다.</td>
                    </tr>          
                </table>
                
                <table class="basic" border="0">
                   <tr>
                        <th>제6조(인감증명서 첨부)</th>
                    </tr>
                    <tr>
                        <td>“갑”과 “을”이 위ㆍ수탁계약해지 확인서를 작성한 경우 “갑”과 “을”은 이에 대한 의사의 진의를 표시하기 위해 “갑”의 법인인감증명서(개인사업자의 경우 개인인감증명서)와 “을”의 인감증명서를 확인서에 첨부한다. 이 경우 인감증명서는 3개월 이내 교부된 것으로 한다.</td>
                    </tr>          
                </table>
                
                <p class="pdt20">　이 계약의 체결을 증명하기 위하여 위탁자와 수탁자의 상호 전자 명을 이용한다.</p>
				<form action="/expert/contract/signaction" method="POST" id="contractForm">

					<dl class="date">
						<dt class=""><input type="text" id="contract_date_y" name="contract_date_y" size="4" maxlength="4" value="<?=date("Y")?>" numberOnly></dt>
						<dd>년</dd>
						<dt><input type="text" id="contract_date_m" name="contract_date_m" size="2" maxlength="2" value="<?=date("m")?>" numberOnly></dt>
						<dd>월</dd>
						<dt><input type="text" id="contract_date_d" name="contract_date_d" size="2" maxlength="2" value="<?=date("d")?>" numberOnly></dt>
						<dd>일</dd>
					</dl>
					<input type="hidden" id="contract_sign" name="contract_sign" value=""/>
				</form>
            </div>	
        </section>
           
            
        <div class="tableBasic">
           <p class="tableTitle">갑</p>
            <table class="contracTable" style="">
                <colgroup>
                    <col style="width: 30%">
                    <col style="width: 55%">
                    <col style="width: 15%">
                </colgroup>
                 <tr>                        
                    <th>전화</th>
                    <td colspan="2"><?=$wsco['co_tel']?></td>
                </tr>
                <tr>
                    <th>사업자등록번호</th>
                    <td><?=$wsco['bs_number']?></td>
                    <td><img src="/assets/expert/img/sign.png"></td>
                </tr>
                <tr>
                    <th>사무소</th>
                    <td colspan="2"><?=$wsco['co_address']?> </td>
                </tr>
            </table>
        </div><!-- //tableBasic-->
            
            
        <div class="tableBasic pdt20">
           <p class="tableTitle">을</p>
            <table class="contracTable" style="">
                <tr>
                    <th width="30%">사업자등록번호</th>
                    <td colspan="2"><?=$member['bs_number']?></td>
                </tr>
                 <tr>
                    <th>주민등록번호</th>
                    <td colspan="2"><?=$member['reg_number']?></td>
                </tr>
                 <tr>
                    <th>차주명</th>
                    <td><?=$member['ceo']?></td>
                    <td><?=$member['co_tel']?></td>
                </tr
                <?if(@$truck != null){?>
                <tr>
                    <th>차량번호</th>
                    <td colspan="2"><?=$truck['car_1']?></td>
                </tr>
                <?}?>
                <tr>
                    <th>주소</th>
                    <td colspan="2"><?=$member['co_address']?></td>
                </tr>                              
            </table>
            
            <div class="signWrap">               
            <!-- //jquery.signaturepad
                 //json2.min -->             
               
                <div class="sigPad" id="smoothed" style="width:338px;">
                    <div class="sigNav">
                       <p class="drawIt">서명</p>
                       <span class="clearButton"><a href="#clear">지우기</a></span>
                    </div>                

                    <div class="sig sigWrapper" style="height:auto;">
                        <div class="typed"></div>
                        <canvas class="pad" width="335px"  height="250"></canvas>
                        <input type="hidden" name="output" class="output">
                    </div>                   
                    <button onClick="javascript:contractSubmit()">확인</button>                    
                </div><!--//sigPad-->
            </div><!--//signWrap-->                        
                                                
        </div><!-- //tableBasic-->
        
        
        
     </div><!-- //mobileContract-->
	
</body>    
	<script>
		$(document).ready(function() {
			

		  $('#smoothed').signaturePad({
			drawOnly:true, 
			drawBezierCurves:true, 
			lineTop:200
		  });
		});
		$("input:text[numberOnly]").on("keyup", function() {
			$(this).val($(this).val().replace(/[^0-9]/g,""));
		});

		function contractSubmit(){
			if($('#contract_date_y').val().length < 4){
				alert("날짜를 정확히 입력해주세요.");
				$('#contract_date_y').focus();
			}else if($('#contract_date_m').val().length < 2){
				alert("날짜를 정확히 입력해주세요.");
				$('#contract_date_m').focus();
			}else if($('#contract_date_d').val().length < 2){
				alert("날짜를 정확히 입력해주세요.");
				$('#contract_date_d').focus();
			}else if(!$('#smoothed').signaturePad().validateForm()){			
			}else{
				if(confirm("해당 내용으로 계약을 진행하시겠습니까?")){
					$('#contract_sign').val($('#smoothed').signaturePad().getSignatureImage());
					$('#contractForm').submit();
				}				
			}
		}
	</script>
</html>