<!-- 청구총액 시작 -->
<div class="basicDeduction">
	<h3>지급&공제총액</h3>
	<table class="basicTable">
		<tr>
			<th width="20%">항목</th>
			<th>공급가</th>
			<th>부가세</th>
			<th>합계</th>
		</tr>
		<tr>
			<td class="red">지급총액</td>
			<td class="subTotalBg"><?= char_num($total['tr_fee_supp']) ?></td>
			<td class="subTotalBg"><?= char_num($total['tr_fee_vat']) ?></td>
			<td class="subTotalBg"><?= char_num($total['tr_fee_tot']) ?></td>
		</tr>
		<tr>
			<td class="red">공제총액</td>
			<td class="subTotalBg"><?= char_num($total['mf_fee_supp']) ?></td>
			<td class="subTotalBg"><?= char_num($total['mf_fee_vat']) ?></td>
			<td class="subTotalBg"><?= char_num($total['mf_fee_tot']) ?></td>
		</tr>
	</table>
</div>
<!-- // 청구총액 -->

<!-- 청구총액 시작 -->
<div class="basicDeduction">
	<h3>공제후지급(청구)액</h3>
	<table class="basicTable">
		<tr>
			<th>공급가</th>
			<th>부가세</th>
			<th>합계</th>
		</tr>
		<tr>
			<td class="subTotalBg"><?= char_num($total['tr_fee_supp']-$total['mf_fee_supp']) ?></td>
			<td class="subTotalBg"><?= char_num($total['tr_fee_vat']-$total['mf_fee_vat']) ?></td>
			<td class="subTotalBg"><?= char_num($total['total_balance']) ?></td>
		</tr>
	</table>
</div>
<!-- // 청구총액 -->
