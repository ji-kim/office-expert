<aside>
	<div class="asideTop">
		<i class="xi-close xi-x" id="menuClose"></i>
	</div>	
	<p class="helloName"><strong><?=$member['driver']?> 기사님</strong> 안녕하세요.</p>
	
	<!--버튼 메뉴 4개-->
	<div class="btnMenu">
		<div class="Home">
			<a href="/expert/invoice">
				<i class="xi-home"></i>
				<span class="mgt5">홈으로</span>
			</a>
		</div>
		<div class="management">
			<a href="/expert/invoice">
				<i class="xi-receipt mgb5"></i>
				<span class="mgt5">위수탁관리</span>
			</a>
		</div>
		<div class="infoCar">
			<a href="/expert/car">
				<i class="xi-truck"></i>
				<span class="mgt5">차량정보</span>
			</a>
		</div>
		<div class="question">
			<a href="/expert/qa">
				<i class="xi-help"></i>
				<span class="mgt5">1:1문의</span>
			</a>
		</div>
	</div>
	
	<!--메뉴 리스트-->
	<ul class="listMenu">
		<li>
			<a href="/expert/contract">
				<i class="xi-document"></i> 계약현황<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
		<li>
			<a href="/expert/allocation">
				<i class="xi-repeat"></i> 배차<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
		<li>
			<a href="/expert/ins_isp">
				<i class="xi-calendar-check"></i> 보험/검사정보<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>		
		<li>
			<a href="/expert/payment/balance">
				<i class="xi-won"></i> 월별정산서<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
		<li>
			<a href="/expert/payment/history">
				<i class="xi-calendar-list"></i> 납부이력<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
		<!--li>
			<a href="/expert/payment/taxbill">
				<i class="xi-won"></i> 세금계산서<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li-->
		<li>
			<a href="/expert/notice/notice_list">
				<i class="xi-bell-o"></i> 알림<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
        <?if(!strstr($_SERVER['HTTP_USER_AGENT'],"APP")){?>
		<li>
			<a href="/expert/login/logout">
				<i class="xi-switch-off"></i> 로그아웃<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
        <?}?>

	</ul>	
</aside>

<div id="maskLayer" ></div>
<header>   
    <div class="header">
       <span class="menuBtn" id="menuOpen" ><i class="xi-bars xi-x"></i></span>
       <h1 class="headerLogo"><img src="/assets/expert/img/logo_red.png"/></h1>
        <?if($truck != null){?>
        <button class="onWork" onClick="onWork('<?=@$member['userid']?>','<?=@$truck['car_1']?>')" <?=@$_SESSION['is_connected'] == "1" ? "style='display:none'" : ""?>>출근</button>
        <button class="offWork" onClick="offWork('<?=@$member['userid']?>','<?=@$truck['car_1']?>')" <?=@$_SESSION['is_connected'] == "0" ? "style='display:none'" : ""?>>퇴근</button>
        <?}?>
    </div>
</header>

<main>
