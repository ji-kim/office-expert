<div class="carWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">운행차량정보</h2>		
	<section class="topCommon">	
		<!--상단 및 타이틀 box-->
		<div class="box">
			<!-- 폰트 레드컬러로 변경시  boxText css-->
			<div class="boxTextNavy">서울 85바 4503</div>			
   		</div>
   		
		<!--상단고객정보-->
		<div class="infoDetail">   		
			<h2 class="infoTit">고객정보</h2>	
			<!-- div 테이블 변경시 table-2cols table-4cols table-5cols -->
			<div class="table table-2cols">			
				<div class="cell40 tableBTop">소속</div>
				<div class="cell60 tableBTop">케이티지엘에스(주)</div>

				<div class="cell40">위·수탁차주</div>
				<div class="cell60">장기영</div>

				<div class="cell40">배차지</div>
				<div class="cell60">한국승강기안전공단</div>					

				<div class="cell40">차량번호</div>
				<div class="cell60">광주89아2033</div>

				<div class="cell40">차대번호</div>
				<div class="cell60">KNFWXZ76ADK771273</div>

				<div class="cell40">차명</div>
				<div class="cell60">봉고Ⅲ파워게이트 </div>

				<div class="cell40">차종</div>
				<div class="cell60">소형/봉고Ⅲ파워게이트 </div>

				<div class="cell40">용도</div>
				<div class="cell60">일반영업용</div>

				<div class="cell40">연식</div>
				<div class="cell60">2013-04-24</div>

				<div class="cell40">형식</div>
				<div class="cell60">PUL6AF-12-PGT1</div>

				<div class="cell40">원동기형식</div>
				<div class="cell60">D4CB</div>					
			</div>
		</div>	 		
	</section>
		
	<div class="basicDeduction">
		<h2 class="pdt30"> 수리이력</h2>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="27%">수리항목</th>
					<th>비용</th>
					<th>날짜</th>
					<th>비고</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="center">필터교환</td>
					<td>35,000</td>
					<td>2019.02.18</td>
					<td></td>
				</tr>
			</tbody>
		</table>		
	</div>
	
	<div class="basicDeduction">
		<h2> 주유이력</h2>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="27%">날짜</th>
					<th>위치</th>
					<th>주유금액</th>
					<th>비고</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="center">2019.03.10</td>
					<td>방배주유소</td>
					<td>50,000</td>
					<td></td>
				</tr>
			</tbody>
		</table>		
	</div>
	
	
</div><!--//carWrap-->