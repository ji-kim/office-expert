<aside>
	<div class="asideTop">
		<i class="xi-close xi-x" id="menuClose"></i>
	</div>	
	<p class="helloName"><strong>홍길동 기사님</strong> 안녕하세요.</p>
	
	<!--버튼 메뉴 4개-->
	<div class="btnMenu">
		<div class="Home">
			<a href="/expert/design/invoice">
				<i class="xi-home"></i>
				<span class="mgt5">홈으로</span>
			</a>
		</div>
		<div class="management">
			<a href="/expert/design/invoice">
				<i class="xi-receipt mgb5"></i>
				<span class="mgt5">위수탁관리</span>
			</a>
		</div>
		<div class="infoCar">
			<a href="/expert/design/car">
				<i class="xi-truck"></i>
				<span class="mgt5">차량정보</span>
			</a>
		</div>
		<div class="question">
			<a href="">
				<i class="xi-help"></i>
				<span class="mgt5">1:1문의</span>
			</a>
		</div>
	</div>
	
	<!--메뉴 리스트-->
	<ul class="listMenu">
		<li>
			<a href="/expert/design/contract">
				<i class="xi-document"></i> 계약현황<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
		<li>
			<a href="/expert/design/alloc">
				<i class="xi-repeat"></i> 배차<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
		<li>
			<a href="/expert/design/ins_isp">
				<i class="xi-calendar-check"></i> 보험/검사정보<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>		
		<li>
			<a href="/expert/design/pay_balance">
				<i class="xi-won"></i> 수납내역<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
		<li>
			<a href="/expert/design/pay_history">
				<i class="xi-calendar-list"></i> 납부이력<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
		<li>
			<a href="">
				<i class="xi-bell-o"></i> 알림<span class="listBtn"><i class="xi-angle-right"></i></span>
			</a>
		</li>
	</ul>	
</aside>

<div id="maskLayer" ></div>
<header>   
    <div class="header">
       <span class="menuBtn" id="menuOpen" ><i class="xi-bars xi-x"></i></span>
       <h1 class="headerLogo"><img src="/assets/expert/img/logo_red.png"/></h1>
       <button class="onWork">출근</button><!-- <button class="offWork">퇴근</button>-->
    </div>
</header>

<main>