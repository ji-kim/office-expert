<div class="instWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle type">보험 & 검사정보</h2>		
	<section class="topCommon">	
		<!--상단 및 타이틀 box-->
   		
		<!--보험정보-->
		<div class="infoDetail mnone">   		
			<h2 class="infoTit">보험정보</h2>	
			<!-- div 테이블 변경시 table-2cols table-4cols table-5cols -->
			<div class="table table-2cols">			
				<div class="cell40 tableBTop">소속</div>
				<div class="cell60 tableBTop">(주)아이디일일구닷컴</div>

				<div class="cell40">보험사</div>
				<div class="cell60">동부화재</div>
				
				<div class="cell40">분납횟수</div>
				<div class="cell60">6회</div>
				
				<div class="cell40">가입일</div>
				<div class="cell60">2019.09.01</div>
				
				<div class="cell40">만기일</div>
				<div class="cell60">2019.09.01</div>
				
				<div class="cell40">가입금액</div>
				<div class="cell60">1,381,510</div>
			</div>
			<!--//계약상세정보-->
			<div class="commonBtn">
				<button type="button" class="navyBg"> 증명서보기</button>
			</div>	
		</div>
				
	</section>
	
	<!--이전검사정보-->
	<div class="basicDeduction">
		<h2 class="pdt30"> 이전검사정보</h2>
		<table class="basicTable">
			<tr>
				<th width="50%">지정검사일</th>
				<td class="center">2018.10.13</td>
			</tr>
			<tr>
				<th>최종검사일</th>
				<td class="center">2018.10.13</td>
			</tr>
		</table>		
	</div>
	<!--//이전검사정보-->
	
	
	<!--다음검사정보-->	
	<div class="basicDeduction">
		<h2> 다음검사일</h2>
		<table class="basicTable">
			<tr>
				<th width="50%">다음지정검사일</th>
				<td class="center">2018.10.13</td>
			</tr>
			<tr>
				<th>다음검사개시일</th>
				<td class="center">2018.10.13</td>
			</tr>
			<tr>
				<th>다음검사만료일</th>
				<td class="center">2018.10.13</td>
			</tr>
		</table>		
	</div>
	<!--//다음검사정보-->	
	
</div><!--//contractWrap-->