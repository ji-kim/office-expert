<div class="payHistoryWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">납부이력</h2>		
	
	<!--수납정보-->
	<div class="basicDeduction">
		<h2 class="mgt20">납부정보</h2>
		<table class="basicTable">
			<tr>
				<th width="27%">납부일시</th>
				<th>입금액</th>
				<th>납부</th>
				<th>비고</th>
			</tr>
			<tr>
				<td class="center">2019.01.22</td>
				<td>861,000</td>
				<td>전자결제</td>
				<td></td>
			</tr>
			<tr>
				<td class="center">2019.02.12</td>
				<td>775,000</td>
				<td>전자결제</td>
				<td></td>
			</tr>
			<tr>
				<td class="center">2019.03.18</td>
				<td>882,000</td>
				<td>무통장</td>
				<td></td>
			</tr>
		</table>
	</div>
	<!--//수납정보-->
	
</div><!--//contractWrap-->