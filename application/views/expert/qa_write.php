<div class="payHistoryWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">Q & A</h2>		
	
	<!--수납정보-->
	<div class="basicDeduction">
		<h2 class="mgt20">문의하기</h2>
		<form action="/expert/qa/writeaction" id="writeForm" method="POST">
		<table class="basicTable">				
			<tr>					
				<th class="center"><?="제목"?></td>						
			</tr>
			<tr>			
				<td class="center"><input type="text" name="subject" style="width:100%;"/></td>
			</tr>
			<tr>					
				<th class="center"><?="질문내용"?></td>						
			</tr>
			<tr>			
				<td class="center"><textarea name="question" style="width:100%;height:200px;"></textarea></td>
			</tr>
			
		</table>
		<h2 class="mgt20">
			<a href="javascript:formSubmit();">작성</a>
		</h2>
	</div>

	
	
	<!--//수납정보-->
	
</div><!--//contractWrap-->

<script>
	function formSubmit(){
		var form = document.getElementById("writeForm");
		if(form.subject.value == ""){
			alert("제목을 입력해주세요.");
		}else if(form.question.value == ""){
			alert("내용을 입력해주세요.");
		}else{
			if(confirm("해당 내용을 등록하시겠습니까?")){
				form.submit();
			}
		}
	}

</script>