	<!-- 청구총액 시작 -->
	<div class="basicDeduction">
		<h2>청구총액</h2>
		<table class="basicTable">
			<tr>
				<th width="20%">항목</th>
				<th>공급가</th>
				<th>부가세</th>
				<th>합계</th>
			</tr>
			<tr>
				<td class="red">소계</td>
				<td class="subTotalBg"><?= char_num($total['mf_fee_supp']) ?></td>
				<td class="subTotalBg"><?= char_num($total['mf_fee_vat']) ?></td>
				<td class="subTotalBg"><?= char_num($total['mf_fee_tot']) ?></td>
			</tr>
		</table>
	</div>
	<!-- // 청구총액 -->
