<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:26
 */
?>

<script>
	if (window.Android) {
        window.Android.getDeviceToken('<?=$_SESSION['userid']?>'); // 로그인시 안드로이드 앱으로 아이디, 차량번호 전달
    }

</script>

<div class="invoiceWrap">
	<h2 class="subTitle"><?= $title?></h2>

	<!--상단 및 타이틀 box-->
	<section class="topCommon">
		<div class="box">
			<?= $mf['total_balance_view']?>

			<div class="calendarCheck">
				<div class="calendarCheckWrap ">
				<input type="text" class="datepicker-here" value="">
				<button onclick="myDatepicker.show()" class="datepickerBtn" id="datepicker1"><?=substr($month,0,4)?>년 <?=substr($month,5,2)?>월<span><i class="xi-angle-down"></i></span></button>
				</div>
			</div>
		</div>
   		<!--상단 및 타이틀 box-->

<?php
if(isset($mf['df_id'])) {
?>
		<!--상단고객정보-->
   		<div class="infoDetail">
			<h2 class="infoTit">고객정보</h2>
			<!-- div 테이블 변경시 table-2cols table-4cols table-5cols -->
			<div class="table table-2cols">
				<div class="cell40 tableBTop">소속</div>
				<div class="cell60 tableBTop"><?=(!empty($wsco['co_name']))?$wsco['co_name']:"-"?></div>

				<div class="cell40">위·수탁차주</div>
				<div class="cell60"><?=(!empty($member['co_name']))?$member['co_name']:"-"?></div>

				<div class="cell40">배차지</div>
				<div class="cell60"><?=(!empty($baecha_co['co_name']))?$baecha_co['co_name']:"-"?></div>

				<div class="cell40">전화</div>
				<div class="cell60"><?=$member['co_tel']?></div>

				<div class="cell40">차량번호</div>
				<div class="cell60"><?=(!empty($truck['car_1']))?$truck['car_1']:"-"?></div>

				<div class="cell40">톤수</div>
				<div class="cell60"><?=(!empty($truck['carinfo_11']))?$truck['carinfo_11']:"-"?></div>

				<div class="cell40">용도</div>
				<div class="cell60"><?=(empty($member['c']))?"-":$member['c']?></div>

				<div class="cell40">계약일</div>
				<div class="cell60"><?=$member['N']?></div>
			</div>
		</div>
		<!-- //상단고객정보-->
<?php
}
?>

	</section>
	<!-- //타이틀 및 고객정보 내용-->
	<?
	//chlrmstnskqso
	        //수수료
	        $vat = 3.3;


	        //price_vat는 수정하지 마세요
	        $price_vat = (100 + $vat) / 100;
	        $last_levy['balance_amount'] = 150000;
	        $username = '홍길동';

	        $last_levy['paid_amount'] = !empty($last_levy['paid_amount'])?$last_levy['paid_amount']:0;
	        $final_balance = $total['mf_fee_tot'] - $last_levy['paid_amount'];
	    ?>
		<!-- 최근 수납정보 시작 -->
		<div class="basicDeduction">
			<h3>최근수납(지급)정보</h3>
			<table class="basicTable">
				<tr>
					<th>최근수납일</th>
					<th width="30%">수납액</th>
					<th width="40%">수납후 미수(지급)액</th>
				</tr>
				<tr>
					<td class="subTotalBg"><?=(!empty($last_levy_log['pay_date']))?$last_levy_log['pay_date']:"" ?></td>
					<td class="subTotalBg"><?= (!empty($last_levy['paid_amount']))?char_num($last_levy['paid_amount']):"" ?></td>
					<td class="subTotalBg"><?= (!empty($last_levy['paid_amount']))?char_num($final_balance):"" ?></td>
				</tr>
			</table>
		</div>
		<!-- // 최근 수납정보 -->

<!-- Load  -->
<?php
	if(isset($mf['df_id'])) {
		// 거래정산 내역
		if ($mf['tr_type']=='T' || $mf['tr_type']=='MT') $this->load->view('expert/_transaction', $tr_settings);

		// 관리비공제 내역
		if ($mf['tr_type']=='M' || $mf['tr_type']=='MT') $this->load->view('expert/_invoice', $tr_settings);

		if ($mf['balance_type']=='1') { //청구
			$this->load->view('expert/_summary01');
		} else if ($mf['balance_type']=='2') { //지급
			$this->load->view('expert/_summary02');
		}

		// 합계 및 입금정보 & 전자결재
		$this->load->view('expert/_payment_info');
	}
?>
<!-- End  -->

</div><!-- // invoiceWrap-->


<script>
    var date = new Date();
    var setdate = new Date(<?=substr($month,0,4)?>, <?=substr($month,5,3)?>, 01);
    date.setMonth(date.getMonth()-1);
    var myDatepicker = $('.datepicker-here').datepicker({
        language: 'ko',
        minView: "months",
        view: "months",
        autoClose: true,
        position: "bottom center",
        startDate: setdate,
        maxDate: date,
        onSelect: function (fd, date) {
            // Do nothing if selection was cleared
            if (!date) return;
            location.href='/expert/invoice/'+fd;
        }
    }).data('datepicker');
</script>
