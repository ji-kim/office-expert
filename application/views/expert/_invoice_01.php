<section class="invoiceDetail">
<?php
    if(isset($wsm_info)) {
?>
    <!-- [table] 기본공제S -->
    <div class="basicDeduction">
        <p class="Tit"><span>공제</span> 기본공제</p>
        <table class="basicDeductionTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <td class="leftTit">항목</td>
                <td>공급가</td>
                <td>부가세</td>
                <td width="23%">합계</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftTit">위·탁관리비</td>
                <td><?= char_num($wsm_info['wst_mfee']) ?></td>
                <td><?= char_num($wsm_info['mfee_vat']) ?></td>
                <td class="B"><?= char_num($wsm_info['wst_mfee'] + $wsm_info['mfee_vat']) ?></td>
            </tr>
            <tr>
                <td class="leftTit">협회비</td>
                <td><?= char_num($wsm_info['org_fee']) ?></td>
                <td>0</td>
                <td class="B"><?= char_num($wsm_info['org_fee']) ?></td>
            </tr>
            <tr>
                <td class="leftTit">차고지비</td>
                <td><?= char_num($wsm_info['grg_fee']) ?></td>
                <td><?= char_num($wsm_info['grg_fee_vat']) ?></td>
                <td class="B"><?= char_num($wsm_info['grg_fee'] + $wsm_info['grg_fee_vat']) ?></td>
            </tr>
            <tr>
                <td class="leftTit">기 타</td>
                <td><?= char_num($wsm_info['etc']) ?></td>
                <td>0</td>
                <td class="B"><?= char_num($wsm_info['etc']) ?></td>
            </tr>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="subtotaldetail"><?= char_num($wsm_info['total_amount']) ?></td>
                <td class="subtotaldetail"><?= char_num($wsm_info['total_vat']) ?></td>
                <td class="B subtotaldetail"><?= char_num($wsm_info['total_price']) ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 기본공제E -->
<?php
    }

    if(isset($gongje_info)) {
?>
    <!-- [table] 일반공제S -->
    <div class="generalDeduction">
        <p class="Tit"><span>공제</span> 일반공제</p>
        <table class="generalDeductionTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr><td class="leftTit">항목</td><td>공급가</td><td>부가세</td><td>합계</td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftTit">범칙금</td>
                <td><?=char_num($gongje_info['fine_fee'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($gongje_info['fine_fee'])?></td>
            </tr>
            <tr>
                <td class="leftTit">미납</td>
                <td><?=char_num($gongje_info['not_paid'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($gongje_info['not_paid'])?></td>
            </tr>
            <tr>
                <td class="leftTit">자동차세</td>
                <td><?=char_num($gongje_info['car_tax'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($gongje_info['car_tax'])?></td>
            </tr>
            <tr>
                <td class="leftTit">환경분담금</td>
                <td><?=char_num($gongje_info['env_fee'])?></td>
                <td>0</td>
                <td class="B"><?=char_num($gongje_info['env_fee'])?></td>
            </tr>
            <tr>
                <td class="leftTit">차고지비</td>
                <td><?=char_num($gongje_info['grg_fee'])?></td>
                <td><?=char_num($gongje_info['grg_fee_vat'])?></td>
                <td class="B"><?=char_num($gongje_info['grg_fee']+$gongje_info['grg_fee_vat'])?></td>
            </tr>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="subtotaldetail"><?=char_num($gongje_info['total_amount'])?></td>
                <td class="subtotaldetail"><?=char_num($gongje_info['total_vat'])?></td>
                <td class="B subtotaldetail"><?=char_num($gongje_info['total_price'])?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 일반공제E -->
<?php
    }

    if(isset($insur_info)) {
?>
    <!-- [table] 각종보험공제S -->
    <div class="insuranceExpenseDeduction">
        <p class="Tit"><span>공제</span> 각종보험공제</p>
        <table class="insuranceExpenseDeductionTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr><td width="48%" class="leftTit">항목</td><td>금액</td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftTit">자동차보험</td>
                <td class="B"><?=char_num($insur_info['ins_car'])?></td>
            </tr>
            <tr>
                <td class="leftTit">적재물보험</td>
                <td class="B"><?=char_num($insur_info['ins_load'])?></td>
            </tr>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="B subtotaldetail"><?=char_num($insur_info['total_price'])?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 각종보험공제E -->
<?php
    }

    if(isset($rf_gongje_info)) {
?>
    <!-- [table] 환급형공제S -->
    <div class="refundDeduction">
        <p class="Tit"><span>공제</span> 환급형공제</p>
        <table class="refundDeductionTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr><td width="48%" class="leftTit">항목</td><td>금액</td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftTit">해지담보</td>
                <td class="B"><?=char_num((empty($rf_gongje_info['gj_termination_mortgage'])?0:$rf_gongje_info['gj_termination_mortgage']))?></td>
            </tr>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="B subtotaldetail"><?=char_num($rf_gongje_info['total_price'])?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 환급형공제E -->
<?php
    }

    if(!empty($total['price'])) {
?>
    <!-- [table] 청구총액S -->
    <div class="claimAmount">
        <p class="Tit">청구총액</p>
        <table class="claimAmountTable" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <td class="leftTit">항목</td>
                <td>공급가</td>
                <td>부가세</td>
                <td>합계</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="subtotalTit">소 계</td>
                <td class="subtotaldetail"><?= char_num($total['sub']) ?></td>
                <td class="subtotaldetail"><?= char_num($total['vat']) ?></td>
                <td class="B subtotaldetail"><?= char_num($total['price']) ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- [table] 청구총액E -->
<?php
    }
?>
    <!-- 비고S -->
    <div class="note">
        <p class="Tit">비 고</p>
        <div class="noteD"><br /><br /><br /></div>
    </div>
    <!-- 비고E -->
    <!-- 입금계좌S -->
    <div class="accountInfo">
        <p class="Tit">입금계좌</p>
        <div class="Info">
            <ul>
                <li><span class="_Info"><?=$bank['bank_name']?></span><?=$bank['account_no']?></li>
                <li><span class="_Info">예금주</span><?=$bank['account_name']?></li>
            </ul>
        </div>
    </div>
    <!-- 입금계좌E -->
</section>