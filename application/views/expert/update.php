<style>
    .updateWrap{
        width: 100%;
        height: 100%;
    }
    .updateWrap .update_header {
        height: 150px;
        background: url(/assets/expert/img/logo_wht.png) #2c3941 no-repeat center;
        background-size: 200px;
    }
    .updateWrap .updateTex {
        text-align: center;
        width: 80%;
        margin: 0 10%;
        color: #2c3941;
        margin-top: 40px;
    }
    .updateWrap .updateTex .updateTitle {
        font-weight: bold;
        font-size: 26px;
    }
    .updateWrap .updateTex .updateSubTitle {
        margin-top: 20px;
        font-size: 16px;
    }
    .updateWrap .updateTex .updateBtn {
        background-color: #7266ba;
        color: #fff;
        width:100%;
        height: 50px;
        outline:none;
        border: 0;
        cursor : pointer;
        margin-top: 50px;
        font-weight: bold;
        font-size: 16px;
    }
</style>


<div class="updateWrap">
    <div class="update_header">
        <!-- <img src="/assets/expert/img/logo_wht.png" class="logoImg" /> -->
    </div>
    <div class="updateTex">
        <h2 class="updateTitle">업데이트 안내</h2>
        <p class="updateSubTitle">최신 앱 버전이 업데이트 되었습니다. <br/>
            업데이트 진행 후 이용하실 수 있습니다.<br/>
            <b>앱스토어에서 '델렉스 엑스퍼트'를 검색 후 업데이트를 진행해주세요.</b>
            </p>
        <!--button type="button" onClick="update()" class="updateBtn">버전 업데이트</button-->
    </div>
</div>
<script>
    function update() {
        window.open("http://market.android.com/details?id=com.delex.delexexpert");
    }
</script>