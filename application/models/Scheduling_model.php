<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * 	@author : themetic.net
 * 	date	: 21 April, 2015
 * 	Inventory & Invoice Management System
 * 	http://themetic.net
 *  version: 1.0
 */

class Scheduling_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function get_branchs($branch_id=NULL)
    {
 		$this->db->select('a.*, b.dp_id, b.co_name, b.ceo');
		$this->db->join('tbl_members b', 'a.customer_id = b.dp_id', 'left');
		$this->_order_by = 'a.list_order ASC';
		if(!empty($branch_id)) $this->db->where('a.ct_id', $branch_id);
        $all_branch = $this->db->get('tbl_contract_coops a')->result();

		if (!empty($all_branch)) {
            return $all_branch;
        } else {
            return array();
        }
    }

    public function get_first_branch($ct_id)
    {
		//order by 작동안됨 확인 필요
      $this->db->limit(1,0);
      $this->db->select('b.dp_id, b.co_name, b.ceo');
      $this->db->join('tbl_members b', 'a.customer_id = b.dp_id', 'left');
      $this->db->_order_by = 'a.list_order asc';
      $first_branch = $this->db->where('a.ct_id', $ct_id)->get('tbl_contract_coops a')->row();
   	  if (!empty($first_branch)) {
        return $first_branch;
      } else {
        return '';
      }
    }

    public function get_branch_info($id)
    {
      $branch = $this->db->where('dp_id', $id)->get('tbl_members')->row();
   	  if (!empty($branch)) {
        return $branch;
      } else {
        return '';
      }
    }


    public function get_drivers($ct_id, $branch_id)
    {
				//
       $this->db->_order_by = 'a.co_name asc';
       $all_partners = $this->db->select('b.*, a.co_name, a.ceo, a.dp_id, a.bs_number, a.co_tel')->where('b.ct_id', $ct_id)->where('b.customer_id', $branch_id)->join('tbl_contract_partners b', 'a.dp_id = b.partner_id', 'left')->get('tbl_members a')->result();

   	   if (!empty($all_partners)) {
          return $all_partners;
       } else {
          return array();
       }
    }

    public function get_checkers($ct_id, $branch_id)
    {
				//
       $this->db->_order_by = 'fullname asc';
       $all_checkers = $this->db->select('*')->where('ct_id', $ct_id)->where('customer_id', $branch_id)->get('tbl_contract_checker')->result();

   	   if (!empty($all_checkers)) {
          return $all_checkers;
       } else {
          return array();
       }
    }

    public function get_work_list($ct_id, $branch_id, $alloc_date)
    {
       $this->db->select('a.*, b.ceo as driver_name, c.fullname as checker_name');
       $this->db->join('tbl_members b', 'a.driver_id = b.dp_id', 'left');
       $this->db->join('tbl_contract_checker c', 'a.checker_id = c.checker_id', 'left');
       $this->db->_order_by = 'a.qno asc';
	   $all_works = $this->db->where('a.ct_id', $ct_id)->where('a.branch_id', $branch_id)->where('a.alloc_date', $alloc_date)->get('tbl_scheduling_works a')->result();

   	   if (!empty($all_works)) {
          return $all_works;
       } else {
          return array();
       }
    }

        public function get_work_status( $alloc_date, $branch_id, $driver_id=NULL)
        {
    		$this->db->limit(1,0);
    		$work_status = $this->db->select('is_register, is_confirm, move_to, subwork_status')->where('br_id', $branch_id)->where('alloc_date', $alloc_date)->get('tbl_allocation_works')->result();

    		if (!empty($work_status)) {
                return $work_status;
            } else {
                return array();
            }
        }

        public function work_list_all($params)
        {
      		if(empty($params['alloc_date']) || empty($params['br_id'])) exit;

      		$qry  = " select * from tbl_allocation_works where 1 ";
      		if(!empty($params['search_by']) && $params['search_by'] == "M" && !empty($params['alloc_month'])) {
      			$qry .=  " and alloc_date like '".$params['alloc_month']."%' ";
      		} else if(!empty($params['alloc_date'])) {      $qry .=  " and alloc_date = '".$params['alloc_date']."' ";    }
      		if(!empty($params['br_id'])) {      $qry .=  " and br_id = '".$params['br_id']."' ";    }
      		if(!empty($params['checker_id'])) {      $qry .=  " and checker_id = '".$params['checker_id']."' ";    }
      		if(!empty($params['driver_id'])) {      $qry .=  " and driver_id = '".$params['driver_id']."' ";    }
      		if(!empty($params['is_confirm'])) {      $qry .=  " and is_confirm = '".$params['is_confirm']."' ";    }
      		if(!empty($params['driver_confirm'])) {      $qry .=  " and driver_confirm = '".$params['driver_confirm']."' ";    }
      		$qry .=  " order by alloc_date asc, work_from asc ";

          $this->db->_order_by = 'alloc_date asc, work_from asc';
          $work_list = $this->db->select('aw.*, dr.Gisa_Name as driver_name, ch.mb_name as checker_name')->where('aw.alloc_date', $params['alloc_date'])->where('aw.br_id', $params['br_id'])->join('tbl_driver dr', 'dr.Gisa_Idxno = aw.driver_id', 'left')->join('tbl_checker ch', 'ch.idx = aw.checker_id', 'left')->get('tbl_allocation_works aw')->result();
          // $work_list = $this->db->query($qry);

    		  if (!empty($work_list)) {
            return $work_list;
          } else {
            return array();
          }
    }


	//계약별 거래항목
    public function get_items($ct_id)
    {
        $this->db->_order_by = 'co_name asc';
        $all_items = $this->db->where('ct_id', $ct_id)->get('tbl_contract_items')->result();

		if (!empty($all_items)) {
            return $all_items;
        } else {
            return array();
        }
    }
}
