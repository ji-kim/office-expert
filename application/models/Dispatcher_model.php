<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * 	@author : deltaworks
 * 	date	: 21 April, 2018
 * 	Dispatcher
 * 	http://deltaworks.co.kr
 *  version: 1.0
 */

class Dispatcher_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

}
