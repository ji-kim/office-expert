<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mongo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('mongo_db');
    }

    // 출근 차량 전체 목록
    public function getworklist() {
        $data = $this->mongo_db->get('df_working_drivers');
        return $data;
    }

    /**
     * 차량 운행 경로 데이터
     *
     * @param $carnum       String          :: 차량번호
     * @param $start_date   Timestamp        :: 시작일
     * @param $end_date     Timestamp        :: 종료일
     * @param $inout        all | In | Out  :: 전체 / 출근 / 퇴근
     */
    public function gpsLog($carnum, $start_date, $end_date, $inout)
    {
        $data = null;
        $field = array(
            'carNum' => 1,
            'driveDate' => 1,
            'checkIn' => 1,
            'checkOut' => 1,
        );

        if($inout === 'In' || $inout === 'Out'){
            $field = array(
                'carNum' => 1,
                'driveDate' => 1,
                'check'.$inout => 1
            );
        }

        $data = $this->mongo_db->aggregate(
            'df_driver_logs',
            [
                [
                    '$project' => [
                        'carNum' => 1,
                        'driveDate' => 1,
                        'checkIn' => [
                            '$filter' => [
                                'input' => '$checkIn',
                                'as' => 'c',
                                'cond' => [
                                    '$and' => [
                                        ['$gte' => ['$$c.timestamp', $start_date]],
                                        ['$lte' => ['$$c.timestamp', $end_date]]
                                    ]

                                ]
                            ]
                        ],
                        'checkOut' => [
                            '$filter' => [
                                'input' => '$checkOut',
                                'as' => 'c',
                                'cond' => [
                                    '$and' => [
                                        ['$gte' => ['$$c.timestamp', $start_date]],
                                        ['$lte' => ['$$c.timestamp', $end_date]]
                                    ]

                                ]
                            ]
                        ],
                    ]
                ],
                [
                    '$match' => [
                        'carNum' => $carnum,
                        '$or' => [
                            ['checkIn.0' => ['$exists' => true]],
                            ['checkOut.0' => ['$exists' => true]],
                        ]
                    ]
                ],
                [
                    '$project' => $field
                ]
            ],
            [
                'allowDiskUse' => true
            ]
        );

        return $data;
    }
}
