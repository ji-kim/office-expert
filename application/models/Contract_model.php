<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contract_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function get_all_contracts($status = NULL) {
        $this->db->select('tbl_contract.*', FALSE);
        $this->db->select('tbl_members.*', FALSE);
        $this->db->from('tbl_contract');
        $this->db->join('tbl_members', 'tbl_contract.s_co_id = tbl_members.userid', 'left');
        $this->db->where('tbl_contract.ct_edate >=', date("Y-m-d"));
        if (!empty($status)) {
            $this->db->where('tbl_contract.ct_status', $status);
            $result = $this->db->get();
            //$result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }

        return $result;
    }

    public function get_all_scontracts($status = NULL) {
        $this->db->select('tbl_contract.*', FALSE);
        $this->db->select('tbl_members.*', FALSE);
        $this->db->from('tbl_contract');
        $this->db->join('tbl_members', 'tbl_contract.s_co_id = tbl_members.userid', 'left');
        $this->db->where('tbl_contract.is_subcontract', 'Y');
        if (!empty($status)) {
            $this->db->where('tbl_contract.sct_status', $status);
            $query_result = $this->db->get();
            $result = $query_result->result();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }

        return $result;
    }

}
