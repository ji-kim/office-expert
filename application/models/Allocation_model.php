<?php

class Allocation_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function get_branchs($branch_id=NULL)
    {
        $this->db->_order_by = 'co_name asc';
		if(!empty($branch_id)) $this->db->where('dp_id', $branch_id);
        $all_branch = $this->db->where('mb_type', 'branch')->get('tbl_members')->result();

		if (!empty($all_branch)) {
            return $all_branch;
        } else {
            return array();
        }
    }

    public function get_first_branch()
    {
		$this->db->limit(1,0);
        $first_branch = $this->db->where('mb_type', 'branch')->get('tbl_members')->row();

		if (!empty($first_branch)) {
            return $first_branch->dp_id;
        } else {
            return '';
        }
    }

    public function get_drivers($branch_id)
    {
        $all_branch = $this->db->where('mb_type', 'partner')->where('br_id', $branch_id)->get('tbl_members')->result();

		if (!empty($all_branch)) {
            return $all_branch;
        } else {
            return array();
        }
    }

    public function get_work_status( $alloc_date, $branch_id, $driver_id=NULL)
    {
		$this->db->limit(1,0);
		$work_status = $this->db->select('is_register, is_confirm, move_to, subwork_status')->where('br_id', $branch_id)->where('alloc_date', $alloc_date)->get('tbl_allocation_works')->result();

		if (!empty($work_status)) {
            return $work_status;
        } else {
            return array();
        }
    }

    public function work_list_all($params)
    {
		if(empty($params['alloc_date']) || empty($params['br_id'])) exit;

		$qry  = " select * from tbl_allocation_works where 1 ";
		if(!empty($params['search_by']) && $params['search_by'] == "M" && !empty($params['alloc_month'])) {
			$qry .=  " and alloc_date like '".$params['alloc_month']."%' ";    
		} else if(!empty($params['alloc_date'])) {      $qry .=  " and alloc_date = '".$params['alloc_date']."' ";    }
		if(!empty($params['br_id'])) {      $qry .=  " and br_id = '".$params['br_id']."' ";    }
		if(!empty($params['checker_id'])) {      $qry .=  " and checker_id = '".$params['checker_id']."' ";    }
		if(!empty($params['driver_id'])) {      $qry .=  " and driver_id = '".$params['driver_id']."' ";    }
		if(!empty($params['is_confirm'])) {      $qry .=  " and is_confirm = '".$params['is_confirm']."' ";    }
		if(!empty($params['driver_confirm'])) {      $qry .=  " and driver_confirm = '".$params['driver_confirm']."' ";    }
		$qry .=  " order by alloc_date asc, work_from asc ";

        $this->db->_order_by = 'alloc_date asc, work_from asc';
        $work_list = $this->db->select('aw.*, dr.Gisa_Name as driver_name, ch.mb_name as checker_name')->where('aw.alloc_date', $params['alloc_date'])->where('aw.br_id', $params['br_id'])->join('tbl_driver dr', 'dr.Gisa_Idxno = aw.driver_id', 'left')->join('tbl_checker ch', 'ch.idx = aw.checker_id', 'left')->get('tbl_allocation_works aw')->result();
       // $work_list = $this->db->query($qry);

		if (!empty($work_list)) {
            return $work_list;
        } else {
            return array();
        }
    }

}