<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Description of Allocation_model
 *
 * @author NaYeM
 */
class Allocation_model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_work_list($alloc_date, $driver_id)
    {
		if(empty($alloc_date) || empty($driver_id)) exit;
        $this->db->_order_by = 'alloc_date asc, work_from asc';
        $work_list = $this->db->get_where('tbl_allocation_works', array('alloc_date' => $alloc_date, 'driver_id' => $driver_id))->result();

		if (!empty($work_list)) {
            return $work_list;
        } else {
            return array();
        }
    }

}
