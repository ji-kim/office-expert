<?php
/**
 * Description of Project_Model
 *
 * @author NaYeM
 */
class Member_model extends MY_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('mongo_db');
        $this->load->database();
    }

    // 회원정보
    public function member($userid)
    {
      //$where = "(REPLACE(userid,'-','') = '" . $userid . "' or REPLACE(reg_number,'-','') = '" . $userid . "')";
      $where = "(REPLACE(userid,'-','') = '" . $userid . "' and mb_type = 'partner')";

        $query = $this->db->get_where('tbl_members', $where);
        //$query = $this->db->from('tbl_members')->where($where);
        return $query->row_array();
    }

    // 회원정보 by dp_id
    public function member_by_id($dp_id)
    {
        $query = $this->db->get_where('tbl_members', array('dp_id' => $dp_id));
        return $query->row_array();
    }

    // 회원정보 by code
    public function member_by_code($code)
    {
          $query = $this->db->get_where('tbl_members', array('code' => $code, 'mb_type' => 'customer'));
          return $query->row_array();
      }

    // 로그인체크
    public function login($userid, $userpwd)
    {
        $query = $this->db->get_where('tbl_members', array('userid' => $userid, 'userpwd' => $userpwd));
        return $query->row_array();
    }

    // 출근 체크
    public function work($userid) {
        $this->mongo_db->where('loginId', $userid);
        $data = $this->mongo_db->get('df_working_drivers');
        return $data;
    }


	// 디바이스 토큰 삽입
    public function setDeviceToken($userid , $device_token){
        $data['device_token'] = '';
        $this->db->where('device_token',$device_token);
        $this->db->update('tbl_members',$data);

        $data['device_token'] = $device_token;
        $this->db->where("REPLACE(userid,'-','')",$userid);
        $this->db->update('tbl_members',$data);

    }
}
