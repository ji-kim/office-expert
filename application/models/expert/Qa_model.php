<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Description of Qa_model
 *
 * @author NaYeM
 */
class Qa_model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function qnaList(){
		$query = $this->db->order_by('created_date', 'DESC')->get_where('tbl_qna', array('userid' => '1'));		
        return $query->result();	
	}

	public function qnaView($index){
		$query = $this->db->order_by('created_date', 'DESC')->get_where('tbl_qna', array('userid' => '1', 'qna_id' => $index));		
        return $query->result();	
	}

	public function qnaWrite(){
		$insert = array(
			'userid' => "1",
            'title' => $this->input->get_post("subject"),
            'question' => $this->input->get_post("question"),
            'created_date' => date("Y-m-d H:i:s")
        );
        return $this->db->insert('tbl_qna', $insert);
	}

}
