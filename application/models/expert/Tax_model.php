<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Description of Tax_model
 *
 * @author NaYeM
 */
class Tax_model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

}
