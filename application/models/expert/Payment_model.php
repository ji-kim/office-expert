<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Description of Payment_model
 *
 * @author NaYeM
 */
class Payment_model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // 내 결제 이력
    public function history()
    {
        $query = $this->db->get_where('tbl_payment_log', array('payment_buyer_id' => $_SESSION['userid']));
        return $query->result();
    }

	 // 내 결제 이력
    public function taxbill()
    {
		$this->db_2 = $this->load->database('tbagent', TRUE);
        $this->db_2->select('*')->from('TB_TAX t');
		$this->db_2->join('TB_TAX_LINE tl','t.NO = tl.NO','left');
		$this->db_2->where('t.DC_RMK2',$_SESSION['userid']);
		$result = $this->db_2->get();
		
        return $result->result();
    }

}
