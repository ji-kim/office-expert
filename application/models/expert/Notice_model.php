<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Description of Notice_model
 *
 * @author NaYeM
 */
class Notice_model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function noticeList(){


        $this->db->select('a.*,ap.read')->from('tbl_announcements_push ap');
        $this->db->join('tbl_announcements a','a.announcements_id = ap.announcements_id','left');
        $this->db->where('ap.to_user_id',$_SESSION['dp_id']);
        $this->db->where('a.title != ', "");

        $result = $this->db->get();

        return $result->result();

    }

    public function noticeView($id){
        $this->db->update('tbl_announcements_push',array("read" => 1),array("announcements_id" => $id , "to_user_id" => $_SESSION['dp_id']));
        $query = $this->db->get_where('tbl_announcements', array('announcements_id' => $id));

        return $query->row();

    }

}
