<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function asset_url(){ return base_url().'assets/expert'; }
function css_url(){ return base_url().'assets/expert/css'; }
function js_url(){ return base_url().'assets/expert/js'; }
function img_url(){ return base_url().'assets/expert/img'; }
function random(){ return "?x=".time(); }
//이전달
function prev_month() {
    $newdate = strtotime ( "-1 month" , strtotime ( date("Y-m-d") ) );
    return date ( "Y-m" , $newdate );
}

//문자열 숫자처리 천단위 , NULL은 0
function char_num($var) {
    return (empty($var))?"0":number_format($var,0);
}

function null_check($array_var) {
    $return_data = array();
    foreach ($array_var as $key => $item) {
        $return_data[$key] = (empty($item))?"":$item;
    }

    return $return_data;
}

function work_check($userid) {
    $CI = &get_instance();
    $CI->load->model('expert/Member_model');
    $result = $CI->Member_model->work($userid);

    if(count($result) > 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}