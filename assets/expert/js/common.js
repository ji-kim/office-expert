$(function(){
    // 메뉴온
    $("header #menuOpen").on("click", function(){
        $("aside, #maskLayer").show();
        $("body").css("overflow", "hidden");
    });

    // 메뉴오프
    $("#maskLayer, #menuClose").on("click", function(){
        $("aside, #maskLayer").hide();
        $("body").css("overflow", "auto");
    });

});

function login() {
    var loginform = $(".login form");
    var userid = $(".userid");
    var userpwd = $(".userpwd");

    if(userid.val() == "") {
        alert('아이디를 입력하세요.');
        userid.focus();
        return;
    }
    if(userpwd.val() == "") {
        alert('비밀번호를 입력하세요.');
        userpwd.focus();
        return;
    }
    loginform.submit();
}


function onWork(userid, carnum) {
    if (window.Android) {
        window.Android.onWork(userid, carnum); // 로그인시 안드로이드 앱으로 아이디, 차량번호 전달
        $(".onWork").hide();
        $(".offWork").show();
        $.get("/expert/Login/isconnected/1");
        alert("출근처리 되었습니다.");


    } else {
        alert("출근은 앱에서만 가능합니다.");

    }
}

function offWork(userid, carnum){
    if (window.Android) {
        window.Android.offWork(userid, carnum); // 로그인시 안드로이드 앱으로 아이디, 차량번호 전달
        $(".offWork").hide();
        $(".onWork").show();
        $.get("/expert/Login/isconnected/0");
        alert("퇴근처리 되었습니다.");
    } else {
        alert("퇴근은 앱에서만 가능합니다.");

    }
}