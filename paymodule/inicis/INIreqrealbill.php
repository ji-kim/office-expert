<?php

/* INIreqrealbill.php
 *
 * 실시간 빌링 결제처리한다.
 * 이 페이지는 자체 보안성이 없으므로, 반드시 Secure Web Server에서 사용하십시오.
 * 코드에 대한 자세한 설명은 매뉴얼을 참조하십시오.
 * <주의> 구매자의 세션을 반드시 체크하도록하여 부정거래를 방지하여 주십시요.
 *  
 * http://www.inicis.com
 * Copyright (C) 2004 Inicis Co., Ltd. All rights reserved.
 */

/* * ************************
 * 1. 라이브러리 인클루드 *
 * ************************ */
require("INIpay41Lib.php");
// error_reporting = E_ALL;
/* * *************************************
 * 2. INIpay41 클래스의 인스턴스 생성 *
 * ************************************* */
$inipay = new INIpay41;



/* * *********************
 * 2. 정보 설정 *
 * ********************* */
$inipay->m_inipayHome = "/home/www/INIbill41";      // INIpay Home (절대경로로 적절히 수정)
$inipay->m_keyPw = "1111";        // 키패스워드(상점아이디에 따라 변경)
$inipay->m_type = "reqrealbill";       // 고정 (절대 수정금지)
$inipay->m_pgId = "INIpayBill";       // 고정 (절대 수정금지)
$inipay->m_payMethod = "Card";           // 고정 (절대 수정금지)
$inipay->m_billtype = "Card";              // 고정 (절대 수정금지)
$inipay->m_subPgIp = "203.238.3.10";       // 고정 (절대 수정금지)
$inipay->m_debug = "true";        // 로그모드("true"로 설정하면 상세한 로그가 생성됨)
$inipay->m_mid = $mid;         // 상점아이디
$inipay->m_billKey = $billkey;        // billkey 입력
$inipay->m_goodName = $goodname;       // 상품명 (최대 40자)
$inipay->m_currency = $currency;       // 화폐단위 
$inipay->m_price = $price;        // 가격 
$inipay->m_buyerName = $buyername;       // 구매자 (최대 15자) 
$inipay->m_buyerTel = $buyertel;       // 구매자이동전화 
$inipay->m_buyerEmail = $buyeremail;       // 구매자이메일
$inipay->m_cardQuota = $cardquota;       // 할부기간
$inipay->m_quotaInterest = $quotainterest;      // 무이자 할부 여부 (1:YES, 0:NO)
$inipay->m_url = "http://".$_SERVER['HTTP_HOST'];    // 상점 인터넷 주소
$inipay->m_cardPass = $cardpass;       // 키드 비번(앞 2자리)
$inipay->m_regNumber = $regnumber;       // 주민 번호 및 사업자 번호 입력
$inipay->m_authentification = $authentification; //( 신용카드 빌링 관련 공인 인증서로 인증을 받은 경우 고정값 "01"로 세팅)  
$inipay->m_oid = $oid;        //주문번호
$inipay->m_merchantreserved1 = $MerchantReserved1;  // Tax : 부가세 , TaxFree : 면세 (예 : Tax=10&TaxFree=10) 
$inipay->m_merchantreserved2 = $MerchantReserved2;  // 예비2
$inipay->m_merchantreserved3 = $MerchantReserved3;  // 예비3

$billkey = $inipay->m_billKey;
/* * ******************************
 * 3. 실시간 신용카드 빌링 요청 *
 * ****************************** */
$inipay->startAction();


/* * **********************************************************
 * 4. 실시간 신용카드 빌링 결과                             *
 * ***********************************************************
 *                                                          *
 * $inipay->m_tid 	  // 거래번호                       *
 * $inipay->m_resultCode  // "00"이면 성공                  *
 * $inipay->m_resultMsg   // 결과에 대한 설명               *
 * $inipay->m_authCode    // 승인번호                       *
 * $inipay->m_pgAuthDate  // 이니시스 승인날짜 (YYYYMMDD)   *
 * $inipay->m_pgAuthTime  // 이니시스 승인시간 (HHMMSS)     *
 * $inipay->m_prtcCode		// 부분취소가능여부 (1:가능 , 0:불가능)	*
 *                                                          *
 * ********************************************************** */
?>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<?
if($inipay->m_resultCode == "00") {
    //DB처리

    //로지넥스화 시킬 때 반드시 변경시켜야함
    $mysql_hostname = '15.164.150.169:4417';
    $mysql_username = 'user_delex';
    $mysql_password = 'votm@delex19';
    $mysql_database = 'delex_deltaon_com';


    $connect = mysqli_connect($mysql_hostname, $mysql_username, $mysql_password, $mysql_database);

    mysqli_select_db($connect, $mysql_database) or die("Connect Failed");

    $sql = "INSERT INTO tbl_payment_log";
    $sql .= " (payment_log_amount , payment_log_oid , payment_log_tid , payment_buyer_id , payment_buyer_carnum , payment_log_date)";
    $sql .= " values ('" . $inipay->m_price . "','" . $inipay->m_oid . "','" . $_POST['billkey'] . "','" . $_REQUEST['userid'] . "','" . $_REQUEST['carnum'] . "',now())";

    mysqli_query($connect, $sql);

    $sql = "SELECT * FROM tbl_delivery_fee_levy WHERE idx = " . $_POST['levy_idx'];
    $result = mysqli_query($connect, $sql);
    $row = $result->fetch_array(MYSQLI_ASSOC);
    $paid_amount = $row['paid_amount'] + $_POST['real_price'];
    $balance_amount = $row['balance_amount'] - $_POST['real_price'];
    $df_id = $row['df_id'];
    $dp_id = $row['dp_id'];
    $pay_status = 'N';
    if ($paid_amount > 0 && $balance_amount > 0) {
        $pay_status = 'P';
    } else if ($paid_amount > 0 && $balance_amount == 0) {
        $pay_status = 'F';
    } else if ($paid_amount > 0 && $balance_amount < 0) {
        $pay_status = 'O';
    }

    $sql = "UPDATE tbl_delivery_fee_levy";
    $sql .= " SET ";
    $sql .= " paid_amount = " . $paid_amount;
    $sql .= " ,balance_amount = " . $balance_amount;
    $sql .= " ,pay_status = '" . $pay_status . "'";
    $sql .= " WHERE idx = " . $row['idx'];

    mysqli_query($connect, $sql);

    $sql = "INSERT INTO tbl_delivery_fee_levy_log";
    $sql .= " (pid , df_id , dp_id , pay_amount , pay_date , pay_datetime , payment_methods_id , balance_amount , remark)";
    $sql .= " values ('" . $row['idx'] . "','".$df_id."','".$dp_id."','" . $_POST['real_price'] . "','" . date("Y-m-d") . "','" . date("Y-m-d H:i:s") . "','" . "1" . "'," . $balance_amount . ",'" . $_POST['oid'] . "')";

    mysqli_query($connect, $sql);

    if ($_POST['autopay'] == '0') {
        $sql = "UPDATE tbl_members";
        $sql .= " SET ";
        $sql .= " autopay = 'Y'";
        $sql .= " ,billkey = '" . $billkey . "'";
        $sql .= " WHERE  replace(userid,\"-\",\"\") = '" . $_POST['userid'] . "'";


        mysqli_query($connect, $sql);
    }

    mysqli_close($connect);
}else{
    echo $inipay->m_resultMsg;
    exit;
}

//test


?>


<script>

location.href="/expert/payment/history";
</script>
</html>

<!--html>
    <head>

        <title>INIpay 실시간 신용카드 빌링 데모</title>
        <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">

        <script>
            var openwin = window.open("childwin.html", "childwin", "width=299,height=149");
            openwin.close();
        </script>

        <style type="text/css">
            BODY{font-size:9pt; line-height:160%}
            TD{font-size:9pt; line-height:160%}
            INPUT{font-size:9pt;}
            .emp{background-color:#E0EFFE;}
        </style>

    </head>

    <body>
        <table border=0 width=500>
            <tr>
                <td>
                    <hr noshade size=1>
                    <b>실시간 빌링 요청 결과</b>
                    <hr noshade size=1>
                </td>
            </tr>
        </table>
        <br>

        <table border=0 width=500>
            <tr>
                <td align=right nowrap>결과코드 : </td>
                <td><?php echo($inipay->m_resultCode); ?></td>
            </tr>
            <tr>
                <td align=right nowrap>결과내용 : </td>
                <td><font class=emp><?php echo($inipay->m_resultMsg); ?></font></td>
            </tr>
            <tr>
                <td align=right nowrap>거래번호 : </td>
                <td><?php echo($inipay->m_tid); ?></td>
            </tr>
            <tr>
                <td align=right nowrap>승인번호 : </td>
                <td><?php echo($inipay->m_authCode); ?></td>
            </tr>
            <tr>
                <td align=right nowrap>승인날짜 : </td>
                <td><?php echo($inipay->m_pgAuthDate); ?></td>
            </tr>
            <tr>
                <td align=right nowrap>승인시각 : </td>
                <td><?php echo($inipay->m_pgAuthTime); ?></td>
            </tr>
            <tr>
                <td align=right nowrap>부분취소가능여부 : </td>
                <td><?php echo($inipay->m_prtcCode); ?></td>
            </tr>
            <tr>
            <tr>
                <td colspan=2><hr noshade size=1></td>
            </tr>
            <tr>
                <td align=right colspan=2>Copyright Inicis, Co.<br>www.inicis.com</td>
            </tr>
        </table>
    </body>
</html-->
