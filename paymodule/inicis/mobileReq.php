<?php

    $P_STATUS   = $_POST['P_STATUS'];
    $P_REQ_URL  = $_POST['P_REQ_URL'];
    $P_TID      = $_POST['P_TID'];
    $P_MID      = "deltapay01";


    function makeParam($P_TID , $P_MID){
        return "P_TID=".$P_TID."&P_MID=".$P_MID;
    }

    function parseData($receiveMsg){
        $returnArr = explode("&",$receiveMsg);
        $return = array();
        foreach($returnArr as $value){
            $tmpArr = explode("=",$value);
            $return[$tmpArr[0]] = $tmpArr[1];
        }
        return $return;
    }

    function chkTid($P_TID){
        return !empty($P_TID);
    }
    function connectSocket($host,$port){
        $sock = socket_create(AF_INET , SOCK_STREAM , SOL_TCP);
        socket_connect($sock,$host,$port);
        return $sock;
    }
    function requestSocket($sock,$param){
        socket_write($sock,$param,strlen($param));
        return socket_read($sock,1024);
    }


    function request_curl($url, $is_post=0, $data=array(), $custom_header=null) {
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL,$url);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt ($ch, CURLOPT_SSLVERSION,1);
        curl_setopt ($ch, CURLOPT_POST, $is_post);
        if($is_post) {
            curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt ($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt ($ch, CURLOPT_HEADER, true);

        if($custom_header) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $custom_header);
        }
        $result[0] = curl_exec ($ch);
        $result[1] = curl_errno($ch);
        $result[2] = curl_error($ch);
        $result[3] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        return $result;
    }




if($P_STATUS == "00" && chkTid($P_TID)) {
    $params = array("P_TID" => $P_TID , "P_MID" => $P_MID);
    print_r($params);
    $result = request_curl($P_REQ_URL , 1 , $params);
    $parseData = parseData($result[0]);

    if($parseData['P_STATUS'] == "00") {
        //로지넥스화 시킬 때 반드시 변경시켜야함
        $mysql_hostname = '15.164.150.169:4417';
        $mysql_username = 'user_delex';
        $mysql_password = 'votm@delex19';
        $mysql_database = 'delex_deltaon_com';


        $connect = mysqli_connect($mysql_hostname, $mysql_username, $mysql_password, $mysql_database);

        mysqli_select_db($connect, $mysql_database) or die("Connect Failed");

        $noti = explode("|", $parseData['P_NOTI']);
        $levy_idx = $noti[0];
        $userid = $noti[1];
        //$carnum = iconv("euc-kr", "utf-8", $noti[2]);
        $carnum = '광주89아2033';
        $price = $noti[3];

        $sql = "INSERT INTO tbl_payment_log";
        $sql .= " (payment_log_amount , payment_log_oid , payment_log_tid , payment_buyer_id , payment_buyer_carnum , payment_log_date)";
        $sql .= " values ('" . $parseData['P_AMT'] . "','" . $parseData['P_OID'] . "','" . $parseData['P_TID'] . "','" . $userid . "','" . $carnum . "',now())";


        mysqli_query($connect, $sql);

        $sql = "SELECT * FROM tbl_delivery_fee_levy WHERE idx = " . $levy_idx;
        $result = mysqli_query($connect, $sql);
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $paid_amount = $row['paid_amount'] + $price;
        $balance_amount = $row['balance_amount'] - $price;
        $df_id = $row['df_id'];
        $dp_id = $row['dp_id'];
        $pay_status = 'N';
        if ($paid_amount > 0 && $balance_amount > 0) {
            $pay_status = 'P';
        } else if ($paid_amount > 0 && $balance_amount == 0) {
            $pay_status = 'F';
        } else if ($paid_amount > 0 && $balance_amount < 0) {
            $pay_status = 'O';
        }


        $sql = "UPDATE tbl_delivery_fee_levy";
        $sql .= " SET ";
        $sql .= " paid_amount = " . $paid_amount;
        $sql .= " ,balance_amount = " . $balance_amount;
        $sql .= " ,pay_status = '" . $pay_status . "'";
        $sql .= " WHERE idx = " . $row['idx'];

        mysqli_query($connect, $sql);

        $sql = "INSERT INTO tbl_delivery_fee_levy_log";
        $sql .= " (pid , df_id , dp_id , pay_amount , pay_date , pay_datetime , payment_methods_id , balance_amount , remark)";
        $sql .= " values ('" . $row['idx'] . "','" . $df_id . "','" . $dp_id . "','" . $price . "','" . date("Y-m-d") . "','" . date("Y-m-d H:i:s") . "','" . "1" . "'," . $balance_amount . ",'" . $parseData['P_OID'] . "')";

        mysqli_query($connect, $sql);
        mysqli_close($connect);

        ?>
        <script>
            opener.location.href = "/expert/payment/history";
            self.close();

        </script>

        <?
    }else{
        ?>
        <script>


        opener.alert("결제에 실패하였습니다.");
        self.close();
        </script>
        <?
    }

}
?>
</html>